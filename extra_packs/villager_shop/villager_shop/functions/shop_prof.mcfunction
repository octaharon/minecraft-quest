# ---------------------------------------------------------
# This Datapack was made by CommandGeek
# Please do not copy or credit this as your own work!
# Youtube: www.youtube.com/CommandGeek
#---------------------------------------------------------

playsound minecraft:block.note_block.pling master @a ~ ~ ~ 1 2

execute as @e[type=villager,tag=selected_shop] unless data entity @s Offers.Recipes run tag @s add no_trade

scoreboard players add @e[type=villager,tag=selected_shop] VillShopProf 1


scoreboard players set @e[type=villager,tag=shop,scores={VillShopProf=16..}] VillShopProf 1

execute as @e[type=villager,tag=shop,nbt=!{VillagerData:{profession:"minecraft:armorer"}},scores={VillShopProf=1}] run data modify entity @s VillagerData.profession set value "minecraft:armorer"
execute as @e[type=villager,tag=shop,nbt=!{VillagerData:{profession:"minecraft:butcher"}},scores={VillShopProf=2}] run data modify entity @s VillagerData.profession set value "minecraft:butcher"
execute as @e[type=villager,tag=shop,nbt=!{VillagerData:{profession:"minecraft:cartographer"}},scores={VillShopProf=3}] run data modify entity @s VillagerData.profession set value "minecraft:cartographer"
execute as @e[type=villager,tag=shop,nbt=!{VillagerData:{profession:"minecraft:cleric"}},scores={VillShopProf=4}] run data modify entity @s VillagerData.profession set value "minecraft:cleric"
execute as @e[type=villager,tag=shop,nbt=!{VillagerData:{profession:"minecraft:farmer"}},scores={VillShopProf=5}] run data modify entity @s VillagerData.profession set value "minecraft:farmer"
execute as @e[type=villager,tag=shop,nbt=!{VillagerData:{profession:"minecraft:fisherman"}},scores={VillShopProf=6}] run data modify entity @s VillagerData.profession set value "minecraft:fisherman"
execute as @e[type=villager,tag=shop,nbt=!{VillagerData:{profession:"minecraft:fletcher"}},scores={VillShopProf=7}] run data modify entity @s VillagerData.profession set value "minecraft:fletcher"
execute as @e[type=villager,tag=shop,nbt=!{VillagerData:{profession:"minecraft:leatherworker"}},scores={VillShopProf=8}] run data modify entity @s VillagerData.profession set value "minecraft:leatherworker"
execute as @e[type=villager,tag=shop,nbt=!{VillagerData:{profession:"minecraft:librarian"}},scores={VillShopProf=9}] run data modify entity @s VillagerData.profession set value "minecraft:librarian"
execute as @e[type=villager,tag=shop,nbt=!{VillagerData:{profession:"minecraft:nitwit"}},scores={VillShopProf=10}] run data modify entity @s VillagerData.profession set value "minecraft:nitwit"
execute as @e[type=villager,tag=shop,nbt=!{VillagerData:{profession:"minecraft:none"}},scores={VillShopProf=11}] run data modify entity @s VillagerData.profession set value "minecraft:none"
execute as @e[type=villager,tag=shop,nbt=!{VillagerData:{profession:"minecraft:mason"}},scores={VillShopProf=12}] run data modify entity @s VillagerData.profession set value "minecraft:mason"
execute as @e[type=villager,tag=shop,nbt=!{VillagerData:{profession:"minecraft:shepherd"}},scores={VillShopProf=13}] run data modify entity @s VillagerData.profession set value "minecraft:shepherd"
execute as @e[type=villager,tag=shop,nbt=!{VillagerData:{profession:"minecraft:toolsmith"}},scores={VillShopProf=14}] run data modify entity @s VillagerData.profession set value "minecraft:toolsmith"
execute as @e[type=villager,tag=shop,nbt=!{VillagerData:{profession:"minecraft:weaponsmith"}},scores={VillShopProf=15}] run data modify entity @s VillagerData.profession set value "minecraft:weaponsmith"


execute as @e[type=villager,tag=selected_shop,tag=no_trade] run data remove entity @s Offers.Recipes[]
tag @e[tag=no_trade] remove no_trade