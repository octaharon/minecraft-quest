# ---------------------------------------------------------
# This Datapack was made by CommandGeek
# Please do not copy or credit this as your own work!
# Youtube: www.youtube.com/CommandGeek
#---------------------------------------------------------

function villager_shop:tellraw

execute at @s run playsound minecraft:block.note_block.bit master @p ~ ~ ~ 1 0
# stopsound @p neutral

tag @e[tag=selected_shop] remove selected_shop
tag @s add selected_shop
data modify entity @s Health set value 1024.0f
