# ---------------------------------------------------------
# This Datapack was made by CommandGeek
# Please do not copy or credit this as your own work!
# Youtube: www.youtube.com/CommandGeek
#---------------------------------------------------------

playsound minecraft:block.note_block.pling master @a ~ ~ ~ 1 2

data modify entity @e[type=villager,tag=selected_shop,limit=1] Offers.Recipes append value {buy:{id:"air",Count:1b},buyB:{id:"air",Count:1b},sell:{id:"air",Count:1b},xp:0,rewardExp:0b,maxUses:9999999}

execute as @e[type=villager,tag=selected_shop] at @s run data modify entity @s Offers.Recipes[-1].buy set from block ~ ~-1 ~ Items[{Slot:0b}]
execute as @e[type=villager,tag=selected_shop] at @s run data modify entity @s Offers.Recipes[-1].buyB set from block ~ ~-1 ~ Items[{Slot:2b}]
execute as @e[type=villager,tag=selected_shop] at @s run data modify entity @s Offers.Recipes[-1].sell set from block ~ ~-1 ~ Items[{Slot:4b}]