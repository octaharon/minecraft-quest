# ---------------------------------------------------------
# This Datapack was made by CommandGeek
# Please do not copy or credit this as your own work!
# Youtube: www.youtube.com/CommandGeek
#---------------------------------------------------------

execute at @e[type=villager,tag=selected_shop] run fill ~ ~ ~ ~ ~-1 ~ air
execute at @e[type=villager,tag=selected_shop] run kill @e[tag=shop_maker,limit=1,sort=nearest,distance=..3]
kill @e[type=villager,tag=selected_shop]
