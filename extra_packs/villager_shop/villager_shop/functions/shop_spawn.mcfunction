# ---------------------------------------------------------
# This Datapack was made by CommandGeek
# Please do not copy or credit this as your own work!
# Youtube: www.youtube.com/CommandGeek
#---------------------------------------------------------

data merge entity @s {Profession:0,CustomName:'{"text":"Shop Keeper","color":"green","bold":"true"}',CustomNameVisible:1,NoAI:1b,PersistenceRequired:1b,Health:10000,Attributes:[{Name:"generic.maxHealth",Base:10000}],Invulnerable:1,Tags:["shop"]}

tp @s ~ ~.3 ~
setblock ~ ~ ~ air

summon armor_stand ~ ~3 ~ {Invisible:1b,Marker:1b,NoGravity:1b,CustomName:'{"text":"Shop Maker","color":"light_purple"}',CustomNameVisible:1b,Tags:["shop_maker"]}

scoreboard players set @a[scores={VillShopSpawn=1..}] VillShopSpawn 0
