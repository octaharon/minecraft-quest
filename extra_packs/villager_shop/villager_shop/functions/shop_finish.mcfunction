# ---------------------------------------------------------
# This Datapack was made by CommandGeek
# Please do not copy or credit this as your own work!
# Youtube: www.youtube.com/CommandGeek
#---------------------------------------------------------

summon item ~ ~ ~ {Item:{id:"villager_spawn_egg",Count:1b,tag:{Enchantments:[{lvl:1s,id:"mending"}],HideFlags:39,display:{Name:'{"text":"Shop"}'},EntityTag:{id:"minecraft:villager"}}},Tags:["shop_egg"]}

playsound minecraft:entity.experience_orb.pickup master @a

data modify entity @e[type=item,tag=shop_egg,limit=1] Item.tag.EntityTag merge from entity @e[tag=selected_shop,limit=1]

data remove entity @e[type=item,tag=shop_egg,limit=1] Item.tag.EntityTag.Pos
data remove entity @e[type=item,tag=shop_egg,limit=1] Item.tag.EntityTag.Rotation
data remove entity @e[type=item,tag=shop_egg,limit=1] Item.tag.EntityTag.Motion
data remove entity @e[type=item,tag=shop_egg,limit=1] Item.tag.EntityTag.UUIDMost
data remove entity @e[type=item,tag=shop_egg,limit=1] Item.tag.EntityTag.UUIDLeast
data modify entity @e[type=item,tag=shop_egg,limit=1] Item.tag.EntityTag.Attributes[0].Base set value 10d
data modify entity @e[type=item,tag=shop_egg,limit=1] Item.tag.EntityTag.Tags set value ["public_shop"]
execute as @e[type=item,tag=shop_egg] run data modify entity @s Item.tag.display.Name set from entity @s Item.tag.EntityTag.CustomName
