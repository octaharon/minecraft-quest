# ---------------------------------------------------------
# This Datapack was made by CommandGeek
# Please do not copy or credit this as your own work!
# Youtube: www.youtube.com/CommandGeek
#---------------------------------------------------------


# Welcome Message

tellraw @a ["",{"text":"Villager Shop Maker ","color":"gold"},{"text":">>","color":"white"},{"text":" by ","color":"gold"},{"text":"CommandGeek","underlined":true,"color":"green","clickEvent":{"action":"open_url","value":"https://www.youtube.com/commandgeek"}}]

# Scoreboard Objectives

scoreboard objectives add VillShopType dummy
scoreboard objectives add VillShopSpawn minecraft.used:minecraft.villager_spawn_egg
scoreboard objectives add VillShopSec dummy
scoreboard objectives add VillShopProf dummy
scoreboard objectives add VillShopNameV dummy
scoreboard objectives add VillShopName dummy
scoreboard objectives add VillShopLevel dummy

team add selected_shop
team modify selected_shop color green