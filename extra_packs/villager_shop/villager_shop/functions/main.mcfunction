#---------------------------------------------------------
# This Datapack was made by CommandGeek
# Please do not copy or credit this as your own work!
# Youtube: www.youtube.com/CommandGeek
#---------------------------------------------------------

# MISC #1

execute as @e[type=villager,tag=shop] at @s run tp @s ~ ~ ~ facing entity @p[distance=..8,gamemode=!spectator]
execute as @e[type=villager,tag=public_shop] at @s run tp @s ~ ~ ~ facing entity @p[distance=..8,gamemode=!spectator]


execute at @e[type=villager,tag=shop] run fill ~ ~-1 ~ ~ ~-1 ~ hopper{CustomName:'{"text":"Add Trade","color":"dark_red"}'} replace air



execute as @e[tag=shop_maker,type=armor_stand] at @s run tp @s ~ ~ ~ ~3 ~
execute at @e[tag=shop_maker,type=armor_stand] run particle minecraft:end_rod ^ ^-3 ^.9 0 0 0 0 1
execute at @e[tag=shop_maker,type=armor_stand] run particle minecraft:end_rod ^ ^-3 ^-.9 0 0 0 0 1

# Hopper Below

execute at @e[type=villager,tag=shop] run replaceitem block ~ ~-1 ~ container.3 player_head{CustomModelData:1,display:{Name:'{"text":"="}'},SkullOwner:{Id:"763c17e1-9f0f-438b-bfe6-ce26f85b5793",Properties:{textures:[{Value:"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNDlmMTgxYjQxM2RhYTI2NDI0ZmRlMDYzZmYzMmNkOTcxNWM5NTI1NmIwZjI1MDM3YzJmMGVkZTFkNWU1M2I1In19fQ=="}]}}}

execute at @e[type=villager,tag=shop] run replaceitem block ~ ~-1 ~ container.1 player_head{CustomModelData:1,display:{Name:'{"text":"+"}'},SkullOwner:{Id:"4721d0df-0210-403f-a281-e0467b104fb6",Properties:{textures:[{Value:"eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOWEyZDg5MWM2YWU5ZjZiYWEwNDBkNzM2YWI4NGQ0ODM0NGJiNmI3MGQ3ZjFhMjgwZGQxMmNiYWM0ZDc3NyJ9fX0="}]}}}

execute as @a if data entity @s Inventory[{id:"minecraft:player_head",tag:{CustomModelData:1}}] run clear @s player_head{CustomModelData:1}

# Select Villager

execute as @e[type=villager,tag=shop,nbt=!{Health:1024.0f}] run function villager_shop:select_villager

effect give @e[type=villager,tag=selected_shop] glowing 1 1 true
execute as @e[type=villager,tag=selected_shop] at @s unless entity @a[distance=..5] run tag @s remove selected_shop
effect clear @e[type=villager,tag=shop,tag=!selected_shop] glowing

team join selected_shop @e[type=villager,tag=selected_shop]
team leave @e[type=villager,tag=shop,tag=!selected_shop]
team leave @e[type=villager,tag=shop,tag=!selected_shop]

# execute at @e[type=villager,tag=selected_shop] run particle minecraft:happy_villager ~ ~1 ~ .5 1 .5 0 1


# Villager Shop Type Biome

scoreboard players set @e[type=villager,tag=shop,scores={VillShopType=8..}] VillShopType 1

execute as @e[type=villager,tag=shop,nbt=!{VillagerData:{type:"minecraft:desert"}},scores={VillShopType=1}] run data modify entity @s VillagerData.type set value "minecraft:desert"
execute as @e[type=villager,tag=shop,nbt=!{VillagerData:{type:"minecraft:jungle"}},scores={VillShopType=2}] run data modify entity @s VillagerData.type set value "minecraft:jungle"
execute as @e[type=villager,tag=shop,nbt=!{VillagerData:{type:"minecraft:plains"}},scores={VillShopType=3}] run data modify entity @s VillagerData.type set value "minecraft:plains"
execute as @e[type=villager,tag=shop,nbt=!{VillagerData:{type:"minecraft:savanna"}},scores={VillShopType=4}] run data modify entity @s VillagerData.type set value "minecraft:savanna"
execute as @e[type=villager,tag=shop,nbt=!{VillagerData:{type:"minecraft:snow"}},scores={VillShopType=5}] run data modify entity @s VillagerData.type set value "minecraft:snow"
execute as @e[type=villager,tag=shop,nbt=!{VillagerData:{type:"minecraft:swamp"}},scores={VillShopType=6}] run data modify entity @s VillagerData.type set value "minecraft:swamp"
execute as @e[type=villager,tag=shop,nbt=!{VillagerData:{type:"minecraft:taiga"}},scores={VillShopType=7}] run data modify entity @s VillagerData.type set value "minecraft:taiga"


# Villager Shop Profession



# Shop Level

scoreboard players set @e[type=villager,tag=shop,scores={VillShopLevel=5..}] VillShopLevel 0

execute as @e[type=villager,tag=shop,nbt=!{VillagerData:{level:1}},scores={VillShopLevel=0}] run data modify entity @s VillagerData.level set value 1
execute as @e[type=villager,tag=shop,nbt=!{VillagerData:{level:2}},scores={VillShopLevel=1}] run data modify entity @s VillagerData.level set value 2
execute as @e[type=villager,tag=shop,nbt=!{VillagerData:{level:3}},scores={VillShopLevel=2}] run data modify entity @s VillagerData.level set value 3
execute as @e[type=villager,tag=shop,nbt=!{VillagerData:{level:4}},scores={VillShopLevel=3}] run data modify entity @s VillagerData.level set value 4
execute as @e[type=villager,tag=shop,nbt=!{VillagerData:{level:5}},scores={VillShopLevel=4}] run data modify entity @s VillagerData.level set value 5




# Shop Name

execute at @e[tag=shop,tag=get_name] run setblock ~ ~ ~ chain_command_block[facing=up,conditional=true]
# execute store result score @s VillShopName run data get block 152 4 -152 Command
execute as @e[tag=shop,tag=get_name] at @s store result score @s VillShopName run data get block ~ ~ ~ Command


execute as @e[tag=shop,tag=get_name] at @s if score @s VillShopName matches 1.. run function villager_shop:get_name

# Shop Name Hide

scoreboard players set @e[type=villager,tag=shop,scores={VillShopNameV=3..}] VillShopNameV 1

execute as @e[type=villager,tag=shop,nbt=!{CustomNameVisible:1b},scores={VillShopNameV=2}] run data modify entity @s CustomNameVisible set value 1b
execute as @e[type=villager,tag=shop,scores={VillShopNameV=1}] if data entity @s CustomNameVisible run data modify entity @s CustomNameVisible set value 0b

    
# Spawning


execute as @e[type=villager] unless score @s VillShopSec matches 2.. run scoreboard players add @s VillShopSec 1
execute as @e[type=villager,scores={VillShopSec=1}] at @s if block ~ ~ ~ chest if entity @a[distance=..8,gamemode=creative,scores={VillShopSpawn=1..}] run function villager_shop:shop_spawn 
scoreboard players set @a[scores={VillShopSpawn=1..}] VillShopSpawn 0




