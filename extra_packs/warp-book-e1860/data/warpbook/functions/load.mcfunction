scoreboard objectives add dummy dummy
execute unless score #warpbook:init dummy matches 1.. run function warpbook:init
function warpbook:version/check
execute unless score #version dummy = #warpbook:init dummy run function warpbook:init
execute if score #version dummy matches 113 run function warpbook:1.13/load
execute if score #version dummy matches 114 run function warpbook:1.14/load
execute if score #version dummy matches 115 run function warpbook:1.15/load
execute if score #version dummy matches 116 run function warpbook:1.16/load

scoreboard objectives add warp.sis dummy
scoreboard objectives add warp.sis.o dummy