#> Add Tool Blocks
    setblock 20202020 0 20202020 minecraft:yellow_shulker_box[facing=down]
    setblock 20202021 0 20202020 minecraft:birch_wall_sign[facing=east]

#> Add protective Bedrock
# Y = 0
#   Z = ..19
        setblock 20202020 0 20202019 minecraft:bedrock
        setblock 20202021 0 20202019 minecraft:bedrock
#   Z = ..20
        setblock 20202019 0 20202020 minecraft:bedrock
        setblock 20202022 0 20202020 minecraft:bedrock
#   Z = ..21
        setblock 20202020 0 20202021 minecraft:bedrock
        setblock 20202021 0 20202021 minecraft:bedrock
# Y = 1
#   Z = ..20
        setblock 20202020 1 20202020 minecraft:bedrock
        setblock 20202021 1 20202020 minecraft:bedrock