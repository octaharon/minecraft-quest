#forceload remove
scoreboard objectives remove dummy
#execute if score #version dummy matches 113 run function warpbook:1.13/uninstall
execute if score #version dummy matches 114 run function warpbook:1.14/uninstall
execute if score #version dummy matches 115 run function warpbook:1.15/uninstall
execute if score #version dummy matches 116 run function warpbook:1.16/uninstall

scoreboard objectives remove warp
scoreboard objectives remove warp.drop
scoreboard objectives remove warp.isHoldingBK
scoreboard objectives remove warp.isSneaking
scoreboard objectives remove warp.Jumped
scoreboard objectives remove warp.lectern