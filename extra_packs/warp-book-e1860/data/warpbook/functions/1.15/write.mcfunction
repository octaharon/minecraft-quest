data merge entity @s {Motion:[0d,0d,0d],PickupDelay:0s,Item:{tag:{display:{Name:'{"text":"Warp SunFlower","italic":false}'}, Enchantments:[{lvl:0s,id:""}],Written:1,Pos:[0d,0d,0d],Dimension:0,Rotation:[0d]}}}

execute store result entity @s Item.tag.Pos[0] double 1 run data get entity @p[scores={warp.isHoldingBK=1,warp.isSneaking=0..}] Pos[0]
execute store result entity @s Item.tag.Pos[1] double 1 run data get entity @p[scores={warp.isHoldingBK=1,warp.isSneaking=0..}] Pos[1]
execute store result entity @s Item.tag.Pos[2] double 1 run data get entity @p[scores={warp.isHoldingBK=1,warp.isSneaking=0..}] Pos[2]
execute store result entity @s Item.tag.Rotation[0] float 1 run data get entity @p[scores={warp.isHoldingBK=1,warp.isSneaking=0..}] Rotation[0]
execute store result entity @s Item.tag.Dimension int 1 run data get entity @p[scores={warp.isHoldingBK=1,warp.isSneaking=0..}] Dimension