execute store result entity @s Pos[0] double 1 run data get entity @s Item.tag.Pos[0]
execute store result entity @s Pos[1] double 1 run data get entity @s Item.tag.Pos[1]
execute store result entity @s Pos[2] double 1 run data get entity @s Item.tag.Pos[2]
execute store result entity @s Rotation[0] float 1 run data get entity @s Item.tag.Rotation[0]
data modify entity @s[tag=!noPickup] PickupDelay set value 0s

execute as @p[tag=ValidTeleport] run tellraw @s ["",{"text":"Successfully Teleported using ","color":"green"},{"score":{"name":"#TempXP","objective":"warp"},"color":"green"},{"text":" of XP!","color":"green"}]
execute unless entity @s[nbt={Item:{tag:{free:1b}}}] as @p[tag=ValidTeleport,gamemode=!creative] run function warpbook:1.15/payxp

#~New Teleport = 1.14.1 pre-release 1 or more

execute at @s[nbt={Item:{tag:{Dimension:-1}}}] positioned ~ ~ ~ in minecraft:the_nether run tp @s ~ ~ ~
execute at @s[nbt={Item:{tag:{Dimension:0}}}] positioned ~ ~ ~ in minecraft:overworld run tp @s ~ ~ ~
execute at @s[nbt={Item:{tag:{Dimension:1}}}] positioned ~ ~ ~ in minecraft:the_end run tp @s ~ ~ ~

execute at @s[nbt={Item:{tag:{Dimension:-1}}}] in minecraft:the_nether run tp @p[tag=ValidTeleport] ~0.5 ~0.5 ~0.5
execute at @s[nbt={Item:{tag:{Dimension:0}}}] in minecraft:overworld run tp @p[tag=ValidTeleport] ~0.5 ~0.5 ~0.5
execute at @s[nbt={Item:{tag:{Dimension:1}}}] in minecraft:the_end run tp @p[tag=ValidTeleport] ~0.5 ~0.5 ~0.5


function mdiv:clear_shulker_box
data modify block 20202020 0 20202020 Items insert 0 from entity @s Item
#Remove the Line below after MC-134669 gets fixed
#execute store result block 20202020 0 20202020 Items[0].tag.fixGhost byte 1 unless data block 20202020 0 20202020 Items[{tag:{fixGhost:1b}}]
execute as @a[tag=ValidTeleport,tag=!LecternTeleport,limit=1] unless data entity @s SelectedItem store success score #warp.teleport.temp dummy run function mdiv:apply/mainhand
execute as @a[tag=ValidTeleport,tag=!LecternTeleport,limit=1] if data entity @s SelectedItem if score #warp.teleport.temp dummy matches 0 store success score #warp.teleport.temp dummy run loot give @s mine 20202020 0 20202020 minecraft:air{drop_contents:1b}
execute if score #warp.teleport.temp dummy matches 0 at @a[tag=ValidTeleport,tag=!LecternTeleport,limit=1] run loot spawn ~ ~ ~ mine 20202020 0 20202020 minecraft:air{drop_contents:1b}
kill @s





#~Old Teleport (1.14 or before):
#execute if entity @s[nbt={Item:{tag:{Dimension:-1}}}] in minecraft:the_nether run tp @s
#execute if entity @s[nbt={Item:{tag:{Dimension:0}}}] in minecraft:overworld run tp @s
#execute if entity @s[nbt={Item:{tag:{Dimension:1}}}] in minecraft:the_end run tp @s
#execute at @s run tp ~0.5 ~0.5 ~0.5

#execute if entity @s[nbt={Item:{tag:{Dimension:-1}}}] in minecraft:the_nether run tp @p[tag=ValidTeleport] @s
#execute if entity @s[nbt={Item:{tag:{Dimension:0}}}] in minecraft:overworld run tp @p[tag=ValidTeleport] @s
#execute if entity @s[nbt={Item:{tag:{Dimension:1}}}] in minecraft:the_end run tp @p[tag=ValidTeleport] @s