function mdiv:setup/offhand
    data modify block 20202020 0 20202020 Items[0].tag.Books prepend from entity @s SelectedItem.tag
    data remove block 20202020 0 20202020 Items[0].tag.Books[0].pages
    data remove block 20202020 0 20202020 Items[0].tag.Books[0].author
    data remove block 20202020 0 20202020 Items[0].tag.Books[0].Books

    data modify block 20202020 0 20202020 Items[0].tag.Books[0].display.Lore prepend value '[{"text":""}]'
    data modify block 20202020 0 20202020 Items[0].tag.Books[0].display.Lore prepend value '[{"text":"warp will get extracted to it","color": "dark_red","italic":false}]'
    data modify block 20202020 0 20202020 Items[0].tag.Books[0].display.Lore prepend value '[{"text":"If it\'s a extractor, the current","color": "dark_red","italic":false}]'
    data modify block 20202020 0 20202020 Items[0].tag.Books[0].display.Lore prepend value '[{"text":"Main-hand I\'ll save him at me","color": "dark_aqua","italic":false}]'
    data modify block 20202020 0 20202020 Items[0].tag.Books[0].display.Lore prepend value '[{"text":"If you have a Warp Book in your","color": "dark_aqua","italic":false}]'
    data modify block 20202020 0 20202020 Items[0].tag.Books[0].display.Lore prepend value '[{"text":"to Cycle between Saved Books","color": "aqua","italic":false}]'
    data modify block 20202020 0 20202020 Items[0].tag.Books[0].display.Lore prepend value '[{"text":"Sneak + Scroll with me in your Off-Hand","color": "aqua","italic":false}]'
    data modify block 20202020 0 20202020 Items[0].tag.Books[0].display.Lore prepend value '[{"text":""}]'
    data modify block 20202020 0 20202020 Items[0].tag.Books[0].display.Lore prepend value '[{"text":"Multiple Warp Book","color":"gold","bold":true,"italic":false}]'
    data modify block 20202020 0 20202020 Items[0].tag.Books[0].display.Lore prepend value '{"text":""}'

function mdiv:apply/offhand

replaceitem entity @s weapon.mainhand minecraft:air

execute at @s positioned ~ ~1.31999 ~ run kill @e[type=item,nbt={Item:{tag:{wb:1b}}},distance=..2,limit=1,sort=nearest]
function warpbook:1.15/cycle/cycle
#execute unless data block 20202020 0 20202020 Items[0].tag.Dimension run function warpbook:1.15/cycle/cycle