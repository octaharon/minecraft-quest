function mdiv:setup/offhand
    data modify block 20202020 0 20202020 Items[0].tag.Books append from block 20202020 0 20202020 Items[0].tag
    data remove block 20202020 0 20202020 Items[0].tag.Books[].Books
    data modify block 20202020 0 20202020 Items[0].tag.Books[0].Books set from block 20202020 0 20202020 Items[0].tag.Books
    data remove block 20202020 0 20202020 Items[0].tag.Books[0].Books[0]
    data modify block 20202020 0 20202020 Items[0].tag.display set from block 20202020 0 20202020 Items[0].tag.Books[0].display
    data modify block 20202020 0 20202020 Items[0].tag.Pos set from block 20202020 0 20202020 Items[0].tag.Books[0].Pos
    data modify block 20202020 0 20202020 Items[0].tag.Rotation set from block 20202020 0 20202020 Items[0].tag.Books[0].Rotation
    data modify block 20202020 0 20202020 Items[0].tag.Dimension set from block 20202020 0 20202020 Items[0].tag.Books[0].Dimension
    data remove block 20202020 0 20202020 Items[0].tag.Books[0]
    
    execute unless data block 20202020 0 20202020 Items[0].tag.Books[0].Dimension run data remove block 20202020 0 20202020 Items[0].tag.Books[0]

function mdiv:apply/offhand
title @s actionbar  ["",{"nbt": "Items[0].tag.display.Name","block": "20202020 0 20202020","interpret":true}]