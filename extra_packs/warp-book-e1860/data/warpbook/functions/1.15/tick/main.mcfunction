execute if score #Book warp matches 1 run function warpbook:1.15/tick/book
execute if score #Lectern warp matches 1 run function warpbook:1.15/tick/lectern
execute if score #Title warp matches 1 run function warpbook:1.15/tick/title
execute as @a[scores={warp.isSneaking=1..},nbt={Inventory:[{Slot:-106b,tag:{mwb:1b}}]}] unless score @s warp.sis = @s warp.sis.o run function warpbook:1.15/cycle/check
execute as @a[scores={warp.Jumped=1..},nbt={Inventory:[{Slot:-106b,tag:{mwb:1b}}]}] run function warpbook:1.15/cycle/main

# Slot Tracker
execute as @a run scoreboard players operation @s warp.sis.o = @s warp.sis
execute as @a store result score @s warp.sis run data get entity @s SelectedItemSlot


#Fix Desenchantment when you rename the item
execute as @a run function warpbook:1.15/mdiv/fix/enchantment/hands/main

scoreboard players reset * warp.lectern
scoreboard players reset * warp.isHoldingBK
scoreboard players reset * warp.Jumped
scoreboard players reset * warp.isSneaking
scoreboard players reset * warp.drop
scoreboard players set @a[nbt={SelectedItem:{tag:{title:"Warp Book"}}}] warp.isHoldingBK 1

schedule function warpbook:1.15/tick/main 1t