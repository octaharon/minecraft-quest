scoreboard players reset #TempTotal warp

#function warpbook:1.15/update/temp

#execute if score #CurrentX warp >= #TempX warp run scoreboard players operation #TempX warp >< #CurrentX warp
#execute if score #CurrentY warp >= #TempY warp run scoreboard players operation #TempY warp >< #CurrentY warp
#execute if score #CurrentZ warp >= #TempZ warp run scoreboard players operation #TempZ warp >< #CurrentZ warp
#scoreboard players operation #TempX warp -= #CurrentX warp
#scoreboard players operation #TempY warp -= #CurrentY warp
#scoreboard players operation #TempZ warp -= #CurrentZ warp

#scoreboard players operation #TempXP warp += #TempX warp
#scoreboard players operation #TempXP warp += #TempY warp
#scoreboard players operation #TempXP warp += #TempZ warp

#tellraw @a ["",{"text":"#Counter ","color":"green"},{"score":{"name":"#Counter","objective":"warp"},"color":"gold"},{"text":"\n#TempXP ","color":"gold"},{"score":{"name":"#TempXP","objective":"warp"},"color":"gold"},{"text":"\n#TempTotal ","color":"gold"},{"score":{"name":"#TempTotal","objective":"warp"},"color":"gold"}]
#tellraw @a ["",{"text":"#TempY ","color":"dark_blue"},{"score":{"name":"#TempY","objective":"warp"},"color":"dark_blue"}]
#tellraw @a ["",{"text":"#CurrentY ","color":"dark_blue"},{"score":{"name":"#CurrentY","objective":"warp"},"color":"dark_blue"}]


#~Get Square of difference of each Dimension and sum

#Differences:
scoreboard players operation #TempX warp -= #CurrentX warp
scoreboard players operation #TempY warp -= #CurrentY warp
scoreboard players operation #TempZ warp -= #CurrentZ warp

#If Any Value is bigger then (-)30000 Divide every value by 1000 by recursion with counter
scoreboard players set #CounterToDecrease warp 0

execute if score #TempX warp matches 30000.. run function warpbook:1.15/update/decrease
execute if score #TempZ warp matches 30000.. run function warpbook:1.15/update/decrease
execute if score #TempX warp matches ..-30000 run function warpbook:1.15/update/decrease
execute if score #TempZ warp matches ..-30000 run function warpbook:1.15/update/decrease


#Squares:
scoreboard players operation #TempX warp *= #TempX warp
scoreboard players operation #TempY warp *= #TempY warp
scoreboard players operation #TempZ warp *= #TempZ warp

#Sum:
scoreboard players operation #TempTotal warp += #TempX warp
scoreboard players operation #TempTotal warp += #TempY warp
scoreboard players operation #TempTotal warp += #TempZ warp

#~Get Sqrt of result sum
scoreboard players set #Counter warp 0
scoreboard players operation #TempXP warp = #TempTotal warp
scoreboard players operation #TempXP warp /= #2 warp
function warpbook:1.15/update/sqrt_loop

execute if score #CounterToDecrease warp matches 1.. run function warpbook:1.15/update/fix_value
execute if score #TempXP warp matches ..-1 run scoreboard players set #TempXP 21476483647

#~Set Amout of Blocks that can be Travelled per XP
scoreboard players operation #TempXP warp /= #BlocksPerXp warp