scoreboard players add #Counter warp 1
scoreboard players operation #sqrtBackup warp = #sqrtTemp warp
scoreboard players operation #sqrtTemp warp = #TempTotal warp
scoreboard players operation #sqrtTemp warp /= #TempXP warp
scoreboard players operation #sqrtTemp warp += #TempXP warp
scoreboard players operation #sqrtTemp warp /= #2 warp
scoreboard players operation #TempXP warp = #sqrtTemp warp
execute if score #Counter warp matches ..16 unless score #sqrtBackup warp = #sqrtTemp warp run function warpbook:1.15/update/sqrt_loop