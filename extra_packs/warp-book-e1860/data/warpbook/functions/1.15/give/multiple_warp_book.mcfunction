advancement revoke @s only warpbook:recipes/multiple_warp_book
recipe take @s warpbook:multiple_warp_book
clear @s minecraft:barrier 1

execute store result score @s warp run loot give @s loot warpbook:multiple_warp_book
execute at @s[scores={warp=0}] run loot spawn ~ ~ ~ loot warpbook:multiple_warp_book

schedule function warpbook:1.15/clear/barrier 1t

scoreboard players reset @s warp

execute if entity @s[nbt={Inventory:[{id:"minecraft:barrier"}]}] run function warpbook:1.15/give/multiple_warp_book
