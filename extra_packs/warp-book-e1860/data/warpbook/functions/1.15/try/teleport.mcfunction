data merge entity @s[tag=!suicide] {Motion:[0d,0.04d,0d],PickupDelay:500s,Invulnerable:1b}
tp ~ ~-0.81999 ~
execute at @s positioned ~ ~-0.5 ~ run tag @p[scores={warp.isHoldingBK=1},distance=..4] add ValidTeleport
execute as @p[tag=ValidTeleport] run function warpbook:1.15/update/all

execute if score #CurrentXP warp < #TempXP warp run tellraw @p[tag=ValidTeleport] ["",{"text":"You don't have enought XP to warp.\nYou have ","color":"dark_red"},{"score":{"name":"#CurrentXP","objective":"warp"},"color":"gold"},{"text":"/","color":"dark_gray"},{"score":{"name":"#TempXP","objective":"warp"},"color":"green"}]

execute if score #CurrentXP warp < #TempXP warp if entity @p[tag=ValidTeleport,gamemode=creative] run function warpbook:1.15/teleport
execute if score #CurrentXP warp < #TempXP warp if entity @s[nbt={Item:{tag:{free:1b}}}] run function warpbook:1.15/teleport
execute if score #CurrentXP warp >= #TempXP warp run function warpbook:1.15/teleport
tag @a remove ValidTeleport
#tellraw @a ["",{"text":"You have ","color":"green"},{"score":{"name":"#CurrentXP","objective":"warp"},"color":"gold"},{"text":"/","color":"dark_gray"},{"score":{"name":"#TempXP","objective":"warp"},"color":"green"}]