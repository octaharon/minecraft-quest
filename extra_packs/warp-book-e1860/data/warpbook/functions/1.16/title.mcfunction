function warpbook:1.16/update/current
function warpbook:1.16/update/tempholding
#title @s actionbar ["",{"text":"x: ","color":"gold"},{"score":{"name":"@s","objective":"warp.temp.x"},"color":"green"},{"text":"     ","color":"dark_blue"},{"text":"y: ","color":"gold"},{"score":{"name":"@s","objective":"warp.temp.y"},"color":"green"},{"text":"     ","color":"dark_blue"},{"text":"z: ","color":"gold"},{"score":{"name":"@s","objective":"warp.temp.z"},"color":"green"}]



execute if score #TempDim warp matches 0 run title @s actionbar ["",{"text":"x: ","color":"gold"},{"score":{"name":"#TempX","objective":"warp"},"color":"green"},{"text":"     y: ","color":"gold"},{"score":{"name":"#TempY","objective":"warp"},"color":"green"},{"text":"     z: ","color":"gold"},{"score":{"name":"#TempZ","objective":"warp"},"color":"green"},{"text":"     dim:","color":"gold"},{"text":" Overworld","color":"green"}]

execute if score #TempDim warp matches -1 run title @s actionbar ["",{"text":"x: ","color":"gold"},{"score":{"name":"#TempX","objective":"warp"},"color":"green"},{"text":"     y: ","color":"gold"},{"score":{"name":"#TempY","objective":"warp"},"color":"green"},{"text":"     z: ","color":"gold"},{"score":{"name":"#TempZ","objective":"warp"},"color":"green"},{"text":"     dim:","color":"gold"},{"text":" Nether","color":"dark_red"}]

execute if score #TempDim warp matches 1 run title @s actionbar ["",{"text":"x: ","color":"gold"},{"score":{"name":"#TempX","objective":"warp"},"color":"green"},{"text":"     y: ","color":"gold"},{"score":{"name":"#TempY","objective":"warp"},"color":"green"},{"text":"     z: ","color":"gold"},{"score":{"name":"#TempZ","objective":"warp"},"color":"green"},{"text":"     dim:","color":"gold"},{"text":" The End","color":"yellow"}]