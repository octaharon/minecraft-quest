# Change Book from Main Hand
function mdiv:setup/mainhand
    # Check if he have more then one book
    execute store result score #mdiv.temp dummy run data get block 20202020 0 20202020 Items[0].Count

    # Extract and Fix some things
    data modify block 20202020 0 20202020 Items[0].tag set from entity @s Inventory[{Slot:-106b}].tag
    data modify block 20202020 0 20202020 Items[0].Count set value 1b
    data modify block 20202020 0 20202020 Items[0].tag.wb set value 1b
    data modify block 20202020 0 20202020 Items[0].id set value "minecraft:written_book"
    
    # Add Default things from normal Warp Book
    loot insert 20202020 0 20202020 loot warpbook:warp_book
    data modify block 20202020 0 20202020 Items[0].tag.pages set from block 20202020 0 20202020 Items[-1].tag.pages
    data modify block 20202020 0 20202020 Items[0].tag.author set from block 20202020 0 20202020 Items[-1].tag.author
    data remove block 20202020 0 20202020 Items[-1]

    # Remove modifications made by Multiple Warp Book
    data remove block 20202020 0 20202020 Items[0].tag.Books
    data remove block 20202020 0 20202020 Items[0].tag.mwb
    data remove block 20202020 0 20202020 Items[0].tag.mwbe
    data remove block 20202020 0 20202020 Items[0].tag.display.Lore[0]
    data remove block 20202020 0 20202020 Items[0].tag.display.Lore[0]
    data remove block 20202020 0 20202020 Items[0].tag.display.Lore[0]
    data remove block 20202020 0 20202020 Items[0].tag.display.Lore[0]
    data remove block 20202020 0 20202020 Items[0].tag.display.Lore[0]
    data remove block 20202020 0 20202020 Items[0].tag.display.Lore[0]
    data remove block 20202020 0 20202020 Items[0].tag.display.Lore[0]
    data remove block 20202020 0 20202020 Items[0].tag.display.Lore[0]
    data remove block 20202020 0 20202020 Items[0].tag.display.Lore[0]
    data remove block 20202020 0 20202020 Items[0].tag.display.Lore[0]

function mdiv:apply/mainhand

# Try to give/drop extra Extractor books the player may had
function mdiv:clear_shulker_box
    loot insert 20202020 0 20202020 loot warpbook:multiple_warp_book_extractor
    execute store result block 20202020 0 20202020 Items[0].Count byte 1 run scoreboard players remove #mdiv.temp dummy 1
    execute store result score #mdiv.temp dummy run loot give @s mine 20202020 0 20202020 air{drop_contents:1b}
execute if score #mdiv.temp dummy matches 0 at @s run function mdiv:drop

# Update Off-Hand Item
function mdiv:setup/offhand

    data modify block 20202020 0 20202020 Items[0].tag.Books[0].Books set from block 20202020 0 20202020 Items[0].tag.Books
    data remove block 20202020 0 20202020 Items[0].tag.Books[0].Books[0]
    data modify block 20202020 0 20202020 Items[0].tag.display set from block 20202020 0 20202020 Items[0].tag.Books[0].display
    data modify block 20202020 0 20202020 Items[0].tag.Pos set from block 20202020 0 20202020 Items[0].tag.Books[0].Pos
    data modify block 20202020 0 20202020 Items[0].tag.Rotation set from block 20202020 0 20202020 Items[0].tag.Books[0].Rotation
    data modify block 20202020 0 20202020 Items[0].tag.Dimension set from block 20202020 0 20202020 Items[0].tag.Books[0].Dimension
    execute unless data block 20202020 0 20202020 Items[0].tag.Books[] run data remove block 20202020 0 20202020 Items[0]
    loot insert 20202020 0 20202020 loot warpbook:multiple_warp_book
    data remove block 20202020 0 20202020 Items[0].tag.Books[0]
    
    execute unless data block 20202020 0 20202020 Items[0].tag.Books[0].Dimension run data remove block 20202020 0 20202020 Items[0].tag.Books[0]

function mdiv:apply/offhand


title @s actionbar  ["",{"nbt": "Items[0].tag.display.Name","block": "20202020 0 20202020","interpret":true}]