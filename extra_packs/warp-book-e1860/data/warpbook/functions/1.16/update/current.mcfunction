execute store result score #CurrentX warp run data get entity @s Pos[0]
execute store result score #CurrentY warp run data get entity @s Pos[1]
execute store result score #CurrentZ warp run data get entity @s Pos[2]
execute store result score #CurrentRot warp run data get entity @s Rotation[0]
execute store result score #CurrentDim warp run data get entity @s Dimension
execute store result score #CurrentXP warp run data get entity @s XpTotal