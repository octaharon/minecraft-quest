scoreboard players operation #TempX warp /= #1000 warp
scoreboard players operation #TempY warp /= #1000 warp
scoreboard players operation #TempZ warp /= #1000 warp

scoreboard players add #CounterToDecrease warp 1

execute if score #TempX warp matches 30000.. run function warpbook:1.16/update/decrease
execute if score #TempZ warp matches 30000.. run function warpbook:1.16/update/decrease
execute if score #TempX warp matches ..-30000 run function warpbook:1.16/update/decrease
execute if score #TempZ warp matches ..-30000 run function warpbook:1.16/update/decrease
