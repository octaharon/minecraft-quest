recipe take @s warpbook:impossible
advancement revoke @s only warpbook:recipes/impossible

execute at @s run summon minecraft:area_effect_cloud ~ ~ ~ {Duration:0}
execute at @s align xyz run tp @s ~ ~ ~ ~ ~

execute if data entity @s SelectedItem run loot replace entity @s weapon.offhand loot warpbook:warp_book
execute unless data entity @s SelectedItem run loot replace entity @s weapon.mainhand loot warpbook:warp_book

execute positioned as @e[type=area_effect_cloud,limit=1,sort=nearest] rotated as @s run tp @s ~ ~ ~ ~ ~