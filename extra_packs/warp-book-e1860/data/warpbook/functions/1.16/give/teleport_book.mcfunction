advancement revoke @s only warpbook:recipes/teleport_book
recipe take @s warpbook:teleport_book
clear @s minecraft:barrier 1

execute store result score @s warp run loot give @s loot warpbook:teleport_book
execute at @s[scores={warp=0}] run loot spawn ~ ~ ~ loot warpbook:teleport_book

schedule function warpbook:1.16/clear/barrier 1t

execute if entity @s[nbt={Inventory:[{id:"minecraft:barrier"}]}] run function warpbook:1.16/give/teleport_book
