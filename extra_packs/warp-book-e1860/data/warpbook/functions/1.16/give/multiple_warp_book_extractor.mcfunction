advancement revoke @s only warpbook:recipes/multiple_warp_book_extractor
recipe take @s warpbook:multiple_warp_book_extractor
clear @s minecraft:barrier 1

execute store result score @s warp run loot give @s loot warpbook:multiple_warp_book_extractor
execute at @s[scores={warp=0}] run loot spawn ~ ~ ~ loot warpbook:multiple_warp_book_extractor

schedule function warpbook:1.16/clear/barrier 1t

scoreboard players reset @s warp

execute if entity @s[nbt={Inventory:[{id:"minecraft:barrier"}]}] run function warpbook:1.16/give/multiple_warp_book_extractor
