tag @s add ValidTeleport
    execute at @s positioned ~ ~1.31999 ~ run summon item ~ ~ ~ {Age:5998s,PickupDelay:-1s,Tags:["suicide"],Motion:[0d,0.04d,0d],Invulnerable:1b,Item:{id:"written_book",Count:1b}}
    tag @s add LecternTeleport
        execute as @e[type=item,tag=suicide,distance=..7,limit=1] run function warpbook:1.16/lectern/book-merge-tp-suicide
    tag @s remove LecternTeleport