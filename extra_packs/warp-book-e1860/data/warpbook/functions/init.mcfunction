scoreboard players set #0 dummy 0
scoreboard players set #1 dummy 1
scoreboard players set #2 dummy 2
scoreboard players set #3 dummy 3
scoreboard players set #10 dummy 10
scoreboard players set #-1 dummy -1
function warpbook:version/check
scoreboard players operation #warpbook:init dummy = #version dummy
execute if score #version dummy matches 113 run function warpbook:1.13/init
execute if score #version dummy matches 114 run function warpbook:1.14/init
execute if score #version dummy matches 115 run function warpbook:1.15/init
execute if score #version dummy matches 116 run function warpbook:1.16/init


scoreboard objectives add warp dummy
scoreboard objectives add warp.drop minecraft.dropped:minecraft.written_book
scoreboard objectives add warp.lectern minecraft.custom:minecraft.interact_with_lectern

scoreboard players add #BlocksPerXp warp 0
scoreboard players set #1000 warp 1000

execute if score #BlocksPerXp warp matches 0 run scoreboard players set #BlocksPerXp warp 70
scoreboard players set #2 warp 2

scoreboard objectives add warp.isSneaking minecraft.custom:minecraft.sneak_time
scoreboard objectives add warp.isHoldingBK dummy
#scoreboard objectives add warp.isHoldingMB dummy
scoreboard objectives add warp.Jumped minecraft.custom:minecraft.jump

scoreboard players add #Book warp 0
scoreboard players add #Lectern warp 0
scoreboard players add #Title warp 0

execute unless score #FirstRun warp matches 0 run scoreboard players set #Book warp 1
execute unless score #FirstRun warp matches 0 run scoreboard players set #Lectern warp 1
execute unless score #FirstRun warp matches 0 run scoreboard players set #Title warp 1
scoreboard players add #FirstRun warp 0