execute if score Biome Biome5556 matches 64 run function cm_module_megamix:biomes/64_snowy_tundra
execute if score Biome Biome5556 matches 65 run function cm_module_megamix:biomes/65_snowy_mountains
execute if score Biome Biome5556 matches 66 run function cm_module_megamix:biomes/66_ice_spikes
execute if score Biome Biome5556 matches 67 run function cm_module_megamix:biomes/67_mountains
execute if score Biome Biome5556 matches 68 run function cm_module_megamix:biomes/68_mountains_cold
execute if score Biome Biome5556 matches 69 run function cm_module_megamix:biomes/69_wooded_mountains
execute if score Biome Biome5556 matches 70 run function cm_module_megamix:biomes/70_wooded_mountains_cold
execute if score Biome Biome5556 matches 71 run function cm_module_megamix:biomes/71_gravelly_mountains