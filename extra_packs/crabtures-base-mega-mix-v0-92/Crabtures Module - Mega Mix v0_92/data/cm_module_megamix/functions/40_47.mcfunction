execute if score Biome Biome5556 matches 40 run function cm_module_megamix:biomes/40_taiga
execute if score Biome Biome5556 matches 41 run function cm_module_megamix:biomes/41_taiga_hills
execute if score Biome Biome5556 matches 42 run function cm_module_megamix:biomes/42_taiga_mountains
execute if score Biome Biome5556 matches 43 run function cm_module_megamix:biomes/43_snowy_taiga
execute if score Biome Biome5556 matches 44 run function cm_module_megamix:biomes/44_snowy_taiga_hills
execute if score Biome Biome5556 matches 45 run function cm_module_megamix:biomes/45_snowy_taiga_mountains
execute if score Biome Biome5556 matches 46 run function cm_module_megamix:biomes/46_giant_tree_taiga
execute if score Biome Biome5556 matches 47 run function cm_module_megamix:biomes/47_giant_tree_taiga_hills