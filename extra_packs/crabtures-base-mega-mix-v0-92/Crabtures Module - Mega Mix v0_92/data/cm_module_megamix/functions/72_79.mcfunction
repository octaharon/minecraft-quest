execute if score Biome Biome5556 matches 72 run function cm_module_megamix:biomes/72_modified_gravelly_mountains
execute if score Biome Biome5556 matches 73 run function cm_module_megamix:biomes/73_mountain_edge
execute if score Biome Biome5556 matches 74 run function cm_module_megamix:biomes/74_badlands
execute if score Biome Biome5556 matches 75 run function cm_module_megamix:biomes/75_badlands_plateau
execute if score Biome Biome5556 matches 76 run function cm_module_megamix:biomes/76_modified_badlands_plateau
execute if score Biome Biome5556 matches 77 run function cm_module_megamix:biomes/77_wooded_badlands_plateau
execute if score Biome Biome5556 matches 78 run function cm_module_megamix:biomes/78_modified_wooded_badlands_plateau
execute if score Biome Biome5556 matches 79 run function cm_module_megamix:biomes/79_eroded_badlands