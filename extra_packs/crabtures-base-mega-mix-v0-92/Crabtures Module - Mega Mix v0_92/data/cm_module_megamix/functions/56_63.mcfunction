execute if score Biome Biome5556 matches 56 run function cm_module_megamix:biomes/56_savanna_plateau
execute if score Biome Biome5556 matches 57 run function cm_module_megamix:biomes/57_shattered_savanna
execute if score Biome Biome5556 matches 58 run function cm_module_megamix:biomes/58_shattered_savanna_plateau
execute if score Biome Biome5556 matches 59 run function cm_module_megamix:biomes/59_flower_forest
execute if score Biome Biome5556 matches 60 run function cm_module_megamix:biomes/60_sunflower_plains
execute if score Biome Biome5556 matches 61 run function cm_module_megamix:biomes/61_desert
execute if score Biome Biome5556 matches 62 run function cm_module_megamix:biomes/62_desert_hills
execute if score Biome Biome5556 matches 63 run function cm_module_megamix:biomes/63_desert_lakes