execute if score Biome Biome5556 matches 32 run function cm_module_megamix:biomes/32_dark_forest_hills
execute if score Biome Biome5556 matches 33 run function cm_module_megamix:biomes/33_jungle
execute if score Biome Biome5556 matches 34 run function cm_module_megamix:biomes/34_jungle_hills
execute if score Biome Biome5556 matches 35 run function cm_module_megamix:biomes/35_modified_jungle
execute if score Biome Biome5556 matches 36 run function cm_module_megamix:biomes/36_jungle_edge
execute if score Biome Biome5556 matches 37 run function cm_module_megamix:biomes/37_modified_jungle_edge
execute if score Biome Biome5556 matches 38 run function cm_module_megamix:biomes/38_bamboo_jungle
execute if score Biome Biome5556 matches 39 run function cm_module_megamix:biomes/39_bamboo_jungle_hills