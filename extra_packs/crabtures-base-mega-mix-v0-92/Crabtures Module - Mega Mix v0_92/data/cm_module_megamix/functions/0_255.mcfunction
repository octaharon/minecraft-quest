scoreboard players operation Biome Biome5556 = @s Biome5556
scoreboard players operation BiomeRegion BiomeRegion5556 = @s BiomeRegion5556

scoreboard players set Landed ExtraLogic5556 0

execute if score Biome Biome5556 matches 0..31 run function cm_module_megamix:0_31
execute if score Biome Biome5556 matches 32..63 run function cm_module_megamix:32_63
execute if score Biome Biome5556 matches 64..95 run function cm_module_megamix:64_95
execute if score Biome Biome5556 matches 96..127 run function cm_module_megamix:96_127

execute if score Biome Biome5556 matches 257.. run kill @s