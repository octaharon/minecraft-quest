execute if score Biome Biome5556 matches 48 run function cm_module_megamix:biomes/48_giant_spruce_taiga
execute if score Biome Biome5556 matches 49 run function cm_module_megamix:biomes/49_giant_spruce_taiga_hills
execute if score Biome Biome5556 matches 50 run function cm_module_megamix:biomes/50_mushroom_fields
execute if score Biome Biome5556 matches 51 run function cm_module_megamix:biomes/51_mushroom_field_shore
execute if score Biome Biome5556 matches 52 run function cm_module_megamix:biomes/52_swamp_water
execute if score Biome Biome5556 matches 53 run function cm_module_megamix:biomes/53_swamp_land
execute if score Biome Biome5556 matches 54 run function cm_module_megamix:biomes/54_swamp_hills
execute if score Biome Biome5556 matches 55 run function cm_module_megamix:biomes/55_savanna