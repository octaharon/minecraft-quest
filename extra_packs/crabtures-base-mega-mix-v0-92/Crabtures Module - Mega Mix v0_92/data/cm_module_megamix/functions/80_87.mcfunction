execute if score Biome Biome5556 matches 80 run function cm_module_megamix:biomes/80_the_end
execute if score Biome Biome5556 matches 81 run function cm_module_megamix:biomes/81_small_end_islands
execute if score Biome Biome5556 matches 82 run function cm_module_megamix:biomes/82_end_midlands
execute if score Biome Biome5556 matches 83 run function cm_module_megamix:biomes/83_end_highlands
execute if score Biome Biome5556 matches 84 run function cm_module_megamix:biomes/84_end_barrens
execute if score Biome Biome5556 matches 85 run function cm_module_megamix:biomes/85_end_sky
execute if score Biome Biome5556 matches 86 run function cm_module_megamix:biomes/86_end_dungeon
execute if score Biome Biome5556 matches 87 run function cm_module_megamix:biomes/87_the_void