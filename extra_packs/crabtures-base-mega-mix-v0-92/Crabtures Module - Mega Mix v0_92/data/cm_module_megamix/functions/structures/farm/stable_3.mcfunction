forceload add ~-12 ~-12 ~12 ~12

function structures_crabmaster:randomizers/randomizer_2

execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air if score Random RandomTwo5556 matches 0..249 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:farm_stable",integrity:1.00,posX:-1,posY:-6,posZ:-8,rotation:"NONE"} replace
execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air if score Random RandomTwo5556 matches 250..499 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:farm_stable",integrity:1.00,posX:8,posY:-6,posZ:-1,rotation:"CLOCKWISE_90"} replace
execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air if score Random RandomTwo5556 matches 500..749 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:farm_stable",integrity:1.00,posX:1,posY:-6,posZ:8,rotation:"CLOCKWISE_180"} replace
execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air if score Random RandomTwo5556 matches 750..999 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:farm_stable",integrity:1.00,posX:-8,posY:-6,posZ:1,rotation:"COUNTERCLOCKWISE_90"} replace

execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air run fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace

scoreboard players set Base TimerOne5556 0
execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air positioned ~ ~-6 ~ if score Random RandomTwo5556 matches 0..249 run function cm_module_megamix:structures/farm/stable_base_1
execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air positioned ~ ~-6 ~ if score Random RandomTwo5556 matches 250..499 run function cm_module_megamix:structures/farm/stable_base_2
execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air positioned ~ ~-6 ~ if score Random RandomTwo5556 matches 500..749 run function cm_module_megamix:structures/farm/stable_base_3
execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air positioned ~ ~-6 ~ if score Random RandomTwo5556 matches 750..999 run function cm_module_megamix:structures/farm/stable_base_4

execute as @e[tag=Spawner,tag=C5556,distance=..32] at @s run function cm_module_megamix:structures/farm/stable_spawn

kill @e[tag=Spawner,tag=C5556,distance=..16]
kill @e[type=minecraft:item,distance=..16]

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999
scoreboard players set Landed ExtraLogic5556 1