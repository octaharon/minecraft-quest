function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..419 run function cm_module_megamix:structures/farm/crops_spawn_2
execute if score Random RandomOne5556 matches 420..839 run function cm_module_megamix:structures/farm/crops_spawn_3
execute if score Random RandomOne5556 matches 840..919 run function cm_module_megamix:structures/farm/crops_spawn_4
execute if score Random RandomOne5556 matches 920..999 run function cm_module_megamix:structures/farm/crops_spawn_5

fill ~ ~2 ~ ~ ~2 ~ minecraft:redstone_block replace
fill ~ ~1 ~ ~ ~2 ~ minecraft:air replace