scoreboard players set Landed ExtraLogic5556 0
execute if predicate structures_crabmaster:randomizers/0_9 positioned ~22 ~8 ~-8 run function cm_module_megamix:structures/farm/crops_land_up
scoreboard players set Landed ExtraLogic5556 0
execute if predicate structures_crabmaster:randomizers/0_9 positioned ~12 ~8 ~10 run function cm_module_megamix:structures/farm/crops_land_up
scoreboard players set Landed ExtraLogic5556 0
execute if predicate structures_crabmaster:randomizers/0_9 positioned ~-22 ~8 ~3 run function cm_module_megamix:structures/farm/crops_land_down
scoreboard players set Landed ExtraLogic5556 0
execute if predicate structures_crabmaster:randomizers/0_9 positioned ~15 ~8 ~-8 run function cm_module_megamix:structures/farm/crops_land_down
scoreboard players set Landed ExtraLogic5556 0
execute if predicate structures_crabmaster:randomizers/0_9 positioned ~2 ~8 ~22 run function cm_module_megamix:structures/farm/crops_land_right
scoreboard players set Landed ExtraLogic5556 0
execute if predicate structures_crabmaster:randomizers/0_9 positioned ~-13 ~8 ~13 run function cm_module_megamix:structures/farm/crops_land_right
scoreboard players set Landed ExtraLogic5556 0
execute if predicate structures_crabmaster:randomizers/0_9 positioned ~5 ~8 ~-22 run function cm_module_megamix:structures/farm/crops_land_left
scoreboard players set Landed ExtraLogic5556 0
execute if predicate structures_crabmaster:randomizers/0_9 positioned ~8 ~8 ~-14 run function cm_module_megamix:structures/farm/crops_land_left

execute as @e[tag=CrowSpot,tag=C5556,distance=..128] at @s run function cm_module_megamix:structures/small_parts/scarecrow_pick

execute positioned ~-8 ~ ~1 if predicate structures_crabmaster:randomizers/0_15 run function cm_module_megamix:structures/small_parts/campfire_1
execute positioned ~-12 ~ ~-12 if predicate structures_crabmaster:randomizers/0_05 run function cm_module_megamix:structures/small_parts/small_tower_1
execute positioned ~-6 ~ ~ if predicate structures_crabmaster:randomizers/0_1 run function cm_module_megamix:structures/small_parts/camp_bench_1
execute positioned ~-8 ~ ~-3 if predicate structures_crabmaster:randomizers/0_1 run function cm_module_megamix:structures/small_parts/camp_table_1
execute positioned ~6 ~ ~ if predicate structures_crabmaster:randomizers/0_8 run function cm_module_megamix:structures/small_parts/farm_hay_1
execute positioned ~-6 ~ ~ if predicate structures_crabmaster:randomizers/0_7 run function cm_module_megamix:structures/small_parts/farm_hay_1
execute positioned ~ ~ ~6 if predicate structures_crabmaster:randomizers/0_6 run function cm_module_megamix:structures/small_parts/farm_hay_1
execute positioned ~ ~ ~-6 if predicate structures_crabmaster:randomizers/0_5 run function cm_module_megamix:structures/small_parts/farm_hay_1
execute positioned ~12 ~ ~12 if predicate structures_crabmaster:randomizers/0_2 run function cm_module_megamix:structures/small_parts/outhouse_1
execute positioned ~7 ~ ~7 if predicate structures_crabmaster:randomizers/0_5 run function cm_module_megamix:structures/small_parts/farm_leanto_1
execute positioned ~7 ~ ~-7 if predicate structures_crabmaster:randomizers/0_5 run function cm_module_megamix:structures/small_parts/farm_logs_1
execute positioned ~6 ~ ~-4 if predicate structures_crabmaster:randomizers/0_5 run function cm_module_megamix:structures/small_parts/farm_logs_1
execute positioned ~5 ~ ~-12 if predicate structures_crabmaster:randomizers/0_5 run function cm_module_megamix:structures/small_parts/farm_logs_1
execute positioned ~-4 ~ ~-7 if predicate structures_crabmaster:randomizers/0_6 run function cm_module_megamix:structures/small_parts/farm_well_1
execute positioned ~-4 ~ ~18 if predicate structures_crabmaster:randomizers/0_1 run function cm_module_megamix:structures/small_parts/wagon_1
execute positioned ~-4 ~ ~18 if predicate structures_crabmaster:randomizers/0_01 run function cm_module_megamix:structures/ruins/wagon_1

execute positioned ~1 ~ ~ if predicate structures_crabmaster:randomizers/0_95 run function cm_module_megamix:structures/farm/barn_1
execute positioned ~-6 ~ ~1 if predicate structures_crabmaster:randomizers/0_8 run function cm_module_megamix:structures/farm/silo_1
execute positioned ~6 ~ ~1 if predicate structures_crabmaster:randomizers/0_1 run function cm_module_megamix:structures/farm/silo_1
execute positioned ~-50 ~ ~10 if predicate structures_crabmaster:randomizers/0_05 run function cm_module_megamix:structures/farm/silo_1
execute positioned ~5 ~ ~11 if predicate structures_crabmaster:randomizers/0_85 run function cm_module_megamix:structures/farm/stable_1


fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:farm_house",integrity:1.00,posX:-4,posY:-8,posZ:-3,rotation:"NONE"} replace
fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace

scoreboard players set Base TimerOne5556 0
execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air positioned ~ ~-8 ~ run function cm_module_megamix:structures/farm/farm_house_base

execute as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..16] at @s run summon minecraft:villager ~ ~ ~
execute as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..16] at @s if predicate structures_crabmaster:randomizers/0_95 run summon minecraft:villager ~ ~ ~ 
execute as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..16] at @s if predicate structures_crabmaster:randomizers/0_7 run summon minecraft:villager ~ ~ ~ {Age:-6000}
execute as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..16] at @s if predicate structures_crabmaster:randomizers/0_6 run summon minecraft:villager ~ ~ ~ {Age:-12000}
execute as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..16] at @s if predicate structures_crabmaster:randomizers/0_5 run summon minecraft:villager ~ ~ ~ {Age:-18000}
execute as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..16] at @s if predicate structures_crabmaster:randomizers/0_4 run summon minecraft:villager ~ ~ ~ {Age:-24000}

kill @e[tag=Spawner,tag=C5556,distance=..16]
kill @e[type=minecraft:item,distance=..64]

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999