function structures_crabmaster:randomizers/randomizer_1

scoreboard players set Landed ExtraLogic5556 0

execute positioned ~ ~16 ~32 if score Random RandomOne5556 matches 0..249 run function cm_module_megamix:structures/farm/stable_2
execute positioned ~-28 ~16 ~16 if score Random RandomOne5556 matches 250..499 run function cm_module_megamix:structures/farm/stable_2
execute positioned ~-42 ~16 ~0 if score Random RandomOne5556 matches 500..749 run function cm_module_megamix:structures/farm/stable_2
execute positioned ~-32 ~16 ~-32 if score Random RandomOne5556 matches 750..999 run function cm_module_megamix:structures/farm/stable_2