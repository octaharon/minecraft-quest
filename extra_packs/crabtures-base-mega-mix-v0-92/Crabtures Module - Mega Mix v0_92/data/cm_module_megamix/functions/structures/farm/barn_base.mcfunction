scoreboard players set Base Results5556 0
scoreboard players add Base TimerOne5556 1

execute if score Base TimerOne5556 matches 0..1 store success score Base Results5556 run fill ~1 ~ ~-6 ~17 ~-2 ~5 minecraft:cobblestone replace #structures_crabmaster:air_water_dirt
execute if score Base TimerOne5556 matches 2..3 store success score Base Results5556 run fill ~3 ~ ~-5 ~15 ~-2 ~4 minecraft:cobblestone replace #structures_crabmaster:air_water_dirt
execute if score Base TimerOne5556 matches 4..5 store success score Base Results5556 run fill ~5 ~ ~-4 ~13 ~-2 ~3 minecraft:cobblestone replace #structures_crabmaster:air_water_dirt
execute if score Base TimerOne5556 matches 6..7 store success score Base Results5556 run fill ~7 ~ ~-3 ~11 ~-2 ~2 minecraft:cobblestone replace #structures_crabmaster:air_water_dirt
execute if score Base TimerOne5556 matches 8.. store success score Base Results5556 run fill ~9 ~ ~-2 ~9 ~-2 ~1 minecraft:cobblestone replace #structures_crabmaster:air_water_dirt

execute if score Base Results5556 matches 1.. positioned ~ ~-3 ~ run function cm_module_megamix:structures/farm/barn_base