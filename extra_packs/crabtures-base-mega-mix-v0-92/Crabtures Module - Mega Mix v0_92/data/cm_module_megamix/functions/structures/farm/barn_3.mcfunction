forceload add ~-16 ~-16 ~16 ~16

#function structures_crabmaster:randomizers/randomizer_2

execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:farm_barn",integrity:1.00,posX:0,posY:-6,posZ:-7,rotation:"NONE"} replace
#execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air if score Random RandomTwo5556 matches 250..499 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:farm_well",integrity:1.00,posX:7,posY:-6,posZ:0,rotation:"CLOCKWISE_90"} replace
#execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air if score Random RandomTwo5556 matches 500..749 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:farm_well",integrity:1.00,posX:0,posY:-6,posZ:7,rotation:"CLOCKWISE_180"} replace
#execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air if score Random RandomTwo5556 matches 750..999 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:farm_well",integrity:1.00,posX:-7,posY:-6,posZ:0,rotation:"COUNTERCLOCKWISE_90"} replace

execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air run fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace

execute as @e[tag=Spawner,tag=C5556,distance=..32] at @s run function cm_module_megamix:structures/farm/barn_spawn

scoreboard players set Base TimerOne5556 0
execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air positioned ~ ~-6 ~ run function cm_module_megamix:structures/farm/barn_base

kill @e[tag=Spawner,tag=C5556,distance=..16]
kill @e[type=minecraft:item,distance=..16]

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999
scoreboard players set Landed ExtraLogic5556 1