function structures_crabmaster:randomizers/randomizer_1

execute as @s at @s unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air if score Random RandomOne5556 matches 0..499 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:lost_ore_1",integrity:1.00,posX:-3,posY:-1,posZ:-3,rotation:"NONE"} replace
execute as @s at @s unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air if score Random RandomOne5556 matches 500..749 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:lost_ore_2",integrity:1.00,posX:-3,posY:-1,posZ:-3,rotation:"NONE"} replace
execute as @s at @s unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air if score Random RandomOne5556 matches 750..874 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:lost_ore_3",integrity:1.00,posX:-3,posY:-1,posZ:-3,rotation:"NONE"} replace
execute as @s at @s unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air if score Random RandomOne5556 matches 875..999 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:lost_ore_4",integrity:1.00,posX:-3,posY:-1,posZ:-3,rotation:"NONE"} replace

execute as @s at @s unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air run fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace

kill @e[type=minecraft:item,distance=..16]

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999