function structures_crabmaster:randomizers/randomizer_1

execute as @s at @s if score Random RandomOne5556 matches 0..249 run fill ~ 62 ~ ~ 62 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:village_boat",integrity:1.00,posX:-4,posY:-1,posZ:-14,rotation:"NONE"} replace
execute as @s at @s if score Random RandomOne5556 matches 250..499 run fill ~ 62 ~ ~ 62 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:village_boat",integrity:1.00,posX:14,posY:-1,posZ:-4,rotation:"CLOCKWISE_90"} replace
execute as @s at @s if score Random RandomOne5556 matches 500..749 run fill ~ 62 ~ ~ 62 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:village_boat",integrity:1.00,posX:4,posY:-1,posZ:14,rotation:"CLOCKWISE_180"} replace
execute as @s at @s if score Random RandomOne5556 matches 750..999 run fill ~ 62 ~ ~ 62 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:village_boat",integrity:1.00,posX:-14,posY:-1,posZ:4,rotation:"COUNTERCLOCKWISE_90"} replace

execute as @s at @s run fill ~ 63 ~ ~ 63 ~ minecraft:redstone_block replace

execute as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..32] at @s run summon minecraft:villager ~ ~ ~
execute as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..32] at @s if predicate structures_crabmaster:randomizers/0_5 run summon minecraft:villager ~ ~ ~
execute as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..32] at @s if predicate structures_crabmaster:randomizers/0_5 run summon minecraft:villager ~ ~ ~
execute as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..32] at @s if predicate structures_crabmaster:randomizers/0_25 run summon minecraft:villager ~ ~ ~
execute as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..32] at @s if predicate structures_crabmaster:randomizers/0_25 run summon minecraft:villager ~ ~ ~

kill @e[type=minecraft:item,distance=..16]
execute as @s at @s run kill @e[tag=Spawner,tag=C5556,distance=..32]

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999