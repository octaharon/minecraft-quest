forceload add ~-12 ~-12 ~12 ~12

execute unless block ~ ~-3 ~ #structures_crabmaster:water_lava unless block ~ ~ ~ #structures_crabmaster:water_lava unless block ~ ~4 ~ #structures_crabmaster:water_lava unless block ~ ~8 ~ #structures_crabmaster:water_lava unless block ~ ~13 ~ #structures_crabmaster:water_lava run function cm_module_megamix:structures/mush_cave/spawn_1

execute positioned ~5 ~ ~3 if predicate structures_crabmaster:randomizers/0_9 run function cm_module_megamix:structures/mush_cave/loop_up
execute positioned ~-3 ~ ~5 if predicate structures_crabmaster:randomizers/0_9 run function cm_module_megamix:structures/mush_cave/loop_right
execute positioned ~-5 ~ ~-3 if predicate structures_crabmaster:randomizers/0_9 run function cm_module_megamix:structures/mush_cave/loop_down
execute positioned ~3 ~ ~-5 if predicate structures_crabmaster:randomizers/0_9 run function cm_module_megamix:structures/mush_cave/loop_left

execute as @e[tag=MushSpot,tag=C5556] at @s run function cm_module_megamix:structures/mush_cave/plant_1

kill @e[type=minecraft:item,distance=..16]

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999