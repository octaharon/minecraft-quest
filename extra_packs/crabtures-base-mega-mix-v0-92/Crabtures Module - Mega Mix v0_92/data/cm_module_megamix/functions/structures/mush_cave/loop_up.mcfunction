forceload add ~-8 ~-8 ~8 ~8

execute unless block ~ ~-3 ~ #structures_crabmaster:water_lava unless block ~ ~ ~ #structures_crabmaster:water_lava unless block ~ ~4 ~ #structures_crabmaster:water_lava unless block ~ ~8 ~ #structures_crabmaster:water_lava unless block ~ ~13 ~ #structures_crabmaster:water_lava run function cm_module_megamix:structures/mush_cave/spawn_1

execute if predicate structures_crabmaster:randomizers/0_4 positioned ~5 ~ ~3 run function cm_module_megamix:structures/mush_cave/loop_up
execute if predicate structures_crabmaster:randomizers/0_2 positioned ~-3 ~ ~5 run function cm_module_megamix:structures/mush_cave/loop_up
execute if predicate structures_crabmaster:randomizers/0_1 positioned ~ ~ ~-5 run function cm_module_megamix:structures/mush_cave/loop_left

execute if predicate structures_crabmaster:randomizers/0_025 positioned ~5 ~-1 ~ run function cm_module_megamix:structures/mush_cave/loop_up
execute if predicate structures_crabmaster:randomizers/0_075 positioned ~5 ~1 ~ run function cm_module_megamix:structures/mush_cave/loop_up
execute if predicate structures_crabmaster:randomizers/0_025 positioned ~ ~-1 ~5 run function cm_module_megamix:structures/mush_cave/loop_up
execute if predicate structures_crabmaster:randomizers/0_075 positioned ~ ~1 ~5 run function cm_module_megamix:structures/mush_cave/loop_up