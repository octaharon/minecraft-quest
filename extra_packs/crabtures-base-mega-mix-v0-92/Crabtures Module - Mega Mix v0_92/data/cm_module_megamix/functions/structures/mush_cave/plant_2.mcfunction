function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..499 run function cm_module_megamix:structures/mush_cave/plant_3
execute if score Random RandomOne5556 matches 500..999 run function cm_module_megamix:structures/mush_cave/plant_4

fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace
fill ~ ~-2 ~ ~ ~2 ~ minecraft:mushroom_stem replace
