function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..249 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:large_bog_base",integrity:1.00,posX:-12,posY:-5,posZ:-12,rotation:"NONE"} replace
execute if score Random RandomOne5556 matches 250..499 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:large_bog_base",integrity:1.00,posX:12,posY:-5,posZ:-12,rotation:"CLOCKWISE_90"} replace
execute if score Random RandomOne5556 matches 500..749 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:large_bog_base",integrity:1.00,posX:12,posY:-5,posZ:12,rotation:"CLOCKWISE_180"} replace
execute if score Random RandomOne5556 matches 750..999 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:large_bog_base",integrity:1.00,posX:-12,posY:-5,posZ:12,rotation:"COUNTERCLOCKWISE_90"} replace
fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace

execute as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..16] at @s if predicate structures_crabmaster:randomizers/0_4 run summon minecraft:bee ~ ~ ~
execute as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..16] at @s if predicate structures_crabmaster:randomizers/0_4 run summon minecraft:bee ~ ~ ~ 
execute as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..16] at @s if predicate structures_crabmaster:randomizers/0_4 run summon minecraft:bee ~ ~ ~ 
execute as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..16] at @s if predicate structures_crabmaster:randomizers/0_4 run summon minecraft:bee ~ ~ ~ 
execute as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..16] at @s if predicate structures_crabmaster:randomizers/0_4 run summon minecraft:bee ~ ~ ~ 
execute as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..16] at @s if predicate structures_crabmaster:randomizers/0_4 run summon minecraft:bee ~ ~ ~ 
execute as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..16] at @s if predicate structures_crabmaster:randomizers/0_4 run summon minecraft:bee ~ ~ ~ 

execute as @e[distance=..32,tag=Spawner,tag=C5556] at @s run execute as @e[distance=..1,type=!minecraft:area_effect_cloud,limit=8] at @s run data merge entity @s {CanBreakDoors:1,PersistenceRequired:1}

kill @e[tag=Spawner,tag=C5556,distance=..32]
kill @e[type=minecraft:item,distance=..32]

function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 220..499 run function cm_module_megamix:structures/huge_tree
execute if score Random RandomOne5556 matches 500..624 run function cm_module_megamix:structures/mushroom_house
execute if score Random RandomOne5556 matches 625..749 run function cm_module_megamix:structures/tree_house

execute if score Random RandomOne5556 matches 750..874 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:mega_mush_red",integrity:1.00,posX:-9,posY:-2,posZ:-9,rotation:"NONE"} replace
execute if score Random RandomOne5556 matches 875..999 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:mega_mush_brown",integrity:1.00,posX:-9,posY:-2,posZ:-9,rotation:"NONE"} replace
execute if score Random RandomOne5556 matches 666..999 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:mega_mush_roots",integrity:1.00,posX:-5,posY:-18,posZ:-5,rotation:"NONE"} replace

execute if score Random RandomOne5556 matches 666..999 run fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999