function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..229 run function cm_module_megamix:structures/sky_island/spawn_2
execute if score Random RandomOne5556 matches 230..459 run function cm_module_megamix:structures/sky_island/spawn_3
execute if score Random RandomOne5556 matches 460..689 run function cm_module_megamix:structures/sky_island/spawn_4
execute if score Random RandomOne5556 matches 690..919 run function cm_module_megamix:structures/sky_island/spawn_5

execute if score Random RandomOne5556 matches 0..919 run fill ~ ~-1 ~ ~ ~-1 ~ minecraft:redstone_block replace
execute if score Random RandomOne5556 matches 0..919 run fill ~ ~-1 ~ ~ ~ ~ minecraft:air replace

kill @e[type=minecraft:item,distance=..16]