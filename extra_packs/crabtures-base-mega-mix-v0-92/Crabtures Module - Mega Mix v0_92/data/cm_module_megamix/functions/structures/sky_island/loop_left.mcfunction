forceload add ~-8 ~-8 ~8 ~8

execute positioned ~ ~ ~ if block ~ ~ ~ minecraft:air if block ~ ~3 ~ minecraft:air if block ~ ~6 ~ minecraft:air run function cm_module_megamix:structures/sky_island/spawn_1

#setblock ~ ~ ~ minecraft:red_wool

execute if predicate structures_crabmaster:randomizers/0_25 positioned ~3 ~ ~-4 run function cm_module_megamix:structures/sky_island/loop_left
execute if predicate structures_crabmaster:randomizers/0_25 positioned ~4 ~ ~3 run function cm_module_megamix:structures/sky_island/loop_left
execute if predicate structures_crabmaster:randomizers/0_1 positioned ~-4 ~ ~ run function cm_module_megamix:structures/sky_island/loop_down

execute if predicate structures_crabmaster:randomizers/0_1 positioned ~4 ~-1 ~ run function cm_module_megamix:structures/sky_island/loop_left
execute if predicate structures_crabmaster:randomizers/0_05 positioned ~4 ~1 ~ run function cm_module_megamix:structures/sky_island/loop_left
execute if predicate structures_crabmaster:randomizers/0_1 positioned ~ ~-1 ~-4 run function cm_module_megamix:structures/sky_island/loop_left
execute if predicate structures_crabmaster:randomizers/0_05 positioned ~ ~1 ~-4 run function cm_module_megamix:structures/sky_island/loop_left