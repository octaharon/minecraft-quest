forceload add ~-8 ~-8 ~8 ~8

function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..499 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:space_monolith",integrity:1.00,posX:-4,posY:-3,posZ:0,rotation:"NONE"} replace
execute if score Random RandomOne5556 matches 500..999 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:space_monolith",integrity:1.00,posX:0,posY:-3,posZ:-4,rotation:"CLOCKWISE_90"} replace

fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace

kill @e[type=minecraft:item,distance=..16]

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999