forceload add ~-12 ~-12 ~12 ~12

execute positioned ~ ~3 ~ unless block ~ ~-6 ~ #structures_crabmaster:water_lava unless block ~ ~-3 ~ #structures_crabmaster:water_lava unless block ~ ~ ~ #structures_crabmaster:water_lava unless block ~ ~4 ~ #structures_crabmaster:water_lava unless block ~ ~8 ~ #structures_crabmaster:water_lava unless block ~ ~13 ~ #structures_crabmaster:water_lava run function cm_module_megamix:structures/lava_cave/spawn_1

execute positioned ~4 ~ ~-3 if predicate structures_crabmaster:randomizers/0_75 run function cm_module_megamix:structures/lava_cave/loop_up
execute positioned ~3 ~ ~4 if predicate structures_crabmaster:randomizers/0_75 run function cm_module_megamix:structures/lava_cave/loop_right
execute positioned ~-4 ~ ~3 if predicate structures_crabmaster:randomizers/0_75 run function cm_module_megamix:structures/lava_cave/loop_down
execute positioned ~-3 ~ ~-4 if predicate structures_crabmaster:randomizers/0_75 run function cm_module_megamix:structures/lava_cave/loop_left

kill @e[type=minecraft:item,distance=..16]

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999