forceload add ~-12 ~-12 ~12 ~12

scoreboard players set Loot ExtraLogic5556 0
scoreboard players set Mobs ExtraLogic5556 0

execute unless block ~ ~ ~ #structures_crabmaster:water_lava unless block ~ ~4 ~ #structures_crabmaster:water_lava unless block ~ ~8 ~ #structures_crabmaster:water_lava run fill ~-5 ~-1 ~-5 ~5 ~4 ~5 minecraft:mossy_cobblestone replace #structures_crabmaster:cave
execute unless block ~ ~ ~ #structures_crabmaster:water_lava unless block ~ ~4 ~ #structures_crabmaster:water_lava unless block ~ ~8 ~ #structures_crabmaster:water_lava run fill ~-4 ~ ~-4 ~4 ~3 ~4 minecraft:air replace

function structures_crabmaster:randomizers/randomizer_1

execute positioned ~6 ~ ~-6 if predicate structures_crabmaster:randomizers/0_55 run function cm_module_megamix:structures/larger_dungeon/up
execute positioned ~6 ~ ~6 if predicate structures_crabmaster:randomizers/0_55 run function cm_module_megamix:structures/larger_dungeon/right
execute positioned ~-6 ~ ~6 if predicate structures_crabmaster:randomizers/0_55 run function cm_module_megamix:structures/larger_dungeon/down
execute positioned ~-6 ~ ~-6 if predicate structures_crabmaster:randomizers/0_55 run function cm_module_megamix:structures/larger_dungeon/left

function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..249 if block ~ ~-1 ~4 minecraft:mossy_cobblestone if block ~ ~4 ~4 minecraft:mossy_cobblestone run fill ~ ~ ~4 ~ ~ ~4 minecraft:chest[facing=north]{LootTable:"minecraft:chests/simple_dungeon"} replace
execute if score Random RandomOne5556 matches 250..499 if block ~ ~-1 ~-4 minecraft:mossy_cobblestone if block ~ ~4 ~-4 minecraft:mossy_cobblestone run fill ~ ~ ~-4 ~ ~ ~-4 minecraft:chest[facing=south]{LootTable:"minecraft:chests/simple_dungeon"} replace
execute if score Random RandomOne5556 matches 500..749 if block ~4 ~-1 ~ minecraft:mossy_cobblestone if block ~4 ~4 ~ minecraft:mossy_cobblestone run fill ~4 ~ ~ ~4 ~ ~ minecraft:chest[facing=west]{LootTable:"minecraft:chests/simple_dungeon"} replace
execute if score Random RandomOne5556 matches 750..999 if block ~-4 ~-1 ~ minecraft:mossy_cobblestone if block ~-4 ~4 ~ minecraft:mossy_cobblestone run fill ~-4 ~ ~ ~-4 ~ ~ minecraft:chest[facing=east]{LootTable:"minecraft:chests/simple_dungeon"} replace

function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..450 if block ~ ~-1 ~ minecraft:mossy_cobblestone if block ~ ~4 ~ minecraft:mossy_cobblestone run fill ~ ~ ~ ~ ~ ~ minecraft:spawner{SpawnData:{id:zombie}} replace
execute if score Random RandomOne5556 matches 451..700 if block ~ ~-1 ~ minecraft:mossy_cobblestone if block ~ ~4 ~ minecraft:mossy_cobblestone run fill ~ ~ ~ ~ ~ ~ minecraft:spawner{SpawnData:{id:spider}} replace
execute if score Random RandomOne5556 matches 701..850 if block ~ ~-1 ~ minecraft:mossy_cobblestone if block ~ ~4 ~ minecraft:mossy_cobblestone run fill ~ ~ ~ ~ ~ ~ minecraft:spawner{SpawnData:{id:skeleton}} replace
execute if score Random RandomOne5556 matches 851..920 if block ~ ~-1 ~ minecraft:mossy_cobblestone if block ~ ~4 ~ minecraft:mossy_cobblestone run fill ~ ~ ~ ~ ~ ~ minecraft:spawner{SpawnData:{id:cave_spider}} replace
execute if score Random RandomOne5556 matches 921..999 if block ~ ~-1 ~ minecraft:mossy_cobblestone if block ~ ~4 ~ minecraft:mossy_cobblestone run fill ~ ~ ~ ~ ~ ~ minecraft:spawner{SpawnData:{id:witch}} replace

kill @e[type=minecraft:item,distance=..16]

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999
