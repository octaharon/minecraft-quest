forceload add ~-12 ~-12 ~12 ~12

execute unless block ~ ~ ~ #structures_crabmaster:water_lava unless block ~ ~4 ~ #structures_crabmaster:water_lava unless block ~ ~8 ~ #structures_crabmaster:water_lava run fill ~-4 ~-1 ~-4 ~4 ~4 ~4 minecraft:mossy_cobblestone replace #structures_crabmaster:cave
execute unless block ~ ~ ~ #structures_crabmaster:water_lava unless block ~ ~4 ~ #structures_crabmaster:water_lava unless block ~ ~8 ~ #structures_crabmaster:water_lava run fill ~-3 ~ ~-3 ~3 ~3 ~3 minecraft:air replace

function structures_crabmaster:randomizers/randomizer_1

execute positioned ~6 ~ ~6 if predicate structures_crabmaster:randomizers/0_25 run function cm_module_megamix:structures/larger_dungeon/up
execute positioned ~ ~ ~6 if predicate structures_crabmaster:randomizers/0_1 run function cm_module_megamix:structures/larger_dungeon/right
execute positioned ~-6 ~ ~6 if predicate structures_crabmaster:randomizers/0_4 run function cm_module_megamix:structures/larger_dungeon/right

function structures_crabmaster:randomizers/randomizer_1
scoreboard players operation Loot ExtraLogic5556 += Random RandomOne5556
function structures_crabmaster:randomizers/randomizer_1
scoreboard players operation Mobs ExtraLogic5556 += Random RandomOne5556

function structures_crabmaster:randomizers/randomizer_1

execute if score Loot ExtraLogic5556 matches 1000..1800 if score Random RandomOne5556 matches 0..333 if block ~ ~-1 ~4 minecraft:mossy_cobblestone if block ~ ~4 ~3 minecraft:mossy_cobblestone run fill ~ ~ ~3 ~ ~ ~3 minecraft:chest[facing=north]{LootTable:"minecraft:chests/simple_dungeon"} replace
execute if score Loot ExtraLogic5556 matches 1000..1800 if score Random RandomOne5556 matches 334..666 if block ~4 ~-1 ~ minecraft:mossy_cobblestone if block ~3 ~4 ~ minecraft:mossy_cobblestone run fill ~3 ~ ~ ~3 ~ ~ minecraft:chest[facing=west]{LootTable:"minecraft:chests/simple_dungeon"} replace
execute if score Loot ExtraLogic5556 matches 1000..1800 if score Random RandomOne5556 matches 667..999 if block ~-4 ~-1 ~ minecraft:mossy_cobblestone if block ~-3 ~4 ~ minecraft:mossy_cobblestone run fill ~-3 ~ ~ ~-3 ~ ~ minecraft:chest[facing=east]{LootTable:"minecraft:chests/simple_dungeon"} replace

function structures_crabmaster:randomizers/randomizer_1

execute if score Mobs ExtraLogic5556 matches 1000..1800 if score Random RandomOne5556 matches 0..333 if block ~ ~-1 ~ minecraft:mossy_cobblestone if block ~ ~4 ~ minecraft:mossy_cobblestone run fill ~ ~ ~ ~ ~ ~ minecraft:spawner{SpawnData:{id:zombie}} replace
execute if score Mobs ExtraLogic5556 matches 1000..1800 if score Random RandomOne5556 matches 334..666 if block ~ ~-1 ~ minecraft:mossy_cobblestone if block ~ ~4 ~ minecraft:mossy_cobblestone run fill ~ ~ ~ ~ ~ ~ minecraft:spawner{SpawnData:{id:spider}} replace
execute if score Mobs ExtraLogic5556 matches 1000..1800 if score Random RandomOne5556 matches 667..999 if block ~ ~-1 ~ minecraft:mossy_cobblestone if block ~ ~4 ~ minecraft:mossy_cobblestone run fill ~ ~ ~ ~ ~ ~ minecraft:spawner{SpawnData:{id:skeleton}} replace

execute if score Loot ExtraLogic5556 matches 1000.. run scoreboard players set Loot ExtraLogic5556 0
execute if score Mobs ExtraLogic5556 matches 1000.. run scoreboard players set Mobs ExtraLogic5556 0

kill @e[type=minecraft:item,distance=..16]

scoreboard players set Random RandomOne5556 99999
