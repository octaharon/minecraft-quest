execute positioned ~ ~14 ~ run function cm_module_megamix:structures/big_parts/huge_tree_branch_1
execute positioned ~ ~12 ~ run function cm_module_megamix:structures/big_parts/huge_tree_branch_2
execute positioned ~ ~13 ~ run function cm_module_megamix:structures/big_parts/huge_tree_branch_3
execute positioned ~ ~14 ~ run function cm_module_megamix:structures/big_parts/huge_tree_branch_4

function structures_crabmaster:randomizers/randomizer_2

execute positioned ~ ~16 ~ if score Random RandomTwo5556 matches 0..499 run function cm_module_megamix:structures/big_parts/huge_tree_branch_5
execute positioned ~ ~16 ~ if score Random RandomTwo5556 matches 500..999 run function cm_module_megamix:structures/big_parts/huge_tree_branch_6

execute positioned ~8 ~15 ~7 unless block ~ ~ ~ minecraft:air if predicate structures_crabmaster:randomizers/0_4 run function cm_module_megamix:structures/small_parts/beehive
execute positioned ~7 ~14 ~-7 unless block ~ ~ ~ minecraft:air if predicate structures_crabmaster:randomizers/0_4 run function cm_module_megamix:structures/small_parts/beehive
execute positioned ~-7 ~16 ~-6 unless block ~ ~ ~ minecraft:air if predicate structures_crabmaster:randomizers/0_4 run function cm_module_megamix:structures/small_parts/beehive
execute positioned ~-6 ~15 ~6 unless block ~ ~ ~ minecraft:air if predicate structures_crabmaster:randomizers/0_4 run function cm_module_megamix:structures/small_parts/beehive

function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..249 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:mega_tree",integrity:1.00,posX:-9,posY:-11,posZ:-9,rotation:"NONE"} replace
execute if score Random RandomOne5556 matches 250..499 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:mega_tree",integrity:1.00,posX:9,posY:-11,posZ:-9,rotation:"CLOCKWISE_90"} replace
execute if score Random RandomOne5556 matches 500..749 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:mega_tree",integrity:1.00,posX:9,posY:-11,posZ:9,rotation:"CLOCKWISE_180"} replace
execute if score Random RandomOne5556 matches 750..999 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:mega_tree",integrity:1.00,posX:-9,posY:-11,posZ:9,rotation:"COUNTERCLOCKWISE_90"} replace
fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace


kill @e[type=minecraft:item,distance=..16]

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999