forceload add ~-4 ~-4 ~4 ~4

execute positioned ~ ~ ~ unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~ ~ #structures_crabmaster:water_lava unless block ~ ~1 ~ #structures_crabmaster:water_lava run function cm_module_megamix:structures/oasis/bush_patch_3

execute positioned ~3 ~6 ~2 if predicate structures_crabmaster:randomizers/0_2 run function cm_module_megamix:structures/oasis/bush_patch_1
execute positioned ~-2 ~6 ~3 if predicate structures_crabmaster:randomizers/0_2 run function cm_module_megamix:structures/oasis/bush_patch_1
execute positioned ~-3 ~6 ~-2 if predicate structures_crabmaster:randomizers/0_2 run function cm_module_megamix:structures/oasis/bush_patch_1
execute positioned ~2 ~6 ~-3 if predicate structures_crabmaster:randomizers/0_2 run function cm_module_megamix:structures/oasis/bush_patch_1

kill @e[type=minecraft:item,distance=..16]

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999