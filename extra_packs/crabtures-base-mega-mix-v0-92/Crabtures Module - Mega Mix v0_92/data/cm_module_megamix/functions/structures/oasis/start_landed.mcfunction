execute positioned ~ ~ ~ unless block ~ ~-2 ~ #structures_crabmaster:water_lava unless block ~ ~ ~ #structures_crabmaster:water_lava unless block ~ ~2 ~ #structures_crabmaster:water_lava run function cm_module_megamix:structures/oasis/spawn_1

execute positioned ~4 ~ ~ unless block ~ ~-1 ~ #structures_crabmaster:extra_landing run function cm_module_megamix:structures/oasis/loop_up
execute positioned ~-4 ~ ~ unless block ~ ~-1 ~ #structures_crabmaster:extra_landing run function cm_module_megamix:structures/oasis/loop_down
execute positioned ~ ~ ~4 unless block ~ ~-1 ~ #structures_crabmaster:extra_landing run function cm_module_megamix:structures/oasis/loop_right
execute positioned ~ ~ ~-4 unless block ~ ~-1 ~ #structures_crabmaster:extra_landing run function cm_module_megamix:structures/oasis/loop_left

execute if predicate structures_crabmaster:randomizers/0_95 positioned ~-9 ~ ~-8 run function cm_module_megamix:structures/palm_forest/land_up
execute if predicate structures_crabmaster:randomizers/0_95 positioned ~8 ~ ~9 run function cm_module_megamix:structures/palm_forest/land_down
execute if predicate structures_crabmaster:randomizers/0_95 positioned ~9 ~ ~-8 run function cm_module_megamix:structures/palm_forest/land_right
execute if predicate structures_crabmaster:randomizers/0_95 positioned ~-8 ~ ~9 run function cm_module_megamix:structures/palm_forest/land_left

execute if predicate structures_crabmaster:randomizers/0_5 positioned ~-16 ~ ~-11 run function cm_module_megamix:structures/palm_forest/land_up
execute if predicate structures_crabmaster:randomizers/0_5 positioned ~11 ~ ~16 run function cm_module_megamix:structures/palm_forest/land_down
execute if predicate structures_crabmaster:randomizers/0_5 positioned ~16 ~ ~-11 run function cm_module_megamix:structures/palm_forest/land_right
execute if predicate structures_crabmaster:randomizers/0_5 positioned ~-11 ~ ~16 run function cm_module_megamix:structures/palm_forest/land_left

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999
scoreboard players set Landed ExtraLogic5556 1