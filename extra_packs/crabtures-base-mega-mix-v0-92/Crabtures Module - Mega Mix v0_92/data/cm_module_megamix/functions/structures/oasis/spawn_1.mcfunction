function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..124 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:lake_1",integrity:1.0,posX:-3,posY:-4,posZ:-3,rotation:"NONE"} replace
execute if score Random RandomOne5556 matches 125..249 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:lake_2",integrity:1.0,posX:-3,posY:-4,posZ:-3,rotation:"NONE"} replace
execute if score Random RandomOne5556 matches 250..374 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:lake_3",integrity:1.0,posX:-3,posY:-4,posZ:-3,rotation:"NONE"} replace
execute if score Random RandomOne5556 matches 375..499 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:lake_4",integrity:1.0,posX:-3,posY:-4,posZ:-3,rotation:"NONE"} replace
execute if score Random RandomOne5556 matches 500..624 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:lake_5",integrity:1.0,posX:-3,posY:-4,posZ:-3,rotation:"NONE"} replace
execute if score Random RandomOne5556 matches 625..749 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:lake_6",integrity:1.0,posX:-3,posY:-4,posZ:-3,rotation:"NONE"} replace
execute if score Random RandomOne5556 matches 750..874 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:lake_7",integrity:1.0,posX:-3,posY:-4,posZ:-3,rotation:"NONE"} replace
execute if score Random RandomOne5556 matches 875..999 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:lake_8",integrity:1.0,posX:-3,posY:-4,posZ:-3,rotation:"NONE"} replace

fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace
fill ~ ~ ~ ~ ~1 ~ minecraft:air replace