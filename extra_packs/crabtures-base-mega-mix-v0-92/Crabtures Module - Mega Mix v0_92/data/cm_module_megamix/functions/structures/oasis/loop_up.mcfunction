forceload add ~-8 ~-8 ~8 ~8

scoreboard players set OasisEdge ExtraLogic5556 0
execute store success score OasisEdge ExtraLogic5556 positioned ~ ~ ~ unless block ~ ~-2 ~ #structures_crabmaster:water_lava unless block ~ ~ ~ #structures_crabmaster:water_lava unless block ~ ~2 ~ #structures_crabmaster:water_lava run function cm_module_megamix:structures/oasis/spawn_1
execute if score OasisEdge ExtraLogic5556 matches 0 positioned ~ ~6 ~ run function cm_module_megamix:structures/oasis/bush_patch_1

#setblock ~ ~ ~ minecraft:yellow_wool

execute store success score OasisEdge ExtraLogic5556 if predicate structures_crabmaster:randomizers/0_35 positioned ~5 ~ ~-1 unless block ~ ~-1 ~ #structures_crabmaster:extra_landing run function cm_module_megamix:structures/oasis/loop_up
execute if score OasisEdge ExtraLogic5556 matches 0 positioned ~7 ~6 ~-2 run function cm_module_megamix:structures/oasis/bush_patch_1
execute store success score OasisEdge ExtraLogic5556 if predicate structures_crabmaster:randomizers/0_35 positioned ~-2 ~ ~5 unless block ~ ~-1 ~ #structures_crabmaster:extra_landing run function cm_module_megamix:structures/oasis/loop_up
execute if score OasisEdge ExtraLogic5556 matches 0 positioned ~-3 ~6 ~7 run function cm_module_megamix:structures/oasis/bush_patch_1