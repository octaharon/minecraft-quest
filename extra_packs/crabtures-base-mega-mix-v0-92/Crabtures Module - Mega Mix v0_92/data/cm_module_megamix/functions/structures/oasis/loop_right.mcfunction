forceload add ~-8 ~-8 ~8 ~8

scoreboard players set OasisEdge ExtraLogic5556 0
execute store success score OasisEdge ExtraLogic5556 positioned ~ ~ ~ unless block ~ ~-2 ~ #structures_crabmaster:water_lava unless block ~ ~ ~ #structures_crabmaster:water_lava unless block ~ ~2 ~ #structures_crabmaster:water_lava run function cm_module_megamix:structures/oasis/spawn_1
execute if score OasisEdge ExtraLogic5556 matches 0 positioned ~ ~6 ~ run function cm_module_megamix:structures/oasis/bush_patch_1

#setblock ~ ~ ~ minecraft:blue_wool

execute store success score OasisEdge ExtraLogic5556 if predicate structures_crabmaster:randomizers/0_35 positioned ~1 ~ ~5 unless block ~ ~-1 ~ #structures_crabmaster:extra_landing run function cm_module_megamix:structures/oasis/loop_right
execute if score OasisEdge ExtraLogic5556 matches 0 positioned ~2 ~6 ~7 run function cm_module_megamix:structures/oasis/bush_patch_1
execute store success score OasisEdge ExtraLogic5556 if predicate structures_crabmaster:randomizers/0_35 positioned ~-5 ~ ~-2 unless block ~ ~-1 ~ #structures_crabmaster:extra_landing run function cm_module_megamix:structures/oasis/loop_right
execute if score OasisEdge ExtraLogic5556 matches 0 positioned ~-7 ~6 ~-3 run function cm_module_megamix:structures/oasis/bush_patch_1