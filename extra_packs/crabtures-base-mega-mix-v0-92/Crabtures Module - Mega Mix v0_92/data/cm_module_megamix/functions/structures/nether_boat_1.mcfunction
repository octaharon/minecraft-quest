function structures_crabmaster:randomizers/randomizer_1

execute as @e[distance=..24,limit=1,sort=random,tag=Chest,tag=C5556,tag=Set1] at @s if score Random RandomOne5556 matches 1.. run fill ~ ~ ~ ~ ~ ~ minecraft:chest{LootTable:"minecraft:chests/nether_bridge"} replace
execute as @e[distance=..24,limit=1,sort=random,tag=Chest,tag=C5556,tag=Set2] at @s if score Random RandomOne5556 matches 550.. run fill ~ ~ ~ ~ ~ ~ minecraft:chest{LootTable:"minecraft:chests/nether_bridge"} replace
execute as @e[distance=..24,limit=1,sort=random,tag=Chest,tag=C5556,tag=Set3] at @s if score Random RandomOne5556 matches 825.. run fill ~ ~ ~ ~ ~ ~ minecraft:chest{LootTable:"minecraft:chests/nether_bridge"} replace
execute as @e[distance=..24,limit=1,sort=random,tag=Chest,tag=C5556,tag=Set4] at @s if score Random RandomOne5556 matches 962.. run fill ~ ~ ~ ~ ~ ~ minecraft:chest{LootTable:"minecraft:chests/nether_bridge"} replace

execute as @e[distance=..24,limit=1,sort=random,tag=Spawner,tag=C5556] at @s if score Random RandomOne5556 matches 1.. run summon minecraft:wither_skeleton ~ ~ ~
execute as @e[distance=..24,limit=1,sort=random,tag=Spawner,tag=C5556] at @s if score Random RandomOne5556 matches 300.. run summon minecraft:wither_skeleton ~ ~ ~
execute as @e[distance=..24,limit=1,sort=random,tag=Spawner,tag=C5556] at @s if score Random RandomOne5556 matches 600.. run summon minecraft:wither_skeleton ~ ~ ~
execute as @e[distance=..24,limit=1,sort=random,tag=Spawner,tag=C5556] at @s if score Random RandomOne5556 matches 800.. run summon minecraft:wither_skeleton ~ ~ ~
execute as @e[distance=..24,limit=1,sort=random,tag=Spawner,tag=C5556] at @s if score Random RandomOne5556 matches 900.. run summon minecraft:wither_skeleton ~ ~ ~


execute as @e[distance=..24,limit=1,sort=random,tag=Spawner,tag=C5556] at @s if score Random RandomOne5556 matches 200..300 run summon minecraft:blaze ~ ~ ~
execute as @e[distance=..24,limit=1,sort=random,tag=Spawner,tag=C5556] at @s if score Random RandomOne5556 matches 250..400 run summon minecraft:blaze ~ ~ ~
execute as @e[distance=..24,limit=1,sort=random,tag=Spawner,tag=C5556] at @s if score Random RandomOne5556 matches 350..550 run summon minecraft:blaze ~ ~ ~
execute as @e[distance=..24,limit=1,sort=random,tag=Spawner,tag=C5556] at @s if score Random RandomOne5556 matches 500..700 run summon minecraft:blaze ~ ~ ~

execute as @e[distance=..32,tag=Spawner,tag=C5556] at @s run execute as @e[distance=..1,type=!minecraft:area_effect_cloud,limit=8] at @s run data merge entity @s {CanBreakDoors:1,PersistenceRequired:1}
