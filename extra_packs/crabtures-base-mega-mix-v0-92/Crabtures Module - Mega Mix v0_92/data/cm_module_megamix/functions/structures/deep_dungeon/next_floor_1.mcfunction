fill ~-2 ~ ~-2 ~2 ~-8 ~2 minecraft:mossy_cobblestone replace #structures_crabmaster:cave
fill ~-1 ~ ~-1 ~1 ~-8 ~1 minecraft:air replace

fill ~-5 ~-9 ~-5 ~5 ~-4 ~5 minecraft:mossy_cobblestone replace #structures_crabmaster:cave

function structures_crabmaster:randomizers/randomizer_1

execute positioned ~6 ~-8 ~-6 if score Random RandomOne5556 matches 0..249 run function cm_module_megamix:structures/deep_dungeon/up_left
execute positioned ~6 ~-8 ~6 if score Random RandomOne5556 matches 250..499 run function cm_module_megamix:structures/deep_dungeon/up_right
execute positioned ~-6 ~-8 ~6 if score Random RandomOne5556 matches 500..749 run function cm_module_megamix:structures/deep_dungeon/down_right
execute positioned ~-6 ~-8 ~-6 if score Random RandomOne5556 matches 750..999 run function cm_module_megamix:structures/deep_dungeon/down_left


fill ~-4 ~-8 ~-4 ~4 ~-5 ~4 minecraft:air replace

execute unless block ~ ~-9 ~ #structures_crabmaster:air_water run fill ~ ~-1 ~-2 ~ ~-8 ~-2 minecraft:mossy_cobblestone replace minecraft:air
execute unless block ~ ~-9 ~ #structures_crabmaster:air_water run fill ~ ~-1 ~-1 ~ ~-8 ~-1 minecraft:ladder[facing=south] replace minecraft:air