forceload add ~-12 ~-12 ~12 ~12

execute unless block ~ ~ ~ #structures_crabmaster:water_lava unless block ~ ~4 ~ #structures_crabmaster:water_lava unless block ~ ~8 ~ #structures_crabmaster:water_lava run fill ~-4 ~-1 ~-4 ~4 ~4 ~4 minecraft:mossy_cobblestone replace #structures_crabmaster:cave
execute unless block ~ ~ ~ #structures_crabmaster:water_lava unless block ~ ~4 ~ #structures_crabmaster:water_lava unless block ~ ~8 ~ #structures_crabmaster:water_lava run fill ~-3 ~ ~-3 ~3 ~3 ~3 minecraft:air replace


function structures_crabmaster:randomizers/randomizer_1
execute store result score Difficulty ExtraLogic5556 run scoreboard players operation Random RandomOne5556 += DungeonDepth ExtraLogic5556

execute if score Random RandomOne5556 matches 1200..1599 if block ~ ~-1 ~-4 minecraft:mossy_cobblestone if block ~ ~4 ~-3 minecraft:mossy_cobblestone run fill ~ ~ ~-3 ~ ~ ~-3 minecraft:chest[facing=south]{LootTable:"minecraft:chests/simple_dungeon"} replace
execute if score Random RandomOne5556 matches 1600..2200 if block ~-4 ~-1 ~ minecraft:mossy_cobblestone if block ~-3 ~4 ~ minecraft:mossy_cobblestone run fill ~-3 ~ ~ ~-3 ~ ~ minecraft:chest[facing=east]{LootTable:"minecraft:chests/simple_dungeon"} replace

function structures_crabmaster:randomizers/randomizer_1
execute store result score Difficulty ExtraLogic5556 run scoreboard players operation Random RandomOne5556 += DungeonDepth ExtraLogic5556

execute if score Random RandomOne5556 matches 1000..1199 if block ~ ~-1 ~ minecraft:mossy_cobblestone if block ~ ~4 ~ minecraft:mossy_cobblestone run fill ~ ~ ~ ~ ~ ~ minecraft:spawner{SpawnData:{id:spider}} replace
execute if score Random RandomOne5556 matches 1200..1699 if block ~ ~-1 ~ minecraft:mossy_cobblestone if block ~ ~4 ~ minecraft:mossy_cobblestone run fill ~ ~ ~ ~ ~ ~ minecraft:spawner{SpawnData:{id:zombie}} replace
execute if score Random RandomOne5556 matches 1700..1899 if block ~ ~-1 ~ minecraft:mossy_cobblestone if block ~ ~4 ~ minecraft:mossy_cobblestone run fill ~ ~ ~ ~ ~ ~ minecraft:spawner{SpawnData:{id:skeleton}} replace
execute if score Random RandomOne5556 matches 1900..2200 if block ~ ~-1 ~ minecraft:mossy_cobblestone if block ~ ~4 ~ minecraft:mossy_cobblestone run fill ~ ~ ~ ~ ~ ~ minecraft:spawner{SpawnData:{id:cave_spider}} replace


function structures_crabmaster:randomizers/randomizer_1

execute positioned ~6 ~ ~-6 if score Random RandomOne5556 matches 0..124 run function cm_module_megamix:structures/deep_dungeon/up_left
execute positioned ~-6 ~ ~6 if score Random RandomOne5556 matches 125..249 run function cm_module_megamix:structures/deep_dungeon/down_right
execute positioned ~-6 ~ ~ if score Random RandomOne5556 matches 500..624 run function cm_module_megamix:structures/deep_dungeon/down
execute positioned ~-6 ~ ~-6 if score Random RandomOne5556 matches 250..499 run function cm_module_megamix:structures/deep_dungeon/down_left
execute positioned ~ ~ ~-6 if score Random RandomOne5556 matches 625..749 run function cm_module_megamix:structures/deep_dungeon/left
execute positioned ~ ~ ~ if score Random RandomOne5556 matches 750..999 run function cm_module_megamix:structures/deep_dungeon/next_floor

kill @e[type=minecraft:item,distance=..16]

scoreboard players set Random RandomOne5556 99999
