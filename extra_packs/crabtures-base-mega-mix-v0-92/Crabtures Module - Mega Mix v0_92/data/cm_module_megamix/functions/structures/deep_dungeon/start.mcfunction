scoreboard players set DungeonDepth ExtraLogic5556 0

function structures_crabmaster:randomizers/randomizer_1

execute store result score DungeonDepth ExtraLogic5556 run scoreboard players operation Random RandomOne5556 /= Two Maths5556

fill ~-2 ~ ~-2 ~2 ~-8 ~2 minecraft:mossy_cobblestone replace #structures_crabmaster:cave
fill ~-3 ~4 ~-3 ~3 ~4 ~3 minecraft:mossy_cobblestone
fill ~-1 ~5 ~-1 ~1 ~5 ~1 minecraft:mossy_cobblestone
fill ~-2 ~-8 ~-2 ~-2 ~3 ~-2 minecraft:mossy_cobblestone
fill ~2 ~-8 ~2 ~2 ~3 ~2 minecraft:mossy_cobblestone
fill ~2 ~-8 ~-2 ~2 ~3 ~-2 minecraft:mossy_cobblestone
fill ~-2 ~-8 ~2 ~-2 ~3 ~2 minecraft:mossy_cobblestone
fill ~-1 ~ ~-1 ~1 ~-8 ~1 minecraft:air replace

fill ~-5 ~-13 ~-5 ~5 ~-8 ~5 minecraft:mossy_cobblestone replace #structures_crabmaster:cave

function structures_crabmaster:randomizers/randomizer_1

execute positioned ~6 ~-12 ~-6 if score Random RandomOne5556 matches 0..249 run function cm_module_megamix:structures/deep_dungeon/up_left
execute positioned ~6 ~-12 ~6 if score Random RandomOne5556 matches 250..499 run function cm_module_megamix:structures/deep_dungeon/up_right
execute positioned ~-6 ~-12 ~6 if score Random RandomOne5556 matches 500..749 run function cm_module_megamix:structures/deep_dungeon/down_right
execute positioned ~-6 ~-12 ~-6 if score Random RandomOne5556 matches 750..999 run function cm_module_megamix:structures/deep_dungeon/down_left


fill ~-4 ~-12 ~-4 ~4 ~-9 ~4 minecraft:air replace

fill ~ ~-1 ~-2 ~ ~-12 ~-2 minecraft:mossy_cobblestone replace minecraft:air
fill ~ ~-1 ~-1 ~ ~-12 ~-1 minecraft:ladder[facing=south] replace minecraft:air

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999