forceload add ~-12 ~-12 ~12 ~12

execute unless block ~ ~ ~ #structures_crabmaster:water_lava unless block ~ ~4 ~ #structures_crabmaster:water_lava unless block ~ ~8 ~ #structures_crabmaster:water_lava run fill ~-7 ~-1 ~-7 ~7 ~5 ~7 minecraft:mossy_cobblestone replace #structures_crabmaster:cave
execute unless block ~ ~ ~ #structures_crabmaster:water_lava unless block ~ ~4 ~ #structures_crabmaster:water_lava unless block ~ ~8 ~ #structures_crabmaster:water_lava run fill ~-6 ~ ~-6 ~6 ~4 ~6 minecraft:air replace

function structures_crabmaster:randomizers/randomizer_1
execute store result score Difficulty ExtraLogic5556 run scoreboard players operation Random RandomOne5556 += DungeonDepth ExtraLogic5556

execute if score Random RandomOne5556 matches 1000..2200 if block ~ ~-1 ~6 minecraft:mossy_cobblestone if block ~ ~5 ~6 minecraft:mossy_cobblestone run fill ~ ~ ~6 ~ ~ ~6 minecraft:chest[facing=north]{LootTable:"minecraft:chests/simple_dungeon"} replace
execute if score Random RandomOne5556 matches 1300..2200 if block ~6 ~-1 ~ minecraft:mossy_cobblestone if block ~6 ~5 ~ minecraft:mossy_cobblestone run fill ~6 ~ ~ ~6 ~ ~ minecraft:chest[facing=west]{LootTable:"minecraft:chests/simple_dungeon"} replace
execute if score Random RandomOne5556 matches 1800..2200 if block ~-6 ~-1 ~ minecraft:mossy_cobblestone if block ~-6 ~5 ~ minecraft:mossy_cobblestone run fill ~-6 ~ ~ ~-6 ~ ~ minecraft:chest[facing=east]{LootTable:"minecraft:chests/simple_dungeon"} replace


execute if score Random RandomOne5556 matches 900..2200 if block ~ ~-1 ~4 minecraft:mossy_cobblestone if block ~ ~5 ~4 minecraft:mossy_cobblestone run fill ~ ~ ~4 ~ ~ ~4 minecraft:spawner{SpawnData:{id:zombie}} replace
execute if score Random RandomOne5556 matches 1100..2200 if block ~4 ~-1 ~ minecraft:mossy_cobblestone if block ~4 ~5 ~ minecraft:mossy_cobblestone run fill ~4 ~ ~ ~4 ~ ~ minecraft:spawner{SpawnData:{id:spider}} replace
execute if score Random RandomOne5556 matches 1300..2200 if block ~4 ~-1 ~ minecraft:mossy_cobblestone if block ~4 ~5 ~4 minecraft:mossy_cobblestone run fill ~4 ~ ~4 ~4 ~ ~4 minecraft:spawner{SpawnData:{id:skeleton}} replace
execute if score Random RandomOne5556 matches 1800..2200 if block ~-4 ~-1 ~-4 minecraft:mossy_cobblestone if block ~-4 ~5 ~-4 minecraft:mossy_cobblestone run fill ~-4 ~ ~-4 ~-4 ~ ~-4 minecraft:spawner{SpawnData:{id:witch}} replace
execute if score Random RandomOne5556 matches 1500..2200 if block ~4 ~-1 ~-4 minecraft:mossy_cobblestone if block ~4 ~5 ~-4 minecraft:mossy_cobblestone run fill ~4 ~ ~-4 ~4 ~ ~-4 minecraft:spawner{SpawnData:{id:cave_spider}} replace

kill @e[type=minecraft:item,distance=..16]

scoreboard players set Random RandomOne5556 99999
scoreboard players set DungeonDepth ExtraLogic5556 99999
