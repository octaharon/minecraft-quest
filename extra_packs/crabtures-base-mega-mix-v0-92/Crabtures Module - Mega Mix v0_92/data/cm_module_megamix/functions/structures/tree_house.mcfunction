execute positioned ~7 ~ ~ if predicate structures_crabmaster:randomizers/0_15 run function cm_module_megamix:structures/small_parts/crops_patch_1
execute positioned ~-5 ~ ~-8 if predicate structures_crabmaster:randomizers/0_15 run function cm_module_megamix:structures/small_parts/crops_patch_1
execute positioned ~-7 ~ ~7 if predicate structures_crabmaster:randomizers/0_15 run function cm_module_megamix:structures/small_parts/crops_patch_1

execute as @e[tag=CrowSpot,tag=C5556,distance=..64] at @s run function cm_module_megamix:structures/small_parts/scarecrow_pick

execute if predicate structures_crabmaster:randomizers/0_2 run function cm_module_megamix:structures/small_parts/campfire_1
execute if predicate structures_crabmaster:randomizers/0_5 run function cm_module_megamix:structures/small_parts/outhouse_1
execute if predicate structures_crabmaster:randomizers/0_1 run function cm_module_megamix:structures/small_parts/camp_bench_1
execute if predicate structures_crabmaster:randomizers/0_15 run function cm_module_megamix:structures/small_parts/camp_table_1
execute if predicate structures_crabmaster:randomizers/0_1 run function cm_module_megamix:structures/small_parts/farm_hay_1
execute if predicate structures_crabmaster:randomizers/0_2 run function cm_module_megamix:structures/small_parts/farm_leanto_1
execute if predicate structures_crabmaster:randomizers/0_3 run function cm_module_megamix:structures/small_parts/farm_logs_1
execute if predicate structures_crabmaster:randomizers/0_05 run function cm_module_megamix:structures/small_parts/farm_well_1

execute positioned ~ ~14 ~ run function cm_module_megamix:structures/big_parts/huge_tree_branch_1
execute positioned ~ ~12 ~ run function cm_module_megamix:structures/big_parts/huge_tree_branch_2
execute positioned ~ ~13 ~ run function cm_module_megamix:structures/big_parts/huge_tree_branch_3
execute positioned ~ ~14 ~ run function cm_module_megamix:structures/big_parts/huge_tree_branch_4

function structures_crabmaster:randomizers/randomizer_2

execute positioned ~ ~16 ~ if score Random RandomTwo5556 matches 0..499 run function cm_module_megamix:structures/big_parts/huge_tree_branch_5
execute positioned ~ ~16 ~ if score Random RandomTwo5556 matches 500..999 run function cm_module_megamix:structures/big_parts/huge_tree_branch_6

execute positioned ~8 ~15 ~7 unless block ~ ~ ~ minecraft:air if predicate structures_crabmaster:randomizers/0_35 run function cm_module_megamix:structures/small_parts/beehive
execute positioned ~7 ~14 ~-7 unless block ~ ~ ~ minecraft:air if predicate structures_crabmaster:randomizers/0_35 run function cm_module_megamix:structures/small_parts/beehive
execute positioned ~-7 ~16 ~-6 unless block ~ ~ ~ minecraft:air if predicate structures_crabmaster:randomizers/0_35 run function cm_module_megamix:structures/small_parts/beehive
execute positioned ~-6 ~15 ~6 unless block ~ ~ ~ minecraft:air if predicate structures_crabmaster:randomizers/0_35 run function cm_module_megamix:structures/small_parts/beehive

function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..249 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:mega_tree_house",integrity:1.00,posX:-9,posY:-10,posZ:-9,rotation:"NONE"} replace
execute if score Random RandomOne5556 matches 250..499 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:mega_tree_house",integrity:1.00,posX:9,posY:-10,posZ:-9,rotation:"CLOCKWISE_90"} replace
execute if score Random RandomOne5556 matches 500..749 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:mega_tree_house",integrity:1.00,posX:9,posY:-10,posZ:9,rotation:"CLOCKWISE_180"} replace
execute if score Random RandomOne5556 matches 750..999 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:mega_tree_house",integrity:1.00,posX:-9,posY:-10,posZ:9,rotation:"COUNTERCLOCKWISE_90"} replace
fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace


function structures_crabmaster:randomizers/randomizer_2

execute if score Random RandomTwo5556 matches 0..750 as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..16] at @s run summon minecraft:villager
execute if score Random RandomTwo5556 matches 0..750 as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..16] at @s if predicate structures_crabmaster:randomizers/0_9 run summon minecraft:villager ~ ~ ~
execute if score Random RandomTwo5556 matches 0..750 as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..16] at @s if predicate structures_crabmaster:randomizers/0_6 run summon minecraft:villager ~ ~ ~ {Age:-6000}
execute if score Random RandomTwo5556 matches 0..750 as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..16] at @s if predicate structures_crabmaster:randomizers/0_4 run summon minecraft:villager ~ ~ ~ {Age:-12000}
execute if score Random RandomTwo5556 matches 0..750 as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..16] at @s if predicate structures_crabmaster:randomizers/0_2 run summon minecraft:villager ~ ~ ~ {Age:-18000}

execute if score Random RandomTwo5556 matches 751..900 as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..16] at @s run summon minecraft:witch
execute if score Random RandomTwo5556 matches 751..900 as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..16] at @s if predicate structures_crabmaster:randomizers/0_5 run summon minecraft:witch
execute if score Random RandomTwo5556 matches 751..900 as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..16] at @s if predicate structures_crabmaster:randomizers/0_2 run summon minecraft:witch

execute if score Random RandomTwo5556 matches 901..999 as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..16] at @s run summon minecraft:vindicator
execute if score Random RandomTwo5556 matches 901..999 as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..16] at @s if predicate structures_crabmaster:randomizers/0_8 run summon minecraft:pillager
execute if score Random RandomTwo5556 matches 901..999 as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..16] at @s if predicate structures_crabmaster:randomizers/0_8 run summon minecraft:pillager
execute if score Random RandomTwo5556 matches 901..999 as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..16] at @s if predicate structures_crabmaster:randomizers/0_6 run summon minecraft:pillager
execute if score Random RandomTwo5556 matches 901..999 as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..16] at @s if predicate structures_crabmaster:randomizers/0_4 run summon minecraft:pillager
execute if score Random RandomTwo5556 matches 901..999 as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..16] at @s if predicate structures_crabmaster:randomizers/0_2 run summon minecraft:vindicator

execute as @e[distance=..32,tag=Spawner,tag=C5556] at @s run execute as @e[distance=..1,type=!minecraft:area_effect_cloud,limit=8] at @s run data merge entity @s {CanBreakDoors:1,PersistenceRequired:1}

kill @e[tag=Spawner,tag=C5556,distance=..16]
kill @e[type=minecraft:item,distance=..16]

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999