function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..224 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:giant_mush_brown",integrity:1.00,posX:-3,posY:-2,posZ:-3,rotation:"NONE"} replace
execute if score Random RandomOne5556 matches 225..449 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:giant_mush_red",integrity:1.00,posX:-3,posY:-2,posZ:-3,rotation:"NONE"} replace
execute if score Random RandomOne5556 matches 450..674 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:huge_mush_red",integrity:1.00,posX:-5,posY:-2,posZ:-5,rotation:"NONE"} replace
execute if score Random RandomOne5556 matches 675..899 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:huge_mush_brown",integrity:1.00,posX:-5,posY:-2,posZ:-5,rotation:"NONE"} replace

execute if score Random RandomOne5556 matches 900..949 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:mega_mush_red",integrity:1.00,posX:-9,posY:-2,posZ:-9,rotation:"NONE"} replace
execute if score Random RandomOne5556 matches 950..999 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:mega_mush_brown",integrity:1.00,posX:-9,posY:-2,posZ:-9,rotation:"NONE"} replace

execute if score Random RandomOne5556 matches 900..999 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:mega_mush_roots",integrity:1.00,posX:-5,posY:-18,posZ:-5,rotation:"NONE"} replace

fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace

execute if score Random RandomOne5556 matches 0..224 run fill ~ ~ ~ ~1 ~-4 ~1 minecraft:mushroom_stem replace #structures_crabmaster:air_water_dirt
execute if score Random RandomOne5556 matches 0..224 run fill ~ ~-5 ~ ~1 ~-7 ~1 minecraft:mushroom_stem replace #structures_crabmaster:air_water_dirt

execute if score Random RandomOne5556 matches 225..449 run fill ~-1 ~ ~ ~2 ~-4 ~1 minecraft:mushroom_stem replace #structures_crabmaster:air_water_dirt
execute if score Random RandomOne5556 matches 225..449 run fill ~ ~ ~-1 ~1 ~-4 ~2 minecraft:mushroom_stem replace #structures_crabmaster:air_water_dirt
execute if score Random RandomOne5556 matches 225..449 run fill ~ ~-5 ~ ~1 ~-7 ~1 minecraft:mushroom_stem replace #structures_crabmaster:air_water_dirt

execute if score Random RandomOne5556 matches 450..899 run fill ~-1 ~ ~-2 ~1 ~-3 ~2 minecraft:mushroom_stem replace #structures_crabmaster:air_water_dirt
execute if score Random RandomOne5556 matches 450..899 run fill ~-2 ~ ~-1 ~2 ~-3 ~1 minecraft:mushroom_stem replace #structures_crabmaster:air_water_dirt
execute if score Random RandomOne5556 matches 450..899 run fill ~-1 ~-4 ~-1 ~1 ~-6 ~1 minecraft:mushroom_stem replace #structures_crabmaster:air_water_dirt
execute if score Random RandomOne5556 matches 450..899 run fill ~ ~-7 ~ ~ ~-9 ~ minecraft:mushroom_stem replace #structures_crabmaster:air_water_dirt

kill @e[type=minecraft:item,distance=..16]

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999