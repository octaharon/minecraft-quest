scoreboard players set Base Results5556 0
scoreboard players add Base TimerOne5556 1

execute if score Base TimerOne5556 matches 0..3 store success score Base Results5556 run fill ~-4 ~ ~-1 ~4 ~-2 ~1 minecraft:cobblestone replace #structures_crabmaster:air_water_dirt
execute if score Base TimerOne5556 matches 0..3 store success score Base Results5556 run fill ~-1 ~ ~-4 ~1 ~-2 ~4 minecraft:cobblestone replace #structures_crabmaster:air_water_dirt
execute if score Base TimerOne5556 matches 0..5 store success score Base Results5556 run fill ~-3 ~ ~-3 ~3 ~-2 ~3 minecraft:cobblestone replace #structures_crabmaster:air_water_dirt

execute if score Base TimerOne5556 matches 6..7 store success score Base Results5556 run fill ~-2 ~ ~-2 ~2 ~-2 ~2 minecraft:cobblestone replace #structures_crabmaster:air_water_dirt
execute if score Base TimerOne5556 matches 8..9 store success score Base Results5556 run fill ~-1 ~ ~-1 ~1 ~-2 ~1 minecraft:cobblestone replace #structures_crabmaster:air_water_dirt
execute if score Base TimerOne5556 matches 10.. store success score Base Results5556 run fill ~ ~ ~ ~ ~-2 ~ minecraft:cobblestone replace #structures_crabmaster:air_water_dirt

execute if score Base Results5556 matches 1.. positioned ~ ~-3 ~ run function cm_module_megamix:structures/towers/stone_base