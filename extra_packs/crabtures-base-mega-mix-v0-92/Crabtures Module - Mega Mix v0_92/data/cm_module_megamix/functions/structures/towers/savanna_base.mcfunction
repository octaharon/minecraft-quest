scoreboard players set Base Results5556 0

execute store success score Base Results5556 run fill ~ ~ ~ ~ ~-2 ~ minecraft:spruce_fence replace #structures_crabmaster:air_water_plants
execute store success score Base Results5556 run fill ~3 ~ ~3 ~3 ~-2 ~3 minecraft:spruce_fence replace #structures_crabmaster:air_water_plants
execute store success score Base Results5556 run fill ~-3 ~ ~-3 ~-3 ~-2 ~-3 minecraft:spruce_fence replace #structures_crabmaster:air_water_plants
execute store success score Base Results5556 run fill ~3 ~ ~-3 ~3 ~-2 ~-3 minecraft:spruce_fence replace #structures_crabmaster:air_water_plants
execute store success score Base Results5556 run fill ~-3 ~ ~3 ~-3 ~-2 ~3 minecraft:spruce_fence replace #structures_crabmaster:air_water_plants
execute store success score Base Results5556 run fill ~2 ~ ~ ~3 ~-2 ~ minecraft:spruce_fence replace #structures_crabmaster:air_water_plants
execute store success score Base Results5556 run fill ~-2 ~ ~ ~-3 ~-2 ~ minecraft:spruce_fence replace #structures_crabmaster:air_water_plants
execute store success score Base Results5556 run fill ~ ~ ~2 ~ ~-2 ~3 minecraft:spruce_fence replace #structures_crabmaster:air_water_plants
execute store success score Base Results5556 run fill ~ ~ ~-2 ~ ~-2 ~-3 minecraft:spruce_fence replace #structures_crabmaster:air_water_plants

execute if block ~ ~-3 ~ #structures_crabmaster:air_water_plants run scoreboard players add Base Results5556 1
execute if block ~3 ~-3 ~3 #structures_crabmaster:air_water_plants run scoreboard players add Base Results5556 1
execute if block ~-3 ~-3 ~-3 #structures_crabmaster:air_water_plants run scoreboard players add Base Results5556 1
execute if block ~3 ~-3 ~-3 #structures_crabmaster:air_water_plants run scoreboard players add Base Results5556 1
execute if block ~-3 ~-3 ~3 #structures_crabmaster:air_water_plants run scoreboard players add Base Results5556 1

execute if score Base Results5556 matches 1.. positioned ~ ~-3 ~ run function cm_module_megamix:structures/towers/savanna_base