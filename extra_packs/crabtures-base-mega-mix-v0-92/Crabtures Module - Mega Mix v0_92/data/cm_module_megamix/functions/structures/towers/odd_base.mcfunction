scoreboard players set Base Results5556 0

fill ~-3 ~ ~-3 ~-3 ~-2 ~-3 minecraft:mossy_stone_bricks replace #structures_crabmaster:air_water_dirt
fill ~2 ~ ~-3 ~2 ~-2 ~-3 minecraft:mossy_stone_bricks replace #structures_crabmaster:air_water_dirt
fill ~-3 ~ ~2 ~-3 ~-2 ~2 minecraft:mossy_stone_bricks replace #structures_crabmaster:air_water_dirt
fill ~2 ~ ~2 ~2 ~-2 ~2 minecraft:mossy_stone_bricks replace #structures_crabmaster:air_water_dirt
fill ~ ~ ~ ~-1 ~-2 ~-1 minecraft:mossy_stone_bricks replace #structures_crabmaster:air_water_dirt

execute if block ~ ~-3 ~ #structures_crabmaster:air_water_dirt run scoreboard players add Base Results5556 1
execute if block ~4 ~-3 ~4 #structures_crabmaster:air_water_dirt run scoreboard players add Base Results5556 1
execute if block ~-4 ~-3 ~-4 #structures_crabmaster:air_water_dirt run scoreboard players add Base Results5556 1
execute if block ~4 ~-3 ~-4 #structures_crabmaster:air_water_dirt run scoreboard players add Base Results5556 1
execute if block ~-4 ~-3 ~4 #structures_crabmaster:air_water_dirt run scoreboard players add Base Results5556 1

execute if score Base Results5556 matches 1.. positioned ~ ~-3 ~ run function cm_module_megamix:structures/towers/odd_base