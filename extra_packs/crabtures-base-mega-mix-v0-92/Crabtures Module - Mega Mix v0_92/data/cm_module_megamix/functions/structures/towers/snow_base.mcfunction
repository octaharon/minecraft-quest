scoreboard players set Base Results5556 0
scoreboard players add Base TimerOne5556 1

execute if score Base TimerOne5556 matches 0..2 store success score Base Results5556 run fill ~-5 ~ ~-2 ~5 ~-2 ~2 minecraft:snow_block replace #structures_crabmaster:air_water_dirt
execute if score Base TimerOne5556 matches 0..2 store success score Base Results5556 run fill ~-2 ~ ~-5 ~2 ~-2 ~5 minecraft:snow_block replace #structures_crabmaster:air_water_dirt
execute if score Base TimerOne5556 matches 0..4 store success score Base Results5556 run fill ~-4 ~ ~-3 ~4 ~-2 ~3 minecraft:snow_block replace #structures_crabmaster:air_water_dirt
execute if score Base TimerOne5556 matches 0..4 store success score Base Results5556 run fill ~-3 ~ ~-4 ~3 ~-2 ~4 minecraft:snow_block replace #structures_crabmaster:air_water_dirt

execute if score Base TimerOne5556 matches 5..6 store success score Base Results5556 run fill ~-3 ~ ~-3 ~3 ~-2 ~3 minecraft:snow_block replace #structures_crabmaster:air_water_dirt
execute if score Base TimerOne5556 matches 7..8 store success score Base Results5556 run fill ~-2 ~ ~-2 ~2 ~-2 ~2 minecraft:snow_block replace #structures_crabmaster:air_water_dirt
execute if score Base TimerOne5556 matches 9.. store success score Base Results5556 run fill ~-1 ~ ~-1 ~1 ~-2 ~1 minecraft:snow_block replace #structures_crabmaster:air_water_dirt

execute if score Base Results5556 matches 1.. positioned ~ ~-3 ~ run function cm_module_megamix:structures/towers/snow_base