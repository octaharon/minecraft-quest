#forceload add ~-16 ~-16 ~16 ~16

function structures_crabmaster:randomizers/randomizer_1
function structures_crabmaster:randomizers/randomizer_2

fill ~-1 ~1 ~-1 ~ ~9 ~ minecraft:air replace
fill ~-4 ~12 ~-4 ~4 ~16 ~4 minecraft:air replace


execute if score Random RandomOne5556 matches 0..249 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:tower_wood",integrity:0.99,posX:-5,posY:0,posZ:-5,rotation:"NONE"} replace

execute if score Random RandomOne5556 matches 250..499 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:tower_wood",integrity:0.98,posX:-5,posY:0,posZ:-5,rotation:"NONE"} replace

execute if score Random RandomOne5556 matches 500..749 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:tower_wood",integrity:0.96,posX:-5,posY:0,posZ:-5,rotation:"NONE"} replace

execute if score Random RandomOne5556 matches 750..999 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:tower_wood",integrity:0.94,posX:-5,posY:0,posZ:-5,rotation:"NONE"} replace

fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace
fill ~ ~ ~ ~ ~2 ~ minecraft:air replace

execute positioned ~ ~-1 ~ run function cm_module_megamix:structures/towers/wood_base

function structures_crabmaster:randomizers/randomizer_1
function structures_crabmaster:randomizers/randomizer_2

execute as @e[limit=1,sort=random,tag=Chest,tag=C5556,tag=Set1,distance=..24] at @s if score Random RandomOne5556 matches 1.. run fill ~ ~ ~ ~ ~ ~ minecraft:barrel[facing=up]{LootTable:"minecraft:chests/simple_dungeon"} replace
execute as @e[limit=1,sort=random,tag=Chest,tag=C5556,tag=Set2,distance=..24] at @s if score Random RandomOne5556 matches 700.. run fill ~ ~ ~ ~ ~ ~ minecraft:barrel[facing=up]{LootTable:"minecraft:chests/simple_dungeon"} replace
execute as @e[limit=1,sort=random,tag=Chest,tag=C5556,tag=Set3,distance=..24] at @s if score Random RandomOne5556 matches 950.. run fill ~ ~ ~ ~ ~ ~ minecraft:barrel[facing=up]{LootTable:"minecraft:chests/simple_dungeon"} replace

execute as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..24] at @s run function cm_module_megamix:structures/towers/wood_mobs

execute as @e[distance=..32,tag=Spawner,tag=C5556] at @s run execute as @e[distance=..1,type=!minecraft:area_effect_cloud,limit=8] at @s run data merge entity @s {CanBreakDoors:1,PersistenceRequired:1}
execute as @e[distance=..32,tag=Spawner,tag=C5556] at @s run execute as @e[distance=..1,type=!minecraft:area_effect_cloud,limit=8] at @s run effect give @s minecraft:fire_resistance 180 1 true

kill @e[type=minecraft:item,distance=..16]
kill @e[tag=Spawner,tag=C5556,distance=..24]
kill @e[tag=Chest,tag=C5556,distance=..24]

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999