execute if score Random RandomTwo5556 matches ..150 run summon minecraft:zombie ~ ~ ~
execute if score Random RandomTwo5556 matches 151..300 run summon minecraft:zombie_villager ~ ~ ~
execute if score Random RandomTwo5556 matches 301..600 run summon minecraft:drowned ~ ~ ~
execute if score Random RandomTwo5556 matches 601..900 run summon minecraft:skeleton ~ ~ ~
execute if score Random RandomTwo5556 matches 901..999 run summon minecraft:witch ~ ~ ~

scoreboard players remove Random RandomOne5556 101
scoreboard players remove Random RandomTwo5556 20
execute as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=1..32] at @s if score Random RandomOne5556 matches 0.. run function cm_module_megamix:structures/towers/ocean_mobs