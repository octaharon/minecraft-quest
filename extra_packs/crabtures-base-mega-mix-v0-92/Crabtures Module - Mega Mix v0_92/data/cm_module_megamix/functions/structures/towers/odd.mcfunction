forceload add ~-8 ~-8 ~8 ~8

function structures_crabmaster:randomizers/randomizer_1
function structures_crabmaster:randomizers/randomizer_2

execute if score Random RandomOne5556 matches 0..249 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:odd_tower",integrity:0.99,posX:-3,posY:-1,posZ:-3,rotation:"NONE"} replace
execute if score Random RandomOne5556 matches 250..499 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:odd_tower",integrity:0.95,posX:-3,posY:-1,posZ:-3,rotation:"NONE"} replace
execute if score Random RandomOne5556 matches 500..749 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:odd_tower",integrity:0.85,posX:-3,posY:-1,posZ:-3,rotation:"NONE"} replace
execute if score Random RandomOne5556 matches 750..999 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:odd_tower",integrity:0.75,posX:-3,posY:-1,posZ:-3,rotation:"NONE"} replace

fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace
fill ~ ~ ~ ~ ~1 ~ minecraft:mossy_stone_bricks replace
fill ~ ~ ~ ~1 ~29 ~1 minecraft:mossy_stone_bricks keep

execute positioned ~1 ~-1 ~1 run function cm_module_megamix:structures/towers/odd_base

kill @e[tag=Chest,tag=C5556,distance=..24]

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999