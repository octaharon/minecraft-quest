scoreboard players set Base Results5556 0
scoreboard players add Base TimerOne5556 1

execute if score Base TimerOne5556 matches 0..6 store success score Base Results5556 run fill ~-1 ~ ~ ~5 ~-2 ~5 minecraft:orange_terracotta replace #structures_crabmaster:air_water_dirt
execute if score Base TimerOne5556 matches 0..5 store success score Base Results5556 run fill ~-5 ~ ~-2 ~1 ~-2 ~4 minecraft:light_gray_terracotta replace #structures_crabmaster:air_water_dirt
execute if score Base TimerOne5556 matches 0..4 store success score Base Results5556 run fill ~-1 ~ ~-5 ~4 ~-2 ~ minecraft:terracotta replace #structures_crabmaster:air_water_dirt

execute if score Base TimerOne5556 matches 7..10 store success score Base Results5556 run fill ~ ~ ~1 ~4 ~-2 ~4 minecraft:orange_terracotta replace #structures_crabmaster:air_water_dirt
execute if score Base TimerOne5556 matches 6..9 store success score Base Results5556 run fill ~-4 ~ ~-1 ~ ~-2 ~3 minecraft:light_gray_terracotta replace #structures_crabmaster:air_water_dirt
execute if score Base TimerOne5556 matches 5..8 store success score Base Results5556 run fill ~ ~ ~-4 ~3 ~-2 ~-1 minecraft:terracotta replace #structures_crabmaster:air_water_dirt

execute if score Base TimerOne5556 matches 11.. store success score Base Results5556 run fill ~1 ~ ~2 ~3 ~-2 ~3 minecraft:orange_terracotta replace #structures_crabmaster:air_water_dirt
execute if score Base TimerOne5556 matches 10.. store success score Base Results5556 run fill ~-3 ~ ~ ~-1 ~-2 ~2 minecraft:light_gray_terracotta replace #structures_crabmaster:air_water_dirt
execute if score Base TimerOne5556 matches 9.. store success score Base Results5556 run fill ~1 ~ ~-3 ~2 ~-2 ~-2 minecraft:terracotta replace #structures_crabmaster:air_water_dirt

execute if block ~ ~-3 ~ #structures_crabmaster:air_water_dirt run scoreboard players add Base Results5556 1
execute if block ~4 ~-3 ~4 #structures_crabmaster:air_water_dirt run scoreboard players add Base Results5556 1
execute if block ~-4 ~-3 ~-4 #structures_crabmaster:air_water_dirt run scoreboard players add Base Results5556 1
execute if block ~4 ~-3 ~-4 #structures_crabmaster:air_water_dirt run scoreboard players add Base Results5556 1
execute if block ~-4 ~-3 ~4 #structures_crabmaster:air_water_dirt run scoreboard players add Base Results5556 1

execute if score Base Results5556 matches 1.. positioned ~ ~-3 ~ run function cm_module_megamix:structures/towers/terracotta_base