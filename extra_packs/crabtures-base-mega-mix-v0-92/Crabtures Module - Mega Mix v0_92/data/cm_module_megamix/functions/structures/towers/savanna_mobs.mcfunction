execute if score Random RandomTwo5556 matches ..120 run summon minecraft:zombie ~ ~ ~
execute if score Random RandomTwo5556 matches 121..240 run summon minecraft:zombie_villager ~ ~ ~
execute if score Random RandomTwo5556 matches 241..400 run summon minecraft:spider ~ ~1 ~
execute if score Random RandomTwo5556 matches 401..600 run summon minecraft:skeleton ~ ~ ~
execute if score Random RandomTwo5556 matches 601..999 run summon minecraft:husk ~ ~1 ~

scoreboard players remove Random RandomOne5556 101
scoreboard players remove Random RandomTwo5556 20
execute as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=1..32] at @s if score Random RandomOne5556 matches 0.. run function cm_module_megamix:structures/towers/savanna_mobs