#forceload add ~-16 ~-16 ~16 ~16

function structures_crabmaster:randomizers/randomizer_1
function structures_crabmaster:randomizers/randomizer_2

fill ~-1 ~ ~-1 ~1 ~11 ~1 minecraft:air replace

execute if score Random RandomOne5556 matches 0..332 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:tower_snow",integrity:1.00,posX:-5,posY:-3,posZ:-5,rotation:"NONE"} replace
execute if score Random RandomTwo5556 matches 0..332 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:tower_snow_extras",integrity:0.4,posX:-5,posY:-5,posZ:-5,rotation:"NONE"} replace

execute if score Random RandomOne5556 matches 333..665 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:tower_snow",integrity:0.96,posX:-5,posY:-3,posZ:-5,rotation:"NONE"} replace
execute if score Random RandomTwo5556 matches 333..665 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:tower_snow_extras",integrity:0.33,posX:-5,posY:-5,posZ:-5,rotation:"NONE"} replace

execute if score Random RandomOne5556 matches 666..999 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:tower_snow",integrity:0.91,posX:-5,posY:-3,posZ:-5,rotation:"NONE"} replace
execute if score Random RandomTwo5556 matches 666..999 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:tower_snow_extras",integrity:0.20,posX:-5,posY:-5,posZ:-5,rotation:"NONE"} replace

fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace
fill ~ ~ ~ ~ ~2 ~ minecraft:air replace

scoreboard players set Base TimerOne5556 0
execute positioned ~ ~-3 ~ run function cm_module_megamix:structures/towers/snow_base

function structures_crabmaster:randomizers/randomizer_1
function structures_crabmaster:randomizers/randomizer_2

execute as @e[limit=1,sort=random,tag=Chest,tag=C5556,tag=Set1,distance=..24] at @s if score Random RandomOne5556 matches 1.. run fill ~ ~ ~ ~ ~ ~ minecraft:barrel[facing=up]{LootTable:"minecraft:chests/village/village_snowy_house"} replace
execute as @e[limit=1,sort=random,tag=Chest,tag=C5556,tag=Set2,distance=..24] at @s if score Random RandomOne5556 matches 530.. run fill ~ ~ ~ ~ ~ ~ minecraft:barrel[facing=up]{LootTable:"minecraft:chests/village/village_snowy_house"} replace
execute as @e[limit=1,sort=random,tag=Chest,tag=C5556,tag=Set3,distance=..24] at @s if score Random RandomOne5556 matches 805.. run fill ~ ~ ~ ~ ~ ~ minecraft:barrel[facing=up]{LootTable:"minecraft:chests/village/village_snowy_house"} replace
execute as @e[limit=1,sort=random,tag=Chest,tag=C5556,tag=Set4,distance=..24] at @s if score Random RandomOne5556 matches 942.. run fill ~ ~ ~ ~ ~ ~ minecraft:barrel[facing=up]{LootTable:"minecraft:chests/village/village_snowy_house"} replace

execute as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..24] at @s run function cm_module_megamix:structures/towers/snow_mobs

execute as @e[distance=..32,tag=Spawner,tag=C5556] at @s run execute as @e[distance=..1,type=!minecraft:area_effect_cloud,limit=8] at @s run data merge entity @s {CanBreakDoors:1,PersistenceRequired:1}
execute as @e[distance=..32,tag=Spawner,tag=C5556] at @s run execute as @e[distance=..1,type=!minecraft:area_effect_cloud,limit=8] at @s run effect give @s minecraft:fire_resistance 180 1 true

kill @e[type=minecraft:item,distance=..16]
kill @e[tag=Spawner,tag=C5556,distance=..24]
kill @e[tag=Chest,tag=C5556,distance=..24]

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999