scoreboard players set Base Results5556 0
scoreboard players add Base TimerOne5556 1

execute if score Base TimerOne5556 matches 0.. store success score Base Results5556 run fill ~ ~ ~ ~ ~-2 ~ minecraft:cobblestone_wall replace #structures_crabmaster:air_water

execute if score Base TimerOne5556 matches 0..4 run fill ~5 ~ ~2 ~5 ~-2 ~2 minecraft:cobblestone_wall replace #structures_crabmaster:air_water
execute if score Base TimerOne5556 matches 0..4 run fill ~-5 ~ ~-2 ~-5 ~-2 ~-2 minecraft:cobblestone_wall replace #structures_crabmaster:air_water
execute if score Base TimerOne5556 matches 0..4 run fill ~5 ~ ~-2 ~5 ~-2 ~-2 minecraft:cobblestone_wall replace #structures_crabmaster:air_water
execute if score Base TimerOne5556 matches 0..4 run fill ~-5 ~ ~2 ~-5 ~-2 ~2 minecraft:cobblestone_wall replace #structures_crabmaster:air_water
execute if score Base TimerOne5556 matches 0..4 run fill ~2 ~ ~5 ~2 ~-2 ~5 minecraft:cobblestone_wall replace #structures_crabmaster:air_water
execute if score Base TimerOne5556 matches 0..4 run fill ~-2 ~ ~-5 ~-2 ~-2 ~-5 minecraft:cobblestone_wall replace #structures_crabmaster:air_water
execute if score Base TimerOne5556 matches 0..4 run fill ~2 ~ ~-5 ~2 ~-2 ~-5 minecraft:cobblestone_wall replace #structures_crabmaster:air_water
execute if score Base TimerOne5556 matches 0..4 run fill ~-2 ~ ~5 ~-2 ~-2 ~5 minecraft:cobblestone_wall replace #structures_crabmaster:air_water

execute if score Base TimerOne5556 matches 4..7 run fill ~6 ~ ~2 ~6 ~-2 ~2 minecraft:cobblestone_wall replace #structures_crabmaster:air_water
execute if score Base TimerOne5556 matches 4..7 run fill ~-6 ~ ~-2 ~-6 ~-2 ~-2 minecraft:cobblestone_wall replace #structures_crabmaster:air_water
execute if score Base TimerOne5556 matches 4..7 run fill ~6 ~ ~-2 ~6 ~-2 ~-2 minecraft:cobblestone_wall replace #structures_crabmaster:air_water
execute if score Base TimerOne5556 matches 4..7 run fill ~-6 ~ ~2 ~-6 ~-2 ~2 minecraft:cobblestone_wall replace #structures_crabmaster:air_water
execute if score Base TimerOne5556 matches 4..7 run fill ~2 ~ ~6 ~2 ~-2 ~6 minecraft:cobblestone_wall replace #structures_crabmaster:air_water
execute if score Base TimerOne5556 matches 4..7 run fill ~-2 ~ ~-6 ~-2 ~-2 ~-6 minecraft:cobblestone_wall replace #structures_crabmaster:air_water
execute if score Base TimerOne5556 matches 4..7 run fill ~2 ~ ~-6 ~2 ~-2 ~-6 minecraft:cobblestone_wall replace #structures_crabmaster:air_water
execute if score Base TimerOne5556 matches 4..7 run fill ~-2 ~ ~6 ~-2 ~-2 ~6 minecraft:cobblestone_wall replace #structures_crabmaster:air_water

execute if score Base TimerOne5556 matches 7.. run fill ~7 ~ ~2 ~7 ~-2 ~2 minecraft:cobblestone_wall replace #structures_crabmaster:air_water
execute if score Base TimerOne5556 matches 7.. run fill ~-7 ~ ~-2 ~-7 ~-2 ~-2 minecraft:cobblestone_wall replace #structures_crabmaster:air_water
execute if score Base TimerOne5556 matches 7.. run fill ~7 ~ ~-2 ~7 ~-2 ~-2 minecraft:cobblestone_wall replace #structures_crabmaster:air_water
execute if score Base TimerOne5556 matches 7.. run fill ~-7 ~ ~2 ~-7 ~-2 ~2 minecraft:cobblestone_wall replace #structures_crabmaster:air_water
execute if score Base TimerOne5556 matches 7.. run fill ~2 ~ ~7 ~2 ~-2 ~7 minecraft:cobblestone_wall replace #structures_crabmaster:air_water
execute if score Base TimerOne5556 matches 7.. run fill ~-2 ~ ~-7 ~-2 ~-2 ~-7 minecraft:cobblestone_wall replace #structures_crabmaster:air_water
execute if score Base TimerOne5556 matches 7.. run fill ~2 ~ ~-7 ~2 ~-2 ~-7 minecraft:cobblestone_wall replace #structures_crabmaster:air_water
execute if score Base TimerOne5556 matches 7.. run fill ~-2 ~ ~7 ~-2 ~-2 ~7 minecraft:cobblestone_wall replace #structures_crabmaster:air_water

execute if block ~ ~-3 ~ #structures_crabmaster:air_water_dirt run scoreboard players add Base Results5556 1
execute if block ~4 ~-3 ~4 #structures_crabmaster:air_water_dirt run scoreboard players add Base Results5556 1
execute if block ~-4 ~-3 ~-4 #structures_crabmaster:air_water_dirt run scoreboard players add Base Results5556 1
execute if block ~4 ~-3 ~-4 #structures_crabmaster:air_water_dirt run scoreboard players add Base Results5556 1
execute if block ~-4 ~-3 ~4 #structures_crabmaster:air_water_dirt run scoreboard players add Base Results5556 1

execute if score Base Results5556 matches 1.. positioned ~ ~-3 ~ run function cm_module_megamix:structures/towers/water_base