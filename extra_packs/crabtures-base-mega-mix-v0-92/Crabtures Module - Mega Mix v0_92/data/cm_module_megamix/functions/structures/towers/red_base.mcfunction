scoreboard players set Base Results5556 0
scoreboard players add Base TimerOne5556 1

execute if score Base TimerOne5556 matches 0..3 store success score Base Results5556 run fill ~-5 ~ ~-1 ~5 ~-2 ~1 minecraft:terracotta replace #structures_crabmaster:air_water_dirt
execute if score Base TimerOne5556 matches 0..3 store success score Base Results5556 run fill ~-1 ~ ~-5 ~1 ~-2 ~5 minecraft:terracotta replace #structures_crabmaster:air_water_dirt
execute if score Base TimerOne5556 matches 0..5 store success score Base Results5556 run fill ~-2 ~ ~-4 ~2 ~-2 ~4 minecraft:terracotta replace #structures_crabmaster:air_water_dirt
execute if score Base TimerOne5556 matches 0..5 store success score Base Results5556 run fill ~-4 ~ ~-2 ~4 ~-2 ~2 minecraft:terracotta replace #structures_crabmaster:air_water_dirt

execute if score Base TimerOne5556 matches 6..7 store success score Base Results5556 run fill ~-2 ~ ~-2 ~2 ~-2 ~2 minecraft:terracotta replace #structures_crabmaster:air_water_dirt
execute if score Base TimerOne5556 matches 8..9 store success score Base Results5556 run fill ~-1 ~ ~-1 ~1 ~-2 ~1 minecraft:terracotta replace #structures_crabmaster:air_water_dirt
execute if score Base TimerOne5556 matches 10.. store success score Base Results5556 run fill ~ ~ ~ ~ ~-2 ~ minecraft:terracotta replace #structures_crabmaster:air_water_dirt

execute if score Base Results5556 matches 1.. positioned ~ ~-3 ~ run function cm_module_megamix:structures/towers/red_base