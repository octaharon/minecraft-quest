function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..446 run function cm_module_megamix:structures/mush_land/plant_3
execute if score Random RandomOne5556 matches 447..999 run function cm_module_megamix:structures/mush_land/plant_4

fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace
fill ~ ~-1 ~ ~ ~1 ~ minecraft:mushroom_stem replace
