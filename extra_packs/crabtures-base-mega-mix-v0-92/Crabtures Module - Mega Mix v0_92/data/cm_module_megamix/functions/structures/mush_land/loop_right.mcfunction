forceload add ~-8 ~-8 ~8 ~8

execute positioned ~ ~-1 ~ unless block ~ ~-3 ~ #structures_crabmaster:water_lava unless block ~ ~ ~ #structures_crabmaster:water_lava unless block ~ ~4 ~ #structures_crabmaster:water_lava unless block ~ ~8 ~ #structures_crabmaster:water_lava unless block ~ ~13 ~ #structures_crabmaster:water_lava run function cm_module_megamix:structures/mush_land/spawn_1

scoreboard players set Landed ExtraLogic5556 0
execute if predicate structures_crabmaster:randomizers/0_25 positioned ~3 ~8 ~6 run function cm_module_megamix:structures/mush_land/land_right
scoreboard players set Landed ExtraLogic5556 0
execute if predicate structures_crabmaster:randomizers/0_3 positioned ~ ~8 ~5 run function cm_module_megamix:structures/mush_land/land_right
scoreboard players set Landed ExtraLogic5556 0
execute if predicate structures_crabmaster:randomizers/0_2 positioned ~-6 ~8 ~5 run function cm_module_megamix:structures/mush_land/land_right
scoreboard players set Landed ExtraLogic5556 0
execute if predicate structures_crabmaster:randomizers/0_15 positioned ~-7 ~8 ~-4 run function cm_module_megamix:structures/mush_land/land_down

scoreboard players set Landed ExtraLogic5556 1