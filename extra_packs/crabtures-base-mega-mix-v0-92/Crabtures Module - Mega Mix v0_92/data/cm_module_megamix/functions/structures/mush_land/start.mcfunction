forceload add ~-8 ~-8 ~8 ~8

scoreboard players set Landed ExtraLogic5556 0
execute if predicate structures_crabmaster:randomizers/0_8 positioned ~4 ~2 ~3 run function cm_module_megamix:structures/mush_land/land_up
scoreboard players set Landed ExtraLogic5556 0
execute if predicate structures_crabmaster:randomizers/0_8 positioned ~-3 ~2 ~4 run function cm_module_megamix:structures/mush_land/land_down
scoreboard players set Landed ExtraLogic5556 0
execute if predicate structures_crabmaster:randomizers/0_8 positioned ~-4 ~2 ~-3 run function cm_module_megamix:structures/mush_land/land_right
scoreboard players set Landed ExtraLogic5556 0
execute if predicate structures_crabmaster:randomizers/0_8 positioned ~3 ~2 ~-4 run function cm_module_megamix:structures/mush_land/land_left

execute as @e[tag=MushSpot,tag=C5556] at @s run function cm_module_megamix:structures/mush_land/plant_1

kill @e[type=minecraft:item,distance=..16]

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999