function structures_crabmaster:randomizers/randomizer_1
function structures_crabmaster:randomizers/randomizer_2

fill ~-2 ~3 ~-3 ~2 ~16 ~3 minecraft:air replace
fill ~-3 ~3 ~-2 ~3 ~16 ~2 minecraft:air replace
fill ~-2 ~17 ~-2 ~2 ~24 ~2 minecraft:air replace

execute positioned ~ ~ ~ run kill @e[tag=MushSpot,tag=C5556,distance=..2]

execute if score Random RandomOne5556 matches 0..249 run function cm_module_megamix:structures/mush_land/spawn_2
execute if score Random RandomOne5556 matches 250..499 run function cm_module_megamix:structures/mush_land/spawn_3
execute if score Random RandomOne5556 matches 500..749 run function cm_module_megamix:structures/mush_land/spawn_4
execute if score Random RandomOne5556 matches 750..999 run function cm_module_megamix:structures/mush_land/spawn_5

fill ~ ~-3 ~ ~ ~-3 ~ minecraft:redstone_block replace
fill ~ ~-3 ~ ~ ~-2 ~ minecraft:dirt replace

kill @e[type=minecraft:item,distance=..16]