function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..239 run function cm_module_megamix:structures/big_sky_island/spawn_2
execute if score Random RandomOne5556 matches 240..479 run function cm_module_megamix:structures/big_sky_island/spawn_3
execute if score Random RandomOne5556 matches 480..719 run function cm_module_megamix:structures/big_sky_island/spawn_4
execute if score Random RandomOne5556 matches 720..960 run function cm_module_megamix:structures/big_sky_island/spawn_5

execute if score Random RandomOne5556 matches 0..960 run fill ~ ~-1 ~ ~ ~-1 ~ minecraft:redstone_block replace
execute if score Random RandomOne5556 matches 0..960 run fill ~ ~-1 ~ ~ ~ ~ minecraft:stone replace

kill @e[type=minecraft:item,distance=..16]