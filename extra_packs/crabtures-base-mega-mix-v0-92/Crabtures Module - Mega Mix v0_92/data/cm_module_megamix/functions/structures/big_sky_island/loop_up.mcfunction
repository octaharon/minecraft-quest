forceload add ~-8 ~-8 ~8 ~8

execute positioned ~ ~ ~ if block ~ ~ ~ minecraft:air if block ~ ~3 ~ minecraft:air if block ~ ~6 ~ minecraft:air run function cm_module_megamix:structures/big_sky_island/spawn_1

execute if predicate structures_crabmaster:randomizers/0_175 positioned ~16 ~ ~2 run function cm_module_megamix:structures/big_sky_island/loop_up
execute if predicate structures_crabmaster:randomizers/0_175 positioned ~-4 ~ ~13 run function cm_module_megamix:structures/big_sky_island/loop_up

execute if predicate structures_crabmaster:randomizers/0_5 positioned ~5 ~ ~-5 run function cm_module_megamix:structures/sky_island/loop_up
execute if predicate structures_crabmaster:randomizers/0_5 positioned ~5 ~ ~5 run function cm_module_megamix:structures/sky_island/loop_right
execute if predicate structures_crabmaster:randomizers/0_45 positioned ~-5 ~ ~5 run function cm_module_megamix:structures/sky_island/loop_down
execute if predicate structures_crabmaster:randomizers/0_45 positioned ~-5 ~ ~-5 run function cm_module_megamix:structures/sky_island/loop_left