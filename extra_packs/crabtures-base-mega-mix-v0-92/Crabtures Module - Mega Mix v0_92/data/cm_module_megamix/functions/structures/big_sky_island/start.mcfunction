forceload add ~-8 ~-8 ~8 ~8

execute unless block ~ ~-3 ~ #structures_crabmaster:water_lava unless block ~ ~ ~ #structures_crabmaster:water_lava unless block ~ ~4 ~ #structures_crabmaster:water_lava run function cm_module_megamix:structures/big_sky_island/spawn_1

execute positioned ~3 ~ ~3 if predicate structures_crabmaster:randomizers/0_75 run function cm_module_megamix:structures/big_sky_island/loop_up
execute positioned ~-3 ~ ~3 if predicate structures_crabmaster:randomizers/0_75 run function cm_module_megamix:structures/big_sky_island/loop_right
execute positioned ~-3 ~ ~-3 if predicate structures_crabmaster:randomizers/0_75 run function cm_module_megamix:structures/big_sky_island/loop_down
execute positioned ~3 ~ ~-3 if predicate structures_crabmaster:randomizers/0_75 run function cm_module_megamix:structures/big_sky_island/loop_left

execute if predicate structures_crabmaster:randomizers/0_9 positioned ~5 ~ ~-5 run function cm_module_megamix:structures/sky_island/loop_up
execute if predicate structures_crabmaster:randomizers/0_9 positioned ~5 ~ ~5 run function cm_module_megamix:structures/sky_island/loop_right
execute if predicate structures_crabmaster:randomizers/0_9 positioned ~-5 ~ ~5 run function cm_module_megamix:structures/sky_island/loop_down
execute if predicate structures_crabmaster:randomizers/0_9 positioned ~-5 ~ ~-5 run function cm_module_megamix:structures/sky_island/loop_left

kill @e[type=minecraft:item,distance=..16]

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999