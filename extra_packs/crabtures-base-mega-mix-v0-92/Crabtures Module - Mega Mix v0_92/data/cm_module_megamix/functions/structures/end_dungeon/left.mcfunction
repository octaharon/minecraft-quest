forceload add ~-12 ~-12 ~12 ~12

function structures_crabmaster:randomizers/randomizer_1
function structures_crabmaster:randomizers/randomizer_2

execute if block ~ ~-1 ~ minecraft:air run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:end_dungeon_room",integrity:1.0,posX:-4,posY:-2,posZ:-4,rotation:"NONE"} replace
execute if block ~ ~-1 ~ minecraft:air if score Random RandomOne5556 matches 0..249 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:end_dungeon_roof_2",integrity:1.0,posX:-4,posY:6,posZ:-4,rotation:"NONE"} replace
execute if block ~ ~-1 ~ minecraft:air if score Random RandomOne5556 matches 250..999 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:end_dungeon_roof_1",integrity:1.0,posX:-4,posY:6,posZ:-4,rotation:"NONE"} replace
execute if block ~ ~-1 ~ minecraft:air if score Random RandomTwo5556 matches 0..249 run fill ~1 ~1 ~ ~1 ~1 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:end_dungeon_base_2",integrity:1.0,posX:-5,posY:-11,posZ:-4,rotation:"NONE"} replace
execute if block ~ ~-1 ~ minecraft:air if score Random RandomTwo5556 matches 250..999 run fill ~1 ~1 ~ ~1 ~1 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:end_dungeon_base_1",integrity:1.0,posX:-5,posY:-6,posZ:-4,rotation:"NONE"} replace
execute if block ~ ~-1 ~ minecraft:air run fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace


execute positioned ~8 ~ ~ if predicate structures_crabmaster:randomizers/0_1 run function cm_module_megamix:structures/end_dungeon/up
execute positioned ~-8 ~ ~ if predicate structures_crabmaster:randomizers/0_05 run function cm_module_megamix:structures/end_dungeon/down
execute positioned ~ ~ ~-8 if predicate structures_crabmaster:randomizers/0_6 run function cm_module_megamix:structures/end_dungeon/left


function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..249 unless block ~ ~ ~ minecraft:air run fill ~ ~1 ~ ~ ~1 ~ minecraft:chest[facing=north]{LootTable:"minecraft:chests/end_city_treasure"} replace
execute if score Random RandomOne5556 matches 250..375 unless block ~ ~ ~ minecraft:air run fill ~ ~1 ~ ~ ~1 ~ minecraft:spawner{SpawnData:{id:endermite}} replace

function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..300 unless block ~3 ~ ~3 minecraft:air run summon minecraft:shulker ~3 ~1 ~3 {Color:16}
execute if score Random RandomOne5556 matches 200..550 unless block ~-3 ~ ~-3 minecraft:air run summon minecraft:shulker ~-3 ~1 ~-3 {Color:16}
execute if score Random RandomOne5556 matches 450..800 unless block ~-3 ~ ~3 minecraft:air run summon minecraft:shulker ~-3 ~1 ~3 {Color:16}
execute if score Random RandomOne5556 matches 700..999 unless block ~3 ~ ~-3 minecraft:air run summon minecraft:shulker ~3 ~1 ~-3 {Color:16}

kill @e[type=minecraft:item,distance=..16]

scoreboard players set Random RandomOne5556 99999
