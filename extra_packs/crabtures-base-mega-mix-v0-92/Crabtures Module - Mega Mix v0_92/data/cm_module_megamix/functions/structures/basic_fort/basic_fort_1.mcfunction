fill ~2 ~-4 ~-1 ~7 ~9 ~4 minecraft:air replace

function structures_crabmaster:randomizers/randomizer_1
function structures_crabmaster:randomizers/randomizer_2

execute if score Random RandomOne5556 matches 0..332 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fort_basic",integrity:0.99,posX:-1,posY:-6,posZ:-4,rotation:"NONE"} replace
execute if score Random RandomTwo5556 matches 0..332 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fort_basic_extras",integrity:0.3,posX:-1,posY:-8,posZ:-4,rotation:"NONE"} replace

execute if score Random RandomOne5556 matches 333..665 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fort_basic",integrity:0.95,posX:-1,posY:-6,posZ:-4,rotation:"NONE"} replace
execute if score Random RandomTwo5556 matches 333..665 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fort_basic_extras",integrity:0.2,posX:-1,posY:-8,posZ:-4,rotation:"NONE"} replace

execute if score Random RandomOne5556 matches 666..999 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fort_basic",integrity:0.90,posX:-1,posY:-6,posZ:-4,rotation:"NONE"} replace
execute if score Random RandomTwo5556 matches 666..999 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fort_basic_extras",integrity:0.1,posX:-1,posY:-8,posZ:-4,rotation:"NONE"} replace

scoreboard players set Base TimerOne5556 0
execute positioned ~4 ~-6 ~1 run function cm_module_megamix:structures/basic_fort/basic_fort_base

scoreboard players set Random RandomOne5556 99999