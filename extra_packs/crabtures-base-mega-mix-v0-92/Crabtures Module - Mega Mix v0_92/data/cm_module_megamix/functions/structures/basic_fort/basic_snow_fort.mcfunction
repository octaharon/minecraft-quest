forceload add ~-16 ~-16 ~16 ~16

function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..249 run function cm_module_megamix:structures/basic_fort/basic_snow_fort_1
execute if score Random RandomOne5556 matches 250..499 run function cm_module_megamix:structures/basic_fort/basic_snow_fort_2
execute if score Random RandomOne5556 matches 500..749 run function cm_module_megamix:structures/basic_fort/basic_snow_fort_3
execute if score Random RandomOne5556 matches 750..999 run function cm_module_megamix:structures/basic_fort/basic_snow_fort_4

fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace
fill ~ ~1 ~ ~ ~2 ~ minecraft:air replace
fill ~ ~ ~ ~ ~ ~ minecraft:snow replace minecraft:structure_block

function structures_crabmaster:randomizers/randomizer_1
function structures_crabmaster:randomizers/randomizer_2

execute as @e[limit=1,sort=random,tag=Chest,tag=C5556,tag=Set1,distance=..24] at @s if score Random RandomOne5556 matches 1.. run fill ~ ~ ~ ~ ~ ~ minecraft:chest{LootTable:"minecraft:chests/woodland_mansion"} replace
execute as @e[limit=1,sort=random,tag=Chest,tag=C5556,tag=Set2,distance=..24] at @s if score Random RandomOne5556 matches 600.. run fill ~ ~ ~ ~ ~ ~ minecraft:chest{LootTable:"minecraft:chests/woodland_mansion"} replace
execute as @e[limit=1,sort=random,tag=Chest,tag=C5556,tag=Set3,distance=..24] at @s if score Random RandomOne5556 matches 865.. run fill ~ ~ ~ ~ ~ ~ minecraft:chest{LootTable:"minecraft:chests/woodland_mansion"} replace
execute as @e[limit=1,sort=random,tag=Chest,tag=C5556,tag=Set4,distance=..24] at @s if score Random RandomOne5556 matches 982.. run fill ~ ~ ~ ~ ~ ~ minecraft:chest{LootTable:"minecraft:chests/woodland_mansion"} replace

execute as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..24] at @s run function cm_module_megamix:structures/basic_fort/basic_snow_fort_mobs

execute as @e[distance=..32,tag=Spawner,tag=C5556] at @s run execute as @e[distance=..1,type=!minecraft:area_effect_cloud,limit=8] at @s run data merge entity @s {CanBreakDoors:1,PersistenceRequired:1}
execute as @e[distance=..32,tag=Spawner,tag=C5556] at @s run execute as @e[distance=..1,type=!minecraft:area_effect_cloud,limit=8] at @s run effect give @s minecraft:fire_resistance 180 1 true

kill @e[type=minecraft:item,distance=..16]
kill @e[tag=Spawner,tag=C5556,distance=..24]
kill @e[tag=Chest,tag=C5556,distance=..24]

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999