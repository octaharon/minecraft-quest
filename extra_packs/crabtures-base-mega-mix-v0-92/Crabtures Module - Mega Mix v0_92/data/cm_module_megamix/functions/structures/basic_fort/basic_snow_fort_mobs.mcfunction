execute if score Random RandomTwo5556 matches ..200 run summon minecraft:zombie ~ ~ ~
execute if score Random RandomTwo5556 matches 201..400 run summon minecraft:vindicator ~ ~ ~
execute if score Random RandomTwo5556 matches 401..480 run summon minecraft:spider ~ ~1 ~
execute if score Random RandomTwo5556 matches 481..650 run summon minecraft:skeleton ~ ~ ~
execute if score Random RandomTwo5556 matches 651..999 run summon minecraft:stray ~ ~ ~

scoreboard players remove Random RandomOne5556 101
scoreboard players remove Random RandomTwo5556 20
execute as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=1..32] at @s if score Random RandomOne5556 matches 0.. run function cm_module_megamix:structures/basic_fort/basic_snow_fort_mobs