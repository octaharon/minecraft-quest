function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..249 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:mega_tree_branch_5",integrity:1.00,posX:-9,posY:0,posZ:-9,rotation:"NONE"} replace
execute if score Random RandomOne5556 matches 250..499 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:mega_tree_branch_5",integrity:1.00,posX:9,posY:0,posZ:-9,rotation:"CLOCKWISE_90"} replace
execute if score Random RandomOne5556 matches 500..749 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:mega_tree_branch_5",integrity:1.00,posX:9,posY:0,posZ:9,rotation:"CLOCKWISE_180"} replace
execute if score Random RandomOne5556 matches 750..999 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:mega_tree_branch_5",integrity:1.00,posX:-9,posY:0,posZ:9,rotation:"COUNTERCLOCKWISE_90"} replace
fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace
fill ~ ~ ~ ~ ~1 ~ minecraft:oak_wood replace

kill @e[type=minecraft:item,distance=..16]
