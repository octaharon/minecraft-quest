execute if predicate structures_crabmaster:randomizers/0_8 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:mega_tree_branch_1",integrity:1.00,posX:-1,posY:0,posZ:-1,rotation:"NONE"} replace
execute if predicate structures_crabmaster:randomizers/0_8 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:mega_tree_branch_1",integrity:1.00,posX:1,posY:0,posZ:-1,rotation:"CLOCKWISE_90"} replace
execute if predicate structures_crabmaster:randomizers/0_25 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:mega_tree_branch_1",integrity:1.00,posX:1,posY:0,posZ:1,rotation:"CLOCKWISE_180"} replace
execute if predicate structures_crabmaster:randomizers/0_25 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:mega_tree_branch_1",integrity:1.00,posX:-1,posY:0,posZ:1,rotation:"COUNTERCLOCKWISE_90"} replace
fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace
fill ~ ~ ~ ~ ~1 ~ minecraft:oak_wood replace

kill @e[type=minecraft:item,distance=..16]
