forceload add ~-8 ~-8 ~8 ~8

execute positioned ~ ~ ~ unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~ ~ #structures_crabmaster:water_lava unless block ~ ~1 ~ #structures_crabmaster:water_lava run function cm_module_megamix:structures/end_crystal_patch/spawn_1

scoreboard players remove SpiralLength ExtraLogic5556 80
scoreboard players remove SpiralCount ExtraLogic5556 3

function structures_crabmaster:randomizers/randomizer_1

execute if score SpiralCount ExtraLogic5556 matches 0.. if score SpiralLength ExtraLogic5556 matches 0.. if score Random RandomOne5556 matches 0..332 positioned ~1 ~8 ~6 run function cm_module_megamix:structures/end_crystal_patch/land_7
execute if score SpiralCount ExtraLogic5556 matches 0.. if score SpiralLength ExtraLogic5556 matches 0.. if score Random RandomOne5556 matches 333..665 positioned ~ ~8 ~7 run function cm_module_megamix:structures/end_crystal_patch/land_7
execute if score SpiralCount ExtraLogic5556 matches 0.. if score SpiralLength ExtraLogic5556 matches 0.. if score Random RandomOne5556 matches 666..999 positioned ~-1 ~8 ~6 run function cm_module_megamix:structures/end_crystal_patch/land_7

execute if score SpiralCount ExtraLogic5556 matches ..-1 if score SpiralLength ExtraLogic5556 matches 0.. positioned ~ ~ ~ run function cm_module_megamix:structures/end_crystal_patch/loop_8_start