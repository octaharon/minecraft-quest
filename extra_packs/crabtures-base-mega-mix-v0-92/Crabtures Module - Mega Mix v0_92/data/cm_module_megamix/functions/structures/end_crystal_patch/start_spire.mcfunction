fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:dark_obelisk",integrity:1.0,posX:-2,posY:-5,posZ:-2,rotation:"NONE"} replace
fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace
fill ~ ~ ~ ~ ~1 ~ minecraft:air replace

function structures_crabmaster:randomizers/randomizer_1
scoreboard players operation SpiralArms ExtraLogic5556 = Random RandomOne5556

function structures_crabmaster:randomizers/randomizer_1
execute store result score SpiralLength ExtraLogic5556 run scoreboard players add Random RandomOne5556 20
scoreboard players set SpiralMore ExtraLogic5556 -2
execute positioned ~-6 ~ ~3 if score SpiralArms ExtraLogic5556 matches 0..300 run function cm_module_megamix:structures/end_crystal_patch/loop_1_start

function structures_crabmaster:randomizers/randomizer_1
execute store result score SpiralLength ExtraLogic5556 run scoreboard players add Random RandomOne5556 20
scoreboard players set SpiralMore ExtraLogic5556 -2
execute positioned ~6 ~ ~4 if score SpiralArms ExtraLogic5556 matches 200..520 run function cm_module_megamix:structures/end_crystal_patch/loop_3_start

function structures_crabmaster:randomizers/randomizer_1
execute store result score SpiralLength ExtraLogic5556 run scoreboard players add Random RandomOne5556 20
scoreboard players set SpiralMore ExtraLogic5556 -2
execute positioned ~4 ~ ~-6 if score SpiralArms ExtraLogic5556 matches 480..700 run function cm_module_megamix:structures/end_crystal_patch/loop_2_start

function structures_crabmaster:randomizers/randomizer_1
execute store result score SpiralLength ExtraLogic5556 run scoreboard players add Random RandomOne5556 20
scoreboard players set SpiralMore ExtraLogic5556 -2
execute positioned ~-3 ~ ~-6 if score SpiralArms ExtraLogic5556 matches 600..999 run function cm_module_megamix:structures/end_crystal_patch/loop_4_start

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999