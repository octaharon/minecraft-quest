execute store result entity @s Pos[1] double 1 run scoreboard players set YPos Maths5556 31

function structures_crabmaster:randomizers/randomizer_1

execute as @s at @s if score Random RandomOne5556 matches 0..249 run fill ~ 31 ~ ~ 31 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:nether_ship",integrity:1.00,posX:-3,posY:-2,posZ:-13,rotation:"NONE"} replace
execute as @s at @s if score Random RandomOne5556 matches 250..499 run fill ~ 31 ~ ~ 31 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:nether_ship",integrity:1.00,posX:13,posY:-2,posZ:-3,rotation:"CLOCKWISE_90"} replace
execute as @s at @s if score Random RandomOne5556 matches 500..749 run fill ~ 31 ~ ~ 31 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:nether_ship",integrity:1.00,posX:3,posY:-2,posZ:13,rotation:"CLOCKWISE_180"} replace
execute as @s at @s if score Random RandomOne5556 matches 750..999 run fill ~ 31 ~ ~ 31 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:nether_ship",integrity:1.00,posX:-13,posY:-2,posZ:3,rotation:"COUNTERCLOCKWISE_90"} replace

execute as @s at @s run fill ~ 32 ~ ~ 32 ~ minecraft:redstone_block replace

execute as @s at @s run function cm_module_megamix:structures/nether_boat_1

execute as @s at @s run kill @e[type=minecraft:item,distance=..16]
execute as @s at @s run kill @e[distance=..32,tag=Spawner,tag=C5556]
execute as @s at @s run kill @e[distance=..32,tag=Chest,tag=C5556]

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999