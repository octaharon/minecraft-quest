#forceload add ~-16 ~-16 ~16 ~16

execute store result entity @s Pos[1] double 1 run scoreboard players set YPos Maths5556 70

function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..249 run fill ~ 63 ~ ~ 63 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:tower_water_crumbled",integrity:0.99,posX:-5,posY:-3,posZ:-5,rotation:"NONE"} replace
execute if score Random RandomOne5556 matches 250..499 run fill ~ 63 ~ ~ 63 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:tower_water_crumbled",integrity:0.9,posX:-5,posY:-3,posZ:-5,rotation:"NONE"} replace
execute if score Random RandomOne5556 matches 500..749 run fill ~ 63 ~ ~ 63 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:tower_water_ruined",integrity:0.99,posX:-5,posY:-3,posZ:-5,rotation:"NONE"} replace
execute if score Random RandomOne5556 matches 750..999 run fill ~ 63 ~ ~ 63 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:tower_water_ruined",integrity:0.9,posX:-5,posY:-3,posZ:-5,rotation:"NONE"} replace

fill ~ 64 ~ ~ 64 ~ minecraft:redstone_block replace
fill ~ 62 ~ ~ 64 ~ minecraft:mossy_cobblestone_wall replace

scoreboard players set Base TimerOne5556 0
execute positioned ~ 61 ~ run function cm_module_megamix:structures/towers/water_base

kill @e[type=minecraft:item,distance=..16]

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999