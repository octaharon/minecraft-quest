forceload add ~-9 ~-9 ~9 ~9

function structures_crabmaster:randomizers/randomizer_2

execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air if score Random RandomTwo5556 matches 0..249 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:ruined_wall",integrity:1.00,posX:-8,posY:-7,posZ:0,rotation:"NONE"} replace
execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air if score Random RandomTwo5556 matches 250..499 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:ruined_wall",integrity:0.98,posX:0,posY:-7,posZ:-8,rotation:"CLOCKWISE_90"} replace
execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air if score Random RandomTwo5556 matches 500..749 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:ruined_wall",integrity:0.96,posX:8,posY:-7,posZ:0,rotation:"CLOCKWISE_180"} replace
execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air if score Random RandomTwo5556 matches 750..999 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:ruined_wall",integrity:0.94,posX:0,posY:-7,posZ:8,rotation:"COUNTERCLOCKWISE_90"} replace

execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air run fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace
execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air run fill ~ ~ ~ ~ ~1 ~ minecraft:air replace

kill @e[type=minecraft:item,distance=..16]

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999