forceload add ~-8 ~-8 ~8 ~8

execute if score BiomeRegion BiomeRegion5556 matches 0..5 unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air run fill ~ ~-3 ~ ~ ~-3 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:grass_base_small",integrity:1.00,posX:-3,posY:-11,posZ:-3,rotation:"NONE"} replace
execute if score BiomeRegion BiomeRegion5556 matches 6..7 unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air run fill ~ ~-3 ~ ~ ~-3 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:sand_base_small",integrity:1.00,posX:-3,posY:-11,posZ:-3,rotation:"NONE"} replace
execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air run fill ~ ~-2 ~ ~ ~-2 ~ minecraft:redstone_block replace

function structures_crabmaster:randomizers/randomizer_2

execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air if score Random RandomTwo5556 matches 0..249 run fill ~ ~-2 ~ ~ ~-2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:wagon_ruined",integrity:1.00,posX:-3,posY:0,posZ:-3,rotation:"NONE"} replace
execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air if score Random RandomTwo5556 matches 250..499 run fill ~ ~-2 ~ ~ ~-2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:wagon_ruined",integrity:0.99,posX:3,posY:0,posZ:-3,rotation:"CLOCKWISE_90"} replace
execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air if score Random RandomTwo5556 matches 500..749 run fill ~ ~-2 ~ ~ ~-2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:wagon_ruined",integrity:0.98,posX:3,posY:0,posZ:3,rotation:"CLOCKWISE_180"} replace
execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air if score Random RandomTwo5556 matches 750..999 run fill ~ ~-2 ~ ~ ~-2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:wagon_ruined",integrity:0.97,posX:-3,posY:0,posZ:3,rotation:"COUNTERCLOCKWISE_90"} replace

execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air run fill ~ ~-1 ~ ~ ~-1 ~ minecraft:redstone_block replace
execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air run fill ~ ~-2 ~ ~ ~-1 ~ minecraft:coarse_dirt replace


kill @e[type=minecraft:item,distance=..16]

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999
scoreboard players set Landed ExtraLogic5556 1