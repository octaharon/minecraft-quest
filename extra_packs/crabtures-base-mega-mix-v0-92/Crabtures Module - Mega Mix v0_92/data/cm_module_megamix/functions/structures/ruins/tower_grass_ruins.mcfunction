#forceload add ~-16 ~-16 ~16 ~16

function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..249 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:tower_grass_crumbled",integrity:0.99,posX:-5,posY:-5,posZ:-5,rotation:"NONE"} replace
execute if score Random RandomOne5556 matches 250..499 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:tower_grass_crumbled",integrity:0.9,posX:-5,posY:-5,posZ:-5,rotation:"NONE"} replace
execute if score Random RandomOne5556 matches 500..749 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:tower_grass_ruined",integrity:0.99,posX:-5,posY:-5,posZ:-5,rotation:"NONE"} replace
execute if score Random RandomOne5556 matches 750..999 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:tower_grass_ruined",integrity:0.9,posX:-5,posY:-5,posZ:-5,rotation:"NONE"} replace

fill ~ ~3 ~ ~ ~3 ~ minecraft:redstone_block replace
fill ~ ~2 ~ ~ ~3 ~ minecraft:air replace

scoreboard players set Base TimerOne5556 0
execute positioned ~ ~-3 ~ run function cm_module_megamix:structures/towers/grass_base

kill @e[type=minecraft:item,distance=..16]

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999