execute positioned ~ ~ ~ unless block ~ ~8 ~ #structures_crabmaster:extra_landing run fill ~ ~7 ~ ~ ~7 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:ocean_vent_10",integrity:1.0,posX:-5,posY:-29,posZ:-5,rotation:"NONE"} replace

execute positioned ~ ~ ~ unless block ~ ~8 ~ #structures_crabmaster:extra_landing run fill ~ ~8 ~ ~ ~8 ~ minecraft:redstone_block replace
execute positioned ~ ~ ~ unless block ~ ~8 ~ #structures_crabmaster:extra_landing run fill ~ ~7 ~ ~ ~8 ~ minecraft:water replace

execute if predicate structures_crabmaster:randomizers/0_95 positioned ~7 ~8 ~3 run function cm_module_megamix:structures/vent_patch/land_up
execute if predicate structures_crabmaster:randomizers/0_95 positioned ~-6 ~8 ~2 run function cm_module_megamix:structures/vent_patch/land_down
execute if predicate structures_crabmaster:randomizers/0_95 positioned ~-3 ~8 ~8 run function cm_module_megamix:structures/vent_patch/land_right
execute if predicate structures_crabmaster:randomizers/0_95 positioned ~-3 ~8 ~-9 run function cm_module_megamix:structures/vent_patch/land_left

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999