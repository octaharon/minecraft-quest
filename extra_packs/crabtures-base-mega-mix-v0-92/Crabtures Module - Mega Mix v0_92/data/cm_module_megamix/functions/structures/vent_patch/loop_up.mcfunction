forceload add ~-8 ~-8 ~8 ~8

execute positioned ~ ~ ~ unless block ~ ~8 ~ #structures_crabmaster:extra_landing run function cm_module_megamix:structures/vent_patch/spawn_1


execute if predicate structures_crabmaster:randomizers/0_15 positioned ~7 ~8 ~4 run function cm_module_megamix:structures/vent_patch/land_up
execute if predicate structures_crabmaster:randomizers/0_25 positioned ~12 ~8 ~-5 run function cm_module_megamix:structures/vent_patch/land_up
execute if predicate structures_crabmaster:randomizers/0_15 positioned ~4 ~8 ~6 run function cm_module_megamix:structures/vent_patch/land_up
execute if predicate structures_crabmaster:randomizers/0_25 positioned ~-3 ~8 ~11 run function cm_module_megamix:structures/vent_patch/land_up