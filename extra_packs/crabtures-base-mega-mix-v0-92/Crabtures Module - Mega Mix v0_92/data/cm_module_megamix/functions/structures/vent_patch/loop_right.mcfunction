forceload add ~-8 ~-8 ~8 ~8

execute positioned ~ ~ ~ unless block ~ ~8 ~ #structures_crabmaster:extra_landing run function cm_module_megamix:structures/vent_patch/spawn_1


execute if predicate structures_crabmaster:randomizers/0_15 positioned ~-6 ~8 ~4 run function cm_module_megamix:structures/vent_patch/land_right
execute if predicate structures_crabmaster:randomizers/0_25 positioned ~-11 ~8 ~-3 run function cm_module_megamix:structures/vent_patch/land_right
execute if predicate structures_crabmaster:randomizers/0_15 positioned ~5 ~8 ~7 run function cm_module_megamix:structures/vent_patch/land_right
execute if predicate structures_crabmaster:randomizers/0_25 positioned ~-4 ~8 ~12 run function cm_module_megamix:structures/vent_patch/land_right