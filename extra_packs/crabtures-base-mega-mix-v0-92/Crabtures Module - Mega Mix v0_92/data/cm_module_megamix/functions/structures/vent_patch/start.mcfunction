execute positioned ~ ~ ~ unless block ~ ~8 ~ #structures_crabmaster:extra_landing run function cm_module_megamix:structures/vent_patch/vent_spawn_1

execute if predicate structures_crabmaster:randomizers/0_75 positioned ~5 ~8 ~1 run function cm_module_megamix:structures/vent_patch/land_up
execute if predicate structures_crabmaster:randomizers/0_75 positioned ~-4 ~8 ~2 run function cm_module_megamix:structures/vent_patch/land_down
execute if predicate structures_crabmaster:randomizers/0_75 positioned ~-3 ~8 ~7 run function cm_module_megamix:structures/vent_patch/land_right
execute if predicate structures_crabmaster:randomizers/0_75 positioned ~-1 ~8 ~-8 run function cm_module_megamix:structures/vent_patch/land_left

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999