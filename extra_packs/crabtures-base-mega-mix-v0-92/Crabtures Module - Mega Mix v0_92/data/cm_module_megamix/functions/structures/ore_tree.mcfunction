function structures_crabmaster:randomizers/randomizer_1

execute as @s at @s if score Random RandomOne5556 matches 0..100 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:ore_island_1",integrity:1.00,posX:-2,posY:-3,posZ:-2,rotation:"NONE"} replace
execute as @s at @s if score Random RandomOne5556 matches 101..399 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:ore_island_2",integrity:1.00,posX:-2,posY:-3,posZ:-2,rotation:"NONE"} replace
execute as @s at @s if score Random RandomOne5556 matches 400..799 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:ore_island_3",integrity:1.00,posX:-2,posY:-3,posZ:-2,rotation:"NONE"} replace
execute as @s at @s if score Random RandomOne5556 matches 800..999 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:ore_island_4",integrity:1.00,posX:-2,posY:-3,posZ:-2,rotation:"NONE"} replace

execute as @s at @s run fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace

kill @e[type=minecraft:item,distance=..16]

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999