forceload add ~-8 ~-8 ~8 ~8

function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..124 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:rogue_portal_1",integrity:1.00,posX:-4,posY:-6,posZ:-4,rotation:"NONE"} replace
execute if score Random RandomOne5556 matches 125..249 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:rogue_portal_1",integrity:1.00,posX:4,posY:-6,posZ:-4,rotation:"CLOCKWISE_90"} replace
execute if score Random RandomOne5556 matches 250..374 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:rogue_portal_1",integrity:1.00,posX:4,posY:-6,posZ:4,rotation:"CLOCKWISE_180"} replace
execute if score Random RandomOne5556 matches 375..499 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:rogue_portal_1",integrity:1.00,posX:-4,posY:-6,posZ:4,rotation:"COUNTERCLOCKWISE_90"} replace

execute if score Random RandomOne5556 matches 500..624 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:rogue_portal_2",integrity:1.00,posX:-4,posY:-6,posZ:-4,rotation:"NONE"} replace
execute if score Random RandomOne5556 matches 625..749 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:rogue_portal_2",integrity:1.00,posX:4,posY:-6,posZ:-4,rotation:"CLOCKWISE_90"} replace
execute if score Random RandomOne5556 matches 750..874 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:rogue_portal_2",integrity:1.00,posX:4,posY:-6,posZ:4,rotation:"CLOCKWISE_180"} replace
execute if score Random RandomOne5556 matches 875..999 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:rogue_portal_2",integrity:1.00,posX:-4,posY:-6,posZ:4,rotation:"COUNTERCLOCKWISE_90"} replace

fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace

kill @e[type=minecraft:item,distance=..16]

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999
scoreboard players set Landed ExtraLogic5556 1