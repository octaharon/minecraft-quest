forceload add ~-12 ~-12 ~12 ~12

fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:nether_dungeon",integrity:1.0,posX:-4,posY:-2,posZ:-4,rotation:"NONE"} replace
fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace


execute positioned ~9 ~ ~ if predicate structures_crabmaster:randomizers/0_25 run function cm_module_megamix:structures/nether_dungeon/up
execute positioned ~ ~ ~-9 if predicate structures_crabmaster:randomizers/0_5 run function cm_module_megamix:structures/nether_dungeon/left

fill ~-1 ~2 ~4 ~1 ~4 ~5 minecraft:air replace

function structures_crabmaster:randomizers/randomizer_1
scoreboard players operation Loot ExtraLogic5556 += Random RandomOne5556
function structures_crabmaster:randomizers/randomizer_1
scoreboard players operation Mobs ExtraLogic5556 += Random RandomOne5556

function structures_crabmaster:randomizers/randomizer_1

execute if score Mobs ExtraLogic5556 matches 1500..2100 if score Random RandomOne5556 matches 0..400 run fill ~ ~2 ~ ~ ~2 ~ minecraft:spawner{SpawnData:{id:blaze}} replace
execute if score Mobs ExtraLogic5556 matches 1500..2100 if score Random RandomOne5556 matches 401..900 run fill ~ ~2 ~ ~ ~2 ~ minecraft:spawner{SpawnData:{id:magma_cube}} replace
execute if score Mobs ExtraLogic5556 matches 1500..2100 if score Random RandomOne5556 matches 901..999 run fill ~ ~2 ~ ~ ~2 ~ minecraft:spawner{SpawnData:{id:wither_skeleton}} replace

function structures_crabmaster:randomizers/randomizer_1

execute if score Loot ExtraLogic5556 matches 1000..1500 if score Random RandomOne5556 matches 0..249 run fill ~-3 ~2 ~-3 ~-3 ~2 ~-3 minecraft:chest[facing=south]{LootTable:"minecraft:chests/nether_bridge"} replace
execute if score Loot ExtraLogic5556 matches 1000..1500 if score Random RandomOne5556 matches 250..499 run fill ~-3 ~2 ~3 ~-3 ~2 ~3 minecraft:chest[facing=east]{LootTable:"minecraft:chests/nether_bridge"} replace
execute if score Loot ExtraLogic5556 matches 1000..1500 if score Random RandomOne5556 matches 500..749 run fill ~3 ~2 ~3 ~3 ~2 ~3 minecraft:chest[facing=north]{LootTable:"minecraft:chests/nether_bridge"} replace
execute if score Loot ExtraLogic5556 matches 1000..1500 if score Random RandomOne5556 matches 750..999 run fill ~3 ~2 ~-3 ~3 ~2 ~-3 minecraft:chest[facing=west]{LootTable:"minecraft:chests/nether_bridge"} replace

function structures_crabmaster:randomizers/randomizer_1
execute positioned ~ ~6 ~ if score Random RandomOne5556 matches 0..300 run function cm_module_megamix:structures/nether_dungeon/chain
execute positioned ~ ~6 ~ if score Random RandomOne5556 matches 301..500 run function cm_module_megamix:structures/nether_dungeon/thin_chain

kill @e[type=minecraft:item,distance=..16]

execute if score Loot ExtraLogic5556 matches 1000.. run scoreboard players set Loot ExtraLogic5556 0
execute if score Mobs ExtraLogic5556 matches 1500.. run scoreboard players set Mobs ExtraLogic5556 0

scoreboard players set Random RandomOne5556 99999
