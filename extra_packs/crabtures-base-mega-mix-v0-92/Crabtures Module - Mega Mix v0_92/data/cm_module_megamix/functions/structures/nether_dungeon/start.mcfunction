forceload add ~-12 ~-12 ~12 ~12

scoreboard players set Loot ExtraLogic5556 0
scoreboard players set Mobs ExtraLogic5556 0

fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:nether_dungeon",integrity:1.0,posX:-4,posY:-2,posZ:-4,rotation:"NONE"} replace
fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace

 
execute positioned ~9 ~ ~ if predicate structures_crabmaster:randomizers/0_75 run function cm_module_megamix:structures/nether_dungeon/up
execute positioned ~ ~ ~9 if predicate structures_crabmaster:randomizers/0_75 run function cm_module_megamix:structures/nether_dungeon/right
execute positioned ~-9 ~ ~ if predicate structures_crabmaster:randomizers/0_75 run function cm_module_megamix:structures/nether_dungeon/down
execute positioned ~ ~ ~-9 if predicate structures_crabmaster:randomizers/0_75 run function cm_module_megamix:structures/nether_dungeon/left

fill ~ ~2 ~ ~ ~2 ~ minecraft:spawner{SpawnData:{id:blaze}} replace

function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..249 run fill ~-3 ~2 ~-3 ~-3 ~2 ~-3 minecraft:chest[facing=south]{LootTable:"minecraft:chests/nether_bridge"} replace
execute if score Random RandomOne5556 matches 250..499 run fill ~-3 ~2 ~3 ~-3 ~2 ~3 minecraft:chest[facing=east]{LootTable:"minecraft:chests/nether_bridge"} replace
execute if score Random RandomOne5556 matches 500..749 run fill ~3 ~2 ~3 ~3 ~2 ~3 minecraft:chest[facing=north]{LootTable:"minecraft:chests/nether_bridge"} replace
execute if score Random RandomOne5556 matches 750..999 run fill ~3 ~2 ~-3 ~3 ~2 ~-3 minecraft:chest[facing=west]{LootTable:"minecraft:chests/nether_bridge"} replace

execute positioned ~ ~6 ~ run function cm_module_megamix:structures/nether_dungeon/chain

kill @e[type=minecraft:item,distance=..16]

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999
