execute positioned ~ ~ ~ unless block ~ ~-3 ~ #structures_crabmaster:water_lava unless block ~ ~ ~ #structures_crabmaster:water_lava unless block ~ ~4 ~ #structures_crabmaster:water_lava run function cm_module_megamix:structures/clay_patch/spawn_1

execute if predicate structures_crabmaster:randomizers/0_9 positioned ~4 ~2 ~1 run function cm_module_megamix:structures/clay_patch/land_up
execute if predicate structures_crabmaster:randomizers/0_9 positioned ~-3 ~2 ~1 run function cm_module_megamix:structures/clay_patch/land_down
execute if predicate structures_crabmaster:randomizers/0_9 positioned ~-2 ~2 ~4 run function cm_module_megamix:structures/clay_patch/land_right
execute if predicate structures_crabmaster:randomizers/0_9 positioned ~ ~2 ~-4 run function cm_module_megamix:structures/clay_patch/land_left

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999