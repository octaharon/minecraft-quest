function structures_crabmaster:randomizers/randomizer_1

execute positioned ~8 ~8 ~9 if score Random RandomOne5556 matches 0..249 run function cm_module_megamix:structures/small_parts/campfire_2
execute positioned ~-9 ~8 ~7 if score Random RandomOne5556 matches 250..499 run function cm_module_megamix:structures/small_parts/campfire_2
execute positioned ~-11 ~8 ~-10 if score Random RandomOne5556 matches 500..749 run function cm_module_megamix:structures/small_parts/campfire_2
execute positioned ~9 ~8 ~-8 if score Random RandomOne5556 matches 750..999 run function cm_module_megamix:structures/small_parts/campfire_2