forceload add ~-8 ~-8 ~8 ~8

execute if score BiomeRegion BiomeRegion5556 matches 0..5 unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air run fill ~ ~-3 ~ ~ ~-3 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:grass_base_medium",integrity:1.00,posX:-5,posY:-12,posZ:-5,rotation:"NONE"} replace
execute if score BiomeRegion BiomeRegion5556 matches 6..7 unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air run fill ~ ~-3 ~ ~ ~-3 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:sand_base_medium",integrity:1.00,posX:-5,posY:-12,posZ:-5,rotation:"NONE"} replace
execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air run fill ~ ~-2 ~ ~ ~-2 ~ minecraft:redstone_block replace

function structures_crabmaster:randomizers/randomizer_2

execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air if score Random RandomTwo5556 matches 0..249 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:wagon",integrity:1.00,posX:-3,posY:-2,posZ:-4,rotation:"NONE"} replace
execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air if score Random RandomTwo5556 matches 250..499 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:wagon",integrity:1.00,posX:4,posY:-2,posZ:-3,rotation:"CLOCKWISE_90"} replace
execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air if score Random RandomTwo5556 matches 500..749 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:wagon",integrity:1.00,posX:3,posY:-2,posZ:4,rotation:"CLOCKWISE_180"} replace
execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air if score Random RandomTwo5556 matches 750..999 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:wagon",integrity:1.00,posX:-4,posY:-2,posZ:3,rotation:"COUNTERCLOCKWISE_90"} replace

execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air run fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace

execute as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..16] at @s run summon minecraft:villager
execute as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..16] at @s if predicate structures_crabmaster:randomizers/0_1 run summon minecraft:villager
execute as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..16] at @s if predicate structures_crabmaster:randomizers/0_05 run summon minecraft:villager ~ ~ ~ {Age:-6000}
execute as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..16] at @s if predicate structures_crabmaster:randomizers/0_025 run summon minecraft:villager ~ ~ ~ {Age:-10000}
execute as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..16] at @s if predicate structures_crabmaster:randomizers/0_01 run summon minecraft:villager ~ ~ ~ {Age:-18000}

kill @e[tag=Spawner,tag=C5556,distance=..16]
kill @e[type=minecraft:item,distance=..16]

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999
scoreboard players set Landed ExtraLogic5556 1