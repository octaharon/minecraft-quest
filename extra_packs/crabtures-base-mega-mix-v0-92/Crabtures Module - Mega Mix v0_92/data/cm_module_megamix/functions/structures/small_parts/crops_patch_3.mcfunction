forceload add ~-8 ~-8 ~8 ~8

function structures_crabmaster:randomizers/randomizer_2

execute if score Random RandomTwo5556 matches 0..500 run function cm_module_megamix:structures/small_parts/crops_patch_3_1
execute if score Random RandomTwo5556 matches 501..749 run function cm_module_megamix:structures/small_parts/crops_patch_3_2
execute if score Random RandomTwo5556 matches 750..999 run function cm_module_megamix:structures/small_parts/crops_patch_3_3

execute positioned ~4 ~6 ~3 if predicate structures_crabmaster:randomizers/0_2 run function cm_module_megamix:structures/small_parts/crops_patch_2
execute positioned ~-3 ~6 ~4 if predicate structures_crabmaster:randomizers/0_2 run function cm_module_megamix:structures/small_parts/crops_patch_2
execute positioned ~-4 ~6 ~-3 if predicate structures_crabmaster:randomizers/0_2 run function cm_module_megamix:structures/small_parts/crops_patch_2
execute positioned ~3 ~6 ~-4 if predicate structures_crabmaster:randomizers/0_2 run function cm_module_megamix:structures/small_parts/crops_patch_2

kill @e[type=minecraft:item,distance=..16]

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999