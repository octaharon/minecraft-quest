function structures_crabmaster:randomizers/randomizer_1

execute positioned ~8 ~8 ~-3 if score Random RandomOne5556 matches 0..249 run function cm_module_megamix:structures/small_parts/camp_table_2
execute positioned ~3 ~8 ~11 if score Random RandomOne5556 matches 250..499 run function cm_module_megamix:structures/small_parts/camp_table_2
execute positioned ~-10 ~8 ~3 if score Random RandomOne5556 matches 500..749 run function cm_module_megamix:structures/small_parts/camp_table_2
execute positioned ~-3 ~8 ~-11 if score Random RandomOne5556 matches 750..999 run function cm_module_megamix:structures/small_parts/camp_table_2