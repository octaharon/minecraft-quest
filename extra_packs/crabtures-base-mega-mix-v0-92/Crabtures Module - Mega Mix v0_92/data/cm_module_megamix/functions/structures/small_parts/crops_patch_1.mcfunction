function structures_crabmaster:randomizers/randomizer_1

execute positioned ~16 ~8 ~5 if score Random RandomOne5556 matches 0..249 run function cm_module_megamix:structures/small_parts/crops_patch_2
execute positioned ~-4 ~8 ~17 if score Random RandomOne5556 matches 250..499 run function cm_module_megamix:structures/small_parts/crops_patch_2
execute positioned ~-17 ~8 ~-3 if score Random RandomOne5556 matches 500..749 run function cm_module_megamix:structures/small_parts/crops_patch_2
execute positioned ~3 ~8 ~-18 if score Random RandomOne5556 matches 750..999 run function cm_module_megamix:structures/small_parts/crops_patch_2