function structures_crabmaster:randomizers/randomizer_1

execute positioned ~4 ~8 ~13 if score Random RandomOne5556 matches 0..249 run function cm_module_megamix:structures/small_parts/outhouse_2
execute positioned ~-12 ~8 ~5 if score Random RandomOne5556 matches 250..499 run function cm_module_megamix:structures/small_parts/outhouse_2
execute positioned ~-14 ~8 ~-4 if score Random RandomOne5556 matches 500..749 run function cm_module_megamix:structures/small_parts/outhouse_2
execute positioned ~1 ~8 ~-12 if score Random RandomOne5556 matches 750..999 run function cm_module_megamix:structures/small_parts/outhouse_2