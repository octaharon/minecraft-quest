forceload add ~-8 ~-8 ~8 ~8

execute if score BiomeRegion BiomeRegion5556 matches 0..5 unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air run fill ~ ~-3 ~ ~ ~-3 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:grass_base_small",integrity:1.00,posX:-3,posY:-11,posZ:-3,rotation:"NONE"} replace
execute if score BiomeRegion BiomeRegion5556 matches 6..7 unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air run fill ~ ~-3 ~ ~ ~-3 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:sand_base_small",integrity:1.00,posX:-3,posY:-11,posZ:-3,rotation:"NONE"} replace
execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air run fill ~ ~-2 ~ ~ ~-2 ~ minecraft:redstone_block replace


execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:small_campfire",integrity:1.00,posX:-2,posY:-5,posZ:-2,rotation:"NONE"} replace

execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava unless block ~ ~-1 ~ minecraft:air run fill ~ ~3 ~ ~ ~3 ~ minecraft:redstone_block replace
#fill ~ ~ ~ ~ ~1 ~ minecraft:air replace

kill @e[type=minecraft:item,distance=..16]

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999