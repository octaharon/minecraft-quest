function structures_crabmaster:randomizers/randomizer_1

scoreboard players set Landed ExtraLogic5556 0

execute positioned ~23 ~8 ~3 if score Random RandomOne5556 matches 0..249 run function cm_module_megamix:structures/small_parts/farm_hay_2
execute positioned ~-5 ~8 ~17 if score Random RandomOne5556 matches 250..499 run function cm_module_megamix:structures/small_parts/farm_hay_2
execute positioned ~-14 ~8 ~-5 if score Random RandomOne5556 matches 500..749 run function cm_module_megamix:structures/small_parts/farm_hay_2
execute positioned ~4 ~8 ~-11 if score Random RandomOne5556 matches 750..999 run function cm_module_megamix:structures/small_parts/farm_hay_2