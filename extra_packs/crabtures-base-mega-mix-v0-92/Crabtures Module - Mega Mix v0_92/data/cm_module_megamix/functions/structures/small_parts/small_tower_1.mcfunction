function structures_crabmaster:randomizers/randomizer_1

execute positioned ~15 ~8 ~13 if score Random RandomOne5556 matches 0..249 run function cm_module_megamix:structures/small_parts/small_tower_2
execute positioned ~-17 ~8 ~11 if score Random RandomOne5556 matches 250..499 run function cm_module_megamix:structures/small_parts/small_tower_2
execute positioned ~-14 ~8 ~-11 if score Random RandomOne5556 matches 500..749 run function cm_module_megamix:structures/small_parts/small_tower_2
execute positioned ~9 ~8 ~-14 if score Random RandomOne5556 matches 750..999 run function cm_module_megamix:structures/small_parts/small_tower_2