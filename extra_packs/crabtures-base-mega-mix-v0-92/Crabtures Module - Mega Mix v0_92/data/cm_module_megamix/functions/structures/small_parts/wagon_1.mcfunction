function structures_crabmaster:randomizers/randomizer_1

scoreboard players set Landed ExtraLogic5556 0

execute positioned ~31 ~12 ~24 if score Random RandomOne5556 matches 0..249 run function cm_module_megamix:structures/small_parts/wagon_2
execute positioned ~-20 ~12 ~37 if score Random RandomOne5556 matches 250..499 run function cm_module_megamix:structures/small_parts/wagon_2
execute positioned ~-34 ~12 ~-19 if score Random RandomOne5556 matches 500..749 run function cm_module_megamix:structures/small_parts/wagon_2
execute positioned ~22 ~12 ~-33 if score Random RandomOne5556 matches 750..999 run function cm_module_megamix:structures/small_parts/wagon_2