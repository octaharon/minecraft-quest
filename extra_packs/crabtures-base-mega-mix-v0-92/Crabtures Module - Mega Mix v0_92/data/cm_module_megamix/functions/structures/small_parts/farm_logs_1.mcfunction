function structures_crabmaster:randomizers/randomizer_1

scoreboard players set Landed ExtraLogic5556 0

execute positioned ~18 ~8 ~-5 if score Random RandomOne5556 matches 0..249 run function cm_module_megamix:structures/small_parts/farm_logs_2
execute positioned ~5 ~8 ~22 if score Random RandomOne5556 matches 250..499 run function cm_module_megamix:structures/small_parts/farm_logs_2
execute positioned ~-27 ~8 ~5 if score Random RandomOne5556 matches 500..749 run function cm_module_megamix:structures/small_parts/farm_logs_2
execute positioned ~-5 ~8 ~-16 if score Random RandomOne5556 matches 750..999 run function cm_module_megamix:structures/small_parts/farm_logs_2