function structures_crabmaster:randomizers/randomizer_1

scoreboard players set Landed ExtraLogic5556 0

execute positioned ~21 ~8 ~4 if score Random RandomOne5556 matches 0..249 run function cm_module_megamix:structures/small_parts/farm_leanto_2
execute positioned ~-6 ~8 ~18 if score Random RandomOne5556 matches 250..499 run function cm_module_megamix:structures/small_parts/farm_leanto_2
execute positioned ~-19 ~8 ~-8 if score Random RandomOne5556 matches 500..749 run function cm_module_megamix:structures/small_parts/farm_leanto_2
execute positioned ~10 ~8 ~-17 if score Random RandomOne5556 matches 750..999 run function cm_module_megamix:structures/small_parts/farm_leanto_2