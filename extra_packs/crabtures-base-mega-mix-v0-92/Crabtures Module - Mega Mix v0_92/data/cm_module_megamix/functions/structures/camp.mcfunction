forceload add ~-16 ~-16 ~16 ~16

execute positioned ~8 ~24 ~24 if predicate structures_crabmaster:randomizers/0_125 run function cm_module_megamix:structures/camp_start
execute positioned ~32 ~24 ~-6 if predicate structures_crabmaster:randomizers/0_125 run function cm_module_megamix:structures/camp_start
execute positioned ~8 ~24 ~-32 if predicate structures_crabmaster:randomizers/0_125 run function cm_module_megamix:structures/camp_start
execute positioned ~-24 ~24 ~-6 if predicate structures_crabmaster:randomizers/0_075 run function cm_module_megamix:structures/camp_start

execute if predicate structures_crabmaster:randomizers/0_99 run function cm_module_megamix:structures/small_parts/campfire_1
execute if predicate structures_crabmaster:randomizers/0_01 run function cm_module_megamix:structures/small_parts/outhouse_1
execute if predicate structures_crabmaster:randomizers/0_01 run function cm_module_megamix:structures/small_parts/small_tower_1
execute positioned ~1 ~ ~1 if predicate structures_crabmaster:randomizers/0_6 run function cm_module_megamix:structures/small_parts/camp_bench_1
execute positioned ~-6 ~ ~-6 if predicate structures_crabmaster:randomizers/0_4 run function cm_module_megamix:structures/small_parts/camp_bench_1
execute positioned ~-5 ~ ~5 if predicate structures_crabmaster:randomizers/0_7 run function cm_module_megamix:structures/small_parts/camp_table_1
execute if predicate structures_crabmaster:randomizers/0_05 run function cm_module_megamix:structures/small_parts/farm_hay_1
execute positioned ~5 ~ ~-2 if predicate structures_crabmaster:randomizers/0_1 run function cm_module_megamix:structures/small_parts/farm_leanto_1
execute if predicate structures_crabmaster:randomizers/0_025 run function cm_module_megamix:structures/small_parts/farm_logs_1
execute if predicate structures_crabmaster:randomizers/0_01 run function cm_module_megamix:structures/small_parts/farm_well_1
execute if predicate structures_crabmaster:randomizers/0_1 run function cm_module_megamix:structures/small_parts/wagon_1
execute if predicate structures_crabmaster:randomizers/0_025 run function cm_module_megamix:structures/ruins/wagon_1

function structures_crabmaster:randomizers/randomizer_1

execute if score BiomeRegion BiomeRegion5556 matches 0..5 unless block ~ ~-1 ~ #structures_crabmaster:water_lava if score Random RandomOne5556 matches 0..249 run fill ~ ~-3 ~ ~ ~-3 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:grass_base_big",integrity:1.00,posX:-4,posY:-14,posZ:-7,rotation:"NONE"} replace
execute if score BiomeRegion BiomeRegion5556 matches 0..5 unless block ~ ~-1 ~ #structures_crabmaster:water_lava if score Random RandomOne5556 matches 250..499 run fill ~ ~-3 ~ ~ ~-3 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:grass_base_big",integrity:1.00,posX:-7,posY:-14,posZ:-4,rotation:"NONE"} replace
execute if score BiomeRegion BiomeRegion5556 matches 0..5 unless block ~ ~-1 ~ #structures_crabmaster:water_lava if score Random RandomOne5556 matches 500..749 run fill ~ ~-3 ~ ~ ~-3 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:grass_base_big",integrity:1.00,posX:-10,posY:-14,posZ:-7,rotation:"NONE"} replace
execute if score BiomeRegion BiomeRegion5556 matches 0..5 unless block ~ ~-1 ~ #structures_crabmaster:water_lava if score Random RandomOne5556 matches 750..999 run fill ~ ~-3 ~ ~ ~-3 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:grass_base_big",integrity:1.00,posX:-7,posY:-14,posZ:-10,rotation:"NONE"} replace

execute if score BiomeRegion BiomeRegion5556 matches 6..7 unless block ~ ~-1 ~ #structures_crabmaster:water_lava if score Random RandomOne5556 matches 0..249 run fill ~ ~-3 ~ ~ ~-3 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:sand_base_big",integrity:1.00,posX:-4,posY:-14,posZ:-7,rotation:"NONE"} replace
execute if score BiomeRegion BiomeRegion5556 matches 6..7 unless block ~ ~-1 ~ #structures_crabmaster:water_lava if score Random RandomOne5556 matches 250..499 run fill ~ ~-3 ~ ~ ~-3 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:sand_base_big",integrity:1.00,posX:-7,posY:-14,posZ:-4,rotation:"NONE"} replace
execute if score BiomeRegion BiomeRegion5556 matches 6..7 unless block ~ ~-1 ~ #structures_crabmaster:water_lava if score Random RandomOne5556 matches 500..749 run fill ~ ~-3 ~ ~ ~-3 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:sand_base_big",integrity:1.00,posX:-10,posY:-14,posZ:-7,rotation:"NONE"} replace
execute if score BiomeRegion BiomeRegion5556 matches 6..7 unless block ~ ~-1 ~ #structures_crabmaster:water_lava if score Random RandomOne5556 matches 750..999 run fill ~ ~-3 ~ ~ ~-3 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:sand_base_big",integrity:1.00,posX:-7,posY:-14,posZ:-10,rotation:"NONE"} replace
execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava run fill ~ ~-2 ~ ~ ~-2 ~ minecraft:redstone_block replace

execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava if score Random RandomOne5556 matches 0..249 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:camp_tent",integrity:1.00,posX:-1,posY:-6,posZ:-3,rotation:"NONE"} replace
execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava if score Random RandomOne5556 matches 250..499 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:camp_tent",integrity:1.00,posX:3,posY:-6,posZ:-1,rotation:"CLOCKWISE_90"} replace
execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava if score Random RandomOne5556 matches 500..749 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:camp_tent",integrity:1.00,posX:1,posY:-6,posZ:3,rotation:"CLOCKWISE_180"} replace
execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava if score Random RandomOne5556 matches 750..999 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:camp_tent",integrity:1.00,posX:-3,posY:-6,posZ:1,rotation:"COUNTERCLOCKWISE_90"} replace

execute unless block ~ ~-1 ~ #structures_crabmaster:water_lava run fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace

execute as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..16] at @s run summon minecraft:villager
execute as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..16] at @s if predicate structures_crabmaster:randomizers/0_2 run summon minecraft:villager
execute as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..16] at @s if predicate structures_crabmaster:randomizers/0_05 run summon minecraft:vindicator

execute as @e[distance=..32,tag=Spawner,tag=C5556] at @s run execute as @e[distance=..1,type=!minecraft:area_effect_cloud,limit=8] at @s run data merge entity @s {CanBreakDoors:1,PersistenceRequired:1}

kill @e[tag=Spawner,tag=C5556,distance=..16]
kill @e[type=minecraft:item,distance=..16]

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999
scoreboard players set Landed ExtraLogic5556 1