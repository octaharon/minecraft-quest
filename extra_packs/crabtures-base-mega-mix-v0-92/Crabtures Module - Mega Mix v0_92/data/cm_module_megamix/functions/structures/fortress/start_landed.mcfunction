scoreboard players set Stairs ExtraLogic5556 0
scoreboard players set Difficulty ExtraLogic5556 8000
scoreboard players set Loot ExtraLogic5556 2000
scoreboard players set LootMin ExtraLogic5556 4000
scoreboard players set Landed ExtraLogic5556 1

execute if score PlayerDir ExtraLogic5556 matches 45..134 positioned ~ ~ ~ run function cm_module_megamix:structures/fortress/down_door
execute if score PlayerDir ExtraLogic5556 matches -45..44 positioned ~ ~ ~ run function cm_module_megamix:structures/fortress/right_door
execute if score PlayerDir ExtraLogic5556 matches -135..-44 positioned ~ ~ ~ run function cm_module_megamix:structures/fortress/up_door
execute if score PlayerDir ExtraLogic5556 matches -180..-134 positioned ~ ~ ~ run function cm_module_megamix:structures/fortress/left_door
execute if score PlayerDir ExtraLogic5556 matches 135..180 positioned ~ ~ ~ run function cm_module_megamix:structures/fortress/left_door

execute if score PlayerDir ExtraLogic5556 matches 45..134 positioned ~9 ~ ~ run function cm_module_megamix:structures/fortress/enter_start_up
execute if score PlayerDir ExtraLogic5556 matches -45..44 positioned ~ ~ ~-9 run function cm_module_megamix:structures/fortress/enter_start_left
execute if score PlayerDir ExtraLogic5556 matches -135..-44 positioned ~-9 ~ ~ run function cm_module_megamix:structures/fortress/enter_start_down
execute if score PlayerDir ExtraLogic5556 matches -180..-134 positioned ~ ~ ~9 run function cm_module_megamix:structures/fortress/enter_start_right
execute if score PlayerDir ExtraLogic5556 matches 135..180 positioned ~ ~ ~9 run function cm_module_megamix:structures/fortress/enter_start_right

scoreboard players add Difficulty ExtraLogic5556 11000
scoreboard players remove LootMin ExtraLogic5556 750
execute as @e[tag=NextFloor,tag=C5556,sort=arbitrary] at @s positioned ~ ~ ~ run function cm_module_megamix:structures/fortress/more_floors
scoreboard players add Difficulty ExtraLogic5556 11000
scoreboard players remove LootMin ExtraLogic5556 750
execute as @e[tag=NextFloor,tag=C5556,sort=arbitrary] at @s positioned ~ ~ ~ run function cm_module_megamix:structures/fortress/more_floors
scoreboard players add Difficulty ExtraLogic5556 11000
scoreboard players remove LootMin ExtraLogic5556 750
execute as @e[tag=NextFloor,tag=C5556,sort=arbitrary] at @s positioned ~ ~ ~ run function cm_module_megamix:structures/fortress/more_floors
scoreboard players add Difficulty ExtraLogic5556 11000
scoreboard players remove LootMin ExtraLogic5556 750
execute as @e[tag=NextFloor,tag=C5556,sort=arbitrary] at @s positioned ~ ~ ~ run function cm_module_megamix:structures/fortress/more_floors
scoreboard players add Difficulty ExtraLogic5556 11000
scoreboard players remove LootMin ExtraLogic5556 750
execute as @e[tag=NextFloor,tag=C5556,sort=arbitrary] at @s positioned ~ ~ ~ run function cm_module_megamix:structures/fortress/more_floors

execute positioned ~ ~ ~ run function cm_module_megamix:structures/fortress/base_check

execute positioned ~ ~ ~ run function cm_module_megamix:structures/fortress/walls_check
execute positioned ~ ~ ~ run function cm_module_megamix:structures/fortress/room_check

kill @e[type=minecraft:item,distance=..16]

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999

#Key
#9 base check - started(blue_wool), finished(red_wool)
#8 scan progress - done(blue_wool)
#7 
#6 loot - yellow_wool
#5 difficulty - blue_wool, green_wool, yellow_wool, orange_wool, red_wool
#4 floor ID - bottom(blue_wool), upper(red_wool)
#3 room ID - blue_wool, red_wool, yellow_wool, lime_wool, white_wool, orange_wool, magenta_wool, light_blue_wool, pink_wool, gray_wool, light_gray_wool, cyan_wool, purple_wool, brown_wool, green_wool, black_wool
#2 area type - entry(red_wool), hallway(lime_wool), room(blue_wool), stairs bottom(orange_wool), stairs top(yellow_wool)
#1 main marker (blue_wool)


#Cross sections
#3 doors - yellow_wool
#2 outer - red_wool
#1 walls - blue_wool

#/execute as @e[type=armor_stand,limit=1,sort=nearest] at @s run function cm_module_megamix:structures/fortress/start
