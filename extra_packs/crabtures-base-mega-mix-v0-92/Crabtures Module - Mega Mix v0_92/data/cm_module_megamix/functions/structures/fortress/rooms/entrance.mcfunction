execute store success score UpDoor ExtraLogic5556 if block ~2 ~2 ~1 minecraft:yellow_wool
execute store success score RightDoor ExtraLogic5556 if block ~1 ~2 ~2 minecraft:yellow_wool
execute store success score DownDoor ExtraLogic5556 if block ~ ~2 ~1 minecraft:yellow_wool
execute store success score LeftDoor ExtraLogic5556 if block ~1 ~2 ~ minecraft:yellow_wool

fill ~2 ~ ~1 ~2 ~ ~1 minecraft:air replace
fill ~1 ~ ~2 ~1 ~ ~2 minecraft:air replace

kill @e[type=!minecraft:player,distance=0..6]
kill @e[type=!minecraft:player,distance=0..6]

#Doors

function structures_crabmaster:randomizers/randomizer_1
execute if score UpDoor ExtraLogic5556 matches 1 run fill ~ ~1 ~ ~ ~1 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_entrance",integrity:1.00,posX:-4,posY:-4,posZ:-7,rotation:"NONE"} replace
execute if score RightDoor ExtraLogic5556 matches 1 run fill ~ ~1 ~ ~ ~1 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_entrance",integrity:1.00,posX:7,posY:-4,posZ:-4,rotation:"CLOCKWISE_90"} replace
execute if score DownDoor ExtraLogic5556 matches 1 run fill ~ ~1 ~ ~ ~1 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_entrance",integrity:1.00,posX:4,posY:-4,posZ:7,rotation:"CLOCKWISE_180"} replace
execute if score LeftDoor ExtraLogic5556 matches 1 run fill ~ ~1 ~ ~ ~1 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_entrance",integrity:1.00,posX:-7,posY:-4,posZ:4,rotation:"COUNTERCLOCKWISE_90"} replace

fill ~ ~ ~ ~ ~ ~ minecraft:redstone_block replace
fill ~ ~1 ~ ~ ~1 ~ minecraft:air replace

#Mobs

execute as @e[sort=random,tag=Spawner,tag=C5556,distance=..8] at @s run execute if predicate structures_crabmaster:randomizers/0_75 run summon minecraft:vindicator ~ ~ ~

execute as @e[sort=random,tag=RSpawner,tag=C5556,distance=..8] at @s run execute if predicate structures_crabmaster:randomizers/0_8 run summon minecraft:pillager ~ ~ ~

execute as @e[distance=..8,tag=C5556] at @s run execute as @e[distance=..1,type=!minecraft:area_effect_cloud,limit=8] at @s run data merge entity @s {CanBreakDoors:1,PersistenceRequired:1}

kill @e[sort=nearest,tag=Spawner,tag=C5556,distance=..16]
kill @e[sort=nearest,tag=RSpawner,tag=C5556,distance=..16]