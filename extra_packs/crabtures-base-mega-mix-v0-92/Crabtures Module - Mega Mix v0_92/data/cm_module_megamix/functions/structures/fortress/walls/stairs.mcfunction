execute if block ~9 ~1 ~ minecraft:lime_wool run fill ~2 ~ ~1 ~2 ~ ~1 minecraft:blue_wool
execute if block ~ ~1 ~9 minecraft:lime_wool run fill ~1 ~ ~2 ~1 ~ ~2 minecraft:blue_wool
execute if block ~-9 ~1 ~ minecraft:lime_wool run fill ~ ~ ~1 ~ ~ ~1 minecraft:blue_wool
execute if block ~ ~1 ~-9 minecraft:lime_wool run fill ~1 ~ ~ ~1 ~ ~ minecraft:blue_wool

execute if block ~9 ~1 ~ minecraft:orange_wool run fill ~2 ~ ~1 ~2 ~ ~1 minecraft:blue_wool
execute if block ~ ~1 ~9 minecraft:orange_wool run fill ~1 ~ ~2 ~1 ~ ~2 minecraft:blue_wool
execute if block ~-9 ~1 ~ minecraft:orange_wool run fill ~ ~ ~1 ~ ~ ~1 minecraft:blue_wool
execute if block ~ ~1 ~-9 minecraft:orange_wool run fill ~1 ~ ~ ~1 ~ ~ minecraft:blue_wool

execute if block ~9 ~1 ~ minecraft:yellow_wool run fill ~2 ~ ~1 ~2 ~ ~1 minecraft:blue_wool
execute if block ~ ~1 ~9 minecraft:yellow_wool run fill ~1 ~ ~2 ~1 ~ ~2 minecraft:blue_wool
execute if block ~-9 ~1 ~ minecraft:yellow_wool run fill ~ ~ ~1 ~ ~ ~1 minecraft:blue_wool
execute if block ~ ~1 ~-9 minecraft:yellow_wool run fill ~1 ~ ~ ~1 ~ ~ minecraft:blue_wool

execute unless block ~9 ~ ~ minecraft:blue_wool run fill ~2 ~1 ~1 ~2 ~1 ~1 minecraft:red_wool
execute unless block ~ ~ ~9 minecraft:blue_wool run fill ~1 ~1 ~2 ~1 ~1 ~2 minecraft:red_wool
execute unless block ~-9 ~ ~ minecraft:blue_wool run fill ~ ~1 ~1 ~ ~1 ~1 minecraft:red_wool
execute unless block ~ ~ ~-9 minecraft:blue_wool run fill ~1 ~1 ~ ~1 ~1 ~ minecraft:red_wool

execute if block ~9 ~1 ~ minecraft:red_wool run fill ~2 ~1 ~1 ~2 ~1 ~1 minecraft:red_wool
execute if block ~ ~1 ~9 minecraft:red_wool run fill ~1 ~1 ~2 ~1 ~1 ~2 minecraft:red_wool
execute if block ~-9 ~1 ~ minecraft:red_wool run fill ~ ~1 ~1 ~ ~1 ~1 minecraft:red_wool
execute if block ~ ~1 ~-9 minecraft:red_wool run fill ~1 ~1 ~ ~1 ~1 ~ minecraft:red_wool

execute if block ~9 ~1 ~ minecraft:lime_wool run fill ~1 ~2 ~1 ~1 ~2 ~1 minecraft:blue_wool
execute if block ~ ~1 ~9 minecraft:lime_wool run fill ~1 ~2 ~1 ~1 ~2 ~1 minecraft:green_wool
execute if block ~-9 ~1 ~ minecraft:lime_wool run fill ~1 ~2 ~1 ~1 ~2 ~1 minecraft:yellow_wool
execute if block ~ ~1 ~-9 minecraft:lime_wool run fill ~1 ~2 ~1 ~1 ~2 ~1 minecraft:orange_wool

execute if block ~9 ~1 ~ minecraft:lime_wool run fill ~1 ~11 ~1 ~1 ~11 ~1 minecraft:blue_wool
execute if block ~ ~1 ~9 minecraft:lime_wool run fill ~1 ~11 ~1 ~1 ~11 ~1 minecraft:green_wool
execute if block ~-9 ~1 ~ minecraft:lime_wool run fill ~1 ~11 ~1 ~1 ~11 ~1 minecraft:yellow_wool
execute if block ~ ~1 ~-9 minecraft:lime_wool run fill ~1 ~11 ~1 ~1 ~11 ~1 minecraft:orange_wool
