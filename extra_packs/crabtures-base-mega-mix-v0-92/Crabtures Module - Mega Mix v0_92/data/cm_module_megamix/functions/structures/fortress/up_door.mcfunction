forceload add ~-8 ~-8 ~8 ~8

fill ~ ~3 ~ ~ ~3 ~ minecraft:blue_wool
fill ~ ~2 ~ ~ ~2 ~ minecraft:blue_wool
fill ~ ~1 ~ ~ ~1 ~ minecraft:red_wool
fill ~ ~ ~ ~ ~ ~ minecraft:blue_wool

fill ~ ~2 ~1 ~ ~2 ~1 minecraft:yellow_wool
fill ~-7 ~2 ~1 ~-7 ~2 ~1 minecraft:yellow_wool

fill ~2 ~ ~1 ~2 ~ ~1 minecraft:blue_wool