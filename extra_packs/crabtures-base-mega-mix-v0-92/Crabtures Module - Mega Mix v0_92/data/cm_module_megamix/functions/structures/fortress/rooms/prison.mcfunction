execute store success score Roof ExtraLogic5556 if block ~1 ~ ~1 minecraft:yellow_wool

execute store success score UpWall ExtraLogic5556 if block ~2 ~ ~1 minecraft:blue_wool
execute store success score RightWall ExtraLogic5556 if block ~1 ~ ~2 minecraft:blue_wool
execute store success score DownWall ExtraLogic5556 if block ~ ~ ~1 minecraft:blue_wool
execute store success score LeftWall ExtraLogic5556 if block ~1 ~ ~ minecraft:blue_wool

execute store success score UpOutWall ExtraLogic5556 if block ~2 ~1 ~1 minecraft:red_wool
execute store success score RightOutWall ExtraLogic5556 if block ~1 ~1 ~2 minecraft:red_wool
execute store success score DownOutWall ExtraLogic5556 if block ~ ~1 ~1 minecraft:red_wool
execute store success score LeftOutWall ExtraLogic5556 if block ~1 ~1 ~ minecraft:red_wool

execute store success score UpDoor ExtraLogic5556 if block ~2 ~2 ~1 minecraft:yellow_wool
execute store success score RightDoor ExtraLogic5556 if block ~1 ~2 ~2 minecraft:yellow_wool
execute store success score DownDoor ExtraLogic5556 if block ~ ~2 ~1 minecraft:yellow_wool
execute store success score LeftDoor ExtraLogic5556 if block ~1 ~2 ~ minecraft:yellow_wool

execute store success score Loot ExtraLogic5556 if block ~ ~5 ~ minecraft:yellow_wool

execute if block ~ ~4 ~ minecraft:blue_wool run scoreboard players set Difficulty ExtraLogic5556 0
execute if block ~ ~4 ~ minecraft:green_wool run scoreboard players set Difficulty ExtraLogic5556 1
execute if block ~ ~4 ~ minecraft:yellow_wool run scoreboard players set Difficulty ExtraLogic5556 2
execute if block ~ ~4 ~ minecraft:orange_wool run scoreboard players set Difficulty ExtraLogic5556 3
execute if block ~ ~4 ~ minecraft:red_wool run scoreboard players set Difficulty ExtraLogic5556 4

execute store success score RoomCenter ExtraLogic5556 if score UpWall ExtraLogic5556 matches 1 if score RightWall ExtraLogic5556 matches 1 if score DownWall ExtraLogic5556 matches 1 if score LeftWall ExtraLogic5556 matches 1
execute store success score RoomSingle ExtraLogic5556 if score UpWall ExtraLogic5556 matches 0 if score RightWall ExtraLogic5556 matches 0 if score DownWall ExtraLogic5556 matches 0 if score LeftWall ExtraLogic5556 matches 0

kill @e[type=!minecraft:player,distance=0..4]
kill @e[type=minecraft:item,distance=0..12]

fill ~-4 ~ ~-4 ~4 ~8 ~4 minecraft:air


#Inner Walls - Corner

execute if score DownWall ExtraLogic5556 matches 1 if score LeftWall ExtraLogic5556 matches 1 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_inner_corner_1",integrity:1.00,posX:-2,posY:-2,posZ:-2,rotation:"CLOCKWISE_180"} replace
execute if score LeftWall ExtraLogic5556 matches 1 if score UpWall ExtraLogic5556 matches 1 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_inner_corner_1",integrity:1.00,posX:2,posY:0,posZ:-2,rotation:"COUNTERCLOCKWISE_90"} replace
execute if score UpWall ExtraLogic5556 matches 1 if score RightWall ExtraLogic5556 matches 1 run fill ~1 ~1 ~ ~1 ~1 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_inner_corner_1",integrity:1.00,posX:1,posY:-1,posZ:2,rotation:"NONE"} replace
execute if score RightWall ExtraLogic5556 matches 1 if score DownWall ExtraLogic5556 matches 1 run fill ~ ~1 ~1 ~ ~1 ~1 minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_inner_corner_1",integrity:1.00,posX:-2,posY:-1,posZ:1,rotation:"CLOCKWISE_90"} replace

fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace
fill ~ ~ ~ ~1 ~2 ~1 minecraft:air replace


#Outer Walls - Roof

execute if score DownOutWall ExtraLogic5556 matches 1 if score Roof ExtraLogic5556 matches 1 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_roof",integrity:1.00,posX:-6,posY:7,posZ:-4,rotation:"NONE"} replace
execute if score LeftOutWall ExtraLogic5556 matches 1 if score Roof ExtraLogic5556 matches 1 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_roof",integrity:1.00,posX:4,posY:9,posZ:-6,rotation:"CLOCKWISE_90"} replace
execute if score UpOutWall ExtraLogic5556 matches 1 if score Roof ExtraLogic5556 matches 1 run fill ~1 ~1 ~ ~1 ~1 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_roof",integrity:1.00,posX:5,posY:8,posZ:4,rotation:"CLOCKWISE_180"} replace
execute if score RightOutWall ExtraLogic5556 matches 1 if score Roof ExtraLogic5556 matches 1 run fill ~ ~1 ~1 ~ ~1 ~1 minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_roof",integrity:1.00,posX:-4,posY:8,posZ:5,rotation:"COUNTERCLOCKWISE_90"} replace

fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace
fill ~ ~ ~ ~1 ~2 ~1 minecraft:air replace


#Outer Walls - Corner

execute if score DownOutWall ExtraLogic5556 matches 1 if score LeftOutWall ExtraLogic5556 matches 1 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_outer_corner",integrity:1.00,posX:-7,posY:-3,posZ:-7,rotation:"NONE"} replace
execute if score LeftOutWall ExtraLogic5556 matches 1 if score UpOutWall ExtraLogic5556 matches 1 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_outer_corner",integrity:1.00,posX:7,posY:-1,posZ:-7,rotation:"CLOCKWISE_90"} replace
execute if score UpOutWall ExtraLogic5556 matches 1 if score RightOutWall ExtraLogic5556 matches 1 run fill ~1 ~1 ~ ~1 ~1 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_outer_corner",integrity:1.00,posX:6,posY:-2,posZ:7,rotation:"CLOCKWISE_180"} replace
execute if score RightOutWall ExtraLogic5556 matches 1 if score DownOutWall ExtraLogic5556 matches 1 run fill ~ ~1 ~1 ~ ~1 ~1 minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_outer_corner",integrity:1.00,posX:-7,posY:-2,posZ:6,rotation:"COUNTERCLOCKWISE_90"} replace

fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace
fill ~ ~ ~ ~1 ~2 ~1 minecraft:air replace


#Outer Walls

execute if score DownOutWall ExtraLogic5556 matches 1 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_outer_1",integrity:1.00,posX:-6,posY:-2,posZ:-4,rotation:"NONE"} replace
execute if score LeftOutWall ExtraLogic5556 matches 1 run fill ~1 ~1 ~ ~1 ~1 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_outer_1",integrity:1.00,posX:3,posY:-1,posZ:-6,rotation:"CLOCKWISE_90"} replace
execute if score UpOutWall ExtraLogic5556 matches 1 run fill ~ ~1 ~1 ~ ~1 ~1 minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_outer_1",integrity:1.00,posX:6,posY:-1,posZ:3,rotation:"CLOCKWISE_180"} replace
execute if score RightOutWall ExtraLogic5556 matches 1 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_outer_1",integrity:1.00,posX:-4,posY:0,posZ:6,rotation:"COUNTERCLOCKWISE_90"} replace

fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace
fill ~ ~ ~ ~1 ~2 ~1 minecraft:air replace

#Inner Outer Walls

execute if score UpOutWall ExtraLogic5556 matches 1 if score UpWall ExtraLogic5556 matches 0 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_inner_1",integrity:1.00,posX:2,posY:-2,posZ:-4,rotation:"NONE"} replace
execute if score RightOutWall ExtraLogic5556 matches 1 if score RightWall ExtraLogic5556 matches 0 run fill ~1 ~1 ~ ~1 ~1 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_inner_1",integrity:1.00,posX:3,posY:-1,posZ:2,rotation:"CLOCKWISE_90"} replace
execute if score DownOutWall ExtraLogic5556 matches 1 if score DownWall ExtraLogic5556 matches 0 run fill ~ ~1 ~1 ~ ~1 ~1 minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_inner_1",integrity:1.00,posX:-2,posY:-1,posZ:3,rotation:"CLOCKWISE_180"} replace
execute if score LeftOutWall ExtraLogic5556 matches 1 if score LeftWall ExtraLogic5556 matches 0 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_inner_1",integrity:1.00,posX:-4,posY:0,posZ:-2,rotation:"COUNTERCLOCKWISE_90"} replace

fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace
fill ~ ~ ~ ~1 ~2 ~1 minecraft:air replace


#Inner Walls

function structures_crabmaster:randomizers/randomizer_1
execute if score UpOutWall ExtraLogic5556 matches 0 if score UpWall ExtraLogic5556 matches 0 if score UpDoor ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 0..999 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_inner_1",integrity:1.00,posX:2,posY:-2,posZ:-4,rotation:"NONE"} replace
execute if score RightOutWall ExtraLogic5556 matches 0 if score RightWall ExtraLogic5556 matches 0 if score RightDoor ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 0..999 run fill ~1 ~1 ~ ~1 ~1 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_inner_1",integrity:1.00,posX:3,posY:-1,posZ:2,rotation:"CLOCKWISE_90"} replace
execute if score DownOutWall ExtraLogic5556 matches 0 if score DownWall ExtraLogic5556 matches 0 if score DownDoor ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 0..999 run fill ~ ~1 ~1 ~ ~1 ~1 minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_inner_1",integrity:1.00,posX:-2,posY:-1,posZ:3,rotation:"CLOCKWISE_180"} replace
execute if score LeftOutWall ExtraLogic5556 matches 0 if score LeftWall ExtraLogic5556 matches 0 if score LeftDoor ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 0..999 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_inner_1",integrity:1.00,posX:-4,posY:0,posZ:-2,rotation:"COUNTERCLOCKWISE_90"} replace

fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace
fill ~ ~ ~ ~1 ~2 ~1 minecraft:air replace


#Doors

function structures_crabmaster:randomizers/randomizer_1
execute if score UpDoor ExtraLogic5556 matches 1 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_door_3",integrity:1.00,posX:2,posY:-2,posZ:-4,rotation:"NONE"} replace
execute if score RightDoor ExtraLogic5556 matches 1 run fill ~1 ~1 ~ ~1 ~1 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_door_3",integrity:1.00,posX:3,posY:-1,posZ:2,rotation:"CLOCKWISE_90"} replace
execute if score DownDoor ExtraLogic5556 matches 1 run fill ~ ~1 ~1 ~ ~1 ~1 minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_door_3",integrity:1.00,posX:-2,posY:-1,posZ:3,rotation:"CLOCKWISE_180"} replace
execute if score LeftDoor ExtraLogic5556 matches 1 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_door_3",integrity:1.00,posX:-4,posY:0,posZ:-2,rotation:"COUNTERCLOCKWISE_90"} replace

fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace
fill ~ ~ ~ ~1 ~2 ~1 minecraft:air replace


#Floor & Ceiling

fill ~-4 ~8 ~-4 ~4 ~8 ~4 minecraft:stone_bricks
fill ~-4 ~ ~-4 ~4 ~ ~4 minecraft:stone_bricks


#Details

function structures_crabmaster:randomizers/randomizer_1
execute if score UpWall ExtraLogic5556 matches 0 if score UpDoor ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 0..300 run fill ~1 ~ ~ ~1 ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_prison_1",integrity:1.00,posX:1,posY:1,posZ:-3,rotation:"NONE"} replace
execute if score UpWall ExtraLogic5556 matches 0 if score UpDoor ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 301..600 run fill ~1 ~ ~ ~1 ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_prison_2",integrity:1.00,posX:1,posY:1,posZ:-3,rotation:"NONE"} replace

function structures_crabmaster:randomizers/randomizer_1
execute if score RightWall ExtraLogic5556 matches 0 if score RightDoor ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 0..300 run fill ~ ~ ~1 ~ ~ ~1 minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_prison_1",integrity:1.00,posX:3,posY:1,posZ:1,rotation:"CLOCKWISE_90"} replace
execute if score RightWall ExtraLogic5556 matches 0 if score RightDoor ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 301..600 run fill ~ ~ ~1 ~ ~ ~1 minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_prison_2",integrity:1.00,posX:3,posY:1,posZ:1,rotation:"CLOCKWISE_90"} replace

function structures_crabmaster:randomizers/randomizer_1
execute if score DownWall ExtraLogic5556 matches 0 if score DownDoor ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 0..300 run fill ~-1 ~ ~ ~-1 ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_prison_1",integrity:1.00,posX:-1,posY:1,posZ:3,rotation:"CLOCKWISE_180"} replace
execute if score DownWall ExtraLogic5556 matches 0 if score DownDoor ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 301..600 run fill ~-1 ~ ~ ~-1 ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_prison_2",integrity:1.00,posX:-1,posY:1,posZ:3,rotation:"CLOCKWISE_180"} replace

function structures_crabmaster:randomizers/randomizer_1
execute if score LeftWall ExtraLogic5556 matches 0 if score LeftDoor ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 0..300 run fill ~ ~ ~-1 ~ ~ ~-1 minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_prison_1",integrity:1.00,posX:-3,posY:1,posZ:-1,rotation:"COUNTERCLOCKWISE_90"} replace
execute if score LeftWall ExtraLogic5556 matches 0 if score LeftDoor ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 301..600 run fill ~ ~ ~-1 ~ ~ ~-1 minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_prison_2",integrity:1.00,posX:-3,posY:1,posZ:-1,rotation:"COUNTERCLOCKWISE_90"} replace

fill ~ ~ ~ ~ ~ ~ minecraft:redstone_block replace
fill ~-4 ~ ~-4 ~4 ~ ~4 minecraft:stone_bricks


#Mobs & Loot

execute as @e[sort=random,tag=VSpawner,tag=C5556,distance=..12] at @s run kill @e[sort=random,tag=Spawner,tag=C5556,distance=..2]

execute as @e[sort=random,tag=Spawner,tag=C5556,distance=..12] at @s run execute if predicate structures_crabmaster:randomizers/0_4 run summon minecraft:vindicator ~ ~ ~
execute as @e[sort=random,tag=VSpawner,tag=C5556,distance=..12] at @s run execute if predicate structures_crabmaster:randomizers/0_15 run summon minecraft:villager ~ ~ ~
execute if score Roof ExtraLogic5556 matches 1 as @e[sort=random,tag=RSpawner,tag=C5556,distance=..12] at @s run execute if predicate structures_crabmaster:randomizers/0_1 run summon minecraft:pillager ~ ~ ~

execute as @e[distance=..8,tag=C5556] at @s run execute as @e[distance=..1,type=!minecraft:area_effect_cloud,limit=8] at @s run data merge entity @s {CanBreakDoors:1,PersistenceRequired:1}

execute if score Loot ExtraLogic5556 matches 1 as @e[limit=1,sort=random,tag=Chest,tag=C5556,distance=..8] at @s run fill ~ ~ ~ ~ ~ ~ minecraft:chest{LootTable:"minecraft:chests/village/village_butcher"} replace


kill @e[sort=nearest,tag=Spawner,tag=C5556,distance=..16]
kill @e[sort=nearest,tag=RSpawner,tag=C5556,distance=..16]
kill @e[sort=nearest,tag=VSpawner,tag=C5556,distance=..16]
kill @e[sort=nearest,tag=Chest,tag=C5556,distance=..16]