forceload add ~-8 ~-8 ~8 ~8

fill ~ ~3 ~ ~ ~3 ~ minecraft:red_wool
fill ~ ~2 ~ ~ ~2 ~ minecraft:yellow_wool
fill ~ ~1 ~ ~ ~1 ~ minecraft:yellow_wool
fill ~ ~ ~ ~ ~ ~ minecraft:blue_wool

scoreboard players set HallDir ExtraLogic5556 0

execute if score HallDir ExtraLogic5556 matches 0 if block ~-9 ~-9 ~ minecraft:blue_wool unless block ~-9 ~ ~ minecraft:blue_wool positioned ~-9 ~ ~ store success score HallDir ExtraLogic5556 run function cm_module_megamix:structures/fortress/upper_floor/start_down
execute if score HallDir ExtraLogic5556 matches 0 if block ~ ~-9 ~9 minecraft:blue_wool unless block ~ ~ ~9 minecraft:blue_wool positioned ~ ~ ~9 store success score HallDir ExtraLogic5556 run function cm_module_megamix:structures/fortress/upper_floor/start_right
execute if score HallDir ExtraLogic5556 matches 0 if block ~ ~-9 ~-9 minecraft:blue_wool unless block ~ ~ ~-9 minecraft:blue_wool positioned ~ ~ ~-9 store success score HallDir ExtraLogic5556 run function cm_module_megamix:structures/fortress/upper_floor/start_left
execute if score HallDir ExtraLogic5556 matches 0 if block ~9 ~-9 ~ minecraft:blue_wool unless block ~9 ~ ~ minecraft:blue_wool positioned ~9 ~ ~ store success score HallDir ExtraLogic5556 run function cm_module_megamix:structures/fortress/upper_floor/start_up

kill @e[type=minecraft:item,distance=..16]

scoreboard players set Random RandomOne5556 99999