execute store success score Roof ExtraLogic5556 if block ~1 ~ ~1 minecraft:yellow_wool

execute store success score UpWall ExtraLogic5556 if block ~2 ~ ~1 minecraft:blue_wool
execute store success score RightWall ExtraLogic5556 if block ~1 ~ ~2 minecraft:blue_wool
execute store success score DownWall ExtraLogic5556 if block ~ ~ ~1 minecraft:blue_wool
execute store success score LeftWall ExtraLogic5556 if block ~1 ~ ~ minecraft:blue_wool

scoreboard players set UpOutWall ExtraLogic5556 0
scoreboard players set RightOutWall ExtraLogic5556 0
scoreboard players set DownOutWall ExtraLogic5556 0
scoreboard players set LeftOutWall ExtraLogic5556 0

execute store success score UpOutWall ExtraLogic5556 unless block ~9 ~1 ~ minecraft:red_wool if block ~2 ~1 ~1 minecraft:red_wool
execute store success score RightOutWall ExtraLogic5556 unless block ~ ~1 ~9 minecraft:red_wool if block ~1 ~1 ~2 minecraft:red_wool
execute store success score DownOutWall ExtraLogic5556 unless block ~-9 ~1 ~ minecraft:red_wool if block ~ ~1 ~1 minecraft:red_wool
execute store success score LeftOutWall ExtraLogic5556 unless block ~ ~1 ~-9 minecraft:red_wool if block ~1 ~1 ~ minecraft:red_wool

execute store success score UpDoor ExtraLogic5556 if block ~2 ~2 ~1 minecraft:yellow_wool
execute store success score RightDoor ExtraLogic5556 if block ~1 ~2 ~2 minecraft:yellow_wool
execute store success score DownDoor ExtraLogic5556 if block ~ ~2 ~1 minecraft:yellow_wool
execute store success score LeftDoor ExtraLogic5556 if block ~1 ~2 ~ minecraft:yellow_wool

execute store success score UpStairs ExtraLogic5556 if block ~1 ~2 ~1 minecraft:blue_wool
execute store success score RightStairs ExtraLogic5556 if block ~1 ~2 ~1 minecraft:green_wool
execute store success score DownStairs ExtraLogic5556 if block ~1 ~2 ~1 minecraft:yellow_wool
execute store success score LeftStairs ExtraLogic5556 if block ~1 ~2 ~1 minecraft:orange_wool

fill ~-4 ~ ~-4 ~4 ~8 ~4 minecraft:air

kill @e[type=!minecraft:player,distance=0..4]
kill @e[type=minecraft:item,distance=0..12]

#Inner Walls - Corner

execute if score DownWall ExtraLogic5556 matches 1 if score LeftWall ExtraLogic5556 matches 1 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_inner_corner",integrity:1.00,posX:-2,posY:-2,posZ:-2,rotation:"CLOCKWISE_180"} replace
execute if score LeftWall ExtraLogic5556 matches 1 if score UpWall ExtraLogic5556 matches 1 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_inner_corner",integrity:1.00,posX:2,posY:0,posZ:-2,rotation:"COUNTERCLOCKWISE_90"} replace
execute if score UpWall ExtraLogic5556 matches 1 if score RightWall ExtraLogic5556 matches 1 run fill ~1 ~1 ~ ~1 ~1 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_inner_corner",integrity:1.00,posX:1,posY:-1,posZ:2,rotation:"NONE"} replace
execute if score RightWall ExtraLogic5556 matches 1 if score DownWall ExtraLogic5556 matches 1 run fill ~ ~1 ~1 ~ ~1 ~1 minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_inner_corner",integrity:1.00,posX:-2,posY:-1,posZ:1,rotation:"CLOCKWISE_90"} replace

fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace
fill ~ ~ ~ ~1 ~2 ~1 minecraft:air replace


#Outer Walls - Roof

execute if score DownOutWall ExtraLogic5556 matches 1 if score Roof ExtraLogic5556 matches 1 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_roof",integrity:1.00,posX:-6,posY:7,posZ:-4,rotation:"NONE"} replace
execute if score LeftOutWall ExtraLogic5556 matches 1 if score Roof ExtraLogic5556 matches 1 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_roof",integrity:1.00,posX:4,posY:9,posZ:-6,rotation:"CLOCKWISE_90"} replace
execute if score UpOutWall ExtraLogic5556 matches 1 if score Roof ExtraLogic5556 matches 1 run fill ~1 ~1 ~ ~1 ~1 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_roof",integrity:1.00,posX:5,posY:8,posZ:4,rotation:"CLOCKWISE_180"} replace
execute if score RightOutWall ExtraLogic5556 matches 1 if score Roof ExtraLogic5556 matches 1 run fill ~ ~1 ~1 ~ ~1 ~1 minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_roof",integrity:1.00,posX:-4,posY:8,posZ:5,rotation:"COUNTERCLOCKWISE_90"} replace

fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace
fill ~ ~ ~ ~1 ~2 ~1 minecraft:air replace


#Outer Walls - Corner

execute if score DownOutWall ExtraLogic5556 matches 1 if score LeftOutWall ExtraLogic5556 matches 1 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_outer_corner",integrity:1.00,posX:-7,posY:-3,posZ:-7,rotation:"NONE"} replace
execute if score LeftOutWall ExtraLogic5556 matches 1 if score UpOutWall ExtraLogic5556 matches 1 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_outer_corner",integrity:1.00,posX:7,posY:-1,posZ:-7,rotation:"CLOCKWISE_90"} replace
execute if score UpOutWall ExtraLogic5556 matches 1 if score RightOutWall ExtraLogic5556 matches 1 run fill ~1 ~1 ~ ~1 ~1 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_outer_corner",integrity:1.00,posX:6,posY:-2,posZ:7,rotation:"CLOCKWISE_180"} replace
execute if score RightOutWall ExtraLogic5556 matches 1 if score DownOutWall ExtraLogic5556 matches 1 run fill ~ ~1 ~1 ~ ~1 ~1 minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_outer_corner",integrity:1.00,posX:-7,posY:-2,posZ:6,rotation:"COUNTERCLOCKWISE_90"} replace

fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace
fill ~ ~ ~ ~1 ~2 ~1 minecraft:air replace


#Outer Walls

function structures_crabmaster:randomizers/randomizer_1
execute if score RightOutWall ExtraLogic5556 matches 1 unless block ~ ~3 ~6 #structures_crabmaster:air_water_plants run scoreboard players remove Random RandomOne5556 700
execute if score UpOutWall ExtraLogic5556 matches 1 unless block ~6 ~3 ~ #structures_crabmaster:air_water_plants run scoreboard players remove Random RandomOne5556 700
execute if score LeftOutWall ExtraLogic5556 matches 1 unless block ~ ~3 ~-6 #structures_crabmaster:air_water_plants run scoreboard players remove Random RandomOne5556 700
execute if score DownOutWall ExtraLogic5556 matches 1 unless block ~-6 ~3 ~ #structures_crabmaster:air_water_plants run scoreboard players remove Random RandomOne5556 700

execute if score DownOutWall ExtraLogic5556 matches 1 if score Random RandomOne5556 matches ..299 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_outer_1",integrity:1.00,posX:-6,posY:-2,posZ:-4,rotation:"NONE"} replace
execute if score DownOutWall ExtraLogic5556 matches 1 if score Random RandomOne5556 matches 300..999 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_outer_2",integrity:1.00,posX:-6,posY:-2,posZ:-4,rotation:"NONE"} replace
execute if score LeftOutWall ExtraLogic5556 matches 1 if score Random RandomOne5556 matches ..299 run fill ~1 ~1 ~ ~1 ~1 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_outer_1",integrity:1.00,posX:3,posY:-1,posZ:-6,rotation:"CLOCKWISE_90"} replace
execute if score LeftOutWall ExtraLogic5556 matches 1 if score Random RandomOne5556 matches 300..999 run fill ~1 ~1 ~ ~1 ~1 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_outer_2",integrity:1.00,posX:3,posY:-1,posZ:-6,rotation:"CLOCKWISE_90"} replace
execute if score UpOutWall ExtraLogic5556 matches 1 if score Random RandomOne5556 matches ..299 run fill ~ ~1 ~1 ~ ~1 ~1 minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_outer_1",integrity:1.00,posX:6,posY:-1,posZ:3,rotation:"CLOCKWISE_180"} replace
execute if score UpOutWall ExtraLogic5556 matches 1 if score Random RandomOne5556 matches 300..999 run fill ~ ~1 ~1 ~ ~1 ~1 minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_outer_2",integrity:1.00,posX:6,posY:-1,posZ:3,rotation:"CLOCKWISE_180"} replace
execute if score RightOutWall ExtraLogic5556 matches 1 if score Random RandomOne5556 matches ..299 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_outer_1",integrity:1.00,posX:-4,posY:0,posZ:6,rotation:"COUNTERCLOCKWISE_90"} replace
execute if score RightOutWall ExtraLogic5556 matches 1 if score Random RandomOne5556 matches 300..999 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_outer_2",integrity:1.00,posX:-4,posY:0,posZ:6,rotation:"COUNTERCLOCKWISE_90"} replace

fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace
fill ~ ~ ~ ~1 ~2 ~1 minecraft:air replace

#Inner Outer Walls

execute if score UpOutWall ExtraLogic5556 matches 1 if score UpWall ExtraLogic5556 matches 0 if score Random RandomOne5556 matches ..299 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_inner_1",integrity:1.00,posX:2,posY:-2,posZ:-4,rotation:"NONE"} replace
execute if score UpOutWall ExtraLogic5556 matches 1 if score UpWall ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 300..999 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_inner_2",integrity:1.00,posX:2,posY:-2,posZ:-4,rotation:"NONE"} replace
execute if score RightOutWall ExtraLogic5556 matches 1 if score RightWall ExtraLogic5556 matches 0 if score Random RandomOne5556 matches ..299 run fill ~1 ~1 ~ ~1 ~1 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_inner_1",integrity:1.00,posX:3,posY:-1,posZ:2,rotation:"CLOCKWISE_90"} replace
execute if score RightOutWall ExtraLogic5556 matches 1 if score RightWall ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 300..999 run fill ~1 ~1 ~ ~1 ~1 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_inner_2",integrity:1.00,posX:3,posY:-1,posZ:2,rotation:"CLOCKWISE_90"} replace
execute if score DownOutWall ExtraLogic5556 matches 1 if score DownWall ExtraLogic5556 matches 0 if score Random RandomOne5556 matches ..299 run fill ~ ~1 ~1 ~ ~1 ~1 minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_inner_1",integrity:1.00,posX:-2,posY:-1,posZ:3,rotation:"CLOCKWISE_180"} replace
execute if score DownOutWall ExtraLogic5556 matches 1 if score DownWall ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 300..999 run fill ~ ~1 ~1 ~ ~1 ~1 minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_inner_2",integrity:1.00,posX:-2,posY:-1,posZ:3,rotation:"CLOCKWISE_180"} replace
execute if score LeftOutWall ExtraLogic5556 matches 1 if score LeftWall ExtraLogic5556 matches 0 if score Random RandomOne5556 matches ..299 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_inner_1",integrity:1.00,posX:-4,posY:0,posZ:-2,rotation:"COUNTERCLOCKWISE_90"} replace
execute if score LeftOutWall ExtraLogic5556 matches 1 if score LeftWall ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 300..999 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_inner_2",integrity:1.00,posX:-4,posY:0,posZ:-2,rotation:"COUNTERCLOCKWISE_90"} replace

fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace
fill ~ ~ ~ ~1 ~2 ~1 minecraft:air replace


#Inner Walls

function structures_crabmaster:randomizers/randomizer_1
execute if score UpOutWall ExtraLogic5556 matches 0 if score UpWall ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 0..999 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_inner_3",integrity:1.00,posX:2,posY:-2,posZ:-4,rotation:"NONE"} replace
execute if score RightOutWall ExtraLogic5556 matches 0 if score RightWall ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 0..999 run fill ~1 ~1 ~ ~1 ~1 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_inner_3",integrity:1.00,posX:3,posY:-1,posZ:2,rotation:"CLOCKWISE_90"} replace
execute if score DownOutWall ExtraLogic5556 matches 0 if score DownWall ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 0..999 run fill ~ ~1 ~1 ~ ~1 ~1 minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_inner_3",integrity:1.00,posX:-2,posY:-1,posZ:3,rotation:"CLOCKWISE_180"} replace
execute if score LeftOutWall ExtraLogic5556 matches 0 if score LeftWall ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 0..999 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_wall_inner_3",integrity:1.00,posX:-4,posY:0,posZ:-2,rotation:"COUNTERCLOCKWISE_90"} replace

fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace
fill ~ ~ ~ ~1 ~2 ~1 minecraft:air replace


#Doors 

execute if score UpWall ExtraLogic5556 matches 1 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_door_1",integrity:1.00,posX:2,posY:0,posZ:-4,rotation:"NONE"} replace
execute if score RightWall ExtraLogic5556 matches 1 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_door_1",integrity:1.00,posX:4,posY:0,posZ:2,rotation:"CLOCKWISE_90"} replace
execute if score DownWall ExtraLogic5556 matches 1 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_door_1",integrity:1.00,posX:-2,posY:0,posZ:4,rotation:"CLOCKWISE_180"} replace
execute if score LeftWall ExtraLogic5556 matches 1 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_door_1",integrity:1.00,posX:-4,posY:0,posZ:-2,rotation:"COUNTERCLOCKWISE_90"} replace

fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace
fill ~ ~ ~ ~ ~1 ~ minecraft:air replace

#Floor & Ceiling

fill ~-4 ~8 ~-4 ~4 ~8 ~4 minecraft:stone_bricks
fill ~-4 ~ ~-4 ~4 ~ ~4 minecraft:stone_bricks


# Stairs

execute if score DownStairs ExtraLogic5556 matches 1 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_stairs_bottom",integrity:1.00,posX:-2,posY:0,posZ:-2,rotation:"NONE"} replace
execute if score LeftStairs ExtraLogic5556 matches 1 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_stairs_bottom",integrity:1.00,posX:2,posY:0,posZ:-2,rotation:"CLOCKWISE_90"} replace
execute if score UpStairs ExtraLogic5556 matches 1 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_stairs_bottom",integrity:1.00,posX:2,posY:0,posZ:2,rotation:"CLOCKWISE_180"} replace
execute if score RightStairs ExtraLogic5556 matches 1 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_stairs_bottom",integrity:1.00,posX:-2,posY:0,posZ:2,rotation:"COUNTERCLOCKWISE_90"} replace
fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace

kill @e[sort=nearest,tag=Spawner,tag=C5556,distance=..16]
kill @e[sort=nearest,tag=RSpawner,tag=C5556,distance=..16]
kill @e[sort=nearest,tag=Chest,tag=C5556,distance=..16]