forceload add ~-8 ~-8 ~8 ~8

function structures_crabmaster:randomizers/randomizer_2
scoreboard players operation Difficulty ExtraLogic5556 += Random RandomTwo5556

fill ~ ~4 ~ ~ ~4 ~ minecraft:blue_wool
fill ~ ~3 ~ ~ ~3 ~ minecraft:blue_wool
fill ~ ~2 ~ ~ ~2 ~ minecraft:red_wool
fill ~ ~1 ~ ~ ~1 ~ minecraft:lime_wool
fill ~ ~ ~ ~ ~ ~ minecraft:blue_wool

scoreboard players remove FloorLength ExtraLogic5556 350

execute unless block ~-9 ~ ~ minecraft:blue_wool unless block ~-9 ~-8 ~ minecraft:red_wool if score FloorLength ExtraLogic5556 matches 1.. positioned ~-9 ~ ~ run function cm_module_megamix:structures/fortress/down
execute if score FloorLength ExtraLogic5556 matches 0.. unless block ~ ~-8 ~9 minecraft:red_wool unless block ~ ~ ~9 minecraft:blue_wool if predicate structures_crabmaster:randomizers/0_1 positioned ~ ~ ~9 run function cm_module_megamix:structures/fortress/start_right
execute if score FloorLength ExtraLogic5556 matches 0.. unless block ~ ~-8 ~-9 minecraft:red_wool unless block ~ ~ ~-9 minecraft:blue_wool if predicate structures_crabmaster:randomizers/0_1 positioned ~ ~ ~-9 run function cm_module_megamix:structures/fortress/start_left

execute unless score Stairs ExtraLogic5556 matches 2 store success score Stairs ExtraLogic5556 unless block ~ ~9 ~ minecraft:blue_wool if score FloorLength ExtraLogic5556 matches -2000..-70
execute if score Stairs ExtraLogic5556 matches 1 run fill ~ ~1 ~ ~ ~1 ~ minecraft:orange_wool

function structures_crabmaster:randomizers/randomizer_1
execute unless score Stairs ExtraLogic5556 matches 1 unless block ~ ~ ~9 minecraft:blue_wool positioned ~ ~ ~9 run fill ~1 ~2 ~ ~1 ~2 ~ minecraft:yellow_wool
execute unless score Stairs ExtraLogic5556 matches 1 unless block ~ ~ ~9 minecraft:blue_wool run fill ~1 ~2 ~2 ~1 ~2 ~2 minecraft:yellow_wool
execute unless score Stairs ExtraLogic5556 matches 1 unless block ~ ~ ~9 minecraft:blue_wool positioned ~ ~ ~9 run function cm_module_megamix:structures/fortress/room

function structures_crabmaster:randomizers/randomizer_1
execute unless score Stairs ExtraLogic5556 matches 1 unless block ~ ~ ~-9 minecraft:blue_wool positioned ~ ~ ~-9 run fill ~1 ~2 ~2 ~1 ~2 ~2 minecraft:yellow_wool
execute unless score Stairs ExtraLogic5556 matches 1 unless block ~ ~ ~-9 minecraft:blue_wool run fill ~1 ~2 ~ ~1 ~2 ~ minecraft:yellow_wool
execute unless score Stairs ExtraLogic5556 matches 1 unless block ~ ~ ~-9 minecraft:blue_wool positioned ~ ~ ~-9 run function cm_module_megamix:structures/fortress/room

execute unless block ~-9 ~ ~ minecraft:blue_wool if score FloorLength ExtraLogic5556 matches -69..0 positioned ~-9 ~ ~ run function cm_module_megamix:structures/fortress/down_door

#execute if score Stairs ExtraLogic5556 matches 1 positioned ~ ~9 ~ run function cm_module_megamix:structures/fortress/upper_floor/start_up

execute if score Stairs ExtraLogic5556 matches 1 positioned ~ ~9 ~ run summon minecraft:area_effect_cloud ~ ~ ~ {Duration:2147483647,CustomName:"\"NextFloor\"",Tags:[C5556,NextFloor,Up]}

scoreboard players set Stairs ExtraLogic5556 2

kill @e[type=minecraft:item,distance=..16]

scoreboard players set Random RandomOne5556 99999