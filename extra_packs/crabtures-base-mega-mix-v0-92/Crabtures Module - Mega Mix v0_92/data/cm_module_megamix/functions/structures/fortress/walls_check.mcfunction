fill ~ ~7 ~ ~ ~7 ~ minecraft:orange_wool

execute if block ~ ~9 ~ minecraft:blue_wool unless block ~ ~15 ~ minecraft:orange_wool positioned ~ ~9 ~ run function cm_module_megamix:structures/fortress/walls_check
execute if block ~9 ~ ~ minecraft:blue_wool unless block ~9 ~7 ~ minecraft:orange_wool positioned ~9 ~ ~ run function cm_module_megamix:structures/fortress/walls_check
execute if block ~ ~ ~9 minecraft:blue_wool unless block ~ ~7 ~9 minecraft:orange_wool positioned ~ ~ ~9 run function cm_module_megamix:structures/fortress/walls_check
execute if block ~-9 ~ ~ minecraft:blue_wool unless block ~-9 ~7 ~ minecraft:orange_wool positioned ~-9 ~ ~ run function cm_module_megamix:structures/fortress/walls_check
execute if block ~ ~ ~-9 minecraft:blue_wool unless block ~ ~7 ~-9 minecraft:orange_wool positioned ~ ~ ~-9 run function cm_module_megamix:structures/fortress/walls_check

execute unless block ~ ~9 ~ minecraft:blue_wool run fill ~1 ~ ~1 ~1 ~ ~1 minecraft:yellow_wool

execute if block ~ ~1 ~ minecraft:blue_wool if block ~ ~2 ~ minecraft:blue_wool run function cm_module_megamix:structures/fortress/walls/blue
execute if block ~ ~1 ~ minecraft:blue_wool if block ~ ~2 ~ minecraft:red_wool run function cm_module_megamix:structures/fortress/walls/red
execute if block ~ ~1 ~ minecraft:blue_wool if block ~ ~2 ~ minecraft:yellow_wool run function cm_module_megamix:structures/fortress/walls/yellow
execute if block ~ ~1 ~ minecraft:blue_wool if block ~ ~2 ~ minecraft:lime_wool run function cm_module_megamix:structures/fortress/walls/lime
execute if block ~ ~1 ~ minecraft:blue_wool if block ~ ~2 ~ minecraft:white_wool run function cm_module_megamix:structures/fortress/walls/white
execute if block ~ ~1 ~ minecraft:blue_wool if block ~ ~2 ~ minecraft:orange_wool run function cm_module_megamix:structures/fortress/walls/orange
execute if block ~ ~1 ~ minecraft:blue_wool if block ~ ~2 ~ minecraft:magenta_wool run function cm_module_megamix:structures/fortress/walls/magenta
execute if block ~ ~1 ~ minecraft:blue_wool if block ~ ~2 ~ minecraft:light_blue_wool run function cm_module_megamix:structures/fortress/walls/light_blue
execute if block ~ ~1 ~ minecraft:blue_wool if block ~ ~2 ~ minecraft:pink_wool run function cm_module_megamix:structures/fortress/walls/pink
execute if block ~ ~1 ~ minecraft:blue_wool if block ~ ~2 ~ minecraft:gray_wool run function cm_module_megamix:structures/fortress/walls/gray

execute if block ~ ~1 ~ minecraft:lime_wool run function cm_module_megamix:structures/fortress/walls/halls
execute if block ~ ~1 ~ minecraft:yellow_wool run function cm_module_megamix:structures/fortress/walls/halls
execute if block ~ ~1 ~ minecraft:orange_wool run function cm_module_megamix:structures/fortress/walls/stairs


#execute if block ~ ~1 ~ minecraft:red_wool run function cm_module_megamix:structures/fortress/matches/red_walls
#execute if block ~ ~1 ~ minecraft:orange_wool run function cm_module_megamix:structures/fortress/matches/orange_walls

kill @e[type=minecraft:item,distance=..16]

scoreboard players set Random RandomOne5556 99999