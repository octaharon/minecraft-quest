fill ~ ~7 ~ ~ ~7 ~ minecraft:red_wool

execute if block ~ ~9 ~ minecraft:blue_wool unless block ~ ~15 ~ minecraft:red_wool positioned ~ ~9 ~ run function cm_module_megamix:structures/fortress/room_check
execute if block ~9 ~ ~ minecraft:blue_wool unless block ~9 ~7 ~ minecraft:red_wool positioned ~9 ~ ~ run function cm_module_megamix:structures/fortress/room_check
execute if block ~ ~ ~9 minecraft:blue_wool unless block ~ ~7 ~9 minecraft:red_wool positioned ~ ~ ~9 run function cm_module_megamix:structures/fortress/room_check
execute if block ~-9 ~ ~ minecraft:blue_wool unless block ~-9 ~7 ~ minecraft:red_wool positioned ~-9 ~ ~ run function cm_module_megamix:structures/fortress/room_check
execute if block ~ ~ ~-9 minecraft:blue_wool unless block ~ ~7 ~-9 minecraft:red_wool positioned ~ ~ ~-9 run function cm_module_megamix:structures/fortress/room_check

execute if block ~ ~1 ~ minecraft:blue_wool if block ~ ~2 ~ minecraft:blue_wool run function cm_module_megamix:structures/fortress/rooms/beds
execute if block ~ ~1 ~ minecraft:blue_wool if block ~ ~2 ~ minecraft:red_wool run function cm_module_megamix:structures/fortress/rooms/storage
execute if block ~ ~1 ~ minecraft:blue_wool if block ~ ~2 ~ minecraft:yellow_wool run function cm_module_megamix:structures/fortress/rooms/room
execute if block ~ ~1 ~ minecraft:blue_wool if block ~ ~2 ~ minecraft:lime_wool run function cm_module_megamix:structures/fortress/rooms/dining
execute if block ~ ~1 ~ minecraft:blue_wool if block ~ ~2 ~ minecraft:white_wool run function cm_module_megamix:structures/fortress/rooms/kitchen
execute if block ~ ~1 ~ minecraft:blue_wool if block ~ ~2 ~ minecraft:orange_wool run function cm_module_megamix:structures/fortress/rooms/guardroom
execute if block ~ ~1 ~ minecraft:blue_wool if block ~ ~2 ~ minecraft:magenta_wool run function cm_module_megamix:structures/fortress/rooms/washroom
execute if block ~ ~1 ~ minecraft:blue_wool if block ~ ~2 ~ minecraft:light_blue_wool run function cm_module_megamix:structures/fortress/rooms/library
execute if block ~ ~1 ~ minecraft:blue_wool if block ~ ~2 ~ minecraft:pink_wool run function cm_module_megamix:structures/fortress/rooms/prison
execute if block ~ ~1 ~ minecraft:blue_wool if block ~ ~2 ~ minecraft:gray_wool run function cm_module_megamix:structures/fortress/rooms/furnace

#execute if block ~ ~1 ~ minecraft:blue_wool if block ~ ~2 ~ minecraft:blue_wool run function cm_module_megamix:structures/fortress/rooms/beds
#execute if block ~ ~1 ~ minecraft:blue_wool if block ~ ~2 ~ minecraft:red_wool run function cm_module_megamix:structures/fortress/rooms/storage
#execute if block ~ ~1 ~ minecraft:blue_wool if block ~ ~2 ~ minecraft:yellow_wool run function cm_module_megamix:structures/fortress/rooms/room
#execute if block ~ ~1 ~ minecraft:blue_wool if block ~ ~2 ~ minecraft:lime_wool run function cm_module_megamix:structures/fortress/rooms/dining
#execute if block ~ ~1 ~ minecraft:blue_wool if block ~ ~2 ~ minecraft:white_wool run function cm_module_megamix:structures/fortress/rooms/kitchen
#execute if block ~ ~1 ~ minecraft:blue_wool if block ~ ~2 ~ minecraft:orange_wool run function cm_module_megamix:structures/fortress/rooms/guardroom
#execute if block ~ ~1 ~ minecraft:blue_wool if block ~ ~2 ~ minecraft:magenta_wool run function cm_module_megamix:structures/fortress/rooms/washroom
#execute if block ~ ~1 ~ minecraft:blue_wool if block ~ ~2 ~ minecraft:light_blue_wool run function cm_module_megamix:structures/fortress/rooms/library
#execute if block ~ ~1 ~ minecraft:blue_wool if block ~ ~2 ~ minecraft:pink_wool run function cm_module_megamix:structures/fortress/rooms/prison
#execute if block ~ ~1 ~ minecraft:blue_wool if block ~ ~2 ~ minecraft:gray_wool run function cm_module_megamix:structures/fortress/rooms/furnace

execute if block ~ ~1 ~ minecraft:lime_wool run function cm_module_megamix:structures/fortress/rooms/halls
execute if block ~ ~1 ~ minecraft:yellow_wool run function cm_module_megamix:structures/fortress/rooms/stairs_top
execute if block ~ ~1 ~ minecraft:orange_wool run function cm_module_megamix:structures/fortress/rooms/stairs_bottom
execute if block ~ ~1 ~ minecraft:red_wool run function cm_module_megamix:structures/fortress/rooms/entrance

kill @e[type=minecraft:item,distance=..16]

scoreboard players set Random RandomOne5556 99999