scoreboard players set Landed ExtraLogic5556 0

tp @s ~ ~ ~ facing entity @p
execute store result score PlayerDir ExtraLogic5556 run data get entity @s Rotation[0] 1

execute if score PlayerDir ExtraLogic5556 matches 45..134 run function cm_module_megamix:structures/fortress/start_land_up
execute if score PlayerDir ExtraLogic5556 matches -45..44 run function cm_module_megamix:structures/fortress/start_land_left
execute if score PlayerDir ExtraLogic5556 matches -135..-44 run function cm_module_megamix:structures/fortress/start_land_down
execute if score PlayerDir ExtraLogic5556 matches -180..-134 run function cm_module_megamix:structures/fortress/start_land_right
execute if score PlayerDir ExtraLogic5556 matches 135..180 run function cm_module_megamix:structures/fortress/start_land_right