forceload add ~-8 ~-8 ~8 ~8

function structures_crabmaster:randomizers/randomizer_2
scoreboard players operation Difficulty ExtraLogic5556 -= Random RandomTwo5556

execute if score Difficulty ExtraLogic5556 matches ..6999 run fill ~ ~4 ~ ~ ~4 ~ minecraft:blue_wool
execute if score Difficulty ExtraLogic5556 matches 7000..13999 run fill ~ ~4 ~ ~ ~4 ~ minecraft:green_wool
execute if score Difficulty ExtraLogic5556 matches 14000..20999 run fill ~ ~4 ~ ~ ~4 ~ minecraft:yellow_wool
execute if score Difficulty ExtraLogic5556 matches 21000..27999 run fill ~ ~4 ~ ~ ~4 ~ minecraft:orange_wool
execute if score Difficulty ExtraLogic5556 matches 28000.. run fill ~ ~4 ~ ~ ~4 ~ minecraft:red_wool

function structures_crabmaster:randomizers/randomizer_2
scoreboard players operation Loot ExtraLogic5556 += Random RandomTwo5556
execute if score Loot ExtraLogic5556 >= LootMin ExtraLogic5556 run fill ~ ~5 ~ ~ ~5 ~ minecraft:yellow_wool
execute if score Loot ExtraLogic5556 >= LootMin ExtraLogic5556 run scoreboard players operation Loot ExtraLogic5556 -= LootMin ExtraLogic5556

fill ~ ~3 ~ ~ ~3 ~ minecraft:red_wool
execute if score Random RandomOne5556 matches 0..99 run fill ~ ~2 ~ ~ ~2 ~ minecraft:blue_wool
execute if score Random RandomOne5556 matches 100..199 run fill ~ ~2 ~ ~ ~2 ~ minecraft:red_wool
execute if score Random RandomOne5556 matches 200..299 run fill ~ ~2 ~ ~ ~2 ~ minecraft:yellow_wool
execute if score Random RandomOne5556 matches 300..399 run fill ~ ~2 ~ ~ ~2 ~ minecraft:lime_wool
execute if score Random RandomOne5556 matches 400..499 run fill ~ ~2 ~ ~ ~2 ~ minecraft:white_wool
execute if score Random RandomOne5556 matches 500..599 run fill ~ ~2 ~ ~ ~2 ~ minecraft:orange_wool
execute if score Random RandomOne5556 matches 600..699 run fill ~ ~2 ~ ~ ~2 ~ minecraft:magenta_wool
execute if score Random RandomOne5556 matches 700..799 run fill ~ ~2 ~ ~ ~2 ~ minecraft:light_blue_wool
execute if score Random RandomOne5556 matches 800..899 run fill ~ ~2 ~ ~ ~2 ~ minecraft:pink_wool
execute if score Random RandomOne5556 matches 900..999 run fill ~ ~2 ~ ~ ~2 ~ minecraft:gray_wool
fill ~ ~1 ~ ~ ~1 ~ minecraft:blue_wool
fill ~ ~ ~ ~ ~ ~ minecraft:blue_wool

execute if block ~9 ~-9 ~ minecraft:blue_wool unless block ~9 ~-8 ~ minecraft:red_wool unless block ~9 ~ ~ minecraft:blue_wool if predicate structures_crabmaster:randomizers/0_4 positioned ~9 ~ ~ run function cm_module_megamix:structures/fortress/upper_floor/room
execute if block ~ ~-9 ~9 minecraft:blue_wool unless block ~ ~-8 ~9 minecraft:red_wool unless block ~ ~ ~9 minecraft:blue_wool if predicate structures_crabmaster:randomizers/0_4 positioned ~ ~ ~9 run function cm_module_megamix:structures/fortress/upper_floor/room
execute if block ~-9 ~-9 ~ minecraft:blue_wool unless block ~-9 ~-8 ~ minecraft:red_wool unless block ~-9 ~ ~ minecraft:blue_wool if predicate structures_crabmaster:randomizers/0_4 positioned ~-9 ~ ~ run function cm_module_megamix:structures/fortress/upper_floor/room
execute if block ~ ~-9 ~-9 minecraft:blue_wool unless block ~ ~-8 ~-9 minecraft:red_wool unless block ~ ~ ~-9 minecraft:blue_wool if predicate structures_crabmaster:randomizers/0_4 positioned ~ ~ ~-9 run function cm_module_megamix:structures/fortress/upper_floor/room

kill @e[type=minecraft:item,distance=..16]