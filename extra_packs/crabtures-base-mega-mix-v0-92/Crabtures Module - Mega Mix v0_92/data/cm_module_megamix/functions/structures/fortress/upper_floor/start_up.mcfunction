forceload add ~-8 ~-8 ~8 ~8

scoreboard players set Stairs ExtraLogic5556 0

execute if block ~ ~-8 ~ minecraft:orange_wool run scoreboard players set Stairs ExtraLogic5556 1

fill ~ ~3 ~ ~ ~3 ~ minecraft:red_wool
fill ~ ~2 ~ ~ ~2 ~ minecraft:yellow_wool
fill ~ ~1 ~ ~ ~1 ~ minecraft:lime_wool
fill ~ ~ ~ ~ ~ ~ minecraft:blue_wool

execute if score Stairs ExtraLogic5556 matches 1 run fill ~ ~1 ~ ~ ~1 ~ minecraft:yellow_wool

function structures_crabmaster:randomizers/randomizer_1
scoreboard players operation FloorLength ExtraLogic5556 = Random RandomOne5556
scoreboard players add FloorLength ExtraLogic5556 500

execute if block ~9 ~-9 ~ minecraft:blue_wool unless block ~9 ~-8 ~ minecraft:red_wool unless block ~9 ~ ~ minecraft:blue_wool positioned ~9 ~ ~ run function cm_module_megamix:structures/fortress/upper_floor/up
#execute unless block ~ ~ ~9 minecraft:blue_wool if predicate structures_crabmaster:randomizers/0_01 positioned ~ ~ ~9 run function cm_module_megamix:structures/fortress/upper_floor/right
#execute unless block ~ ~ ~-9 minecraft:blue_wool if predicate structures_crabmaster:randomizers/0_01 positioned ~ ~ ~-9 run function cm_module_megamix:structures/fortress/upper_floor/left

function structures_crabmaster:randomizers/randomizer_1
execute unless score Stairs ExtraLogic5556 matches 1 if block ~ ~-9 ~9 minecraft:blue_wool unless block ~ ~ ~9 minecraft:blue_wool positioned ~ ~ ~9 run fill ~1 ~2 ~ ~1 ~2 ~ minecraft:yellow_wool
execute unless score Stairs ExtraLogic5556 matches 1 if block ~ ~-9 ~9 minecraft:blue_wool unless block ~ ~ ~9 minecraft:blue_wool run fill ~1 ~2 ~2 ~1 ~2 ~2 minecraft:yellow_wool
execute unless score Stairs ExtraLogic5556 matches 1 if block ~ ~-9 ~9 minecraft:blue_wool unless block ~ ~ ~9 minecraft:blue_wool positioned ~ ~ ~9 run function cm_module_megamix:structures/fortress/upper_floor/room

function structures_crabmaster:randomizers/randomizer_1
execute unless score Stairs ExtraLogic5556 matches 1 if block ~ ~-9 ~-9 minecraft:blue_wool unless block ~ ~ ~-9 minecraft:blue_wool positioned ~ ~ ~-9 run fill ~1 ~2 ~2 ~1 ~2 ~2 minecraft:yellow_wool
execute unless score Stairs ExtraLogic5556 matches 1 if block ~ ~-9 ~-9 minecraft:blue_wool unless block ~ ~ ~-9 minecraft:blue_wool run fill ~1 ~2 ~ ~1 ~2 ~ minecraft:yellow_wool
execute unless score Stairs ExtraLogic5556 matches 1 if block ~ ~-9 ~-9 minecraft:blue_wool unless block ~ ~ ~-9 minecraft:blue_wool positioned ~ ~ ~-9 run function cm_module_megamix:structures/fortress/upper_floor/room

kill @e[type=minecraft:item,distance=..16]

scoreboard players set Random RandomOne5556 99999