execute if block ~9 ~1 ~ minecraft:blue_wool if block ~9 ~2 ~ minecraft:red_wool run fill ~2 ~ ~1 ~2 ~ ~1 minecraft:blue_wool
execute if block ~ ~1 ~9 minecraft:blue_wool if block ~ ~2 ~9 minecraft:red_wool run fill ~1 ~ ~2 ~1 ~ ~2 minecraft:blue_wool
execute if block ~-9 ~1 ~ minecraft:blue_wool if block ~-9 ~2 ~ minecraft:red_wool run fill ~ ~ ~1 ~ ~ ~1 minecraft:blue_wool
execute if block ~ ~1 ~-9 minecraft:blue_wool if block ~ ~2 ~-9 minecraft:red_wool run fill ~1 ~ ~ ~1 ~ ~ minecraft:blue_wool

execute unless block ~9 ~ ~ minecraft:blue_wool run fill ~2 ~1 ~1 ~2 ~1 ~1 minecraft:red_wool
execute unless block ~ ~ ~9 minecraft:blue_wool run fill ~1 ~1 ~2 ~1 ~1 ~2 minecraft:red_wool
execute unless block ~-9 ~ ~ minecraft:blue_wool run fill ~ ~1 ~1 ~ ~1 ~1 minecraft:red_wool
execute unless block ~ ~ ~-9 minecraft:blue_wool run fill ~1 ~1 ~ ~1 ~1 ~ minecraft:red_wool

execute if block ~9 ~1 ~ minecraft:red_wool run fill ~2 ~1 ~1 ~2 ~1 ~1 minecraft:red_wool
execute if block ~ ~1 ~9 minecraft:red_wool run fill ~1 ~1 ~2 ~1 ~1 ~2 minecraft:red_wool
execute if block ~-9 ~1 ~ minecraft:red_wool run fill ~ ~1 ~1 ~ ~1 ~1 minecraft:red_wool
execute if block ~ ~1 ~-9 minecraft:red_wool run fill ~1 ~1 ~ ~1 ~1 ~ minecraft:red_wool
