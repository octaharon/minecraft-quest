fill ~ ~8 ~ ~ ~8 ~ minecraft:red_wool

execute if block ~9 ~ ~ minecraft:blue_wool unless block ~9 ~8 ~ minecraft:red_wool positioned ~9 ~ ~ run function cm_module_megamix:structures/fortress/base_check
execute if block ~ ~ ~9 minecraft:blue_wool unless block ~ ~8 ~9 minecraft:red_wool positioned ~ ~ ~9 run function cm_module_megamix:structures/fortress/base_check
execute if block ~-9 ~ ~ minecraft:blue_wool unless block ~-9 ~8 ~ minecraft:red_wool positioned ~-9 ~ ~ run function cm_module_megamix:structures/fortress/base_check
execute if block ~ ~ ~-9 minecraft:blue_wool unless block ~ ~8 ~-9 minecraft:red_wool positioned ~ ~ ~-9 run function cm_module_megamix:structures/fortress/base_check

execute unless block ~ ~1 ~ minecraft:red_wool positioned ~ ~-1 ~ run function cm_module_megamix:structures/fortress/base

kill @e[type=minecraft:item,distance=..16]

scoreboard players set Random RandomOne5556 99999