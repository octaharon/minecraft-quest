function structures_crabmaster:randomizers/randomizer_1
execute if score Random RandomOne5556 matches ..250 run summon minecraft:zombie ~ ~ ~
execute if score Random RandomOne5556 matches 251..450 run summon minecraft:skeleton ~ ~ ~
execute if score Random RandomOne5556 matches 451..900 run summon minecraft:vindicator ~ ~ ~
execute if score Random RandomOne5556 matches 901..999 run summon minecraft:pillager ~ ~ ~