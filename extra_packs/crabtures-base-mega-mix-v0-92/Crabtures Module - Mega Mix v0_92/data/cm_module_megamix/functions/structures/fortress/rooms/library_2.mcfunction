function structures_crabmaster:randomizers/randomizer_1
execute if score Random RandomOne5556 matches 0..100 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_library_4",integrity:1.00,posX:0,posY:-1,posZ:-2,rotation:"NONE"} replace
execute if score Random RandomOne5556 matches 101..200 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_library_4",integrity:1.00,posX:2,posY:-1,posZ:0,rotation:"CLOCKWISE_90"} replace
execute if score Random RandomOne5556 matches 201..300 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_library_5",integrity:1.00,posX:0,posY:-1,posZ:-2,rotation:"NONE"} replace
execute if score Random RandomOne5556 matches 301..400 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_library_5",integrity:1.00,posX:2,posY:-1,posZ:0,rotation:"CLOCKWISE_90"} replace
execute if score Random RandomOne5556 matches 401..650 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_library_6",integrity:1.00,posX:-1,posY:-1,posZ:-1,rotation:"NONE"} replace
execute if score Random RandomOne5556 matches 651..999 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_library_7",integrity:1.00,posX:-2,posY:-1,posZ:-2,rotation:"NONE"} replace

execute if score Random RandomOne5556 matches 501..999 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_hanging_light",integrity:1.00,posX:-1,posY:6,posZ:-1,rotation:"NONE"} replace


fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace
execute if score Random RandomOne5556 matches 0..200 run fill ~ ~1 ~ ~ ~1 ~ minecraft:air replace
execute if score Random RandomOne5556 matches 401..999 run fill ~ ~2 ~ ~ ~2 ~ minecraft:air replace
