function structures_crabmaster:randomizers/randomizer_1
execute if score Random RandomOne5556 matches ..100 run summon minecraft:zombie ~ ~ ~
execute if score Random RandomOne5556 matches 101..200 run summon minecraft:skeleton ~ ~ ~
execute if score Random RandomOne5556 matches 201..800 run summon minecraft:vindicator ~ ~ ~
execute if score Random RandomOne5556 matches 801..950 run summon minecraft:pillager ~ ~ ~
execute if score Random RandomOne5556 matches 951..999 run summon minecraft:evoker ~ ~ ~