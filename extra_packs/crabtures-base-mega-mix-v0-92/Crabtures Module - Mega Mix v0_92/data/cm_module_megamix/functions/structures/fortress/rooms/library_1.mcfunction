function structures_crabmaster:randomizers/randomizer_1
execute if score UpDoor ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 0..150 run fill ~1 ~ ~ ~1 ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_library_1",integrity:1.00,posX:2,posY:1,posZ:-1,rotation:"NONE"} replace
execute if score UpDoor ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 151..300 run fill ~1 ~ ~ ~1 ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_library_2",integrity:1.00,posX:2,posY:1,posZ:-1,rotation:"NONE"} replace
execute if score UpDoor ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 301..450 run fill ~1 ~ ~ ~1 ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_library_3",integrity:1.00,posX:1,posY:1,posZ:-1,rotation:"NONE"} replace
execute if score UpDoor ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 451..490 run fill ~1 ~ ~ ~1 ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_library_4",integrity:1.00,posX:2,posY:1,posZ:-2,rotation:"NONE"} replace
execute if score UpDoor ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 491..530 run fill ~1 ~ ~ ~1 ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_library_5",integrity:1.00,posX:1,posY:1,posZ:-2,rotation:"NONE"} replace

function structures_crabmaster:randomizers/randomizer_1
execute if score DownDoor ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 0..150 run fill ~-1 ~ ~ ~-1 ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_library_1",integrity:1.00,posX:-2,posY:1,posZ:1,rotation:"CLOCKWISE_180"} replace
execute if score DownDoor ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 151..300 run fill ~-1 ~ ~ ~-1 ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_library_2",integrity:1.00,posX:-2,posY:1,posZ:1,rotation:"CLOCKWISE_180"} replace
execute if score DownDoor ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 301..450 run fill ~-1 ~ ~ ~-1 ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_library_3",integrity:1.00,posX:-1,posY:1,posZ:1,rotation:"CLOCKWISE_180"} replace
execute if score DownDoor ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 451..490 run fill ~-1 ~ ~ ~-1 ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_library_4",integrity:1.00,posX:-2,posY:1,posZ:2,rotation:"CLOCKWISE_180"} replace
execute if score DownDoor ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 491..530 run fill ~-1 ~ ~ ~-1 ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_library_5",integrity:1.00,posX:-1,posY:1,posZ:2,rotation:"CLOCKWISE_180"} replace

function structures_crabmaster:randomizers/randomizer_1
execute if score RightDoor ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 0..150 run fill ~ ~ ~1 ~ ~ ~1 minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_library_1",integrity:1.00,posX:1,posY:1,posZ:2,rotation:"CLOCKWISE_90"} replace
execute if score RightDoor ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 151..300 run fill ~ ~ ~1 ~ ~ ~1 minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_library_2",integrity:1.00,posX:1,posY:1,posZ:2,rotation:"CLOCKWISE_90"} replace
execute if score RightDoor ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 301..450 run fill ~ ~ ~1 ~ ~ ~1 minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_library_3",integrity:1.00,posX:1,posY:1,posZ:1,rotation:"CLOCKWISE_90"} replace
execute if score RightDoor ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 451..490 run fill ~ ~ ~1 ~ ~ ~1 minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_library_4",integrity:1.00,posX:2,posY:1,posZ:2,rotation:"CLOCKWISE_90"} replace
execute if score RightDoor ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 491..530 run fill ~ ~ ~1 ~ ~ ~1 minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_library_5",integrity:1.00,posX:2,posY:1,posZ:1,rotation:"CLOCKWISE_90"} replace

function structures_crabmaster:randomizers/randomizer_1
execute if score LeftDoor ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 0..150 run fill ~ ~ ~-1 ~ ~ ~-1 minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_library_1",integrity:1.00,posX:-1,posY:1,posZ:-2,rotation:"COUNTERCLOCKWISE_90"} replace
execute if score LeftDoor ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 151..300 run fill ~ ~ ~-1 ~ ~ ~-1 minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_library_2",integrity:1.00,posX:-1,posY:1,posZ:-2,rotation:"COUNTERCLOCKWISE_90"} replace
execute if score LeftDoor ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 301..450 run fill ~ ~ ~-1 ~ ~ ~-1 minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_library_3",integrity:1.00,posX:-1,posY:1,posZ:-1,rotation:"COUNTERCLOCKWISE_90"} replace
execute if score LeftDoor ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 451..490 run fill ~ ~ ~-1 ~ ~ ~-1 minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_library_4",integrity:1.00,posX:-2,posY:1,posZ:-2,rotation:"COUNTERCLOCKWISE_90"} replace
execute if score LeftDoor ExtraLogic5556 matches 0 if score Random RandomOne5556 matches 491..530 run fill ~ ~ ~-1 ~ ~ ~-1 minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_library_5",integrity:1.00,posX:-2,posY:1,posZ:-1,rotation:"COUNTERCLOCKWISE_90"} replace


fill ~ ~ ~ ~ ~ ~ minecraft:redstone_block replace
fill ~ ~ ~ ~ ~ ~ minecraft:air replace


function structures_crabmaster:randomizers/randomizer_1
execute if score Random RandomOne5556 matches 0..250 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_library_1",integrity:1.00,posX:0,posY:-1,posZ:-1,rotation:"NONE"} replace
execute if score Random RandomOne5556 matches 251..500 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_library_2",integrity:1.00,posX:1,posY:-1,posZ:0,rotation:"CLOCKWISE_90"} replace
execute if score Random RandomOne5556 matches 501..700 run fill ~ ~2 ~ ~ ~2 ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_library_6",integrity:1.00,posX:-1,posY:-1,posZ:-1,rotation:"NONE"} replace

execute if score Random RandomOne5556 matches 651..999 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:fortress_hanging_light",integrity:1.00,posX:-1,posY:6,posZ:-1,rotation:"NONE"} replace

fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace
execute if score Random RandomOne5556 matches 501..999 run fill ~ ~2 ~ ~ ~2 ~ minecraft:air replace
execute if score Random RandomOne5556 matches 701..999 run fill ~ ~1 ~ ~ ~1 ~ minecraft:air replace