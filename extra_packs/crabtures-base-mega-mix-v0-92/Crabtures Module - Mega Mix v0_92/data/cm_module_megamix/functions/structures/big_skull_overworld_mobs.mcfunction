execute if score Random RandomTwo5556 matches ..100 run summon minecraft:zombie ~ ~ ~
execute if score Random RandomTwo5556 matches 101..600 run summon minecraft:skeleton ~ ~ ~
execute if score Random RandomTwo5556 matches 601..999 run summon minecraft:vindicator ~ ~ ~

scoreboard players remove Random RandomOne5556 121
scoreboard players remove Random RandomTwo5556 20
execute as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=1..32] at @s if score Random RandomOne5556 matches 0.. run function cm_module_megamix:structures/big_skull_overworld_mobs