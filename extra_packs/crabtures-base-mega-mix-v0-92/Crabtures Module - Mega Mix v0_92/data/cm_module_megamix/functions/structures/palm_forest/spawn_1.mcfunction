function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..249 run function cm_module_megamix:structures/palm_forest/spawn_2
execute if score Random RandomOne5556 matches 250..499 run function cm_module_megamix:structures/palm_forest/spawn_3
execute if score Random RandomOne5556 matches 500..749 run function cm_module_megamix:structures/palm_forest/spawn_4
execute if score Random RandomOne5556 matches 750..999 run function cm_module_megamix:structures/palm_forest/spawn_5

fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace
fill ~ ~-1 ~ ~ ~1 ~ minecraft:jungle_log replace