forceload add ~-10 ~-10 ~10 ~10

scoreboard players set SpawnDir ExtraLogic5556 0

execute store success score SpawnDir ExtraLogic5556 if block ~-6 ~1 ~ #structures_crabmaster:extra_landing run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:big_skull",integrity:1.00,posX:-4,posY:-1,posZ:-4,rotation:"NONE"} replace
execute store success score SpawnDir ExtraLogic5556 if block ~ ~1 ~-6 #structures_crabmaster:extra_landing unless score SpawnDir ExtraLogic5556 matches 1 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:big_skull",integrity:0.99,posX:4,posY:-1,posZ:-4,rotation:"CLOCKWISE_90"} replace
execute store success score SpawnDir ExtraLogic5556 if block ~6 ~1 ~ #structures_crabmaster:extra_landing unless score SpawnDir ExtraLogic5556 matches 1 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:big_skull",integrity:0.98,posX:4,posY:-1,posZ:4,rotation:"CLOCKWISE_180"} replace
execute store success score SpawnDir ExtraLogic5556 if block ~ ~1 ~6 #structures_crabmaster:extra_landing unless score SpawnDir ExtraLogic5556 matches 1 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:big_skull",integrity:0.97,posX:-4,posY:-1,posZ:4,rotation:"COUNTERCLOCKWISE_90"} replace

function structures_crabmaster:randomizers/randomizer_1

execute unless score SpawnDir ExtraLogic5556 matches 1 if score Random RandomOne5556 matches 0..249 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:big_skull",integrity:1.00,posX:-4,posY:-1,posZ:-4,rotation:"NONE"} replace
execute unless score SpawnDir ExtraLogic5556 matches 1 if score Random RandomOne5556 matches 250..499 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:big_skull",integrity:0.99,posX:4,posY:-1,posZ:-4,rotation:"CLOCKWISE_90"} replace
execute unless score SpawnDir ExtraLogic5556 matches 1 if score Random RandomOne5556 matches 500..749 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:big_skull",integrity:0.98,posX:4,posY:-1,posZ:4,rotation:"CLOCKWISE_180"} replace
execute unless score SpawnDir ExtraLogic5556 matches 1 if score Random RandomOne5556 matches 750..999 run fill ~ ~ ~ ~ ~ ~ minecraft:structure_block{mode:"LOAD",name:"cm_module_megamix:big_skull",integrity:0.97,posX:-4,posY:-1,posZ:4,rotation:"COUNTERCLOCKWISE_90"} replace


fill ~ ~1 ~ ~ ~1 ~ minecraft:redstone_block replace
fill ~ ~ ~ ~ ~1 ~ minecraft:air replace

function structures_crabmaster:randomizers/randomizer_1
function structures_crabmaster:randomizers/randomizer_2

execute as @e[limit=1,sort=random,tag=Spawner,tag=C5556,distance=..24] at @s run function cm_module_megamix:structures/big_skull_overworld_mobs

execute as @e[distance=..32,tag=Spawner,tag=C5556] at @s run execute as @e[distance=..1,type=!minecraft:area_effect_cloud,limit=8] at @s run data merge entity @s {CanBreakDoors:1,PersistenceRequired:1}
execute as @e[distance=..32,tag=Spawner,tag=C5556] at @s run execute as @e[distance=..1,type=!minecraft:area_effect_cloud,limit=8] at @s run effect give @s minecraft:fire_resistance 180 1 true

execute as @e[limit=1,sort=random,tag=Chest,tag=C5556,distance=..24] at @s run fill ~ ~ ~ ~ ~ ~ minecraft:chest{LootTable:"minecraft:chests/stronghold_crossing"} replace

kill @e[tag=Spawner,tag=C5556,distance=..16]
kill @e[tag=Chest,tag=C5556,distance=..16]
kill @e[type=minecraft:item,distance=..16]

scoreboard players set Random RandomOne5556 99999
scoreboard players set Biome Biome5556 999
scoreboard players set Landed ExtraLogic5556 1