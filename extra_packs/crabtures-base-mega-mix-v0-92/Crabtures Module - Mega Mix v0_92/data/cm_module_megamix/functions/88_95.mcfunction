execute if score Biome Biome5556 matches 88 run function cm_module_megamix:biomes/88_surface_cave
execute if score Biome Biome5556 matches 89 run function cm_module_megamix:biomes/89_high_cave
execute if score Biome Biome5556 matches 90 run function cm_module_megamix:biomes/90_mid_cave
execute if score Biome Biome5556 matches 91 run function cm_module_megamix:biomes/91_low_cave
execute if score Biome Biome5556 matches 92 run function cm_module_megamix:biomes/92_lava_cave
execute if score Biome Biome5556 matches 93 run function cm_module_megamix:biomes/93_very_high_sky
execute if score Biome Biome5556 matches 94 run function cm_module_megamix:biomes/94_high_sky
execute if score Biome Biome5556 matches 95 run function cm_module_megamix:biomes/95_mid_sky