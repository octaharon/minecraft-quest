function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..250 run function cm_module_megamix:structures/huge_mushroom
execute if score Random RandomOne5556 matches 251..500 run function cm_module_megamix:structures/huge_tree
execute if score Random RandomOne5556 matches 501..750 run function cm_module_megamix:structures/mushroom_house
execute if score Random RandomOne5556 matches 751..850 run function cm_module_megamix:structures/basic_fort/basic_fort
execute if score Random RandomOne5556 matches 851..900 run function cm_module_megamix:structures/ruins/basic_fort_ruins
execute if score Random RandomOne5556 matches 901..940 run function cm_module_megamix:structures/ruins/tower_stone_ruins
execute if score Random RandomOne5556 matches 941..970 run function cm_module_megamix:structures/towers/odd
execute if score Random RandomOne5556 matches 971..990 run function cm_module_megamix:structures/mush_land/start
execute if score Random RandomOne5556 matches 991..994 run function cm_module_megamix:structures/end_crystal_patch/start_spire
execute if score Random RandomOne5556 matches 995..998 run function cm_module_megamix:structures/towers/village
execute if score Random RandomOne5556 matches 999 run function cm_module_megamix:structures/end_crystal_patch/start

execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty