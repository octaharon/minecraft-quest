function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..300 run function cm_module_megamix:structures/basic_fort/basic_fort
execute if score Random RandomOne5556 matches 301..500 run function cm_module_megamix:structures/towers/grass
execute if score Random RandomOne5556 matches 501..650 run function cm_module_megamix:structures/ruins/basic_fort_ruins
execute if score Random RandomOne5556 matches 651..800 run function cm_module_megamix:structures/ruins/tower_grass_ruins
execute if score Random RandomOne5556 matches 801..900 run function cm_module_megamix:structures/towers/odd
execute if score Random RandomOne5556 matches 901..960 run function cm_module_megamix:structures/huge_tree
execute if score Random RandomOne5556 matches 961..980 run function cm_module_megamix:structures/tree_house
execute if score Random RandomOne5556 matches 981..999 run function cm_module_megamix:structures/fortress/start

execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty