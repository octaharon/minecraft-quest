function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..200 run function cm_module_megamix:structures/ruins/tower_grass_ruins
execute if score Random RandomOne5556 matches 201..350 run function cm_module_megamix:structures/ruins/basic_fort_ruins
execute if score Random RandomOne5556 matches 351..450 run function cm_module_megamix:structures/basic_fort/basic_fort

execute if score Random RandomOne5556 matches 700..999 run function structures_crabmaster:empty
execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty