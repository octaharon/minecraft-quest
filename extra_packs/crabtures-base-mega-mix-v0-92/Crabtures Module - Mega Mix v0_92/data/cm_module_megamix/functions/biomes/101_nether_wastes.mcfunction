function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..500 run function cm_module_megamix:structures/nether_dungeon/start
execute if score Random RandomOne5556 matches 501..800 run function cm_module_megamix:structures/towers/nether_land
execute if score Random RandomOne5556 matches 801..950 run function cm_module_megamix:structures/big_skull_nether_land
execute if score Random RandomOne5556 matches 951..998 run function cm_module_megamix:structures/rogue_portal_start
execute if score Random RandomOne5556 matches 999 run function cm_module_megamix:structures/monolith

execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty