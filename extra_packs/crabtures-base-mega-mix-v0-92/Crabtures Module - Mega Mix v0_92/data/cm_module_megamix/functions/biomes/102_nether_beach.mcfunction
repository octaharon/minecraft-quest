function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..400 run function cm_module_megamix:structures/towers/nether_land
execute if score Random RandomOne5556 matches 401..700 run function cm_module_megamix:structures/big_skull_nether_land
execute if score Random RandomOne5556 matches 701..900 run function cm_module_megamix:structures/rogue_portal_start
execute if score Random RandomOne5556 matches 901..998 run function cm_module_megamix:structures/end_crystal_patch/start
execute if score Random RandomOne5556 matches 999 run function cm_module_megamix:structures/monolith


execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty