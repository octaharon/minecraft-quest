function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..600 run function cm_module_megamix:structures/big_skull_nether_land
execute if score Random RandomOne5556 matches 701..900 run function cm_module_megamix:structures/nether_dungeon/start
execute if score Random RandomOne5556 matches 901..980 run function cm_module_megamix:structures/end_crystal_patch/start_spire
execute if score Random RandomOne5556 matches 981..999 run function cm_module_megamix:structures/rogue_portal_start

execute if score Random RandomOne5556 matches 601..700 run function structures_crabmaster:empty
execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty