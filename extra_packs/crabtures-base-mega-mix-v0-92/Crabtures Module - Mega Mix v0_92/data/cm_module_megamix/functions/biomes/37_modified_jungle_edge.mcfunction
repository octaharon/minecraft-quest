function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..300 run function cm_module_megamix:structures/towers/grass
execute if score Random RandomOne5556 matches 301..500 run function cm_module_megamix:structures/palm_patch/start
execute if score Random RandomOne5556 matches 501..660 run function cm_module_megamix:structures/ruins/single_wall
execute if score Random RandomOne5556 matches 661..760 run function cm_module_megamix:structures/ruins/basic_fort_ruins
execute if score Random RandomOne5556 matches 761..860 run function cm_module_megamix:structures/palm_forest/start
execute if score Random RandomOne5556 matches 861..950 run function cm_module_megamix:structures/basic_fort/basic_fort
execute if score Random RandomOne5556 matches 951..994 run function cm_module_megamix:structures/ruins/wagon_1
execute if score Random RandomOne5556 matches 995..999 run function cm_module_megamix:structures/mush_land/start

execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty