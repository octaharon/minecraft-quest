function structures_crabmaster:randomizers/randomizer_1

execute if score BiomeRegion BiomeRegion5556 matches 0..1 if score Random RandomOne5556 matches 0..250 run function cm_module_megamix:structures/lost_ore
execute if score BiomeRegion BiomeRegion5556 matches 0..1 if score Random RandomOne5556 matches 251..300 run function cm_module_megamix:structures/larger_dungeon/start
execute if score BiomeRegion BiomeRegion5556 matches 0..1 if score Random RandomOne5556 matches 400..999 run function structures_crabmaster:empty

execute if score BiomeRegion BiomeRegion5556 matches 2 if score Random RandomOne5556 matches 0..250 run function cm_module_megamix:structures/larger_dungeon/start
execute if score BiomeRegion BiomeRegion5556 matches 2 if score Random RandomOne5556 matches 281..400 run function cm_module_megamix:structures/lost_ore
execute if score BiomeRegion BiomeRegion5556 matches 2 if score Random RandomOne5556 matches 500..999 run function structures_crabmaster:empty

execute if score BiomeRegion BiomeRegion5556 matches 3..4 if score Random RandomOne5556 matches 0..150 run function cm_module_megamix:structures/larger_dungeon/start
execute if score BiomeRegion BiomeRegion5556 matches 3..4 if score Random RandomOne5556 matches 151..200 run function cm_module_megamix:structures/lost_ore
execute if score BiomeRegion BiomeRegion5556 matches 3..4 if score Random RandomOne5556 matches 300..999 run function structures_crabmaster:empty

execute if score BiomeRegion BiomeRegion5556 matches 5.. if score Random RandomOne5556 matches 0..200 run function cm_module_megamix:structures/lost_ore
execute if score BiomeRegion BiomeRegion5556 matches 5.. if score Random RandomOne5556 matches 201..300 run function cm_module_megamix:structures/larger_dungeon/start
execute if score BiomeRegion BiomeRegion5556 matches 5.. if score Random RandomOne5556 matches 400..999 run function structures_crabmaster:empty

execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty