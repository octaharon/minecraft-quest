function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..150 run function cm_module_megamix:structures/huge_tree
execute if score Random RandomOne5556 matches 151..280 run function cm_module_megamix:structures/huge_mushroom
execute if score Random RandomOne5556 matches 281..400 run function cm_module_megamix:structures/tree_house
execute if score Random RandomOne5556 matches 401..500 run function cm_module_megamix:structures/ruins/basic_fort_ruins
execute if score Random RandomOne5556 matches 501..600 run function cm_module_megamix:structures/ruins/single_wall
execute if score Random RandomOne5556 matches 601..700 run function cm_module_megamix:structures/ruins/tower_grass_ruins
execute if score Random RandomOne5556 matches 701..800 run function cm_module_megamix:structures/ruins/wagon_1
execute if score Random RandomOne5556 matches 801..850 run function cm_module_megamix:structures/big_skull_overworld_start
execute if score Random RandomOne5556 matches 851..900 run function cm_module_megamix:structures/large_bog
execute if score Random RandomOne5556 matches 901..940 run function cm_module_megamix:structures/mushroom_house
execute if score Random RandomOne5556 matches 941..960 run function cm_module_megamix:structures/basic_fort/basic_fort
execute if score Random RandomOne5556 matches 961..980 run function cm_module_megamix:structures/ruins/tower_stone_ruins
execute if score Random RandomOne5556 matches 981..990 run function cm_module_megamix:structures/mush_land/start
execute if score Random RandomOne5556 matches 991..998 run function cm_module_megamix:structures/towers/stone
execute if score Random RandomOne5556 matches 999 run function cm_module_megamix:structures/end_crystal_patch/start_spire

execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty