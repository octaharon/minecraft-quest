function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..400 run function cm_module_megamix:structures/towers/village
execute if score Random RandomOne5556 matches 401..550 run function cm_module_megamix:structures/ruins/wagon_1
execute if score Random RandomOne5556 matches 551..580 run function cm_module_megamix:structures/rogue_portal_start

execute if score Random RandomOne5556 matches 650..999 run function structures_crabmaster:empty
execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty