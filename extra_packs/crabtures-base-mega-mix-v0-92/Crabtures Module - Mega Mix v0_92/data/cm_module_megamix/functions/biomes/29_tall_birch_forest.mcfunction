function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..300 run function cm_module_megamix:structures/basic_fort/basic_fort
execute if score Random RandomOne5556 matches 301..450 run function cm_module_megamix:structures/ruins/basic_fort_ruins
execute if score Random RandomOne5556 matches 451..600 run function cm_module_megamix:structures/towers/grass
execute if score Random RandomOne5556 matches 601..700 run function cm_module_megamix:structures/ruins/single_wall
execute if score Random RandomOne5556 matches 701..800 run function cm_module_megamix:structures/ruins/tower_grass_ruins
execute if score Random RandomOne5556 matches 801..900 run function cm_module_megamix:structures/ruins/tower_stone_ruins
execute if score Random RandomOne5556 matches 901..950 run function cm_module_megamix:structures/lone_house
execute if score Random RandomOne5556 matches 951..990 run function cm_module_megamix:structures/tree_house
execute if score Random RandomOne5556 matches 991..999 run function cm_module_megamix:structures/ruins/wagon_1

execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty