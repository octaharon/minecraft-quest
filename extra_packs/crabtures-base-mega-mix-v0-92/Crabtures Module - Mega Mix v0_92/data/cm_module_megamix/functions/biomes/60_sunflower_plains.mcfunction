function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..400 run function cm_module_megamix:structures/farm/start
execute if score Random RandomOne5556 matches 401..650 run function cm_module_megamix:structures/ore_tree
execute if score Random RandomOne5556 matches 651..850 run function cm_module_megamix:structures/tree_house
execute if score Random RandomOne5556 matches 851..960 run function cm_module_megamix:structures/towers/grass
execute if score Random RandomOne5556 matches 961..999 run function cm_module_megamix:structures/rogue_portal_start

execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty