function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..500 run function cm_module_megamix:structures/huge_mushroom
execute if score Random RandomOne5556 matches 501..800 run function cm_module_megamix:structures/mush_land/start
execute if score Random RandomOne5556 matches 801..900 run function cm_module_megamix:structures/mushroom_house
execute if score Random RandomOne5556 matches 901..940 run function cm_module_megamix:structures/end_crystal_patch/start_spire
execute if score Random RandomOne5556 matches 941..960 run function cm_module_megamix:structures/ruins/basic_fort_ruins
execute if score Random RandomOne5556 matches 961..980 run function cm_module_megamix:structures/ruins/tower_grass_ruins
execute if score Random RandomOne5556 matches 981..994 run function cm_module_megamix:structures/ruins/single_wall
execute if score Random RandomOne5556 matches 995..998 run function cm_module_megamix:structures/end_crystal_patch/start
execute if score Random RandomOne5556 matches 999 run function cm_module_megamix:structures/rogue_portal_start

execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty