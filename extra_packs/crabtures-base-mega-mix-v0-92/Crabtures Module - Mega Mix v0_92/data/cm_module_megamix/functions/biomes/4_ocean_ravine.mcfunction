function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..500 run function cm_module_megamix:structures/villager_boat
execute if score Random RandomOne5556 matches 501..700 run function cm_module_megamix:structures/towers/ocean
execute if score Random RandomOne5556 matches 701..800 run function cm_module_megamix:structures/vent_patch/start

execute if score Random RandomOne5556 matches 900..999 run function structures_crabmaster:empty
execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty