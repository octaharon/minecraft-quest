function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..200 run function cm_module_megamix:structures/lone_house
execute if score Random RandomOne5556 matches 201..350 run function cm_module_megamix:structures/towers/stone
execute if score Random RandomOne5556 matches 351..500 run function cm_module_megamix:structures/basic_fort/basic_fort
execute if score Random RandomOne5556 matches 501..620 run function cm_module_megamix:structures/ruins/basic_fort_ruins
execute if score Random RandomOne5556 matches 621..720 run function cm_module_megamix:structures/ruins/single_wall
execute if score Random RandomOne5556 matches 721..820 run function cm_module_megamix:structures/ruins/tower_grass_ruins
execute if score Random RandomOne5556 matches 821..920 run function cm_module_megamix:structures/ruins/tower_stone_ruins
execute if score Random RandomOne5556 matches 921..999 run function cm_module_megamix:structures/clay_patch/start

execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty