function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..350 run function cm_module_megamix:structures/ruins/tower_savanna_ruins
execute if score Random RandomOne5556 matches 351..600 run function cm_module_megamix:structures/towers/savanna
execute if score Random RandomOne5556 matches 601..750 run function cm_module_megamix:structures/towers/village
execute if score Random RandomOne5556 matches 751..850 run function cm_module_megamix:structures/towers/wood
execute if score Random RandomOne5556 matches 851..950 run function cm_module_megamix:structures/lone_house
execute if score Random RandomOne5556 matches 951..999 run function cm_module_megamix:structures/huge_tree

execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty