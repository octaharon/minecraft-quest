function structures_crabmaster:randomizers/randomizer_1

execute if score BiomeRegion BiomeRegion5556 matches 0..1 if score Random RandomOne5556 matches 0..500 run function cm_module_megamix:structures/larger_dungeon/start
execute if score BiomeRegion BiomeRegion5556 matches 0..1 if score Random RandomOne5556 matches 501..800 run function cm_module_megamix:structures/lost_ore
execute if score BiomeRegion BiomeRegion5556 matches 0..1 if score Random RandomOne5556 matches 801..950 run function cm_module_megamix:structures/mush_cave/start
execute if score BiomeRegion BiomeRegion5556 matches 0..1 if score Random RandomOne5556 matches 951..999 run function cm_module_megamix:structures/lava_cave/start

execute if score BiomeRegion BiomeRegion5556 matches 2 if score Random RandomOne5556 matches 0..600 run function cm_module_megamix:structures/larger_dungeon/start
execute if score BiomeRegion BiomeRegion5556 matches 2 if score Random RandomOne5556 matches 601..900 run function cm_module_megamix:structures/mush_cave/start
execute if score BiomeRegion BiomeRegion5556 matches 2 if score Random RandomOne5556 matches 901..960 run function cm_module_megamix:structures/lava_cave/start
execute if score BiomeRegion BiomeRegion5556 matches 2 if score Random RandomOne5556 matches 961..999 run function cm_module_megamix:structures/lost_ore

execute if score BiomeRegion BiomeRegion5556 matches 3..4 if score Random RandomOne5556 matches 0..600 run function cm_module_megamix:structures/mush_cave/start
execute if score BiomeRegion BiomeRegion5556 matches 3..4 if score Random RandomOne5556 matches 601..850 run function cm_module_megamix:structures/larger_dungeon/start
execute if score BiomeRegion BiomeRegion5556 matches 3..4 if score Random RandomOne5556 matches 851..951 run function cm_module_megamix:structures/lava_cave/start
execute if score BiomeRegion BiomeRegion5556 matches 3..4 if score Random RandomOne5556 matches 951..999 run function cm_module_megamix:structures/lost_ore

execute if score BiomeRegion BiomeRegion5556 matches 5.. if score Random RandomOne5556 matches 0..500 run function cm_module_megamix:structures/larger_dungeon/start
execute if score BiomeRegion BiomeRegion5556 matches 5.. if score Random RandomOne5556 matches 501..800 run function cm_module_megamix:structures/lava_cave/start
execute if score BiomeRegion BiomeRegion5556 matches 5.. if score Random RandomOne5556 matches 801..950 run function cm_module_megamix:structures/mush_cave/start
execute if score BiomeRegion BiomeRegion5556 matches 5.. if score Random RandomOne5556 matches 951..999 run function cm_module_megamix:structures/lost_ore


execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty