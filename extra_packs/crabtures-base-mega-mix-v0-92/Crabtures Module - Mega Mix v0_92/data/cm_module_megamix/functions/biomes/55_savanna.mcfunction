function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..150 run function cm_module_megamix:structures/wagon_camp_start
execute if score Random RandomOne5556 matches 151..280 run function cm_module_megamix:structures/towers/savanna
execute if score Random RandomOne5556 matches 281..400 run function cm_module_megamix:structures/farm/start
execute if score Random RandomOne5556 matches 401..500 run function cm_module_megamix:structures/lone_house
execute if score Random RandomOne5556 matches 501..600 run function cm_module_megamix:structures/ruins/tower_savanna_ruins
execute if score Random RandomOne5556 matches 601..700 run function cm_module_megamix:structures/palm_patch/start
execute if score Random RandomOne5556 matches 701..780 run function cm_module_megamix:structures/ruins/wagon_1
execute if score Random RandomOne5556 matches 781..850 run function cm_module_megamix:structures/huge_tree
execute if score Random RandomOne5556 matches 851..910 run function cm_module_megamix:structures/tree_house
execute if score Random RandomOne5556 matches 911..960 run function cm_module_megamix:structures/towers/wood
execute if score Random RandomOne5556 matches 961..980 run function cm_module_megamix:structures/ruins/tower_stone_ruins
execute if score Random RandomOne5556 matches 981..990 run function cm_module_megamix:structures/deep_dungeon/start
execute if score Random RandomOne5556 matches 991..995 run function cm_module_megamix:structures/rogue_portal_start
execute if score Random RandomOne5556 matches 996 run function cm_module_megamix:structures/end_crystal_patch/start_spire
execute if score Random RandomOne5556 matches 997 run function cm_module_megamix:structures/end_crystal_patch/start
execute if score Random RandomOne5556 matches 998 run function cm_module_megamix:structures/end_crystal_patch/start_big
execute if score Random RandomOne5556 matches 999 run function cm_module_megamix:structures/monolith


execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty