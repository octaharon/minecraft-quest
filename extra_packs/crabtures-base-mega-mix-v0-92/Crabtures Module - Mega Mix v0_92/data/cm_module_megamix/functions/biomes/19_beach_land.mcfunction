function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..400 run function cm_module_megamix:structures/palm_patch/start
execute if score Random RandomOne5556 matches 401..700 run function cm_module_megamix:structures/towers/village
execute if score Random RandomOne5556 matches 701..950 run function cm_module_megamix:structures/palm_forest/start
execute if score Random RandomOne5556 matches 951..999 run function cm_module_megamix:structures/towers/stone

execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty