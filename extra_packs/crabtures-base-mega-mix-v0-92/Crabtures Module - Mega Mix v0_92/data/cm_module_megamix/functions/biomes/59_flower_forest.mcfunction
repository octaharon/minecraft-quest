function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..400 run function cm_module_megamix:structures/ore_tree
execute if score Random RandomOne5556 matches 401..700 run function cm_module_megamix:structures/towers/grass
execute if score Random RandomOne5556 matches 701..999 run function cm_module_megamix:structures/towers/wood

execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty