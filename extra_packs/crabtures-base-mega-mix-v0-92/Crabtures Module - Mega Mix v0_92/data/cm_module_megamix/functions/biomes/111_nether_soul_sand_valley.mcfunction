function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..800 run function cm_module_megamix:structures/big_skull_nether_land
execute if score Random RandomOne5556 matches 951..980 run function cm_module_megamix:structures/end_crystal_patch/start
execute if score Random RandomOne5556 matches 981..996 run function cm_module_megamix:structures/rogue_portal_start
execute if score Random RandomOne5556 matches 997..999 run function cm_module_megamix:structures/monolith


execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty