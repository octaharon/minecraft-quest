function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..350 run function cm_module_megamix:structures/towers/red
execute if score Random RandomOne5556 matches 351..600 run function cm_module_megamix:structures/big_skull_overworld_start
execute if score Random RandomOne5556 matches 601..800 run function cm_module_megamix:structures/ruins/tower_red_ruins
execute if score Random RandomOne5556 matches 801..920 run function cm_module_megamix:structures/ruins/tower_terracotta_ruins
execute if score Random RandomOne5556 matches 921..970 run function cm_module_megamix:structures/ruins/wagon_1
execute if score Random RandomOne5556 matches 971..990 run function cm_module_megamix:structures/oasis/start
execute if score Random RandomOne5556 matches 991..995 run function cm_module_megamix:structures/deep_dungeon/start
execute if score Random RandomOne5556 matches 996..997 run function cm_module_megamix:structures/rogue_portal_start
execute if score Random RandomOne5556 matches 998 run function cm_module_megamix:structures/end_crystal_patch/start
execute if score Random RandomOne5556 matches 999 run function cm_module_megamix:structures/monolith

execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty