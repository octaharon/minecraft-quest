function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..400 run function cm_module_megamix:structures/basic_fort/basic_fort
execute if score Random RandomOne5556 matches 401..700 run function cm_module_megamix:structures/towers/stone
execute if score Random RandomOne5556 matches 701..800 run function cm_module_megamix:structures/ruins/basic_fort_ruins
execute if score Random RandomOne5556 matches 801..900 run function cm_module_megamix:structures/towers/odd
execute if score Random RandomOne5556 matches 901..980 run function cm_module_megamix:structures/fortress/start
execute if score Random RandomOne5556 matches 981..998 run function cm_module_megamix:structures/deep_dungeon/start
execute if score Random RandomOne5556 matches 999 run function cm_module_megamix:structures/rogue_portal_start


execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty