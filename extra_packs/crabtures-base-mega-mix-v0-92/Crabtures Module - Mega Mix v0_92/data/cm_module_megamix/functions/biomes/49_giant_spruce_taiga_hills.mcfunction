function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..300 run function cm_module_megamix:structures/basic_fort/basic_fort
execute if score Random RandomOne5556 matches 301..500 run function cm_module_megamix:structures/towers/stone
execute if score Random RandomOne5556 matches 501..650 run function cm_module_megamix:structures/ruins/basic_fort_ruins
execute if score Random RandomOne5556 matches 651..750 run function cm_module_megamix:structures/towers/odd
execute if score Random RandomOne5556 matches 751..830 run function cm_module_megamix:structures/ruins/single_wall
execute if score Random RandomOne5556 matches 831..880 run function cm_module_megamix:structures/ruins/tower_stone_ruins
execute if score Random RandomOne5556 matches 881..950 run function cm_module_megamix:structures/fortress/start
execute if score Random RandomOne5556 matches 951..999 run function cm_module_megamix:structures/clay_patch/start

execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty