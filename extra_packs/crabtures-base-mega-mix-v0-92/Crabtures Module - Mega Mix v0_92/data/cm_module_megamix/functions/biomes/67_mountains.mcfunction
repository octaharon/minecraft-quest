function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..300 run function cm_module_megamix:structures/towers/stone
execute if score Random RandomOne5556 matches 301..500 run function cm_module_megamix:structures/towers/odd
execute if score Random RandomOne5556 matches 501..650 run function cm_module_megamix:structures/ruins/tower_stone_ruins
execute if score Random RandomOne5556 matches 650..800 run function cm_module_megamix:structures/towers/village
execute if score Random RandomOne5556 matches 801..850 run function cm_module_megamix:structures/basic_fort/basic_fort
execute if score Random RandomOne5556 matches 851..970 run function cm_module_megamix:structures/fortress/start
execute if score Random RandomOne5556 matches 971..990 run function cm_module_megamix:structures/ruins/basic_fort_ruins
execute if score Random RandomOne5556 matches 991..998 run function cm_module_megamix:structures/deep_dungeon/start
execute if score Random RandomOne5556 matches 999 run function cm_module_megamix:structures/rogue_portal_start


execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty