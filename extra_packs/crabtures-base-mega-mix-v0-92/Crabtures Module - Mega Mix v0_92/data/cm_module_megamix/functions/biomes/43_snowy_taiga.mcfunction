function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..200 run function cm_module_megamix:structures/towers/snow_stone
execute if score Random RandomOne5556 matches 201..400 run function cm_module_megamix:structures/basic_fort/basic_snow_fort
execute if score Random RandomOne5556 matches 401..550 run function cm_module_megamix:structures/ruins/tower_snow_stone_ruins
execute if score Random RandomOne5556 matches 551..700 run function cm_module_megamix:structures/ruins/tower_snow_ruins
execute if score Random RandomOne5556 matches 701..850 run function cm_module_megamix:structures/towers/snow
execute if score Random RandomOne5556 matches 851..950 run function cm_module_megamix:structures/ruins/basic_snow_fort_ruins
execute if score Random RandomOne5556 matches 951..999 run function cm_module_megamix:structures/clay_patch/start

execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty