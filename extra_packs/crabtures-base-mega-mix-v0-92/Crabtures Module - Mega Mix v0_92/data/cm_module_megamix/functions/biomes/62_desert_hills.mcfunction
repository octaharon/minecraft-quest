function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..300 run function cm_module_megamix:structures/big_skull_overworld_start
execute if score Random RandomOne5556 matches 301..550 run function cm_module_megamix:structures/towers/desert
execute if score Random RandomOne5556 matches 551..750 run function cm_module_megamix:structures/palm_patch/start
execute if score Random RandomOne5556 matches 751..900 run function cm_module_megamix:structures/ruins/tower_desert_ruins
execute if score Random RandomOne5556 matches 901..999 run function cm_module_megamix:structures/palm_forest/start


execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty