function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..250 run function cm_module_megamix:structures/towers/stone
execute if score Random RandomOne5556 matches 251..500 run function cm_module_megamix:structures/huge_mushroom
execute if score Random RandomOne5556 matches 501..700 run function cm_module_megamix:structures/towers/odd
execute if score Random RandomOne5556 matches 701..850 run function cm_module_megamix:structures/basic_fort/basic_fort
execute if score Random RandomOne5556 matches 851..900 run function cm_module_megamix:structures/ruins/basic_fort_ruins
execute if score Random RandomOne5556 matches 901..930 run function cm_module_megamix:structures/ruins/tower_stone_ruins
execute if score Random RandomOne5556 matches 931..999 run function cm_module_megamix:structures/fortress/start

execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty