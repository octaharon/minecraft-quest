function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..160 run function cm_module_megamix:structures/basic_fort/basic_fort
execute if score Random RandomOne5556 matches 161..300 run function cm_module_megamix:structures/towers/grass
execute if score Random RandomOne5556 matches 301..420 run function cm_module_megamix:structures/towers/wood
execute if score Random RandomOne5556 matches 421..530 run function cm_module_megamix:structures/ruins/basic_fort_ruins
execute if score Random RandomOne5556 matches 531..630 run function cm_module_megamix:structures/camp_start
execute if score Random RandomOne5556 matches 631..730 run function cm_module_megamix:structures/ruins/tower_grass_ruins
execute if score Random RandomOne5556 matches 731..820 run function cm_module_megamix:structures/lone_house
execute if score Random RandomOne5556 matches 821..880 run function cm_module_megamix:structures/ruins/wagon_1
execute if score Random RandomOne5556 matches 881..920 run function cm_module_megamix:structures/huge_tree
execute if score Random RandomOne5556 matches 921..950 run function cm_module_megamix:structures/tree_house
execute if score Random RandomOne5556 matches 951..970 run function cm_module_megamix:structures/ruins/single_wall
execute if score Random RandomOne5556 matches 971..989 run function cm_module_megamix:structures/deep_dungeon/start
execute if score Random RandomOne5556 matches 990..998 run function cm_module_megamix:structures/small_parts/farm_well_3
execute if score Random RandomOne5556 matches 999 run function cm_module_megamix:structures/fortress/start

execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty