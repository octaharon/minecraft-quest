function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..250 run function cm_module_megamix:structures/towers/stone
execute if score Random RandomOne5556 matches 251..450 run function cm_module_megamix:structures/big_skull_overworld_start
execute if score Random RandomOne5556 matches 451..650 run function cm_module_megamix:structures/basic_fort/basic_fort
execute if score Random RandomOne5556 matches 651..800 run function cm_module_megamix:structures/ruins/basic_fort_ruins
execute if score Random RandomOne5556 matches 801..900 run function cm_module_megamix:structures/towers/odd
execute if score Random RandomOne5556 matches 901..950 run function cm_module_megamix:structures/towers/wood
execute if score Random RandomOne5556 matches 951..980 run function cm_module_megamix:structures/ruins/wagon_1
execute if score Random RandomOne5556 matches 981..999 run function cm_module_megamix:structures/fortress/start

execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty