function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..150 run function cm_module_megamix:structures/towers/grass
execute if score Random RandomOne5556 matches 151..300 run function cm_module_megamix:structures/farm/start
execute if score Random RandomOne5556 matches 301..420 run function cm_module_megamix:structures/lone_house
execute if score Random RandomOne5556 matches 421..520 run function cm_module_megamix:structures/camp_start
execute if score Random RandomOne5556 matches 521..610 run function cm_module_megamix:structures/basic_fort/basic_fort
execute if score Random RandomOne5556 matches 611..710 run function cm_module_megamix:structures/wagon_camp_start
execute if score Random RandomOne5556 matches 711..800 run function cm_module_megamix:structures/ruins/tower_grass_ruins
execute if score Random RandomOne5556 matches 801..850 run function cm_module_megamix:structures/tree_house
execute if score Random RandomOne5556 matches 851..900 run function cm_module_megamix:structures/ruins/wagon_1
execute if score Random RandomOne5556 matches 901..930 run function cm_module_megamix:structures/ruins/basic_fort_ruins
execute if score Random RandomOne5556 matches 931..950 run function cm_module_megamix:structures/ruins/single_wall
execute if score Random RandomOne5556 matches 951..970 run function cm_module_megamix:structures/huge_tree
execute if score Random RandomOne5556 matches 971..984 run function cm_module_megamix:structures/large_bog
execute if score Random RandomOne5556 matches 985..990 run function cm_module_megamix:structures/deep_dungeon/start
execute if score Random RandomOne5556 matches 991..994 run function cm_module_megamix:structures/small_parts/farm_well_3
execute if score Random RandomOne5556 matches 995..996 run function cm_module_megamix:structures/rogue_portal_start
execute if score Random RandomOne5556 matches 997 run function cm_module_megamix:structures/fortress/start
execute if score Random RandomOne5556 matches 998 run function cm_module_megamix:structures/end_crystal_patch/start_spire
execute if score Random RandomOne5556 matches 999 run function cm_module_megamix:structures/monolith

execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty