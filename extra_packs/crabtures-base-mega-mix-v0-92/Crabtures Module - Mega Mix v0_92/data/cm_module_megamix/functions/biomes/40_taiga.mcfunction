function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..180 run function cm_module_megamix:structures/towers/stone
execute if score Random RandomOne5556 matches 181..330 run function cm_module_megamix:structures/lone_house
execute if score Random RandomOne5556 matches 331..450 run function cm_module_megamix:structures/clay_patch/start
execute if score Random RandomOne5556 matches 451..570 run function cm_module_megamix:structures/towers/wood
execute if score Random RandomOne5556 matches 571..660 run function cm_module_megamix:structures/camp_start
execute if score Random RandomOne5556 matches 661..740 run function cm_module_megamix:structures/basic_fort/basic_fort
execute if score Random RandomOne5556 matches 741..790 run function cm_module_megamix:structures/ruins/basic_fort_ruins
execute if score Random RandomOne5556 matches 791..840 run function cm_module_megamix:structures/ruins/single_wall
execute if score Random RandomOne5556 matches 841..880 run function cm_module_megamix:structures/ruins/tower_grass_ruins
execute if score Random RandomOne5556 matches 881..900 run function cm_module_megamix:structures/ruins/tower_stone_ruins
execute if score Random RandomOne5556 matches 901..920 run function cm_module_megamix:structures/ruins/wagon_1
execute if score Random RandomOne5556 matches 921..960 run function cm_module_megamix:structures/huge_tree
execute if score Random RandomOne5556 matches 961..970 run function cm_module_megamix:structures/fortress/start
execute if score Random RandomOne5556 matches 971..984 run function cm_module_megamix:structures/tree_house
execute if score Random RandomOne5556 matches 985..994 run function cm_module_megamix:structures/deep_dungeon/start
execute if score Random RandomOne5556 matches 995..998 run function cm_module_megamix:structures/small_parts/farm_well_3
execute if score Random RandomOne5556 matches 999 run function cm_module_megamix:structures/rogue_portal_start

execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty