function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..250 run function cm_module_megamix:structures/towers/stone
execute if score Random RandomOne5556 matches 251..500 run function cm_module_megamix:structures/huge_mushroom
execute if score Random RandomOne5556 matches 501..640 run function cm_module_megamix:structures/towers/wood
execute if score Random RandomOne5556 matches 641..750 run function cm_module_megamix:structures/basic_fort/basic_fort
execute if score Random RandomOne5556 matches 751..820 run function cm_module_megamix:structures/ruins/tower_stone_ruins
execute if score Random RandomOne5556 matches 821..870 run function cm_module_megamix:structures/ruins/basic_fort_ruins
execute if score Random RandomOne5556 matches 871..910 run function cm_module_megamix:structures/mushroom_house
execute if score Random RandomOne5556 matches 911..980 run function cm_module_megamix:structures/fortress/start
execute if score Random RandomOne5556 matches 981..990 run function cm_module_megamix:structures/large_bog
execute if score Random RandomOne5556 matches 991..994 run function cm_module_megamix:structures/mush_land/start
execute if score Random RandomOne5556 matches 995..999 run function cm_module_megamix:structures/huge_tree

execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty