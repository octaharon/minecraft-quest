function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..250 run function cm_module_megamix:structures/towers/desert
execute if score Random RandomOne5556 matches 251..400 run function cm_module_megamix:structures/palm_patch/start
execute if score Random RandomOne5556 matches 401..550 run function cm_module_megamix:structures/ruins/tower_desert_ruins
execute if score Random RandomOne5556 matches 551..650 run function cm_module_megamix:structures/big_skull_overworld_start
execute if score Random RandomOne5556 matches 651..750 run function cm_module_megamix:structures/oasis/start
execute if score Random RandomOne5556 matches 751..850 run function cm_module_megamix:structures/ruins/wagon_1
execute if score Random RandomOne5556 matches 851..900 run function cm_module_megamix:structures/palm_forest/start
execute if score Random RandomOne5556 matches 901..950 run function cm_module_megamix:structures/ruins/basic_fort_ruins
execute if score Random RandomOne5556 matches 951..970 run function cm_module_megamix:structures/camp_start
execute if score Random RandomOne5556 matches 971..984 run function cm_module_megamix:structures/wagon_camp_start
execute if score Random RandomOne5556 matches 985..995 run function cm_module_megamix:structures/deep_dungeon/start
execute if score Random RandomOne5556 matches 996 run function cm_module_megamix:structures/end_crystal_patch/start
execute if score Random RandomOne5556 matches 997 run function cm_module_megamix:structures/rogue_portal_start
execute if score Random RandomOne5556 matches 998 run function cm_module_megamix:structures/end_crystal_patch/start_big
execute if score Random RandomOne5556 matches 999 run function cm_module_megamix:structures/monolith


execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty