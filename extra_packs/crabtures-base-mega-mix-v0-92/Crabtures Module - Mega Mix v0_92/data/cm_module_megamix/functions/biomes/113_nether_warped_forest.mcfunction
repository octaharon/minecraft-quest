function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..450 run function cm_module_megamix:structures/nether_dungeon/start
execute if score Random RandomOne5556 matches 601..900 run function cm_module_megamix:structures/towers/nether_land
execute if score Random RandomOne5556 matches 901..999 run function cm_module_megamix:structures/rogue_portal_start

execute if score Random RandomOne5556 matches 451..600 run function structures_crabmaster:empty
execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty