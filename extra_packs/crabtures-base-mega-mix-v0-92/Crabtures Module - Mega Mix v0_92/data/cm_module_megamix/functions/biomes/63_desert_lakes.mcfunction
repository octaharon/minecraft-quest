function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..250 run function cm_module_megamix:structures/big_skull_overworld_start
execute if score Random RandomOne5556 matches 251..500 run function cm_module_megamix:structures/palm_patch/start
execute if score Random RandomOne5556 matches 501..650 run function cm_module_megamix:structures/ruins/tower_desert_ruins
execute if score Random RandomOne5556 matches 651..800 run function cm_module_megamix:structures/palm_forest/start
execute if score Random RandomOne5556 matches 801..900 run function cm_module_megamix:structures/towers/desert
execute if score Random RandomOne5556 matches 901..950 run function cm_module_megamix:structures/towers/stone
execute if score Random RandomOne5556 matches 951..980 run function cm_module_megamix:structures/oasis/start
execute if score Random RandomOne5556 matches 981..999 run function cm_module_megamix:structures/end_crystal_patch/start_big


execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty