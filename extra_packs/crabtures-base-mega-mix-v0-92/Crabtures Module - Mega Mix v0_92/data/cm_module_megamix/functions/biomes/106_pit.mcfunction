function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..100 run function cm_module_megamix:structures/ruins/basic_fort_ruins
execute if score Random RandomOne5556 matches 101..190 run function cm_module_megamix:structures/big_skull_overworld_start
execute if score Random RandomOne5556 matches 191..270 run function cm_module_megamix:structures/ruins/tower_stone_ruins
execute if score Random RandomOne5556 matches 270..340 run function cm_module_megamix:structures/mush_cave/start
execute if score Random RandomOne5556 matches 341..400 run function cm_module_megamix:structures/ruins/wagon_1
execute if score Random RandomOne5556 matches 401..430 run function cm_module_megamix:structures/rogue_portal_start

execute if score Random RandomOne5556 matches 550..999 run function structures_crabmaster:empty
execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty