function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..250 run function cm_module_megamix:structures/basic_fort/basic_fort
execute if score Random RandomOne5556 matches 251..460 run function cm_module_megamix:structures/towers/grass
execute if score Random RandomOne5556 matches 461..680 run function cm_module_megamix:structures/ruins/basic_fort_ruins
execute if score Random RandomOne5556 matches 681..800 run function cm_module_megamix:structures/towers/odd
execute if score Random RandomOne5556 matches 801..900 run function cm_module_megamix:structures/ruins/tower_grass_ruins
execute if score Random RandomOne5556 matches 901..950 run function cm_module_megamix:structures/towers/stone
execute if score Random RandomOne5556 matches 951..970 run function cm_module_megamix:structures/tree_house
execute if score Random RandomOne5556 matches 971..990 run function cm_module_megamix:structures/small_parts/farm_well_3
execute if score Random RandomOne5556 matches 991..999 run function cm_module_megamix:structures/fortress/start

execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty