function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..450 run function cm_module_megamix:structures/ruins/tower_water_ruins
execute if score Random RandomOne5556 matches 451..800 run function cm_module_megamix:structures/towers/ocean
execute if score Random RandomOne5556 matches 801..999 run function cm_module_megamix:structures/towers/water

execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty