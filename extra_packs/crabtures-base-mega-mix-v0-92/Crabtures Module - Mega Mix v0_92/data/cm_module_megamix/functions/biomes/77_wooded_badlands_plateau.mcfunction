function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..200 run function cm_module_megamix:structures/camp_start
execute if score Random RandomOne5556 matches 201..350 run function cm_module_megamix:structures/lone_house
execute if score Random RandomOne5556 matches 351..475 run function cm_module_megamix:structures/towers/savanna
execute if score Random RandomOne5556 matches 476..600 run function cm_module_megamix:structures/ruins/tower_savanna_ruins
execute if score Random RandomOne5556 matches 601..700 run function cm_module_megamix:structures/huge_tree
execute if score Random RandomOne5556 matches 701..800 run function cm_module_megamix:structures/towers/wood
execute if score Random RandomOne5556 matches 801..900 run function cm_module_megamix:structures/towers/village
execute if score Random RandomOne5556 matches 901..980 run function cm_module_megamix:structures/tree_house
execute if score Random RandomOne5556 matches 981..999 run function cm_module_megamix:structures/ruins/basic_fort_ruins

execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty