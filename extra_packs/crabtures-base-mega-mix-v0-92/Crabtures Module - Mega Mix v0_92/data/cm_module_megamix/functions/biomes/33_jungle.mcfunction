function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..200 run function cm_module_megamix:structures/towers/grass
execute if score Random RandomOne5556 matches 201..400 run function cm_module_megamix:structures/towers/stone
execute if score Random RandomOne5556 matches 401..550 run function cm_module_megamix:structures/ruins/tower_grass_ruins
execute if score Random RandomOne5556 matches 551..690 run function cm_module_megamix:structures/basic_fort/basic_fort
execute if score Random RandomOne5556 matches 691..800 run function cm_module_megamix:structures/ruins/basic_fort_ruins
execute if score Random RandomOne5556 matches 801..900 run function cm_module_megamix:structures/large_bog
execute if score Random RandomOne5556 matches 901..970 run function cm_module_megamix:structures/palm_forest/start
execute if score Random RandomOne5556 matches 971..994 run function cm_module_megamix:structures/palm_patch/start
execute if score Random RandomOne5556 matches 995..999 run function cm_module_megamix:structures/mush_land/start

execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty