function structures_crabmaster:randomizers/randomizer_1

execute if score BiomeRegion BiomeRegion5556 matches 0 if score Random RandomOne5556 matches 0..80 run function cm_module_megamix:structures/sky_island/start
execute if score BiomeRegion BiomeRegion5556 matches 0 if score Random RandomOne5556 matches 81..120 run function cm_module_megamix:structures/big_sky_island/start
execute if score BiomeRegion BiomeRegion5556 matches 0 if score Random RandomOne5556 matches 121 run function cm_module_megamix:structures/towers/grass
execute if score BiomeRegion BiomeRegion5556 matches 0 if score Random RandomOne5556 matches 122..999 run function structures_crabmaster:empty

execute if score BiomeRegion BiomeRegion5556 matches 1..2 if score Random RandomOne5556 matches 0..600 run function cm_module_megamix:structures/big_sky_island/start
execute if score BiomeRegion BiomeRegion5556 matches 1..2 if score Random RandomOne5556 matches 601..998 run function cm_module_megamix:structures/sky_island/start
execute if score BiomeRegion BiomeRegion5556 matches 1..2 if score Random RandomOne5556 matches 999 run function cm_module_megamix:structures/towers/grass

execute if score BiomeRegion BiomeRegion5556 matches 3..4 if score Random RandomOne5556 matches 0..680 run function cm_module_megamix:structures/big_sky_island/start
execute if score BiomeRegion BiomeRegion5556 matches 3..4 if score Random RandomOne5556 matches 681..998 run function cm_module_megamix:structures/sky_island/start
execute if score BiomeRegion BiomeRegion5556 matches 3..4 if score Random RandomOne5556 matches 999 run function cm_module_megamix:structures/towers/grass

execute if score BiomeRegion BiomeRegion5556 matches 5.. if score Random RandomOne5556 matches 0..100 run function cm_module_megamix:structures/sky_island/start
execute if score BiomeRegion BiomeRegion5556 matches 5.. if score Random RandomOne5556 matches 101..150 run function cm_module_megamix:structures/big_sky_island/start
execute if score BiomeRegion BiomeRegion5556 matches 5.. if score Random RandomOne5556 matches 151 run function cm_module_megamix:structures/towers/grass
execute if score BiomeRegion BiomeRegion5556 matches 5.. if score Random RandomOne5556 matches 152..999 run function structures_crabmaster:empty


execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty