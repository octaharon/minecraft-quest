function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..250 run function cm_module_megamix:structures/towers/water
execute if score Random RandomOne5556 matches 251..500 run function cm_module_megamix:structures/towers/ocean
execute if score Random RandomOne5556 matches 501..700 run function cm_module_megamix:structures/ruins/tower_water_ruins
execute if score Random RandomOne5556 matches 701..850 run function cm_module_megamix:structures/vent_patch/start
execute if score Random RandomOne5556 matches 851..950 run function cm_module_megamix:structures/vent_patch/start_big
execute if score Random RandomOne5556 matches 951..999 run function cm_module_megamix:structures/villager_boat

execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty