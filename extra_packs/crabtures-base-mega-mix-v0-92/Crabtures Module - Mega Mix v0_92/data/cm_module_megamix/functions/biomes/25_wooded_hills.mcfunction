function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..190 run function cm_module_megamix:structures/towers/wood
execute if score Random RandomOne5556 matches 191..350 run function cm_module_megamix:structures/towers/grass
execute if score Random RandomOne5556 matches 351..500 run function cm_module_megamix:structures/basic_fort/basic_fort
execute if score Random RandomOne5556 matches 501..600 run function cm_module_megamix:structures/towers/odd
execute if score Random RandomOne5556 matches 601..700 run function cm_module_megamix:structures/ruins/basic_fort_ruins
execute if score Random RandomOne5556 matches 701..800 run function cm_module_megamix:structures/ruins/tower_grass_ruins
execute if score Random RandomOne5556 matches 801..900 run function cm_module_megamix:structures/huge_tree
execute if score Random RandomOne5556 matches 901..950 run function cm_module_megamix:structures/lone_house
execute if score Random RandomOne5556 matches 951..989 run function cm_module_megamix:structures/tree_house
execute if score Random RandomOne5556 matches 990..996 run function cm_module_megamix:structures/towers/village
execute if score Random RandomOne5556 matches 997..999 run function cm_module_megamix:structures/fortress/start

execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty