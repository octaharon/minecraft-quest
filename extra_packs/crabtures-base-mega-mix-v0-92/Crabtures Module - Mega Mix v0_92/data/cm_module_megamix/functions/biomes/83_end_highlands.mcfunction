function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..500 run function cm_module_megamix:structures/towers/end
execute if score Random RandomOne5556 matches 501..750 run function cm_module_megamix:structures/end_crystal_patch/start_big
execute if score Random RandomOne5556 matches 750..900 run function cm_module_megamix:structures/end_crystal_patch/start
execute if score Random RandomOne5556 matches 901..998 run function cm_module_megamix:structures/end_crystal_patch/start_spire
execute if score Random RandomOne5556 matches 999 run function cm_module_megamix:structures/monolith

execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty