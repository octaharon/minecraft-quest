function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..650 run function cm_module_megamix:structures/end_dungeon/start
execute if score Random RandomOne5556 matches 651..930 run function cm_module_megamix:structures/towers/end
execute if score Random RandomOne5556 matches 931..998 run function cm_module_megamix:structures/monolith
execute if score Random RandomOne5556 matches 999 run function cm_module_megamix:structures/end_crystal_patch/start_spire

execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty