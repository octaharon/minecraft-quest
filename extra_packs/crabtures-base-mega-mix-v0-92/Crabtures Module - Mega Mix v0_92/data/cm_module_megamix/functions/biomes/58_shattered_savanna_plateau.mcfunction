function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..400 run function cm_module_megamix:structures/towers/savanna
execute if score Random RandomOne5556 matches 401..600 run function cm_module_megamix:structures/ruins/tower_savanna_ruins
execute if score Random RandomOne5556 matches 601..750 run function cm_module_megamix:structures/big_skull_overworld_start
execute if score Random RandomOne5556 matches 751..850 run function cm_module_megamix:structures/towers/village
execute if score Random RandomOne5556 matches 851..920 run function cm_module_megamix:structures/towers/wood
execute if score Random RandomOne5556 matches 921..960 run function cm_module_megamix:structures/palm_patch/start
execute if score Random RandomOne5556 matches 961..980 run function cm_module_megamix:structures/towers/stone
execute if score Random RandomOne5556 matches 981..999 run function cm_module_megamix:structures/palm_forest/start


execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty