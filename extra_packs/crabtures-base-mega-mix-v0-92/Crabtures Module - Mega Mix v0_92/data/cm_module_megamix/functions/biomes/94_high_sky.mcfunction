function structures_crabmaster:randomizers/randomizer_1

execute if score BiomeRegion BiomeRegion5556 matches 0 if score Random RandomOne5556 matches 0..150 run function cm_module_megamix:structures/sky_island/start
execute if score BiomeRegion BiomeRegion5556 matches 0 if score Random RandomOne5556 matches 151..220 run function cm_module_megamix:structures/big_sky_island/start
execute if score BiomeRegion BiomeRegion5556 matches 0 if score Random RandomOne5556 matches 221 run function cm_module_megamix:structures/monolith
execute if score BiomeRegion BiomeRegion5556 matches 0 if score Random RandomOne5556 matches 222..999 run function structures_crabmaster:empty

execute if score BiomeRegion BiomeRegion5556 matches 1..2 if score Random RandomOne5556 matches 0..500 run function cm_module_megamix:structures/big_sky_island/start
execute if score BiomeRegion BiomeRegion5556 matches 1..2 if score Random RandomOne5556 matches 501..800 run function cm_module_megamix:structures/sky_island/start
execute if score BiomeRegion BiomeRegion5556 matches 1..2 if score Random RandomOne5556 matches 801 run function cm_module_megamix:structures/monolith
execute if score BiomeRegion BiomeRegion5556 matches 1..2 if score Random RandomOne5556 matches 802..999 run function structures_crabmaster:empty

execute if score BiomeRegion BiomeRegion5556 matches 3..4 if score Random RandomOne5556 matches 0..600 run function cm_module_megamix:structures/big_sky_island/start
execute if score BiomeRegion BiomeRegion5556 matches 3..4 if score Random RandomOne5556 matches 601..950 run function cm_module_megamix:structures/sky_island/start
execute if score BiomeRegion BiomeRegion5556 matches 3..4 if score Random RandomOne5556 matches 951 run function cm_module_megamix:structures/monolith
execute if score BiomeRegion BiomeRegion5556 matches 3..4 if score Random RandomOne5556 matches 952..999 run function structures_crabmaster:empty

execute if score BiomeRegion BiomeRegion5556 matches 5.. if score Random RandomOne5556 matches 0..180 run function cm_module_megamix:structures/sky_island/start
execute if score BiomeRegion BiomeRegion5556 matches 5.. if score Random RandomOne5556 matches 181..280 run function cm_module_megamix:structures/big_sky_island/start
execute if score BiomeRegion BiomeRegion5556 matches 5.. if score Random RandomOne5556 matches 281 run function cm_module_megamix:structures/monolith
execute if score BiomeRegion BiomeRegion5556 matches 5.. if score Random RandomOne5556 matches 282..999 run function structures_crabmaster:empty

execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty