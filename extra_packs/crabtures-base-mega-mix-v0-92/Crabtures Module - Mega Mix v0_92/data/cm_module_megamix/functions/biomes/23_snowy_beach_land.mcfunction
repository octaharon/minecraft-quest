function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..400 run function cm_module_megamix:structures/towers/village
execute if score Random RandomOne5556 matches 401..750 run function cm_module_megamix:structures/towers/snow_stone
execute if score Random RandomOne5556 matches 751..900 run function cm_module_megamix:structures/towers/snow

execute if score Random RandomOne5556 matches 901..999 run function structures_crabmaster:empty
execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty