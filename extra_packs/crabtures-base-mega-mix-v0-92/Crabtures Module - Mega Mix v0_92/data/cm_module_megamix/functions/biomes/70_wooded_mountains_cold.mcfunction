function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..300 run function cm_module_megamix:structures/towers/snow_stone
execute if score Random RandomOne5556 matches 301..600 run function cm_module_megamix:structures/basic_fort/basic_snow_fort
execute if score Random RandomOne5556 matches 601..750 run function cm_module_megamix:structures/ruins/basic_snow_fort_ruins
execute if score Random RandomOne5556 matches 751..900 run function cm_module_megamix:structures/ruins/tower_snow_stone_ruins
execute if score Random RandomOne5556 matches 901..999 run function cm_module_megamix:structures/towers/village

execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty