function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..200 run function cm_module_megamix:structures/towers/wood
execute if score Random RandomOne5556 matches 201..400 run function cm_module_megamix:structures/towers/stone
execute if score Random RandomOne5556 matches 401..550 run function cm_module_megamix:structures/basic_fort/basic_fort
execute if score Random RandomOne5556 matches 551..650 run function cm_module_megamix:structures/towers/odd
execute if score Random RandomOne5556 matches 651..700 run function cm_module_megamix:structures/ruins/tower_stone_ruins
execute if score Random RandomOne5556 matches 701..750 run function cm_module_megamix:structures/tree_house
execute if score Random RandomOne5556 matches 751..800 run function cm_module_megamix:structures/ruins/basic_fort_ruins
execute if score Random RandomOne5556 matches 801..980 run function cm_module_megamix:structures/fortress/start
execute if score Random RandomOne5556 matches 981..999 run function cm_module_megamix:structures/deep_dungeon/start


execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty