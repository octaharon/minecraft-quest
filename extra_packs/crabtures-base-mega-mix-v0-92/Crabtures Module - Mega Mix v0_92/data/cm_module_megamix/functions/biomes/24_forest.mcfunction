function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..179 run function cm_module_megamix:structures/towers/wood
execute if score Random RandomOne5556 matches 180..324 run function cm_module_megamix:structures/towers/grass
execute if score Random RandomOne5556 matches 325..449 run function cm_module_megamix:structures/basic_fort/basic_fort
execute if score Random RandomOne5556 matches 450..549 run function cm_module_megamix:structures/ruins/basic_fort_ruins
execute if score Random RandomOne5556 matches 550..649 run function cm_module_megamix:structures/ruins/tower_grass_ruins
execute if score Random RandomOne5556 matches 650..749 run function cm_module_megamix:structures/lone_house
execute if score Random RandomOne5556 matches 750..799 run function cm_module_megamix:structures/huge_tree
execute if score Random RandomOne5556 matches 800..849 run function cm_module_megamix:structures/tree_house
execute if score Random RandomOne5556 matches 850..889 run function cm_module_megamix:structures/camp_start
execute if score Random RandomOne5556 matches 890..919 run function cm_module_megamix:structures/ruins/single_wall
execute if score Random RandomOne5556 matches 920..954 run function cm_module_megamix:structures/ruins/wagon_1
execute if score Random RandomOne5556 matches 955..974 run function cm_module_megamix:structures/deep_dungeon/start
execute if score Random RandomOne5556 matches 975..988 run function cm_module_megamix:structures/wagon_camp_start
execute if score Random RandomOne5556 matches 989..993 run function cm_module_megamix:structures/large_bog
execute if score Random RandomOne5556 matches 994..997 run function cm_module_megamix:structures/small_parts/farm_well_3
execute if score Random RandomOne5556 matches 998 run function cm_module_megamix:structures/fortress/start
execute if score Random RandomOne5556 matches 999 run function cm_module_megamix:structures/rogue_portal_start

execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty