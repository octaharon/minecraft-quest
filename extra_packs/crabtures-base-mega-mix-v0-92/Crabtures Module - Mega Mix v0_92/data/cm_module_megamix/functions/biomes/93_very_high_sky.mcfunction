function structures_crabmaster:randomizers/randomizer_1

execute if score BiomeRegion BiomeRegion5556 matches 0 if score Random RandomOne5556 matches 0..200 run function cm_module_megamix:structures/sky_island/start
execute if score BiomeRegion BiomeRegion5556 matches 0 if score Random RandomOne5556 matches 201..300 run function cm_module_megamix:structures/big_sky_island/start
execute if score BiomeRegion BiomeRegion5556 matches 0 if score Random RandomOne5556 matches 301 run function cm_module_megamix:structures/monolith
execute if score BiomeRegion BiomeRegion5556 matches 0 if score Random RandomOne5556 matches 302..999 run function structures_crabmaster:empty

execute if score BiomeRegion BiomeRegion5556 matches 1..2 if score Random RandomOne5556 matches 0..400 run function cm_module_megamix:structures/sky_island/start
execute if score BiomeRegion BiomeRegion5556 matches 1..2 if score Random RandomOne5556 matches 401..700 run function cm_module_megamix:structures/big_sky_island/start
execute if score BiomeRegion BiomeRegion5556 matches 1..2 if score Random RandomOne5556 matches 701 run function cm_module_megamix:structures/monolith
execute if score BiomeRegion BiomeRegion5556 matches 1..2 if score Random RandomOne5556 matches 702..999 run function structures_crabmaster:empty

execute if score BiomeRegion BiomeRegion5556 matches 3..4 if score Random RandomOne5556 matches 0..500 run function cm_module_megamix:structures/sky_island/start
execute if score BiomeRegion BiomeRegion5556 matches 3..4 if score Random RandomOne5556 matches 501..850 run function cm_module_megamix:structures/big_sky_island/start
execute if score BiomeRegion BiomeRegion5556 matches 3..4 if score Random RandomOne5556 matches 851 run function cm_module_megamix:structures/monolith
execute if score BiomeRegion BiomeRegion5556 matches 3..4 if score Random RandomOne5556 matches 852..999 run function structures_crabmaster:empty

execute if score BiomeRegion BiomeRegion5556 matches 5.. if score Random RandomOne5556 matches 0..250 run function cm_module_megamix:structures/sky_island/start
execute if score BiomeRegion BiomeRegion5556 matches 5.. if score Random RandomOne5556 matches 251..400 run function cm_module_megamix:structures/big_sky_island/start
execute if score BiomeRegion BiomeRegion5556 matches 5.. if score Random RandomOne5556 matches 401 run function cm_module_megamix:structures/monolith
execute if score BiomeRegion BiomeRegion5556 matches 5.. if score Random RandomOne5556 matches 402..999 run function structures_crabmaster:empty

execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty