function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..300 run function cm_module_megamix:structures/ruins/tower_water_ruins
execute if score Random RandomOne5556 matches 301..550 run function cm_module_megamix:structures/huge_tree
execute if score Random RandomOne5556 matches 551..800 run function cm_module_megamix:structures/ruins/wagon_1
execute if score Random RandomOne5556 matches 801..920 run function cm_module_megamix:structures/ruins/tower_stone_ruins
execute if score Random RandomOne5556 matches 921..980 run function cm_module_megamix:structures/ruins/basic_fort_ruins
execute if score Random RandomOne5556 matches 981..999 run function cm_module_megamix:structures/large_bog


execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty