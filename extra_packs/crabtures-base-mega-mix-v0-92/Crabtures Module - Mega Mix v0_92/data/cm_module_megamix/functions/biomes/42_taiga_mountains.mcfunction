function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..300 run function cm_module_megamix:structures/towers/stone
execute if score Random RandomOne5556 matches 301..550 run function cm_module_megamix:structures/towers/odd
execute if score Random RandomOne5556 matches 551..700 run function cm_module_megamix:structures/basic_fort/basic_fort
execute if score Random RandomOne5556 matches 701..800 run function cm_module_megamix:structures/towers/wood
execute if score Random RandomOne5556 matches 801..880 run function cm_module_megamix:structures/ruins/basic_fort_ruins
execute if score Random RandomOne5556 matches 881..910 run function cm_module_megamix:structures/ruins/tower_stone_ruins
execute if score Random RandomOne5556 matches 911..940 run function cm_module_megamix:structures/towers/village
execute if score Random RandomOne5556 matches 941..970 run function cm_module_megamix:structures/clay_patch/start
execute if score Random RandomOne5556 matches 971..980 run function cm_module_megamix:structures/tree_house
execute if score Random RandomOne5556 matches 981..999 run function cm_module_megamix:structures/fortress/start

execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty