function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..350 run function cm_module_megamix:structures/towers/grass
execute if score Random RandomOne5556 matches 351..600 run function cm_module_megamix:structures/towers/odd
execute if score Random RandomOne5556 matches 601..800 run function cm_module_megamix:structures/towers/stone
execute if score Random RandomOne5556 matches 801..900 run function cm_module_megamix:structures/basic_fort/basic_fort
execute if score Random RandomOne5556 matches 901..950 run function cm_module_megamix:structures/palm_patch/start
execute if score Random RandomOne5556 matches 951..990 run function cm_module_megamix:structures/ruins/basic_fort_ruins
execute if score Random RandomOne5556 matches 991..999 run function cm_module_megamix:structures/palm_forest/start


execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty