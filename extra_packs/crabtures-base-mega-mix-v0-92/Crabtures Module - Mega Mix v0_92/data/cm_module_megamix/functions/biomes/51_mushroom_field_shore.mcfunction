function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..600 run function cm_module_megamix:structures/huge_mushroom
execute if score Random RandomOne5556 matches 601..880 run function cm_module_megamix:structures/mush_land/start
execute if score Random RandomOne5556 matches 881..950 run function cm_module_megamix:structures/ruins/tower_stone_ruins
execute if score Random RandomOne5556 matches 951..999 run function cm_module_megamix:structures/end_crystal_patch/start_spire

execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty