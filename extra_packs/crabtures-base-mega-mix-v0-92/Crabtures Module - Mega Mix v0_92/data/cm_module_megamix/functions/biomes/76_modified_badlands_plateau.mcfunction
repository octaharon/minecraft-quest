function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..400 run function cm_module_megamix:structures/towers/terracotta
execute if score Random RandomOne5556 matches 401..700 run function cm_module_megamix:structures/ruins/tower_terracotta_ruins
execute if score Random RandomOne5556 matches 701..850 run function cm_module_megamix:structures/ruins/tower_red_ruins
execute if score Random RandomOne5556 matches 851..997 run function cm_module_megamix:structures/towers/red
execute if score Random RandomOne5556 matches 998..999 run function cm_module_megamix:structures/end_crystal_patch/start


execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty