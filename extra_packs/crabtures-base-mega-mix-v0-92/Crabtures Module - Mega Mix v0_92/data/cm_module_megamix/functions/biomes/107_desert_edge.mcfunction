function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..200 run function cm_module_megamix:structures/towers/stone
execute if score Random RandomOne5556 matches 201..350 run function cm_module_megamix:structures/ruins/wagon_1
execute if score Random RandomOne5556 matches 351..450 run function cm_module_megamix:structures/ruins/basic_fort_ruins
execute if score Random RandomOne5556 matches 451..455 run function cm_module_megamix:structures/monolith


execute if score Random RandomOne5556 matches 550..999 run function structures_crabmaster:empty
execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty