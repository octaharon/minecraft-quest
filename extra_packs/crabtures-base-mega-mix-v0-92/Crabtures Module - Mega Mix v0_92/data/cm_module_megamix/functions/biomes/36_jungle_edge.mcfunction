function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..250 run function cm_module_megamix:structures/towers/grass
execute if score Random RandomOne5556 matches 251..400 run function cm_module_megamix:structures/palm_patch/start
execute if score Random RandomOne5556 matches 401..500 run function cm_module_megamix:structures/palm_forest/start
execute if score Random RandomOne5556 matches 501..620 run function cm_module_megamix:structures/ruins/single_wall
execute if score Random RandomOne5556 matches 621..720 run function cm_module_megamix:structures/basic_fort/basic_fort
execute if score Random RandomOne5556 matches 721..800 run function cm_module_megamix:structures/ruins/wagon_1
execute if score Random RandomOne5556 matches 801..870 run function cm_module_megamix:structures/ruins/basic_fort_ruins
execute if score Random RandomOne5556 matches 871..920 run function cm_module_megamix:structures/ruins/tower_grass_ruins
execute if score Random RandomOne5556 matches 921..960 run function cm_module_megamix:structures/ruins/tower_stone_ruins
execute if score Random RandomOne5556 matches 961..980 run function cm_module_megamix:structures/camp_start
execute if score Random RandomOne5556 matches 981..994 run function cm_module_megamix:structures/wagon_camp_start
execute if score Random RandomOne5556 matches 995..999 run function cm_module_megamix:structures/deep_dungeon/start


execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty