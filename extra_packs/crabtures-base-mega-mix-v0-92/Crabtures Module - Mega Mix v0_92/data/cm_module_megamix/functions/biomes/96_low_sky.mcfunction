function structures_crabmaster:randomizers/randomizer_1

execute if score BiomeRegion BiomeRegion5556 matches 0 if score Random RandomOne5556 matches 0..10 run function cm_module_megamix:structures/sky_island/start
execute if score BiomeRegion BiomeRegion5556 matches 0 if score Random RandomOne5556 matches 11..16 run function cm_module_megamix:structures/big_sky_island/start
execute if score BiomeRegion BiomeRegion5556 matches 0 if score Random RandomOne5556 matches 17 run function cm_module_megamix:structures/towers/grass
execute if score BiomeRegion BiomeRegion5556 matches 0 if score Random RandomOne5556 matches 18..999 run function structures_crabmaster:empty

execute if score BiomeRegion BiomeRegion5556 matches 1..2 if score Random RandomOne5556 matches 0..500 run function cm_module_megamix:structures/sky_island/start
execute if score BiomeRegion BiomeRegion5556 matches 1..2 if score Random RandomOne5556 matches 501..900 run function cm_module_megamix:structures/big_sky_island/start
execute if score BiomeRegion BiomeRegion5556 matches 1..2 if score Random RandomOne5556 matches 901 run function cm_module_megamix:structures/towers/grass
execute if score BiomeRegion BiomeRegion5556 matches 1..2 if score Random RandomOne5556 matches 902..999 run function structures_crabmaster:empty

execute if score BiomeRegion BiomeRegion5556 matches 3..4 if score Random RandomOne5556 matches 0..600 run function cm_module_megamix:structures/sky_island/start
execute if score BiomeRegion BiomeRegion5556 matches 3..4 if score Random RandomOne5556 matches 601..900 run function cm_module_megamix:structures/big_sky_island/start
execute if score BiomeRegion BiomeRegion5556 matches 3..4 if score Random RandomOne5556 matches 901 run function cm_module_megamix:structures/towers/grass
execute if score BiomeRegion BiomeRegion5556 matches 3..4 if score Random RandomOne5556 matches 902..999 run function structures_crabmaster:empty

execute if score BiomeRegion BiomeRegion5556 matches 5.. if score Random RandomOne5556 matches 0..30 run function cm_module_megamix:structures/sky_island/start
execute if score BiomeRegion BiomeRegion5556 matches 5.. if score Random RandomOne5556 matches 31..45 run function cm_module_megamix:structures/big_sky_island/start
execute if score BiomeRegion BiomeRegion5556 matches 5.. if score Random RandomOne5556 matches 46 run function cm_module_megamix:structures/towers/grass
execute if score BiomeRegion BiomeRegion5556 matches 5.. if score Random RandomOne5556 matches 47..999 run function structures_crabmaster:empty

execute if score Random RandomOne5556 matches -1 run function cm_module_megamix:structures/empty