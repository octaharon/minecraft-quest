execute if score Biome Biome5556 matches 104 run function cm_module_megamix:biomes/104_nether_dungeon
execute if score Biome Biome5556 matches 105 run function cm_module_megamix:biomes/105_river_land
execute if score Biome Biome5556 matches 106 run function cm_module_megamix:biomes/106_pit
execute if score Biome Biome5556 matches 107 run function cm_module_megamix:biomes/107_desert_edge
execute if score Biome Biome5556 matches 108 run function cm_module_megamix:biomes/108_snowy_tundra_edge
execute if score Biome Biome5556 matches 109 run function cm_module_megamix:biomes/109_hot_lake
execute if score Biome Biome5556 matches 110 run function cm_module_megamix:biomes/110_cold_lake
execute if score Biome Biome5556 matches 111 run function cm_module_megamix:biomes/111_nether_soul_sand_valley