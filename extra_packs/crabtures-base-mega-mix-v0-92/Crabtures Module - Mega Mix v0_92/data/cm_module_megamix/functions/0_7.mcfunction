execute if score Biome Biome5556 matches 0 run function cm_module_megamix:biomes/0_null
execute if score Biome Biome5556 matches 1 run function cm_module_megamix:biomes/1_generic
execute if score Biome Biome5556 matches 2 run function cm_module_megamix:biomes/2_ocean
execute if score Biome Biome5556 matches 3 run function cm_module_megamix:biomes/3_deep_ocean
execute if score Biome Biome5556 matches 4 run function cm_module_megamix:biomes/4_ocean_ravine
execute if score Biome Biome5556 matches 5 run function cm_module_megamix:biomes/5_frozen_ocean_water
execute if score Biome Biome5556 matches 6 run function cm_module_megamix:biomes/6_frozen_ocean_land
execute if score Biome Biome5556 matches 7 run function cm_module_megamix:biomes/7_deep_frozen_ocean_water