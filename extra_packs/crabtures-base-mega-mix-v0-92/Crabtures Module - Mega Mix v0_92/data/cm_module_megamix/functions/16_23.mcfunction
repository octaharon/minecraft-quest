execute if score Biome Biome5556 matches 16 run function cm_module_megamix:biomes/16_river
execute if score Biome Biome5556 matches 17 run function cm_module_megamix:biomes/17_frozen_river
execute if score Biome Biome5556 matches 18 run function cm_module_megamix:biomes/18_beach_water
execute if score Biome Biome5556 matches 19 run function cm_module_megamix:biomes/19_beach_land
execute if score Biome Biome5556 matches 20 run function cm_module_megamix:biomes/20_stone_shore_water
execute if score Biome Biome5556 matches 21 run function cm_module_megamix:biomes/21_stone_shore_land
execute if score Biome Biome5556 matches 22 run function cm_module_megamix:biomes/22_snowy_beach_water
execute if score Biome Biome5556 matches 23 run function cm_module_megamix:biomes/23_snowy_beach_land