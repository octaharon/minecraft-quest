execute if score Biome Biome5556 matches 24 run function cm_module_megamix:biomes/24_forest
execute if score Biome Biome5556 matches 25 run function cm_module_megamix:biomes/25_wooded_hills
execute if score Biome Biome5556 matches 26 run function cm_module_megamix:biomes/26_plains
execute if score Biome Biome5556 matches 27 run function cm_module_megamix:biomes/27_birch_forest
execute if score Biome Biome5556 matches 28 run function cm_module_megamix:biomes/28_birch_forest_hills
execute if score Biome Biome5556 matches 29 run function cm_module_megamix:biomes/29_tall_birch_forest
execute if score Biome Biome5556 matches 30 run function cm_module_megamix:biomes/30_tall_birch_hills
execute if score Biome Biome5556 matches 31 run function cm_module_megamix:biomes/31_dark_forest