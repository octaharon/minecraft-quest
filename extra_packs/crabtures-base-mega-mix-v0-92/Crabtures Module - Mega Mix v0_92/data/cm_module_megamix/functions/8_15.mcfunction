execute if score Biome Biome5556 matches 8 run function cm_module_megamix:biomes/8_deep_frozen_ocean_land
execute if score Biome Biome5556 matches 9 run function cm_module_megamix:biomes/9_cold_ocean
execute if score Biome Biome5556 matches 10 run function cm_module_megamix:biomes/10_deep_cold_ocean
execute if score Biome Biome5556 matches 11 run function cm_module_megamix:biomes/11_lukewarm_ocean
execute if score Biome Biome5556 matches 12 run function cm_module_megamix:biomes/12_deep_lukewarm_ocean
execute if score Biome Biome5556 matches 13 run function cm_module_megamix:biomes/13_warm_ocean
execute if score Biome Biome5556 matches 14 run function cm_module_megamix:biomes/14_deep_warm_ocean
execute if score Biome Biome5556 matches 15 run function cm_module_megamix:biomes/15_lake