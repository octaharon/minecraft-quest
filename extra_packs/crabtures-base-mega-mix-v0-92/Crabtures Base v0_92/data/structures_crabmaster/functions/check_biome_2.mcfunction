execute store success score Detected Biome5556 unless score Highest Biome5556 matches 16.. store result score Highest Biome5556 if block ~ ~-1 ~ #structures_crabmaster:dirt run scoreboard players add Dirt Biome5556 3

execute store success score Detected Biome5556 unless score Highest Biome5556 matches 16.. store result score Highest Biome5556 if block ~ ~-1 ~ minecraft:snow_block run scoreboard players add Snow Biome5556 3
execute store success score Detected Biome5556 unless score Highest Biome5556 matches 16.. store result score Highest Biome5556 if block ~ ~-1 ~ minecraft:snow run scoreboard players add Snow Biome5556 3

execute unless score Highest Biome5556 matches 16.. store result score Highest Biome5556 if block ~ ~-1 ~ minecraft:snow run scoreboard players remove Desert Biome5556 8
execute unless score Highest Biome5556 matches 16.. store result score Highest Biome5556 if block ~ ~-1 ~ minecraft:snow run scoreboard players remove Beach Biome5556 8

execute store success score Detected Biome5556 unless score Highest Biome5556 matches 16.. store result score Highest Biome5556 if block ~ ~-1 ~ minecraft:red_sand run scoreboard players add Red_Desert Biome5556 6
execute store success score Detected Biome5556 unless score Highest Biome5556 matches 16.. store result score Highest Biome5556 if block ~ ~-1 ~ minecraft:red_sandstone run scoreboard players add Red_Desert Biome5556 6
execute store success score Detected Biome5556 unless score Highest Biome5556 matches 16.. if block ~ ~-1 ~ minecraft:red_sand run scoreboard players add Beach Biome5556 3

execute store success score Detected Biome5556 unless score Highest Biome5556 matches 16.. store result score Highest Biome5556 if block ~ ~-1 ~ minecraft:water run scoreboard players add Beach Biome5556 4
execute store success score Detected Biome5556 unless score Highest Biome5556 matches 16.. store result score Highest Biome5556 if block ~ ~-1 ~ minecraft:tall_seagrass run scoreboard players add Beach Biome5556 4
execute store success score Detected Biome5556 unless score Highest Biome5556 matches 16.. store result score Highest Biome5556 if block ~ ~-1 ~ minecraft:seagrass run scoreboard players add Beach Biome5556 4

execute unless score Highest Biome5556 matches 16.. store result score Highest Biome5556 if block ~ ~-1 ~ minecraft:water run scoreboard players add Marsh Biome5556 4

execute as @s at @s unless score Detected Biome5556 matches 1.. run function structures_crabmaster:check_biome_3