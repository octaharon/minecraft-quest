#These seem like decent spawn rates, so for now they are the defaults.

scoreboard players set Land C_Towers_Config 10
scoreboard players set Sea C_Towers_Config 6
scoreboard players set Sky C_Towers_Config 0
scoreboard players set Cave C_Towers_Config 17
scoreboard players set Nether_Cave C_Towers_Config 16
scoreboard players set Nether_Sea C_Towers_Config 8
scoreboard players set End_Land C_Towers_Config 7
scoreboard players set End_Sky C_Towers_Config 6