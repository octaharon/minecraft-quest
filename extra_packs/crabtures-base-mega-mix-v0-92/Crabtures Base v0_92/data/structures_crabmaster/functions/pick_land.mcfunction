###This file is one I'd like to vastly optimize, there are a LOT of commands here and I'd prefer to lower it.  This still applies a bit even after the v0.8 update which vastly improved the efficiency of this file.  Land structures are still the least efficient.

#Biomes
#0 null
#1 generic
#2 ocean
#3 deep_ocean
#4 ocean (Ravine)
#5 frozen_ocean (Water)
#6 frozen_ocean (Land)
#7 deep_frozen_ocean (Water)
#8 deep_frozen_ocean (Land)
#9 cold_ocean
#10 deep_cold_ocean
#11 lukewarm_ocean
#12 deep_lukewarm_ocean
#13 warm_ocean
#14 deep_warm_ocean
#15 lake
#16 river (Water)
#17 frozen_river
#18 beach (Water)
#19 beach (Land)
#20 stone_shore (Water)
#21 stone_shore (Land)
#22 snowy_beach (Water)
#23 snowy_beach (Land)
#24 forest
#25 wooded_hills
#26 plains
#27 birch_forest
#28 birch_forest_hills
#29 tall_birch_forest
#30 tall_birch_hills
#31 dark_forest
#32 dark_forest_hills
#33 jungle
#34 jungle_hills
#35 modified_jungle
#36 jungle_edge
#37 modified_jungle_edge
#38 bamboo_jungle
#39 bamboo_jungle_hills
#40 taiga
#41 taiga_hills
#42 taiga_mountains
#43 snowy_taiga
#44 snowy_taiga_hills
#45 snowy_taiga_mountains
#46 giant_tree_taiga
#47 giant_tree_taiga_hills
#48 giant_spruce_taiga
#49 giant_spruce_taiga_hills
#50 mushroom_fields
#51 mushroom_field_shore
#52 swamp (Water)
#53 swamp (Land)
#54 swamp_hills
#55 savanna
#56 savanna_plateau
#57 shattered_savanna
#58 shattered_savanna_plateau
#59 flower_forest
#60 sunflower_plains
#61 desert
#62 desert_hills
#63 desert_lakes
#64 snowy_tundra
#65 snowy_mountains
#66 ice_spikes
#67 mountains
#68 mountains (Cold)
#69 wooded_mountains
#70 wooded_mountains (Cold)
#71 gravelly_mountains
#72 modified_gravelly_mountains
#73 mountain_edge
#74 badlands
#75 badlands_plateau
#76 modified_badlands_plateau
#77 wooded_badlands_plateau
#78 modified_wooded_badlands_plateau
#79 eroded_badlands
#80 the_end
#81 small_end_islands
#82 end_midlands
#83 end_highlands
#84 end_barrens
#85 end_sky
#86 end_dungeon
#87 the_void
#88 surface_cave
#89 high_cave
#90 mid_cave
#91 low_cave
#92 lava_cave
#93 very_high_sky
#94 high_sky
#95 mid_sky
#96 low_sky
#97 village
#98 structure
#99 dungeon
#100 nether_wall
#101 nether_air
#102 nether_beach
#103 nether_ocean
#104 nether_dungeon
#105 river (Land)
#106 pit
#107 desert_edge
#108 snowy_tundra_edge
#109 hot_lake
#110 cold_lake
#111 nether_soul_sand_valley
#112 nether_crimson_forest
#113 nether_warped_forest
#114 nether_basalt_deltas

#999 FINISHED - Don't use this, this is the value used to prevent two structures spawning at the same location.

#Biome Regions
#0 - Snow
#1 - Oceans
#2 - Standard
#3 - Forests
#4 - Jungle
#5 - Savanna
#6 - Desert
#7 - Badlands

###A new and improved version of the previous method of placing a structure on the ground.  Seemingly more efficient than using Spreadplayers, especially around water.
execute as @s at @s run function structures_crabmaster:land_high

###Predicates are great, here is where it figures out which biome to spawn based on which predicate-biome is detected.

scoreboard players set Biome Biome5556 0
execute as @s at @s if predicate structures_crabmaster:biomes/plains run scoreboard players set Biome Biome5556 26
execute as @s at @s if predicate structures_crabmaster:biomes/forest run scoreboard players set Biome Biome5556 24
execute as @s at @s if predicate structures_crabmaster:biomes/desert run scoreboard players set Biome Biome5556 61
execute as @s at @s if predicate structures_crabmaster:biomes/savanna run scoreboard players set Biome Biome5556 55
execute as @s at @s if predicate structures_crabmaster:biomes/taiga run scoreboard players set Biome Biome5556 40
execute as @s at @s if predicate structures_crabmaster:biomes/snowy_tundra run scoreboard players set Biome Biome5556 64
execute as @s at @s if predicate structures_crabmaster:biomes/dark_forest run scoreboard players set Biome Biome5556 31
execute as @s at @s if predicate structures_crabmaster:biomes/taiga_hills run scoreboard players set Biome Biome5556 41
execute as @s at @s if predicate structures_crabmaster:biomes/dark_forest_hills run scoreboard players set Biome Biome5556 32
execute as @s at @s if predicate structures_crabmaster:biomes/savanna_plateau run scoreboard players set Biome Biome5556 56
execute as @s at @s if predicate structures_crabmaster:biomes/desert_hills run scoreboard players set Biome Biome5556 62
execute as @s at @s if predicate structures_crabmaster:biomes/birch_forest run scoreboard players set Biome Biome5556 27
execute as @s at @s if predicate structures_crabmaster:biomes/wooded_hills run scoreboard players set Biome Biome5556 25
execute as @s at @s if predicate structures_crabmaster:biomes/snowy_taiga run scoreboard players set Biome Biome5556 43
execute as @s at @s if predicate structures_crabmaster:biomes/mountains run scoreboard players set Biome Biome5556 67
execute as @s at @s if predicate structures_crabmaster:biomes/mountains_cold run scoreboard players set Biome Biome5556 68
execute as @s at @s if predicate structures_crabmaster:biomes/village run scoreboard players set Biome Biome5556 97
execute as @s at @s if predicate structures_crabmaster:biomes/structures run scoreboard players set Biome Biome5556 98
execute as @s at @s if predicate structures_crabmaster:biomes/dungeon run scoreboard players set Biome Biome5556 99
execute as @s at @s if predicate structures_crabmaster:biomes/pit run scoreboard players set Biome Biome5556 106
execute as @s at @s if block ~ ~-1 ~ minecraft:water run scoreboard players set Biome Biome5556 15

execute as @s at @s if score Biome Biome5556 matches 0 run function structures_crabmaster:pick_land_1

execute as @s at @s if score Biome Biome5556 matches 61 positioned ~16 ~ ~ unless predicate structures_crabmaster:biomes/sand_biomes run scoreboard players set Biome Biome5556 107
execute as @s at @s if score Biome Biome5556 matches 61 positioned ~-16 ~ ~ unless predicate structures_crabmaster:biomes/sand_biomes run scoreboard players set Biome Biome5556 107
execute as @s at @s if score Biome Biome5556 matches 61 positioned ~ ~ ~16 unless predicate structures_crabmaster:biomes/sand_biomes run scoreboard players set Biome Biome5556 107
execute as @s at @s if score Biome Biome5556 matches 61 positioned ~ ~ ~-16 unless predicate structures_crabmaster:biomes/sand_biomes run scoreboard players set Biome Biome5556 107
execute as @s at @s if score Biome Biome5556 matches 64 positioned ~16 ~ ~ unless predicate structures_crabmaster:biomes/snow_biomes run scoreboard players set Biome Biome5556 108
execute as @s at @s if score Biome Biome5556 matches 64 positioned ~-16 ~ ~ unless predicate structures_crabmaster:biomes/snow_biomes run scoreboard players set Biome Biome5556 108
execute as @s at @s if score Biome Biome5556 matches 64 positioned ~ ~ ~16 unless predicate structures_crabmaster:biomes/snow_biomes run scoreboard players set Biome Biome5556 108
execute as @s at @s if score Biome Biome5556 matches 64 positioned ~ ~ ~-16 unless predicate structures_crabmaster:biomes/snow_biomes run scoreboard players set Biome Biome5556 108

execute as @s at @s if score Biome Biome5556 matches 15 if predicate structures_crabmaster:biomes/sand_biomes run scoreboard players set Biome Biome5556 109
execute as @s at @s if score Biome Biome5556 matches 15 if predicate structures_crabmaster:biomes/snow_biomes run scoreboard players set Biome Biome5556 110

###This is where the real fun begins, inside here it divvies up which modular pack gets the chance to spawn something.
#execute as @s at @s run function structures_crabmaster:pack_pool

execute as @s at @s run summon minecraft:area_effect_cloud ~ ~ ~ {Duration:2147483647,CustomName:"\"Structure\"",Tags:[C5556,StructHere]}

execute as @s at @s run scoreboard players set @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] BiomeRegion5556 2
execute as @s at @s if predicate structures_crabmaster:biomes/all_snow run scoreboard players set @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] BiomeRegion5556 0
execute as @s at @s if predicate structures_crabmaster:biomes/all_ocean run scoreboard players set @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] BiomeRegion5556 1
execute as @s at @s if predicate structures_crabmaster:biomes/all_forest run scoreboard players set @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] BiomeRegion5556 3
execute as @s at @s if predicate structures_crabmaster:biomes/all_jungle run scoreboard players set @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] BiomeRegion5556 4
execute as @s at @s if predicate structures_crabmaster:biomes/all_savanna run scoreboard players set @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] BiomeRegion5556 5
execute as @s at @s if predicate structures_crabmaster:biomes/all_desert run scoreboard players set @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] BiomeRegion5556 6
execute as @s at @s if predicate structures_crabmaster:biomes/all_badland run scoreboard players set @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] BiomeRegion5556 7

execute as @s at @s run scoreboard players operation @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] Biome5556 = Biome Biome5556