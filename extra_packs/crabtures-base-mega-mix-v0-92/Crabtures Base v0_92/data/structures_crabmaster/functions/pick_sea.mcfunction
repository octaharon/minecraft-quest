#2 ocean
#3 deep_ocean
#4 ocean_ravine
#5 frozen_ocean_water
#7 deep_frozen_ocean_water
#9 cold_ocean
#10 deep_cold_ocean
#11 lukewarm_ocean
#12 deep_lukewarm_ocean
#13 warm_ocean
#14 deep_warm_ocean
#15 lake
#16 river
#18 beach_water
#20 stone_shore_water
#22 snowy_beach_water
#52 swamp_water
#109 hot_lake
#110 cold_lake

###We are making an assumption that Sea Level is Y = 62.

###Gets it to the bottom of the sea, surface stuff can just force-spawn at Y=62
execute at @s as @s run function structures_crabmaster:land_structure
execute at @s as @s run function structures_crabmaster:sink_structure

scoreboard players set Biome Biome5556 257
scoreboard players set Biome Biome5556 15

execute as @s at @s if predicate structures_crabmaster:biomes/ocean run scoreboard players set Biome Biome5556 2
execute as @s at @s if predicate structures_crabmaster:biomes/deep_ocean run scoreboard players set Biome Biome5556 3
execute as @s at @s if predicate structures_crabmaster:biomes/ocean_ravine run scoreboard players set Biome Biome5556 4
execute as @s at @s if predicate structures_crabmaster:biomes/frozen_ocean run scoreboard players set Biome Biome5556 5
execute as @s at @s if predicate structures_crabmaster:biomes/deep_frozen_ocean run scoreboard players set Biome Biome5556 7
execute as @s at @s if predicate structures_crabmaster:biomes/cold_ocean run scoreboard players set Biome Biome5556 9
execute as @s at @s if predicate structures_crabmaster:biomes/deep_cold_ocean run scoreboard players set Biome Biome5556 10
execute as @s at @s if predicate structures_crabmaster:biomes/lukewarm_ocean run scoreboard players set Biome Biome5556 11
execute as @s at @s if predicate structures_crabmaster:biomes/deep_lukewarm_ocean run scoreboard players set Biome Biome5556 12
execute as @s at @s if predicate structures_crabmaster:biomes/warm_ocean run scoreboard players set Biome Biome5556 13
execute as @s at @s if predicate structures_crabmaster:biomes/deep_warm_ocean run scoreboard players set Biome Biome5556 14
execute as @s at @s if predicate structures_crabmaster:biomes/river run scoreboard players set Biome Biome5556 16
execute as @s at @s if predicate structures_crabmaster:biomes/beach run scoreboard players set Biome Biome5556 18
execute as @s at @s if predicate structures_crabmaster:biomes/stone_shore run scoreboard players set Biome Biome5556 20
execute as @s at @s if predicate structures_crabmaster:biomes/snowy_beach run scoreboard players set Biome Biome5556 22
execute as @s at @s if predicate structures_crabmaster:biomes/swamp run scoreboard players set Biome Biome5556 52
execute as @s at @s if predicate structures_crabmaster:biomes/sand_biomes run scoreboard players set Biome Biome5556 109
execute as @s at @s if predicate structures_crabmaster:biomes/snow_biomes run scoreboard players set Biome Biome5556 110

execute as @s at @s if predicate structures_crabmaster:biomes/structures run scoreboard players set Biome Biome5556 98
execute as @s at @s if predicate structures_crabmaster:biomes/dungeon run scoreboard players set Biome Biome5556 99


execute as @s at @s run summon minecraft:area_effect_cloud ~ ~ ~ {Duration:2147483647,CustomName:"\"Structure\"",Tags:[C5556,StructHere]}

execute as @s at @s run scoreboard players set @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] BiomeRegion5556 2
execute as @s at @s if predicate structures_crabmaster:biomes/all_snow run scoreboard players set @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] BiomeRegion5556 0
execute as @s at @s if predicate structures_crabmaster:biomes/all_ocean run scoreboard players set @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] BiomeRegion5556 1
execute as @s at @s if predicate structures_crabmaster:biomes/all_forest run scoreboard players set @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] BiomeRegion5556 3
execute as @s at @s if predicate structures_crabmaster:biomes/all_jungle run scoreboard players set @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] BiomeRegion5556 4
execute as @s at @s if predicate structures_crabmaster:biomes/all_savanna run scoreboard players set @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] BiomeRegion5556 5
execute as @s at @s if predicate structures_crabmaster:biomes/all_desert run scoreboard players set @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] BiomeRegion5556 6
execute as @s at @s if predicate structures_crabmaster:biomes/all_badland run scoreboard players set @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] BiomeRegion5556 7


execute as @s at @s run scoreboard players operation @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] Biome5556 = Biome Biome5556