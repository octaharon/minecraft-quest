execute store result score XPos Maths5556 run data get entity @s Pos[0]
scoreboard players operation XPos Maths5556 /= Sixteen Maths5556
execute store result entity @s Pos[0] double 1 run scoreboard players operation XPos Maths5556 *= Sixteen Maths5556

execute store result score ZPos Maths5556 run data get entity @s Pos[2]
scoreboard players operation ZPos Maths5556 /= Sixteen Maths5556
execute store result entity @s Pos[2] double 1 run scoreboard players operation ZPos Maths5556 *= Sixteen Maths5556