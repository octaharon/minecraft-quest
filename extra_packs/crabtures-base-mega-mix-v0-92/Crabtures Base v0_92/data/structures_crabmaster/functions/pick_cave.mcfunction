#88 surface_cave
#89 high_cave
#90 mid_cave
#91 low_cave
#92 lava_cave
#99 dungeon

###Caves & Sky are a lot simpler than spawning on Land, mostly just need to randomize position and check if said location is valid to spawn at.

scoreboard players set Biome Biome5556 257

function structures_crabmaster:randomizers/randomizer_1

###Teleports are run in sub-functions to get around glitches, as usual.  Also Surface Cave.
execute as @s at @s if score Random RandomOne5556 matches 0..99 run function structures_crabmaster:pick_cave_1
execute as @s at @s if score Random RandomOne5556 matches 100..499 run function structures_crabmaster:pick_cave_2
execute as @s at @s if score Random RandomOne5556 matches 500..999 run function structures_crabmaster:pick_cave_3

execute as @s at @s unless block ~ ~ ~ #structures_crabmaster:water_lava unless block ~ ~5 ~ #structures_crabmaster:water_lava if score Random RandomOne5556 matches 0..99 run scoreboard players set Biome Biome5556 88
execute as @s at @s unless block ~ ~ ~ #structures_crabmaster:water_lava unless block ~ ~5 ~ #structures_crabmaster:water_lava if score Random RandomOne5556 matches 100..349 run scoreboard players set Biome Biome5556 89
execute as @s at @s unless block ~ ~ ~ #structures_crabmaster:water_lava unless block ~ ~5 ~ #structures_crabmaster:water_lava if score Random RandomOne5556 matches 350..619 run scoreboard players set Biome Biome5556 90
execute as @s at @s unless block ~ ~ ~ #structures_crabmaster:water_lava unless block ~ ~5 ~ #structures_crabmaster:water_lava if score Random RandomOne5556 matches 620..999 run scoreboard players set Biome Biome5556 91

execute as @s at @s if block ~ ~ ~ minecraft:lava run scoreboard players set Biome Biome5556 92
execute as @s at @s if block ~ ~1 ~ minecraft:lava run scoreboard players set Biome Biome5556 92
execute as @s at @s if block ~ ~-2 ~ minecraft:lava run scoreboard players set Biome Biome5556 92
execute as @s at @s if block ~2 ~ ~ minecraft:lava run scoreboard players set Biome Biome5556 92
execute as @s at @s if block ~ ~ ~2 minecraft:lava run scoreboard players set Biome Biome5556 92
execute as @s at @s if block ~-2 ~ ~ minecraft:lava run scoreboard players set Biome Biome5556 92
execute as @s at @s if block ~ ~ ~-2 minecraft:lava run scoreboard players set Biome Biome5556 92
execute as @s at @s if block ~ ~ ~ minecraft:obsidian run scoreboard players set Biome Biome5556 92

execute as @s at @s if predicate structures_crabmaster:biomes/dungeon run scoreboard players set Biome Biome5556 99

execute as @s at @s run summon minecraft:area_effect_cloud ~ ~ ~ {Duration:2147483647,CustomName:"\"Structure\"",Tags:[C5556,StructHere]}

execute as @s at @s run scoreboard players set @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] BiomeRegion5556 2
execute as @s at @s if predicate structures_crabmaster:biomes/all_snow run scoreboard players set @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] BiomeRegion5556 0
execute as @s at @s if predicate structures_crabmaster:biomes/all_ocean run scoreboard players set @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] BiomeRegion5556 1
execute as @s at @s if predicate structures_crabmaster:biomes/all_forest run scoreboard players set @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] BiomeRegion5556 3
execute as @s at @s if predicate structures_crabmaster:biomes/all_jungle run scoreboard players set @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] BiomeRegion5556 4
execute as @s at @s if predicate structures_crabmaster:biomes/all_savanna run scoreboard players set @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] BiomeRegion5556 5
execute as @s at @s if predicate structures_crabmaster:biomes/all_desert run scoreboard players set @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] BiomeRegion5556 6
execute as @s at @s if predicate structures_crabmaster:biomes/all_badland run scoreboard players set @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] BiomeRegion5556 7


execute as @s at @s run scoreboard players operation @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] Biome5556 = Biome Biome5556