#100 nether_wall
#101 nether_wastes
#102 nether_beach
#103 nether_ocean
#104 nether_dungeon
#111 nether_soul_sand_valley
#112 nether_crimson_forest
#113 nether_warped_forest

###Lava at Y = 31?  Yeah, lava sea.  Cool.

scoreboard players set Biome Biome5556 257


execute as @s at @s store result entity @s Pos[1] double 1 run scoreboard players set YPos Maths5556 31

execute as @s at @s if block ~ 31 ~ minecraft:lava run scoreboard players set Biome Biome5556 103

execute as @s at @s run summon minecraft:area_effect_cloud ~ ~ ~ {Duration:2147483647,CustomName:"\"Structure\"",Tags:[C5556,StructHere]}
execute as @s at @s run scoreboard players operation @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] Biome5556 = Biome Biome5556