#This series of commands counts up for each global entity that is being kept in the pool, it is more efficient to use a pool of teleported enemies than to repeatedly spawn and delete them.  
#If too many or not enough of them are detected the whole set is killed and recreated to ensure the correct number exists.

execute unless entity @a[scores={Robin5556=0..}] run scoreboard players set @a Robin5556 0

scoreboard players set Persist_Count TestResult5556 0
execute as @e[tag=Persist,tag=C5556] at @s run scoreboard players add Persist_Count TestResult5556 1

execute if score Persist_Count TestResult5556 matches 7.. run kill @e[tag=Persist,tag=C5556]

execute unless score Persist_Count TestResult5556 matches 6 run summon minecraft:area_effect_cloud 0 128 0 {Duration:2147483647,CustomName:"\"SpiralScan\"",Tags:[C5556,WScan,W1,Persist],PortalCooldown:9999999}
execute unless score Persist_Count TestResult5556 matches 6 run summon minecraft:area_effect_cloud 0 128 0 {Duration:2147483647,CustomName:"\"SpiralScan\"",Tags:[C5556,WScan,W2,Persist],PortalCooldown:9999999}
execute unless score Persist_Count TestResult5556 matches 6 run summon minecraft:area_effect_cloud 0 128 0 {Duration:2147483647,CustomName:"\"SpiralScan\"",Tags:[C5556,WScan,W3,Persist],PortalCooldown:9999999}
execute unless score Persist_Count TestResult5556 matches 6 run summon minecraft:area_effect_cloud 0 128 0 {Duration:2147483647,CustomName:"\"SpiralScan\"",Tags:[C5556,WScan,W4,Persist],PortalCooldown:9999999}
execute unless score Persist_Count TestResult5556 matches 6 run summon minecraft:area_effect_cloud 0 128 0 {Duration:2147483647,CustomName:"\"BuildingScan\"",Tags:[C5556,BScan,Persist],PortalCooldown:9999999}
execute unless score Persist_Count TestResult5556 matches 6 run summon minecraft:area_effect_cloud 0 128 0 {Duration:2147483647,CustomName:"\"BiomeScan\"",Tags:[C5556,TScan,Persist],PortalCooldown:9999999}
