#80 the_end
#81 small_end_islands
#82 end_midlands
#83 end_highlands
#84 end_barrens
#85 end_sky
#86 end_dungeon

###Just gotta make sure the scanner is in the air, then you gotta shove it violently into the ground and see if that ground is end stone.

scoreboard players set Biome Biome5556 257

execute as @s at @s store result entity @s Pos[1] double 1 run scoreboard players set YPos Maths5556 70

execute as @s at @s run function structures_crabmaster:land_structure

execute as @s at @s if predicate structures_crabmaster:biomes/end_midlands run scoreboard players set Biome Biome5556 82
execute as @s at @s if predicate structures_crabmaster:biomes/end_highlands run scoreboard players set Biome Biome5556 83
execute as @s at @s if predicate structures_crabmaster:biomes/small_end_islands run scoreboard players set Biome Biome5556 81
execute as @s at @s if predicate structures_crabmaster:biomes/end_barrens run scoreboard players set Biome Biome5556 84
execute as @s at @s if predicate structures_crabmaster:biomes/end_dungeon run scoreboard players set Biome Biome5556 86
execute as @s at @s if predicate structures_crabmaster:biomes/the_end run scoreboard players set Biome Biome5556 80

execute as @s at @s if block ~ ~-1 ~ minecraft:air run scoreboard players set Biome Biome5556 999
execute as @s at @s if predicate structures_crabmaster:biomes/bottom run scoreboard players set Biome Biome5556 999

execute as @s at @s run summon minecraft:area_effect_cloud ~ ~ ~ {Duration:2147483647,CustomName:"\"Structure\"",Tags:[C5556,StructHere]}
execute as @s at @s run scoreboard players operation @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] Biome5556 = Biome Biome5556