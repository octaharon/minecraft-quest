###The script is simple, if a tile below matches from a pool, move the entity downward.  We don't use teleports because teleports are trash in datapacks.

execute store result score YPos Maths5556 run data get entity @s Pos[1]
scoreboard players set Loops Results5556 0

#execute as @s at @s if block ~ ~-8 ~ #structures_crabmaster:full_landing store result entity @s Pos[1] double 1 store success score Loops Results5556 run scoreboard players remove YPos Maths5556 8
execute as @s at @s if block ~ ~-4 ~ #structures_crabmaster:full_landing store result entity @s Pos[1] double 1 store success score Loops Results5556 run scoreboard players remove YPos Maths5556 4
execute as @s at @s if block ~ ~-2 ~ #structures_crabmaster:full_landing store result entity @s Pos[1] double 1 store success score Loops Results5556 run scoreboard players remove YPos Maths5556 2
execute as @s at @s if block ~ ~-1 ~ #structures_crabmaster:full_landing store result entity @s Pos[1] double 1 store success score Loops Results5556 run scoreboard players remove YPos Maths5556 1

###This is the variation of the script that loops the downward movement, it does not include those final two checks which only apply to grass and flowers that only need to be checked once.
execute positioned ~ ~-1 ~ if score Loops Results5556 matches 1.. run function structures_crabmaster:land_structure_1

execute as @s at @s if block ~ ~-1 ~ #structures_crabmaster:landable store result entity @s Pos[1] double 1 run scoreboard players remove YPos Maths5556 1
execute as @s at @s if block ~ ~-1 ~ #structures_crabmaster:tall_landable store result entity @s Pos[1] double 1 run scoreboard players remove YPos Maths5556 2