#This places the entity and scoreboard value in place to start the protection system.  A variation of this without the tellraw could tecnically be used to protect areas around the player when entering and exiting the Nether?

summon minecraft:area_effect_cloud 0 128 0 {Duration:2147483647,CustomName:"\"WorldProtec\"",Tags:[C5556,WProtec]}

scoreboard players set ProtecGen TestResult5556 1

tellraw @a ["",{"text":"\n\n"},{"text":"Now Protecting Chunks.  Visit All Areas That Need Protection.","color":"green"},{"text":"\n"},{"text":"[Click Here]","color":"gold","clickEvent":{"action":"run_command","value":"/function structures_crabmaster:protect_end"}},{"text":" when protection is completed."}]