#This is a looping system that fills in chunks as if the generator has already interacted with them, allowing the player to protect areas from structures spawning on them if used in an existing world.

tp @e[tag=WProtec,tag=C5556] @r
execute as @e[tag=WProtec,tag=C5556] at @s run function structures_crabmaster:chunk_align

fill ~ 0 ~ ~ 0 ~ minecraft:structure_void
fill ~16 0 ~ ~16 0 ~ minecraft:structure_void
fill ~-16 0 ~ ~-16 0 ~ minecraft:structure_void
fill ~ 0 ~16 ~ 0 ~16 minecraft:structure_void
fill ~ 0 ~-16 ~ 0 ~-16 minecraft:structure_void
fill ~16 0 ~16 ~16 0 ~16 minecraft:structure_void
fill ~-16 0 ~-16 ~-16 0 ~-16 minecraft:structure_void
fill ~-16 0 ~16 ~-16 0 ~16 minecraft:structure_void
fill ~16 0 ~-16 ~16 0 ~-16 minecraft:structure_void

#This simply shows a visual of the bounds to the player.  I would love to find a way to show the player every chunk that is already protected.

particle minecraft:barrier ~8 100 ~-16 11 0 0 0 5 force
particle minecraft:barrier ~8 100 ~32 11 0 0 0 5 force

particle minecraft:barrier ~-16 100 ~8 0 0 11 0 5 force
particle minecraft:barrier ~32 100 ~8 0 0 11 0 5 force