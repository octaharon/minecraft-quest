#100 nether_wall
#101 nether_wastes
#102 nether_beach
#103 nether_ocean
#104 nether_dungeon
#111 nether_soul_sand_valley
#112 nether_crimson_forest
#113 nether_warped_forest

###Caves & Sky are a lot simpler than spawning on Land, mostly just need to randomize position and check if said location is valid to spawn at.

scoreboard players set Biome Biome5556 257

function structures_crabmaster:randomizers/randomizer_1

###Teleports are run in sub-functions to get around glitches, as usual
execute as @s at @s if score Random RandomOne5556 matches 0..499 run function structures_crabmaster:pick_nether_cave_1
execute as @s at @s if score Random RandomOne5556 matches 500..999 run function structures_crabmaster:pick_nether_cave_2

scoreboard players set Biome Biome5556 101
execute as @s at @s if block ~ ~ ~ minecraft:netherrack run scoreboard players set Biome Biome5556 100
execute as @s at @s if predicate structures_crabmaster:biomes/soul_sand_valley run scoreboard players set Biome Biome5556 111
execute as @s at @s if predicate structures_crabmaster:biomes/crimson_forest run scoreboard players set Biome Biome5556 112
execute as @s at @s if predicate structures_crabmaster:biomes/warped_forest run scoreboard players set Biome Biome5556 113
execute as @s at @s if predicate structures_crabmaster:biomes/basalt_deltas run scoreboard players set Biome Biome5556 114
execute as @s at @s if predicate structures_crabmaster:biomes/nether_dungeon run scoreboard players set Biome Biome5556 104
execute as @s at @s if block ~ 32 ~ minecraft:soul_sand run scoreboard players set Biome Biome5556 102
execute as @s at @s if block ~ 32 ~ minecraft:gravel run scoreboard players set Biome Biome5556 102
execute as @s at @s if block ~ 33 ~ minecraft:gravel run scoreboard players set Biome Biome5556 102

execute as @s at @s if score Biome Biome5556 matches 102 store result entity @s Pos[1] double 1 run scoreboard players set YPos Maths5556 32

execute as @s at @s run summon minecraft:area_effect_cloud ~ ~ ~ {Duration:2147483647,CustomName:"\"Structure\"",Tags:[C5556,StructHere]}
execute as @s at @s run scoreboard players operation @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] Biome5556 = Biome Biome5556