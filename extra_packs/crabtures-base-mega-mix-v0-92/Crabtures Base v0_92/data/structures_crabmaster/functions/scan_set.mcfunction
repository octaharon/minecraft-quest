execute if entity @a[scores={Robin5556=1}] run scoreboard players set @a Robin5556 0
scoreboard players set @r[scores={Robin5556=0}] Robin5556 2

execute as @s at @s run tp @s @p[scores={Robin5556=2}]
scoreboard players set @a[scores={Robin5556=2}] Robin5556 1

scoreboard players set @s Distance5556 0
scoreboard players set @s Length5556 10

execute as @s at @s run function structures_crabmaster:chunk_align

execute as @s[tag=W1] at @s run function structures_crabmaster:scan_set_1
execute as @s[tag=W2] at @s run function structures_crabmaster:scan_set_2
execute as @s[tag=W3] at @s run function structures_crabmaster:scan_set_3
execute as @s[tag=W4] at @s run function structures_crabmaster:scan_set_4