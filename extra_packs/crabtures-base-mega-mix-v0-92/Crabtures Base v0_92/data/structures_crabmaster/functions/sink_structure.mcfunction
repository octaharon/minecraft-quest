###Land structure, but for water.

execute store result score YPos Maths5556 run data get entity @s Pos[1]
scoreboard players set Loops Results5556 0

execute as @s at @s if block ~ ~-8 ~ #structures_crabmaster:water_lava store result entity @s Pos[1] double 1 store success score Loops Results5556 run scoreboard players remove YPos Maths5556 8
execute as @s at @s if block ~ ~-4 ~ #structures_crabmaster:water_lava store result entity @s Pos[1] double 1 store success score Loops Results5556 run scoreboard players remove YPos Maths5556 4
execute as @s at @s if block ~ ~-2 ~ #structures_crabmaster:water_lava store result entity @s Pos[1] double 1 store success score Loops Results5556 run scoreboard players remove YPos Maths5556 2
execute as @s at @s if block ~ ~-1 ~ #structures_crabmaster:water_lava store result entity @s Pos[1] double 1 store success score Loops Results5556 run scoreboard players remove YPos Maths5556 1

execute positioned ~ ~-1 ~ if score Loops Results5556 matches 1.. run function structures_crabmaster:sink_structure_1