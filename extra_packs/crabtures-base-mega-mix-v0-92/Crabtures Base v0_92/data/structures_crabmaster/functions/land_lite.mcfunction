###A variation of Land Structure meant to land ontop of trees and flowers.

execute store result score YPos Maths5556 run data get entity @s Pos[1]
scoreboard players set Loops Results5556 0

execute as @s at @s if block ~ ~-2 ~ #structures_crabmaster:landloops store result entity @s Pos[1] double 1 store success score Loops Results5556 run scoreboard players remove YPos Maths5556 2
execute as @s at @s if block ~ ~-1 ~ #structures_crabmaster:landloops store result entity @s Pos[1] double 1 store success score Loops Results5556 run scoreboard players remove YPos Maths5556 1

execute positioned ~ ~-1 ~ if score Loops Results5556 matches 1.. run function structures_crabmaster:land_lite_1

execute as @s at @s if block ~ ~-1 ~ minecraft:grass store result entity @s Pos[1] double 1 run scoreboard players remove YPos Maths5556 1
execute as @s at @s if block ~ ~-1 ~ minecraft:fern store result entity @s Pos[1] double 1 run scoreboard players remove YPos Maths5556 1
execute as @s at @s if block ~ ~-1 ~ minecraft:tall_grass store result entity @s Pos[1] double 1 run scoreboard players remove YPos Maths5556 2
execute as @s at @s if block ~ ~-1 ~ minecraft:large_fern store result entity @s Pos[1] double 1 run scoreboard players remove YPos Maths5556 2