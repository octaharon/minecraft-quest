###These randomizers will be common throughout the many files in this pack.  They are very useful and from my tests do not have bias.  The randomizers work by repeatedly picking between 2 entities with a 50/50 chance adding scores of varying powers of 2.

###This places the Building Scanner at the World Scanner so it is nice and ready to rumble.  This has to be put in a sub-function due to Minecraft's glitchy datapack teleportation issues.
execute as @s at @s run function structures_crabmaster:to_scanner

function structures_crabmaster:randomizers/randomizer_1
execute in minecraft:overworld if score Land C_Towers_Config >= Random RandomOne5556 unless block ~8 62 ~8 #structures_crabmaster:water_lava as @e[limit=1,tag=C5556,tag=BScan,distance=0..] at @s run function structures_crabmaster:pick_land

function structures_crabmaster:randomizers/randomizer_1
execute in minecraft:overworld if score Sea C_Towers_Config >= Random RandomOne5556 if block ~8 62 ~8 #structures_crabmaster:water_lava as @e[limit=1,tag=C5556,tag=BScan,distance=0..] at @s run function structures_crabmaster:pick_sea

function structures_crabmaster:randomizers/randomizer_1
execute in minecraft:overworld if score Sky C_Towers_Config >= Random RandomOne5556 as @e[limit=1,tag=C5556,tag=BScan,distance=0..] at @s run function structures_crabmaster:pick_sky

function structures_crabmaster:randomizers/randomizer_1
execute in minecraft:overworld if score Cave C_Towers_Config >= Random RandomOne5556 as @e[limit=1,tag=C5556,tag=BScan,distance=0..] at @s run function structures_crabmaster:pick_cave

function structures_crabmaster:randomizers/randomizer_1
execute in minecraft:the_nether if score Nether_Cave C_Towers_Config >= Random RandomOne5556 as @e[limit=1,tag=C5556,tag=BScan,distance=0..] at @s run function structures_crabmaster:pick_nether_cave

function structures_crabmaster:randomizers/randomizer_1
execute in minecraft:the_nether if score Nether_Sea C_Towers_Config >= Random RandomOne5556 as @e[limit=1,tag=C5556,tag=BScan,distance=0..] at @s run function structures_crabmaster:pick_nether_sea

function structures_crabmaster:randomizers/randomizer_1
execute in minecraft:the_end if score End_Land C_Towers_Config >= Random RandomOne5556 as @e[limit=1,tag=C5556,tag=BScan,distance=0..] at @s run function structures_crabmaster:pick_end_land

function structures_crabmaster:randomizers/randomizer_1
execute in minecraft:the_end if score End_Sky C_Towers_Config >= Random RandomOne5556 as @e[limit=1,tag=C5556,tag=BScan,distance=0..] at @s run function structures_crabmaster:pick_end_sky

###Yet another function built to ensure Minecraft doesn't decide to explode the teleport command.
execute as @s at @s run function structures_crabmaster:teleport_back