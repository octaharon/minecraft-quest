###Here we teleport around randomly to add some noise to the sampling of nearby tiles.

function structures_crabmaster:randomizers/randomizer_1

execute as @s at @s if score Random RandomOne5556 matches 0..249 run tp @s ~2 ~10 ~
execute as @s at @s if score Random RandomOne5556 matches 250..499 run tp @s ~ ~10 ~2
execute as @s at @s if score Random RandomOne5556 matches 500..749 run tp @s ~-2 ~10 ~
execute as @s at @s if score Random RandomOne5556 matches 750..999 run tp @s ~ ~10 ~-2

execute as @s at @s run function structures_crabmaster:land_lite

###After getting a new random position nearby, it moves on to the actual block/biome detection.  After that it loops until one of the biomes has enough points to count as a succcess.
execute as @s at @s run function structures_crabmaster:check_biome_1

scoreboard players add @s TimerOne5556 1
execute unless score @s TimerOne5556 matches 20.. unless score Highest Biome5556 matches 16.. run function structures_crabmaster:check_biome