###This is the spot that makes the chunk-protection commands loop when the values are set to do so.
execute if score ProtecGen TestResult5556 matches 1 as @e[tag=WProtec,tag=C5556] at @s run function structures_crabmaster:protect
execute if score BeginGen TestResult5556 matches 1 run scoreboard players add Delegate TimerOne5556 1

###This is a set of looping commands that happen only once a second, likely more commands could be delegated between frames like this for efficiency but for now it's just the one that ensures global entities don't despawn.
execute if score Delegate TimerOne5556 matches 1 run function structures_crabmaster:set_scanner
execute if score Delegate TimerOne5556 matches 20.. run scoreboard players set Delegate TimerOne5556 0

###"if entity @p" is my attempt to stop code running on servers if there are no players.  Not sure if it works lol.

###This resets the scanners back to the player position and preps them for another go.
execute as @e[tag=WScan,tag=C5556] at @s unless score @s TimerOne5556 matches 1.. if entity @p run function structures_crabmaster:scan_set

execute as @e[tag=WScan,tag=C5556] at @s run function structures_crabmaster:chunk_align

###This divides up each direction the scanners will move.
execute as @e[tag=WScan,tag=C5556,scores={Direction5556=0}] at @s if entity @p run function structures_crabmaster:spiral/down_1
execute as @e[tag=WScan,tag=C5556,scores={Direction5556=1}] at @s if entity @p run function structures_crabmaster:spiral/left_1
execute as @e[tag=WScan,tag=C5556,scores={Direction5556=2}] at @s if entity @p run function structures_crabmaster:spiral/up_1
execute as @e[tag=WScan,tag=C5556,scores={Direction5556=3}] at @s if entity @p run function structures_crabmaster:spiral/right_1

tag @e[tag=C5556,tag=Module] remove Selected
tag @e[tag=C5556,tag=Module,sort=random,limit=1] add Selected

###Forceload will be used for spawning large structures, this spot here is to ensure all the extra loaded chunks get unloaded, while ensureing the 0,0,0 coordinates of all dimensions stay loaded for the object pool to not break.  
###The execute variant is needed for the End and Nether.
forceload remove all
execute as @a[limit=1,sort=random] at @s run forceload remove all
forceload add 0 0 0 0
execute as @a[limit=1,sort=random] at @s run forceload add 0 0 0 0
