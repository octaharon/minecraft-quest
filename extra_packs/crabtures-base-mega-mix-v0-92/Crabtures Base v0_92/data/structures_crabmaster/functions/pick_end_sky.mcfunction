#80 the_end
#81 small_end_islands
#82 end_midlands
#83 end_highlands
#84 end_barrens
#85 end_sky
#86 end_dungeon

###Caves & Sky are a lot simpler than spawning on Land, mostly just need to randomize position and check if said location is valid to spawn at.

scoreboard players set Biome Biome5556 257

function structures_crabmaster:randomizers/randomizer_1

###Teleports are run in sub-functions to get around glitches, as usual
execute as @s at @s if score Random RandomOne5556 matches 0..499 run function structures_crabmaster:pick_end_sky_1
execute as @s at @s if score Random RandomOne5556 matches 500..999 run function structures_crabmaster:pick_end_sky_2

execute as @s at @s if block ~ ~ ~ minecraft:air run scoreboard players set Biome Biome5556 85
execute as @s at @s if block ~ ~ ~ minecraft:air if predicate structures_crabmaster:biomes/end_barrens run scoreboard players set Biome Biome5556 84
execute as @s at @s if block ~ ~ ~ minecraft:air if predicate structures_crabmaster:biomes/end_dungeon run scoreboard players set Biome Biome5556 86
execute as @s at @s if block ~ ~ ~ minecraft:air if predicate structures_crabmaster:biomes/the_end run scoreboard players set Biome Biome5556 80

execute as @s at @s run summon minecraft:area_effect_cloud ~ ~ ~ {Duration:2147483647,CustomName:"\"Structure\"",Tags:[C5556,StructHere]}
execute as @s at @s run scoreboard players operation @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] Biome5556 = Biome Biome5556