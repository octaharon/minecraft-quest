execute as @s at @s if predicate structures_crabmaster:biomes/taiga_mountains run scoreboard players set Biome Biome5556 42
execute as @s at @s if predicate structures_crabmaster:biomes/jungle run scoreboard players set Biome Biome5556 33
execute as @s at @s if predicate structures_crabmaster:biomes/jungle_hills run scoreboard players set Biome Biome5556 34
execute as @s at @s if predicate structures_crabmaster:biomes/wooded_mountains run scoreboard players set Biome Biome5556 69
execute as @s at @s if predicate structures_crabmaster:biomes/wooded_mountains_cold run scoreboard players set Biome Biome5556 70
execute as @s at @s if predicate structures_crabmaster:biomes/birch_forest_hills run scoreboard players set Biome Biome5556 28
execute as @s at @s if predicate structures_crabmaster:biomes/snowy_taiga_hills run scoreboard players set Biome Biome5556 44
execute as @s at @s if predicate structures_crabmaster:biomes/tall_birch_forest run scoreboard players set Biome Biome5556 29
execute as @s at @s if predicate structures_crabmaster:biomes/tall_birch_hills run scoreboard players set Biome Biome5556 30
execute as @s at @s if predicate structures_crabmaster:biomes/beach run scoreboard players set Biome Biome5556 19
execute as @s at @s if predicate structures_crabmaster:biomes/swamp run scoreboard players set Biome Biome5556 53
execute as @s at @s if predicate structures_crabmaster:biomes/snowy_taiga_mountains run scoreboard players set Biome Biome5556 45
execute as @s at @s if predicate structures_crabmaster:biomes/snowy_mountains run scoreboard players set Biome Biome5556 65
execute as @s at @s if predicate structures_crabmaster:biomes/modified_jungle run scoreboard players set Biome Biome5556 35
execute as @s at @s if predicate structures_crabmaster:biomes/snowy_beach run scoreboard players set Biome Biome5556 23
execute as @s at @s if predicate structures_crabmaster:biomes/frozen_river run scoreboard players set Biome Biome5556 17

execute as @s at @s if score Biome Biome5556 matches 0 run function structures_crabmaster:pick_land_2