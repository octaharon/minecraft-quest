###This creates all the scoreboard values needed for the entire pack.  Standard stuff.

#forceload can be used in 1.14+, this is great for structures that wont fit inside a single chunk.

forceload add 0 0 0 0

scoreboard objectives add FirstRun5556 dummy

scoreboard objectives add RandomOne5556 dummy
scoreboard objectives add RandomTwo5556 dummy
scoreboard objectives add RandomThree5556 dummy

scoreboard objectives add Robin5556 dummy

scoreboard objectives add TestResult5556 dummy
scoreboard objectives add Maths5556 dummy
scoreboard objectives add TimerOne5556 dummy

scoreboard objectives add Direction5556 dummy
scoreboard objectives add Length5556 dummy
scoreboard objectives add Distance5556 dummy
scoreboard objectives add Results5556 dummy

scoreboard objectives add Biome5556 dummy
scoreboard objectives add BiomeRegion5556 dummy

scoreboard objectives add C_Towers_Config dummy

scoreboard objectives add Loops_Count dummy

scoreboard objectives add IslandTest5556 dummy

scoreboard players set One Maths5556 1
scoreboard players set Two Maths5556 2
scoreboard players set Three Maths5556 3
scoreboard players set Five Maths5556 5
scoreboard players set Ten Maths5556 10
scoreboard players set Sixteen Maths5556 16
scoreboard players set Forty Maths5556 40

scoreboard players set KiloMod Maths5556 1000

scoreboard players set XFirst Maths5556 0
scoreboard players set XSecond Maths5556 0
scoreboard players set ZFirst Maths5556 0
scoreboard players set ZSecond Maths5556 0

scoreboard objectives add ExtraLogic5556 dummy

scoreboard players set RandomX RandomOne5556 0
scoreboard players set RandomY RandomOne5556 0
scoreboard players set Random RandomOne5556 0
scoreboard players set RandomX RandomTwo5556 0
scoreboard players set RandomY RandomTwo5556 0
scoreboard players set Random RandomTwo5556 0
scoreboard players set RandomX RandomThree5556 0
scoreboard players set RandomY RandomThree5556 0
scoreboard players set Random RandomThree5556 0

###These commands should never run a second time so long as FirstRun's FirstRun5556 value isn't reset somehow.

execute as @p at @s unless score FirstRun FirstRun5556 matches 1 run scoreboard players set FirstRun FirstRun5556 0

execute unless score FirstRun FirstRun5556 matches 1 run function structures_crabmaster:first_run
execute unless score FirstRun FirstRun5556 matches 1 run function structures_crabmaster:structure_defaults

execute as @p at @s run scoreboard players set FirstRun FirstRun5556 1

#The below is the toggle between pre and post generator start splash-text when loading a world or "/reload"ing it.

execute unless score BeginGen TestResult5556 matches 1 run gamerule sendCommandFeedback false
execute unless score BeginGen TestResult5556 matches 1 run tellraw @a ["",{"text":"\nsendCommandFeedback is now set to: false"}]

execute if score BeginGen TestResult5556 matches 1 run tellraw @a ["",{"text":"\n\n"},{"text":"Structure Generator Running, Configure?","color":"green"},{"text":"\n"},{"text":"[Click Here]","color":"light_purple","clickEvent":{"action":"run_command","value":"/function structures_crabmaster:adjust_frequency"}},{"text":" to adjust structure spawn frequency.\n"},{"text":"[Click Here]","color":"light_purple","clickEvent":{"action":"run_command","value":"/function structures_crabmaster:protect_start"}},{"text":" to protect existing chunks."}]
execute unless score BeginGen TestResult5556 matches 1 run tellraw @a ["",{"text":"\n\n"},{"text":"Crabmaster's Structure Pack v0.92 Loaded.","color":"yellow"},{"text":"\n"},{"text":"[Click Here]","color":"light_purple","clickEvent":{"action":"run_command","value":"/function structures_crabmaster:adjust_frequency"}},{"text":" to adjust structure spawn frequency.\n"},{"text":"[Click Here]","color":"light_purple","clickEvent":{"action":"run_command","value":"/function structures_crabmaster:protect_start"}},{"text":" to protect existing chunks.\nWhen ready, "},{"text":"[Click Here]","color":"gold","clickEvent":{"action":"run_command","value":"/function structures_crabmaster:start_generator"}},{"text":" to start the generator."}]