###All 3 of these sub-functions simply run down the line checking the blocks and setting biome scores depending on what it finds.  These are divided into multiple files as so to make more common biomes run slightly less commands/run more efficiently.
###If I could find a way to optimize this into even less commands I'd feel alive again.

scoreboard players set Detected Biome5556 0

execute store success score Detected Biome5556 unless score Highest Biome5556 matches 16.. store result score Highest Biome5556 if block ~ ~-1 ~ #structures_crabmaster:grass run scoreboard players add Grass Biome5556 3
execute store success score Detected Biome5556 unless score Highest Biome5556 matches 16.. if block ~ ~-1 ~ #structures_crabmaster:grass run scoreboard players add Marsh Biome5556 2

execute store success score Detected Biome5556 unless score Highest Biome5556 matches 16.. store result score Highest Biome5556 if block ~ ~-1 ~ #structures_crabmaster:stone run scoreboard players add Stone Biome5556 6

execute store success score Detected Biome5556 unless score Highest Biome5556 matches 16.. store result score Highest Biome5556 unless block ~ ~-1 ~ minecraft:water if block ~ ~-2 ~ #structures_crabmaster:stone run scoreboard players add Snow_Stone Biome5556 3

execute store success score Detected Biome5556 unless score Highest Biome5556 matches 16.. store result score Highest Biome5556 if block ~ ~-1 ~ minecraft:sand run scoreboard players add Desert Biome5556 4
execute store success score Detected Biome5556 unless score Highest Biome5556 matches 16.. store result score Highest Biome5556 if block ~ ~-1 ~ minecraft:sandstone run scoreboard players add Desert Biome5556 4
execute unless score Highest Biome5556 matches 16.. if block ~ ~-1 ~ minecraft:sand run scoreboard players add Beach Biome5556 3

execute unless score Highest Biome5556 matches 16.. store result score Highest Biome5556 if block ~ ~-1 ~ minecraft:sand run scoreboard players remove Marsh Biome5556 8

execute store success score Detected Biome5556 unless score Highest Biome5556 matches 16.. store result score Highest Biome5556 if block ~ ~-1 ~ minecraft:oak_leaves run scoreboard players add Oak Biome5556 4
execute unless score Highest Biome5556 matches 16.. if block ~ ~-1 ~ minecraft:oak_leaves run scoreboard players add Marsh Biome5556 1

execute store success score Detected Biome5556 unless score Highest Biome5556 matches 16.. store result score Highest Biome5556 if block ~ ~-1 ~ minecraft:spruce_leaves run scoreboard players add Spruce Biome5556 4
execute store success score Detected Biome5556 unless score Highest Biome5556 matches 16.. store result score Highest Biome5556 if block ~ ~-1 ~ minecraft:dark_oak_leaves run scoreboard players add DarkOak Biome5556 4
execute store success score Detected Biome5556 unless score Highest Biome5556 matches 16.. store result score Highest Biome5556 if block ~ ~-1 ~ minecraft:birch_leaves run scoreboard players add Birch Biome5556 4
execute store success score Detected Biome5556 unless score Highest Biome5556 matches 16.. store result score Highest Biome5556 if block ~ ~-1 ~ minecraft:acacia_leaves run scoreboard players add Acacia Biome5556 4
execute store success score Detected Biome5556 unless score Highest Biome5556 matches 16.. store result score Highest Biome5556 if block ~ ~-1 ~ minecraft:jungle_leaves run scoreboard players add Jungle Biome5556 4

execute as @s at @s unless score Detected Biome5556 matches 1.. run function structures_crabmaster:check_biome_2