execute as @s at @s store success score @s TestResult5556 if block ~ 0 ~-32 #structures_crabmaster:air_bedrock_void
execute as @s[scores={TestResult5556=0}] at @s run scoreboard players set @s TimerOne5556 0
execute as @s at @s run tp ~ 128 ~-16

execute if score @s Distance5556 matches ..0 run function structures_crabmaster:spiral/left_extra

scoreboard players remove @s Distance5556 2
scoreboard players remove @s TimerOne5556 1

execute as @s at @s if block ~ 0 ~ #structures_crabmaster:air_bedrock run function structures_crabmaster:scan_world_roll
execute as @s at @s if block ~ 0 ~ #structures_crabmaster:air_bedrock run fill ~ 0 ~ ~ 0 ~ minecraft:structure_void


execute as @s[scores={Direction5556=1,TimerOne5556=1..}] at @s run function structures_crabmaster:spiral/left_2