execute unless score Highest Biome5556 matches 16.. store result score Highest Biome5556 if block ~ ~-1 ~ #structures_crabmaster:flowers run scoreboard players add Flowers Biome5556 8
execute unless score Highest Biome5556 matches 16.. store result score Highest Biome5556 if block ~ ~-1 ~ #minecraft:ice run scoreboard players add Ice Biome5556 6

execute unless score Highest Biome5556 matches 16.. store result score Highest Biome5556 if block ~ ~-1 ~ #minecraft:ice run scoreboard players remove Desert Biome5556 8
execute unless score Highest Biome5556 matches 16.. store result score Highest Biome5556 if block ~ ~-1 ~ #minecraft:ice run scoreboard players remove Beach Biome5556 8

execute unless score Highest Biome5556 matches 16.. store result score Highest Biome5556 if block ~ ~-1 ~ #structures_crabmaster:terracotta run scoreboard players add Terracotta Biome5556 6
execute unless score Highest Biome5556 matches 16.. store result score Highest Biome5556 if block ~ ~-1 ~ minecraft:mycelium run scoreboard players add Mooshroom Biome5556 18
execute unless score Highest Biome5556 matches 16.. store result score Highest Biome5556 if block ~ ~-1 ~ #structures_crabmaster:structures run scoreboard players add Village Biome5556 18
execute unless score Highest Biome5556 matches 16.. store result score Highest Biome5556 if block ~2 ~ ~ #structures_crabmaster:structures run scoreboard players add Village Biome5556 8
execute unless score Highest Biome5556 matches 16.. store result score Highest Biome5556 if block ~-2 ~ ~ #structures_crabmaster:structures run scoreboard players add Village Biome5556 8
execute unless score Highest Biome5556 matches 16.. store result score Highest Biome5556 if block ~ ~ ~2 #structures_crabmaster:structures run scoreboard players add Village Biome5556 8
execute unless score Highest Biome5556 matches 16.. store result score Highest Biome5556 if block ~ ~ ~-2 #structures_crabmaster:structures run scoreboard players add Village Biome5556 8
execute unless score Highest Biome5556 matches 16.. store result score Highest Biome5556 if block ~ ~-1 ~ minecraft:bamboo run scoreboard players add Bamboo Biome5556 8