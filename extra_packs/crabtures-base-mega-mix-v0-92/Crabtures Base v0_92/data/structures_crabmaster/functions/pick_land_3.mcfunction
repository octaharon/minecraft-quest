execute as @s at @s if predicate structures_crabmaster:biomes/badlands run scoreboard players set Biome Biome5556 74
execute as @s at @s if predicate structures_crabmaster:biomes/badlands_plateau run scoreboard players set Biome Biome5556 75
execute as @s at @s if predicate structures_crabmaster:biomes/sunflower_plains run scoreboard players set Biome Biome5556 60
execute as @s at @s if predicate structures_crabmaster:biomes/wooded_badlands_plateau run scoreboard players set Biome Biome5556 77
execute as @s at @s if predicate structures_crabmaster:biomes/swamp_hills run scoreboard players set Biome Biome5556 54
execute as @s at @s if predicate structures_crabmaster:biomes/ice_spikes run scoreboard players set Biome Biome5556 66
execute as @s at @s if predicate structures_crabmaster:biomes/modified_badlands_plateau run scoreboard players set Biome Biome5556 76
execute as @s at @s if predicate structures_crabmaster:biomes/modified_wooded_badlands_plateau run scoreboard players set Biome Biome5556 78
execute as @s at @s if predicate structures_crabmaster:biomes/desert_lakes run scoreboard players set Biome Biome5556 63
execute as @s at @s if predicate structures_crabmaster:biomes/eroded_badlands run scoreboard players set Biome Biome5556 79
execute as @s at @s if predicate structures_crabmaster:biomes/mushroom_fields run scoreboard players set Biome Biome5556 50
execute as @s at @s if predicate structures_crabmaster:biomes/mushroom_field_shore run scoreboard players set Biome Biome5556 51
execute as @s at @s if predicate structures_crabmaster:biomes/river run scoreboard players set Biome Biome5556 105
execute as @s at @s if predicate structures_crabmaster:biomes/cold_ocean run scoreboard players set Biome Biome5556 19
execute as @s at @s if predicate structures_crabmaster:biomes/warm_ocean run scoreboard players set Biome Biome5556 19
execute as @s at @s if predicate structures_crabmaster:biomes/lukewarm_ocean run scoreboard players set Biome Biome5556 19
execute as @s at @s if predicate structures_crabmaster:biomes/ocean run scoreboard players set Biome Biome5556 19

execute as @s at @s if score Biome Biome5556 matches 0 run scoreboard players set Biome Biome5556 1