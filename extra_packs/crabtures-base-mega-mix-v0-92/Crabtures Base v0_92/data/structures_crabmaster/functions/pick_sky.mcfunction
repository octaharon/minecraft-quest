#93 very_high_sky
#94 high_sky
#95 mid_sky
#96 low_sky

#80-97
#100-135
#140-175
#180-215

###Caves & Sky are a lot simpler than spawning on Land, mostly just need to randomize position and check if said location is valid to spawn at.

scoreboard players set Biome Biome5556 257

function structures_crabmaster:randomizers/randomizer_1

execute if score Random RandomOne5556 matches 0..249 unless block ~ 80 ~ minecraft:air run scoreboard players add Random RandomOne5556 250
execute if score Random RandomOne5556 matches 250..499 unless block ~ 101 ~ minecraft:air run scoreboard players add Random RandomOne5556 250
execute if score Random RandomOne5556 matches 0..249 if block ~ 80 ~ minecraft:air run scoreboard players set Biome Biome5556 96
execute if score Random RandomOne5556 matches 250..499 if block ~ 101 ~ minecraft:air run scoreboard players set Biome Biome5556 95
execute if score Random RandomOne5556 matches 500..749 run scoreboard players set Biome Biome5556 94
execute if score Random RandomOne5556 matches 750..999 run scoreboard players set Biome Biome5556 93

function structures_crabmaster:randomizers/randomizer_1

###LOTS of teleports, lots of height variants for sky!
execute as @s at @s if score Biome Biome5556 matches 96 run function structures_crabmaster:pick_sky_1
execute as @s at @s if score Biome Biome5556 matches 95 run function structures_crabmaster:pick_sky_2
execute as @s at @s if score Biome Biome5556 matches 94 run function structures_crabmaster:pick_sky_3
execute as @s at @s if score Biome Biome5556 matches 93 run function structures_crabmaster:pick_sky_4

execute as @s at @s run summon minecraft:area_effect_cloud ~ ~ ~ {Duration:2147483647,CustomName:"\"Structure\"",Tags:[C5556,StructHere]}

execute as @s at @s run scoreboard players set @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] BiomeRegion5556 2
execute as @s at @s if predicate structures_crabmaster:biomes/all_snow run scoreboard players set @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] BiomeRegion5556 0
execute as @s at @s if predicate structures_crabmaster:biomes/all_ocean run scoreboard players set @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] BiomeRegion5556 1
execute as @s at @s if predicate structures_crabmaster:biomes/all_forest run scoreboard players set @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] BiomeRegion5556 3
execute as @s at @s if predicate structures_crabmaster:biomes/all_jungle run scoreboard players set @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] BiomeRegion5556 4
execute as @s at @s if predicate structures_crabmaster:biomes/all_savanna run scoreboard players set @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] BiomeRegion5556 5
execute as @s at @s if predicate structures_crabmaster:biomes/all_desert run scoreboard players set @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] BiomeRegion5556 6
execute as @s at @s if predicate structures_crabmaster:biomes/all_badland run scoreboard players set @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] BiomeRegion5556 7

execute as @s at @s run scoreboard players operation @e[tag=StructHere,tag=C5556,limit=1,sort=nearest,distance=..1] Biome5556 = Biome Biome5556