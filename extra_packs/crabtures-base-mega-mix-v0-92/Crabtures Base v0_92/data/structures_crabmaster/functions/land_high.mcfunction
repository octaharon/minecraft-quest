###This is good for landing structures from high in the sky, such as Y = 128.  It cuts down the total commands for the later land scripts.
execute as @s at @s if block ~ 160 ~ #structures_crabmaster:full_landing store result entity @s Pos[1] double 1 run scoreboard players set YPos Maths5556 160
execute as @s at @s if block ~ 112 ~ #structures_crabmaster:full_landing store result entity @s Pos[1] double 1 run scoreboard players set YPos Maths5556 112
execute as @s at @s if block ~ 96 ~ #structures_crabmaster:full_landing store result entity @s Pos[1] double 1 run scoreboard players set YPos Maths5556 96
execute as @s at @s if block ~ 80 ~ #structures_crabmaster:full_landing store result entity @s Pos[1] double 1 run scoreboard players set YPos Maths5556 80
execute as @s at @s if block ~ 72 ~ #structures_crabmaster:full_landing store result entity @s Pos[1] double 1 run scoreboard players set YPos Maths5556 72
execute as @s at @s if block ~ 68 ~ #structures_crabmaster:full_landing store result entity @s Pos[1] double 1 run scoreboard players set YPos Maths5556 68

execute as @s at @s positioned ~ ~ ~ run function structures_crabmaster:land_structure