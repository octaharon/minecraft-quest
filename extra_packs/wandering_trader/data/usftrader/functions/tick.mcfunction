#give new trades
execute as @e[type=wandering_trader,tag=!usftrader_trader,limit=1] at @s run function usftrader:transform/new

#attack after hit
execute as @e[tag=usftrader_trader,nbt={HurtTime:10s}] at @s run function usftrader:attack

#spawn 
execute unless score #spawn_normal usftrader matches 1 run function usftrader:spawn/test

#activate items
function usftrader:items/tick

#UUID of Player
execute as @a[tag=!usftrader_hasuuid] run function usftrader:uuid

#Redirect the trader
function usftrader:spawn/tick