#get the health
execute store result score @s usftrader_store run data get entity @s Attributes[7].Base 100
#add 2 healths
scoreboard players add @s usftrader_store 5
#if the score isn't over 30 set new health
execute unless score @s usftrader_store matches 100.. store result entity @s Attributes[7].Base double 0.01 run scoreboard players get @s usftrader_store
#kill horse upgrade: health 
execute unless score @s usftrader_store matches 100.. run kill @e[type=item,sort=nearest,limit=1,nbt={Item:{tag:{usftrader_horse_jump:1b}}}]
#sound
execute unless score @s usftrader_store matches 100.. run playsound minecraft:block.anvil.use master @p ~ ~ ~ 10 1.3