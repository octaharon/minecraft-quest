#lock chest
execute unless block ~ ~ ~ chest{Lock:"afkejpwnefwubvpwuinvgewrpgew3gv"} run function usftrader:items/rare/chest_lock/lock
#not your chest
execute if block ~ ~ ~ chest{Lock:"afkejpwnefwubvpwuinvgewrpgew3gv"} unless entity @e[tag=usftrader_chest_lock_new,distance=..0.1] unless score @s usftrader_store = @e[tag=usftrader_chest_lock,distance=..0.1,limit=1] usftrader_store run function usftrader:items/rare/chest_lock/fail
#unlock chest
execute if block ~ ~ ~ chest{Lock:"afkejpwnefwubvpwuinvgewrpgew3gv"} unless entity @e[tag=usftrader_chest_lock_new,distance=..0.1] if score @s usftrader_store = @e[tag=usftrader_chest_lock,distance=..0.1,limit=1] usftrader_store run function usftrader:items/rare/chest_lock/unlock

#reset
tag @e[tag=usftrader_chest_lock_new] remove usftrader_chest_lock_new
