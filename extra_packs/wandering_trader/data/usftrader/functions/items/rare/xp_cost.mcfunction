#get the cost
execute store result score @s usftrader_store run data get entity @s Item.tag.RepairCost
#remove the cost 10 points
scoreboard players remove @s usftrader_store 10
#if the score isn't under 10 set new cost
execute unless score @s usftrader_store matches 0.. store result entity @s Item.tag.RepairCost int 1 run scoreboard players get @s usftrader_store
#kill xp cost reducer
execute unless score @s usftrader_store matches 0.. run kill @e[type=item,sort=nearest,limit=1,nbt={Item:{tag:{usftrader_cost_reducer:1b}}}]
#sound
execute unless score @s usftrader_store matches 0.. run playsound minecraft:block.anvil.use master @p ~ ~ ~ 10 0.5