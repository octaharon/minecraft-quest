#stone
summon minecraft:armor_stand ~ ~ ~ {ArmorItems:[{tag:{usftrader:[{buy:{id:"minecraft:stone",Count:1b},sell:{id:"minecraft:stone",Count:1b}}]},id:"minecraft:emerald",Count:1b}],Tags:["usftrader_randomtrade_common"],NoGravity:1b,Invulnerable:1b,Small:1b,Marker:1b,Invisible:1b,NoBasePlate:1b,PersistenceRequired:1b}

#dirt
summon minecraft:armor_stand ~ ~ ~ {ArmorItems:[{tag:{usftrader:[{buy:{id:"minecraft:dirt",Count:1b},sell:{id:"minecraft:dirt",Count:1b}}]},id:"minecraft:emerald",Count:1b}],Tags:["usftrader_randomtrade_common"],NoGravity:1b,Invulnerable:1b,Small:1b,Marker:1b,Invisible:1b,NoBasePlate:1b,PersistenceRequired:1b}

#sand
summon minecraft:armor_stand ~ ~ ~ {ArmorItems:[{tag:{usftrader:[{buy:{id:"minecraft:sand",Count:1b},sell:{id:"minecraft:sand",Count:1b}}]},id:"minecraft:emerald",Count:1b}],Tags:["usftrader_randomtrade_common"],NoGravity:1b,Invulnerable:1b,Small:1b,Marker:1b,Invisible:1b,NoBasePlate:1b,PersistenceRequired:1b}

#gravel
summon minecraft:armor_stand ~ ~ ~ {ArmorItems:[{tag:{usftrader:[{buy:{id:"minecraft:gravel",Count:1b},sell:{id:"minecraft:gravel",Count:1b}}]},id:"minecraft:emerald",Count:1b}],Tags:["usftrader_randomtrade_common"],NoGravity:1b,Invulnerable:1b,Small:1b,Marker:1b,Invisible:1b,NoBasePlate:1b,PersistenceRequired:1b}

#cobblestone
summon minecraft:armor_stand ~ ~ ~ {ArmorItems:[{tag:{usftrader:[{buy:{id:"minecraft:cobblestone",Count:1b},sell:{id:"minecraft:cobblestone",Count:1b}}]},id:"minecraft:emerald",Count:1b}],Tags:["usftrader_randomtrade_common"],NoGravity:1b,Invulnerable:1b,Small:1b,Marker:1b,Invisible:1b,NoBasePlate:1b,PersistenceRequired:1b}

#stone_sword
summon minecraft:armor_stand ~ ~ ~ {ArmorItems:[{tag:{usftrader:[{buy:{id:"minecraft:stone_sword",Count:1b},sell:{id:"minecraft:stone_sword",Count:1b}}]},id:"minecraft:emerald",Count:1b}],Tags:["usftrader_randomtrade_common"],NoGravity:1b,Invulnerable:1b,Small:1b,Marker:1b,Invisible:1b,NoBasePlate:1b,PersistenceRequired:1b}

