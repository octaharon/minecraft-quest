#Summon random common trades
execute unless entity @e[tag=usftrader_new] run function usftrader:transform/trades_common
execute store result score #randomtrade_common usftrader run execute if entity @e[tag=usftrader_randomtrade_common]
execute unless score #randomtrade_common usftrader matches ..4 run kill @e[tag=usftrader_randomtrade_common,limit=1,sort=random]

#Summon random rare trades
execute unless entity @e[tag=usftrader_new] run function usftrader:transform/trades_rare
execute store result score #randomtrade_rare usftrader run execute if entity @e[tag=usftrader_randomtrade_rare]
execute unless score #randomtrade_rare usftrader matches ..2 run kill @e[tag=usftrader_randomtrade_rare,limit=1,sort=random]

#Summon random epic trade
execute unless entity @e[tag=usftrader_new] run function usftrader:transform/trades_epic
execute store result score #randomtrade_epic usftrader run execute if entity @e[tag=usftrader_randomtrade_epic]
execute unless score #randomtrade_epic usftrader matches ..1 run kill @e[tag=usftrader_randomtrade_epic,limit=1,sort=random]

#Assign Decor to llamas ("Suboptimal" solution with less tags and any number of llamas < 16)
execute unless entity @e[tag=usftrader_new] run function usftrader:transform/decor
execute if entity @e[tag=usftrader_randomtrade_decor] run data modify entity @e[tag=usftrader_new_llama,sort=random,limit=1] DecorItem set from entity @e[tag=usftrader_randomtrade_decor,sort=random,limit=1] ArmorItems[0].tag.DecorItem
execute if entity @e[tag=usftrader_randomtrade_decor] run kill @e[tag=usftrader_randomtrade_decor,sort=random,limit=1]

#finish
execute unless entity @e[tag=usftrader_new] run tag @s add usftrader_new

execute if score #randomtrade_common usftrader matches ..4 if score #randomtrade_rare usftrader matches ..2 if score #randomtrade_epic usftrader matches ..1 run function usftrader:transform/finish