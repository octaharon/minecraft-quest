#XP Cost Reducer
summon minecraft:armor_stand ~ ~ ~ {ArmorItems:[{tag:{usftrader:[{buy:{id:"minecraft:emerald",Count:50b},sell:{id:"minecraft:scute",Count:1b,tag:{display:{Name:'{"text":"XP Cost Reducer","color":"blue","italic":false}',Lore:['{"text":"Drop with a gear on a Smithing Table","color":"gray","italic":false}','{"text":"to reduce the repair costs of an item by 10 levels","color":"gray","italic":false}']},HideFlags:1,CustomModelData:4024112,usftrader_cost_reducer:1b,Enchantments:[{id:"minecraft:protection",lvl:1s}]}}}]},id:"minecraft:emerald",Count:1b}],Tags:["usftrader_randomtrade_rare"],NoGravity:1b,Invulnerable:1b,Small:1b,Marker:1b,Invisible:1b,NoBasePlate:1b,PersistenceRequired:1b}

#Item Magnet
summon minecraft:armor_stand ~ ~ ~ {ArmorItems:[{tag:{usftrader:[{buy:{id:"minecraft:emerald",Count:45b},sell:{id:"minecraft:iron_nugget",Count:1b,tag:{display:{Name:'{"text":"Item Magnet","color":"blue","italic":false}',Lore:['{"text":"Attracts all Items within a Radius of 5 Blocks","color":"gray","italic":false}']},HideFlags:1,CustomModelData:4024112,usftrader_magnet:1b,Enchantments:[{id:"minecraft:protection",lvl:1s}]}}}]},id:"minecraft:emerald",Count:1b}],Tags:["usftrader_randomtrade_rare"],NoGravity:1b,Invulnerable:1b,Small:1b,Marker:1b,Invisible:1b,NoBasePlate:1b,PersistenceRequired:1b}

#Key
#summon minecraft:armor_stand ~ ~ ~ {ArmorItems:[{tag:{usftrader:[{buy:{id:"minecraft:emerald",Count:50b},sell:{id:"minecraft:carrot_on_a_stick",Count:1b,tag:{display:{Name:'{"text":"Key","color":"blue","italic":false}',Lore:['{"text":"Shift right click a Chest to lock and open it","color":"gray","italic":false}']},CustomModelData:4024112,usftrader_chest_lock:1b}}}]},id:"minecraft:emerald",Count:1b}],Tags:["usftrader_randomtrade_rare"],NoGravity:1b,Invulnerable:1b,Small:1b,Marker:1b,Invisible:1b,NoBasePlate:1b,PersistenceRequired:1b}

#nether gate
summon minecraft:armor_stand ~ ~ ~ {ArmorItems:[{tag:{usftrader:[{buy:{id:"minecraft:diamond",Count:15b},buyB:{id:"minecraft:nether_wart",Count:10b},sell:{id:"minecraft:carrot_on_a_stick",Count:1b,tag:{display:{Name:'{"text":"Nether Gate","color":"blue","italic":false}',Lore:['{"text":"Right click to teleport","color":"gray","italic":false}']},CustomModelData:4024114,usftrader_nether_gate:1b}}}]},id:"minecraft:emerald",Count:1b}],Tags:["usftrader_randomtrade_rare"],NoGravity:1b,Invulnerable:1b,Small:1b,Marker:1b,Invisible:1b,NoBasePlate:1b,PersistenceRequired:1b}

#Heroic Brew
summon minecraft:armor_stand ~ ~ ~ {ArmorItems:[{tag:{usftrader:[{buy:{id:"minecraft:emerald",Count:25b},sell:{id:"minecraft:potion",Count:1b,tag:{CustomPotionEffects:[{Id:24,Duration:12000},{Id:26,Amplifier:2,Duration:12000},{Id:32,Amplifier:1,Duration:12000}],CustomPotionColor:11941251,display:{Name:'[{"text":"Heroic Brew","italic":false,"color":"light_purple","bold":true}]',Lore:['[{"text":"Makes you shine so that everyone loves you!","italic":false,"color":"yellow"}]']}}}}]},id:"minecraft:emerald",Count:1b}],Tags:["usftrader_randomtrade_rare"],NoGravity:1b,Invulnerable:1b,Small:1b,Marker:1b,Invisible:1b,NoBasePlate:1b,PersistenceRequired:1b}

#Chainmail Chestplate
summon minecraft:armor_stand ~ ~ ~ {ArmorItems:[{tag:{usftrader:[{buyB:{id:"minecraft:iron_chestplate",Count:1b},buy:{id:"minecraft:emerald",Count:10b},sell:{id:"minecraft:chainmail_chestplate",Count:1b}}]},id:"minecraft:emerald",Count:1b}],Tags:["usftrader_randomtrade_rare"],NoGravity:1b,Invulnerable:1b,Small:1b,Marker:1b,Invisible:1b,NoBasePlate:1b,PersistenceRequired:1b}

#Turtle Egg
summon minecraft:armor_stand ~ ~ ~ {ArmorItems:[{tag:{usftrader:[{buy:{id:"minecraft:emerald",Count:25b},sell:{id:"minecraft:turtle_egg",Count:4b}}]},id:"minecraft:emerald",Count:1b}],Tags:["usftrader_randomtrade_rare"],NoGravity:1b,Invulnerable:1b,Small:1b,Marker:1b,Invisible:1b,NoBasePlate:1b,PersistenceRequired:1b}

#Offering of Thunder
summon minecraft:armor_stand ~ ~ ~ {ArmorItems:[{tag:{usftrader:[{buy:{id:"minecraft:emerald",Count:50b},buyB:{id:"minecraft:gold_nugget",Count:50b},sell:{id:"minecraft:blaze_rod",Count:1b,tag:{q_offering:"thunder",display:{Name:'[{"text":"Offering of Lightning","italic":false,"color":"yellow"}]',Lore:['[{"text":"Makes a great offering at Thunder Shrine","italic":false,"color":"dark_green"}]']},HideFlags:36}}}]},id:"minecraft:emerald",Count:1b}],Tags:["usftrader_randomtrade_rare"],NoGravity:1b,Invulnerable:1b,Small:1b,Marker:1b,Invisible:1b,NoBasePlate:1b,PersistenceRequired:1b}

# Offering of Blood
summon minecraft:armor_stand ~ ~ ~ {ArmorItems:[{tag:{usftrader:[{buy:{id:"minecraft:emerald",Count:50b},buyB:{id:"minecraft:gold_nugget",Count:50b},sell:{id:"minecraft:rotten_flesh",Count:1b,tag:{q_offering:"blood",display:{Name:'{"text":"Offering of Flesh","italic":false,"color":"yellow"}',Lore:['{"text":"Makes a great offering at Blood Shrine","italic":false,"color":"dark_green"}']},HideFlags:36}}}]},id:"minecraft:emerald",Count:1b}],Tags:["usftrader_randomtrade_rare"],NoGravity:1b,Invulnerable:1b,Small:1b,Marker:1b,Invisible:1b,NoBasePlate:1b,PersistenceRequired:1b}

#Cat Spawn Egg
summon minecraft:armor_stand ~ ~ ~ {ArmorItems:[{tag:{usftrader:[{buy:{id:"minecraft:emerald",Count:25b},buyB:{"id":"minecraft:gold_nugget","Count":10},sell:{id:"minecraft:cat_spawn_egg",Count:3b}}]},id:"minecraft:emerald",Count:1b}],Tags:["usftrader_randomtrade_rare"],NoGravity:1b,Invulnerable:1b,Small:1b,Marker:1b,Invisible:1b,NoBasePlate:1b,PersistenceRequired:1b}

#Turtle Spawn Egg
summon minecraft:armor_stand ~ ~ ~ {ArmorItems:[{tag:{usftrader:[{buy:{id:"minecraft:emerald",Count:25b},buyB:{"id":"minecraft:gold_nugget","Count":10},sell:{id:"minecraft:turtle_spawn_egg",Count:1b}}]},id:"minecraft:emerald",Count:1b}],Tags:["usftrader_randomtrade_rare"],NoGravity:1b,Invulnerable:1b,Small:1b,Marker:1b,Invisible:1b,NoBasePlate:1b,PersistenceRequired:1b}