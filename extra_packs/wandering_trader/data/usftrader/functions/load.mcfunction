#define objective usftrader store data for transform, spawn/ store despawntime for spawn / store badness of player for spawn
scoreboard objectives add usftrader dummy
#define objective usftrader_store store health for angry/ store data for items/ store UUID of player
scoreboard objectives add usftrader_store dummy
#define objective usftrader_carrot store data for items->chest lock
scoreboard objectives add usftrader_carrot minecraft.used:minecraft.carrot_on_a_stick
#define objective usftrader_timer timer to change the wandering player direction
scoreboard objectives add usftrader_timer dummy 

execute unless score #spawn_normal usftrader matches 1 run gamerule doTraderSpawning false
execute if score #spawn_normal usftrader matches 1 run gamerule doTraderSpawning true