#define tag usftrader_spawn_new
#define tag usftrader_new_llama
summon wandering_trader ~ ~ ~ {CustomNameVisible:true,CustomName:'{"text": "The Trickster","color":"aqua"}',PersistenceRequired:1b,Tags:["usftrader_spawn_new"]}
summon trader_llama ~1 ~ ~ {DecorItem:{Count:1b,id:"cyan_carpet"},Variant:3,DespawnDelay:48000,Tags:["usftrader_new_llama"],Tame:1,Leashed:true}
summon trader_llama ~-1 ~ ~ {DecorItem:{Count:1b,id:"orange_carpet"},Variant:2,DespawnDelay:48000,Tags:["usftrader_new_llama"],Tame:1,Leashed:true}
execute as @e[type=trader_llama,tag=usftrader_new_llama] at @s run data modify entity @s Leash.UUIDMost set from entity @e[type=wandering_trader,tag=usftrader_spawn_new,sort=nearest,limit=1] UUIDMost
execute as @e[type=trader_llama,tag=usftrader_new_llama] at @s run data modify entity @s Leash.UUIDLeast set from entity @e[type=wandering_trader,tag=usftrader_spawn_new,sort=nearest,limit=1] UUIDLeast
execute as @e[type=trader_llama,tag=usftrader_new_llama] at @s run data modify entity @s OwnerUUIDMost set from entity @e[type=wandering_trader,tag=usftrader_spawn_new,sort=nearest,limit=1] UUIDMost
execute as @e[type=trader_llama,tag=usftrader_new_llama] at @s run data modify entity @s OwnerUUIDLeast set from entity @e[type=wandering_trader,tag=usftrader_spawn_new,sort=nearest,limit=1] UUIDLeast
#set target
function usftrader:spawn/wander

#remove groundtest armorstand
kill @s
tag @a remove usftrader_spawn_player

#sound
playsound minecraft:entity.wandering_trader.reappeared master @s