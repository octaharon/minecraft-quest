scoreboard objectives add ex_info.dummy dummy {"text":"Extra Info Dummies"}
scoreboard players set #daytime ex_info.dummy 0
scoreboard players set #daytime0 ex_info.dummy 0
scoreboard players set #ampm ex_info.dummy 0
scoreboard players set #hour ex_info.dummy 0
scoreboard players set #minute ex_info.dummy 0
scoreboard players set #6 ex_info.dummy 6
scoreboard players set #43 ex_info.dummy 43
scoreboard players set #100 ex_info.dummy 100
scoreboard players set #1000 ex_info.dummy 1000
scoreboard players set #10000 ex_info.dummy 10000

scoreboard objectives add ex_info.x dummy {"text":"Extra Info X coord"}
scoreboard objectives add ex_info.y dummy {"text":"Extra Info Y coord"}
scoreboard objectives add ex_info.z dummy {"text":"Extra Info Z coord"}
scoreboard objectives add ex_info.bear dummy {"text":"Extra Info Bearing"}
scoreboard objectives add ex_info.horse_j dummy {"text":"Extra Info Horse Jump"}
scoreboard objectives add ex_info.horse_s dummy {"text":"Extra Info Horse Speed"}
scoreboard objectives add ex_info.horse_sd dummy {"text":"Extra Info Horse Speed"}

function extra_info:config

execute if score #reducedDebugInfo ex_info.dummy matches 1.. run gamerule reducedDebugInfo true
execute if score #reducedDebugInfo ex_info.dummy matches ..0 run gamerule reducedDebugInfo false