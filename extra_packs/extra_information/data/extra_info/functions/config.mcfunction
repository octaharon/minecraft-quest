# Whether to enable the game rule reducedDebugInfo (hides info on F3 screen) on load [default = 0]
scoreboard players set #reducedDebugInfo ex_info.dummy 0

# Whether the Compass should display bearing in the Overworld [default = 1]
scoreboard players set #compass0 ex_info.dummy 1

# Whether the Compass should display bearing in the Nether [default = 1]
scoreboard players set #compass-1 ex_info.dummy 0

# Whether the Compass should display bearing in the End [default = 1]
scoreboard players set #compass1 ex_info.dummy 0

# Whether the Map should display coordinates in the Overworld [default = 1]
scoreboard players set #map0 ex_info.dummy 1

# Whether the Map should display coordinates in the Nether [default = 0]
scoreboard players set #map-1 ex_info.dummy 0

# Whether the Map should display coordinates in the End [default = 1]
scoreboard players set #map1 ex_info.dummy 1

# Whether the Map should display Y coordinates in enabled dimensions [default = 1]
scoreboard players set #mapY ex_info.dummy 1

# Whether the Clock should display the current time in the Overworld [default = 1]
scoreboard players set #clock0 ex_info.dummy 1

# Whether the Clock should display the current time in the Nether [default = 0]
scoreboard players set #clock-1 ex_info.dummy 0

# Whether the Clock should display the current time in the End [default = 0]
scoreboard players set #clock1 ex_info.dummy 0

# Whether the Lead should display the information of a horse being ridden [default = 1]
scoreboard players set #lead ex_info.dummy 1