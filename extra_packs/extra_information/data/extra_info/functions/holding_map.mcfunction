# get player coordinates
execute store result score @s ex_info.x run data get entity @s Pos[0]
execute store result score @s ex_info.y run data get entity @s Pos[1]
execute store result score @s ex_info.z run data get entity @s Pos[2]

# update players action bar
title @s times 0 1 1
execute unless score #mapY ex_info.dummy matches 1 run title @s actionbar [{"text": "Coordinates: x: "}, {"score":{"name": "@s", "objective": "ex_info.x"}}, {"text": ", z: "}, {"score":{"name": "@s", "objective": "ex_info.z"}}]
execute if score #mapY ex_info.dummy matches 1 run title @s actionbar [{"text": "Coordinates: x: "}, {"score":{"name": "@s", "objective": "ex_info.x"}}, {"text": ", y: "}, {"score":{"name": "@s", "objective": "ex_info.y"}}, {"text": ", z: "}, {"score":{"name": "@s", "objective": "ex_info.z"}}]