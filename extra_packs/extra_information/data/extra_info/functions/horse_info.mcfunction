# get ridden entity data
execute store result score @s ex_info.horse_s run data get entity @s RootVehicle.Entity.Attributes[{Name:"generic.movementSpeed"}].Base 43000
scoreboard players operation @s ex_info.horse_sd = @s ex_info.horse_s
scoreboard players operation @s ex_info.horse_sd %= #1000 ex_info.dummy
scoreboard players operation @s ex_info.horse_s /= #10000 ex_info.dummy

execute store result score @s ex_info.horse_j run data get entity @s RootVehicle.Entity.Attributes[{Name:"horse.jumpStrength"}].Base 10000
execute if score @s ex_info.horse_j matches ..5643 run scoreboard players set @s ex_info.horse_j 1
execute if score @s ex_info.horse_j matches 5644..7152 run scoreboard players set @s ex_info.horse_j 2
execute if score @s ex_info.horse_j matches 7153..8475 run scoreboard players set @s ex_info.horse_j 3
execute if score @s ex_info.horse_j matches 8476..9669 run scoreboard players set @s ex_info.horse_j 4
execute if score @s ex_info.horse_j matches 9670.. run scoreboard players set @s ex_info.horse_j 5

# update players action bar
title @s times 0 1 1
title @s actionbar [{"text": "Horse: speed: "}, {"score":{"name": "@s", "objective": "ex_info.horse_s"}}, {"text": "."}, {"score":{"name": "@s", "objective": "ex_info.horse_sd"}}, {"text": "m/s, jump: "}, {"score":{"name": "@s", "objective": "ex_info.horse_j"}}, {"text": "m"}]