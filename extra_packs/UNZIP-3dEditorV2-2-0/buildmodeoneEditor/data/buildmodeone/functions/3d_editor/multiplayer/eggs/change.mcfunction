# get entities
tag @a[scores={bm1_3d_iu=1..}] add bm1_3d_mpa
execute as @e[tag=bm1_3d_mtps] if score @s bm1_global_id = @a[tag=bm1_3d_mpa,limit=1] bm1_global_id run tag @s add bm1_3d_mpa
execute as @e[tag=bm1_3d_mtss] if score @s bm1_global_id = @a[tag=bm1_3d_mpa,limit=1] bm1_global_id run tag @s add bm1_3d_mpa

# select
execute as @e[tag=bm1_3d_mth,tag=bm1_3d_5,distance=..1.2,sort=nearest,limit=1,tag=!bm1_3d_mtsshere] at @s run tp @e[tag=bm1_3d_mtss,tag=bm1_3d_mpa] ~ ~ ~
tp @e[tag=bm1_3d_mtps,tag=bm1_3d_mpa] @e[tag=bm1_3d_mtss,limit=1,tag=bm1_3d_mpa]

# addons
    ## litemode
    function buildmodeone:3d_editor/addons/litemode/reminder/schedule
    ## cgui
    execute as @a[tag=bm1_3d_mpa,limit=1,sort=nearest] unless score @s bm1_3d_tut_stat matches 1.. as @s run function buildmodeone:3d_editor/cgui/position
# clean
tp ~ ~-10 ~ 
kill @s
scoreboard players set @a[scores={bm1_3d_iu=1..}] bm1_3d_iu 0
tag @e[tag=bm1_3d_mpa] remove bm1_3d_mpa