# get entities
tag @a[scores={bm1_3d_iu=1..}] add bm1_3d_mpa
execute as @e[tag=bm1_3d_mtps] if score @s bm1_global_id = @a[tag=bm1_3d_mpa,limit=1] bm1_global_id run tag @s add bm1_3d_mpa
execute as @e[tag=bm1_3d_mtss] if score @s bm1_global_id = @a[tag=bm1_3d_mpa,limit=1] bm1_global_id run tag @s add bm1_3d_mpa

# visual
execute if entity @e[tag=bm1_3d_mth,distance=..0.5,tag=bm1_3d_5] run particle minecraft:dust 0 1 0.2 1.3 ~ ~0.5 ~ 0.1 0.2 0.1 0 25 normal

# spawn grid
execute positioned ~-0.33 ~ ~-0.33 unless entity @e[tag=bm1_3d_mth,distance=..0.5,tag=bm1_3d_1] run summon armor_stand ~ ~ ~ {CustomNameVisible:0b,NoGravity:1b,Invulnerable:1b,Marker:1b,Invisible:1b,Tags: ["bm1_3d_mth", "bm1_3d_1", "global.ignore", "bm1_3d_update"],ShowArms:1b,DisabledSlots:4144959,Small:1b}
execute positioned ~ ~ ~-0.33 unless entity @e[tag=bm1_3d_mth,distance=..0.5,tag=bm1_3d_2] run summon armor_stand ~ ~ ~ {CustomNameVisible:0b,NoGravity:1b,Invulnerable:1b,Marker:1b,Invisible:1b,Tags: ["bm1_3d_mth", "bm1_3d_2", "global.ignore", "bm1_3d_update"],ShowArms:1b,DisabledSlots:4144959,Small:1b}
execute positioned ~0.33 ~ ~-0.33 unless entity @e[tag=bm1_3d_mth,distance=..0.5,tag=bm1_3d_3] run summon armor_stand ~ ~ ~ {CustomNameVisible:0b,NoGravity:1b,Invulnerable:1b,Marker:1b,Invisible:1b,Tags: ["bm1_3d_mth", "bm1_3d_3", "global.ignore", "bm1_3d_update"],ShowArms:1b,DisabledSlots:4144959,Small:1b}
execute positioned ~-0.33 ~ ~ unless entity @e[tag=bm1_3d_mth,distance=..0.5,tag=bm1_3d_4] run summon armor_stand ~ ~ ~ {CustomNameVisible:0b,NoGravity:1b,Invulnerable:1b,Marker:1b,Invisible:1b,Tags: ["bm1_3d_mth", "bm1_3d_4", "global.ignore", "bm1_3d_update"],ShowArms:1b,DisabledSlots:4144959,Small:1b}
execute positioned ~ ~ ~ unless entity @e[tag=bm1_3d_mth,distance=..0.5,tag=bm1_3d_5] run summon armor_stand ~ ~ ~ {CustomNameVisible:0b,NoGravity:1b,Invulnerable:1b,Marker:1b,Invisible:1b,Tags: ["bm1_3d_mth", "bm1_3d_5", "global.ignore", "bm1_3d_update"],ShowArms:1b,DisabledSlots:4144959,Small:1b}
execute positioned ~0.33 ~ ~ unless entity @e[tag=bm1_3d_mth,distance=..0.5,tag=bm1_3d_6] run summon armor_stand ~ ~ ~ {CustomNameVisible:0b,NoGravity:1b,Invulnerable:1b,Marker:1b,Invisible:1b,Tags: ["bm1_3d_mth", "bm1_3d_6", "global.ignore", "bm1_3d_update"],ShowArms:1b,DisabledSlots:4144959,Small:1b}
execute positioned ~-0.33 ~ ~0.33 unless entity @e[tag=bm1_3d_mth,distance=..0.5,tag=bm1_3d_7] run summon armor_stand ~ ~ ~ {CustomNameVisible:0b,NoGravity:1b,Invulnerable:1b,Marker:1b,Invisible:1b,Tags: ["bm1_3d_mth", "bm1_3d_7", "global.ignore", "bm1_3d_update"],ShowArms:1b,DisabledSlots:4144959,Small:1b}
execute positioned ~ ~ ~0.33 unless entity @e[tag=bm1_3d_mth,distance=..0.5,tag=bm1_3d_8] run summon armor_stand ~ ~ ~ {CustomNameVisible:0b,NoGravity:1b,Invulnerable:1b,Marker:1b,Invisible:1b,Tags: ["bm1_3d_mth", "bm1_3d_8", "global.ignore", "bm1_3d_update"],ShowArms:1b,DisabledSlots:4144959,Small:1b}
execute positioned ~0.33 ~ ~0.33 unless entity @e[tag=bm1_3d_mth,distance=..0.5,tag=bm1_3d_9] run summon armor_stand ~ ~ ~ {CustomNameVisible:0b,NoGravity:1b,Invulnerable:1b,Marker:1b,Invisible:1b,Tags: ["bm1_3d_mth", "bm1_3d_9", "global.ignore", "bm1_3d_update"],ShowArms:1b,DisabledSlots:4144959,Small:1b}

# "fix rotation bug"
execute as @e[tag=bm1_3d_mth,distance=..0.5,limit=9] at @s run tp @s ~ ~ ~ ~360 ~

# select
execute as @e[tag=bm1_3d_mth,tag=bm1_3d_5,distance=..0.5,sort=nearest,limit=1] at @s run tp @e[tag=bm1_3d_mtss,tag=bm1_3d_mpa] ~ ~ ~
tp @e[tag=bm1_3d_mtps,tag=bm1_3d_mpa] @e[tag=bm1_3d_mtss,limit=1,tag=bm1_3d_mpa]

# addons
    ## litemode
    function buildmodeone:3d_editor/addons/litemode/reminder/schedule
    ## cgui
    execute as @a[tag=bm1_3d_mpa,limit=1,sort=nearest] unless score @s bm1_3d_tut_stat matches 1.. as @s run function buildmodeone:3d_editor/cgui/position
    ## barrier
    execute if block ~ ~ ~ air unless entity @e[tag=bm1_3d_mth,tag=bm1_3d_air,distance=..0.5,limit=1] if score @a[tag=bm1_3d_mpa,sort=nearest,distance=..10,limit=1] bm1_3d_add_bar matches 1.. run setblock ~ ~ ~ barrier
    execute if block ~ ~ ~ air unless entity @e[tag=bm1_3d_mth,tag=bm1_3d_air,distance=..0.5,limit=1] unless score @a[tag=bm1_3d_mpa,sort=nearest,distance=..10,limit=1] bm1_3d_add_bar matches 1.. as @e[tag=bm1_3d_mth,tag=bm1_3d_5,distance=..0.5,sort=nearest] run tag @s add bm1_3d_air

# clean
tp ~ ~-10 ~ 
kill @s
scoreboard players set @a[scores={bm1_3d_iu=1..}] bm1_3d_iu 0
tag @e[tag=bm1_3d_mpa] remove bm1_3d_mpa