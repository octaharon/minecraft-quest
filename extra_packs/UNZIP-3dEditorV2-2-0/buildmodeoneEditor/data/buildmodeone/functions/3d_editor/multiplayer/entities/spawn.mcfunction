summon minecraft:area_effect_cloud ~ ~ ~ {Particle:"block air",Radius:0.001f,Duration:2147483647,Tags:["bm1_3d_mtps", "global.ignore", "bm1_3d_noid"]}
execute as @e[tag=bm1_3d_mtps,tag=bm1_3d_noid] unless score @s bm1_global_id matches 0.. run scoreboard players operation @s bm1_global_id = @e[tag=bm1_3d_vars] bm1_global_idmax 
execute as @e[tag=bm1_3d_mtps,tag=bm1_3d_noid] run scoreboard players operation @s bm1_3d_mp_col = @e[tag=bm1_3d_vars] bm1_global_idmax
execute as @e[tag=bm1_3d_mtps,tag=bm1_3d_noid] run function buildmodeone:3d_editor/multiplayer/particles/give_color_id

summon minecraft:area_effect_cloud ~ ~ ~ {Particle:"block air",Radius:0.001f,Duration:2147483647,Tags:["bm1_3d_mtss", "global.ignore", "bm1_3d_noid"]}
execute as @e[tag=bm1_3d_mtss,tag=bm1_3d_noid] unless score @s bm1_global_id matches 0.. run scoreboard players operation @s bm1_global_id = @e[tag=bm1_3d_vars] bm1_global_idmax 

execute as @e[tag=bm1_3d_mtps,tag=bm1_3d_noid] if score @s bm1_global_id matches 0.. run tag @s remove bm1_3d_noid