tp @s ~ ~ ~ ~3 ~

execute rotated ~000 ~ positioned ^1.9 ^0.20 ^ align x positioned ~1.9 ~ ~ if entity @s[distance=..0.85] run particle minecraft:end_rod ~ ~ ~ 0 0 0 0 1 normal
execute rotated ~45 ~ positioned ^ ^0.20 ^-1.9 align z positioned ~ ~ ~-0.9 if entity @s[distance=..0.85] run particle minecraft:end_rod ~ ~ ~ 0 0 0 0 1 normal
execute rotated ~90 ~ positioned ^-1.9 ^0.20 ^ align x positioned ~-0.9 ~ ~ if entity @s[distance=..0.85] run particle minecraft:end_rod ~ ~ ~ 0 0 0 0 1 normal
execute rotated ~135 ~ positioned ^ ^0.20 ^1.9 align z positioned ~ ~ ~1.9 if entity @s[distance=..0.85] run particle minecraft:end_rod ~ ~ ~ 0 0 0 0 1 normal

execute rotated ~180 ~ positioned ^1.9 ^0.20 ^ align x positioned ~1.9 ~ ~ if entity @s[distance=..0.85] run particle minecraft:end_rod ~ ~ ~ 0 0 0 0 1 normal
execute rotated ~220 ~ positioned ^ ^0.20 ^-1.9 align z positioned ~ ~ ~-0.9 if entity @s[distance=..0.85] run particle minecraft:end_rod ~ ~ ~ 0 0 0 0 1 normal
execute rotated ~270 ~ positioned ^-1.9 ^0.20 ^ align x positioned ~-0.9 ~ ~ if entity @s[distance=..0.85] run particle minecraft:end_rod ~ ~ ~ 0 0 0 0 1 normal
execute rotated ~320 ~ positioned ^ ^0.20 ^1.9 align z positioned ~ ~ ~1.9 if entity @s[distance=..0.85] run particle minecraft:end_rod ~ ~ ~ 0 0 0 0 1 normal

# execute positioned ^1.9 ^0.20 ^ run particle minecraft:end_rod ~ ~ ~ 0 0 0 0 1 normal