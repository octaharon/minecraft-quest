tellraw @a ["", {"text": "<< ", "color": "dark_red", "bold":true}, {"text": "Uninstalled ", "color": "red", "bold":true}, {"text": "successfully :(", "color": "gray", "bold":false}]
tellraw @a ["", {"text": "<< ", "color": "yellow", "bold":true}, {"text": "Enable the datpack to use it normally again! ", "color": "gray", "bold":false}]

datapack disable "file/buildmodeoneEditor"
scoreboard objectives remove bm1_3d_items_trg
scoreboard objectives remove bm1_3d_entityC 
scoreboard objectives remove bm1_3d_optState 
scoreboard objectives remove bm1_3d_add_lite 
scoreboard objectives remove bm1_3d_add_cgui 
scoreboard objectives remove bm1_3d_add_liter 
scoreboard objectives remove bm1_3d_msg_timer 
scoreboard objectives remove bm1_3d_items_inv 
scoreboard objectives remove bm1_3d_version 
scoreboard objectives remove bm1_3d_version_n 
scoreboard objectives remove bm1_3d_tut_prog 
scoreboard objectives remove bm1_3d_items_tmp 
scoreboard objectives remove bm1_3d_cgui_tmp 
scoreboard objectives remove bm1_3d_add_bar
scoreboard objectives remove bm1_3d_add_cov
scoreboard objectives remove bm1_3d_add_cov2
scoreboard objectives remove bm1_global_iu
scoreboard objectives remove bm1_global_id
scoreboard objectives remove bm1_global_idmax
scoreboard objectives remove bm1_3d_iu
scoreboard objectives remove bm1_3d_mp_col
scoreboard objectives remove bm1_3d_sel
scoreboard objectives remove bm1_3d_tut_stat
scoreboard objectives remove bm1_3d_mp_mode
scoreboard objectives remove bm1_3d_inv_slot

team remove bm1_3d_esitt 
gamerule sendCommandFeedback true

kill @e[tag=bm1_3d_mtss]
kill @e[tag=bm1_3d_mtps]
kill @e[tag=bm1_3d_vars]
kill @e[tag=bm1_3d_sels]