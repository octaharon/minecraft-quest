tag @s add bm1_3d_mpa
execute as @e[tag=bm1_3d_mtps] if score @s bm1_global_id = @a[tag=bm1_3d_mpa,limit=1,sort=nearest] bm1_global_id run tag @s add bm1_3d_mpa

execute as @e[tag=bm1_3d_mtps,tag=bm1_3d_mpa] at @s run tag @e[tag=bm1_3d_mthb,limit=1,distance=..0.1] remove bm1_3d_rs0
execute as @e[tag=bm1_3d_mtps,tag=bm1_3d_mpa] at @s run tag @e[tag=bm1_3d_mthb,limit=1,distance=..0.1] remove bm1_3d_rs1
execute as @e[tag=bm1_3d_mtps,tag=bm1_3d_mpa] at @s run tag @e[tag=bm1_3d_mthb,limit=1,distance=..0.1] remove bm1_3d_rs2
execute as @e[tag=bm1_3d_mtps,tag=bm1_3d_mpa] at @s run tag @e[tag=bm1_3d_mthb,limit=1,distance=..0.1] remove bm1_3d_rs3

execute as @e[tag=bm1_3d_mtps,tag=bm1_3d_mpa] at @s run tag @e[tag=bm1_3d_mthb,limit=1,distance=..0.1] remove bm1_3d_rsn0
execute as @e[tag=bm1_3d_mtps,tag=bm1_3d_mpa] at @s run tag @e[tag=bm1_3d_mthb,limit=1,distance=..0.1] remove bm1_3d_rsn1
execute as @e[tag=bm1_3d_mtps,tag=bm1_3d_mpa] at @s run tag @e[tag=bm1_3d_mthb,limit=1,distance=..0.1] add bm1_3d_rsn2
execute as @e[tag=bm1_3d_mtps,tag=bm1_3d_mpa] at @s run tag @e[tag=bm1_3d_mthb,limit=1,distance=..0.1] remove bm1_3d_rsn3

tag @e[tag=bm1_3d_mpa] remove bm1_3d_mpa