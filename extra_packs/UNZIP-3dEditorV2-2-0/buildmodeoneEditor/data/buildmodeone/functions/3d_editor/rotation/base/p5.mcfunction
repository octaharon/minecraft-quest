tag @s add bm1_3d_mpa
execute as @e[tag=bm1_3d_mtps] if score @s bm1_global_id = @a[tag=bm1_3d_mpa,limit=1,sort=nearest] bm1_global_id run tag @s add bm1_3d_mpa

execute as @e[tag=bm1_3d_mtps,tag=bm1_3d_mpa] at @s run data merge entity @e[tag=bm1_3d_mth,limit=1,distance=..0.1,sort=nearest] {Marker:0b}
execute as @e[tag=bm1_3d_mtps,tag=bm1_3d_mpa] at @s as @e[tag=bm1_3d_mth,distance=0,limit=1] at @s run tp @s ~ ~ ~ ~5 ~
execute as @e[tag=bm1_3d_mtps,tag=bm1_3d_mpa] at @s run data merge entity @e[tag=bm1_3d_mth,limit=1,distance=..0.1,sort=nearest] {Marker:1b}
execute as @e[tag=bm1_3d_mtps,tag=bm1_3d_mpa] at @s positioned ~ ~-0.9 ~ as @e[tag=bm1_3d_esit,distance=..0.5,limit=1,sort=nearest] at @s run tp @s ~ ~ ~ ~5 0
function buildmodeone:3d_editor/addons/litemode/reminder/schedule

tag @e[tag=bm1_3d_mpa] remove bm1_3d_mpa