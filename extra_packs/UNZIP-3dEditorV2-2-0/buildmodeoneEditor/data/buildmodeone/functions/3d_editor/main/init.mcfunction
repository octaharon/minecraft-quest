# spawn entities
        execute at @a[limit=1] unless entity @e[tag=bm1_3d_vars,type=area_effect_cloud] run summon minecraft:area_effect_cloud ~ ~ ~ {Particle:"block air",Radius:0.001f,Duration:2147483647,Tags:["bm1_3d_vars", "global.ignore"]}
    ## kill old armor stands
        execute at @a[limit=1] as @e[tag=bm1_3d_vars,type=armor_stand] run kill @s
        execute at @a[limit=1] as @e[tag=bm1_3d_mtss,type=armor_stand] run kill @s
        execute at @a[limit=1] as @e[tag=bm1_3d_mtps,type=armor_stand] run kill @s
    ##
#

# gamerule
    gamerule sendCommandFeedback false

# chairs
    team add bm1_3d_esitt 
    team modify bm1_3d_esitt collisionRule never

# scoreboards 
    scoreboard objectives add bm1_3d_items_trg trigger
    scoreboard objectives add bm1_3d_entityC dummy
    scoreboard objectives add bm1_3d_optState dummy
    scoreboard objectives add bm1_3d_add_lite dummy
    scoreboard objectives add bm1_3d_add_liter dummy
    scoreboard objectives add bm1_3d_msg_timer dummy
    scoreboard objectives add bm1_3d_items_inv dummy
    scoreboard objectives add bm1_3d_version dummy
    scoreboard objectives add bm1_3d_version_n dummy
    scoreboard objectives add bm1_3d_tut_prog dummy
    scoreboard objectives add bm1_3d_items_tmp dummy
    scoreboard objectives add bm1_3d_cgui_tmp dummy
    scoreboard objectives add bm1_3d_add_bar dummy
    scoreboard objectives add bm1_3d_add_cov dummy
    scoreboard objectives add bm1_3d_add_cov2 dummy
    scoreboard objectives add bm1_global_iu minecraft.used:minecraft.carrot_on_a_stick 
    scoreboard objectives add bm1_global_id dummy
    scoreboard objectives add bm1_global_idmax dummy
    scoreboard objectives add bm1_3d_iu minecraft.used:minecraft.silverfish_spawn_egg
    scoreboard objectives add bm1_3d_mp_col dummy
    scoreboard objectives add bm1_3d_sel dummy
    scoreboard objectives add bm1_3d_tut_stat dummy
    scoreboard objectives add bm1_3d_mp_mode dummy
    scoreboard objectives add bm1_3d_inv_slot dummy

# set new version (NEW VERSION CODE HERE)
    scoreboard players set @e[tag=bm1_3d_vars] bm1_3d_version_n 2020

# welcome msg
    ## new installed
        execute unless score @e[tag=bm1_3d_vars,limit=1] bm1_3d_version matches 0.. unless entity @e[tag=mth] as @a run function buildmodeone:3d_editor/install_msg/new_installmsg
    ## from 2.0 - 2.0.1 to Latest
        execute as @e[tag=bm1_3d_vars,limit=1] if score @s bm1_3d_version matches 0.. unless score @s bm1_3d_version = @s bm1_3d_version_n if score @s bm1_3d_version matches ..2009 as @a run function buildmodeone:3d_editor/install_msg/with_converter
    ## from 2.1.0 to Latest
        execute as @e[tag=bm1_3d_vars,limit=1] if score @s bm1_3d_version matches 0.. unless score @s bm1_3d_version = @s bm1_3d_version_n if score @s bm1_3d_version matches 2010.. as @a run function buildmodeone:3d_editor/install_msg/no_converter

    ## updated from 1.x to Latest
        execute as @e[tag=bm1_3d_vars,limit=1] unless score @s bm1_3d_version matches 0.. unless score @s bm1_3d_version = @s bm1_3d_version_n as @a if entity @e[tag=mth] run function buildmodeone:3d_editor/install_msg/with_converter
    ##
#

# override version final
    scoreboard players operation @e[tag=bm1_3d_vars] bm1_3d_version = @e[tag=bm1_3d_vars] bm1_3d_version_n

# default settings
    execute unless score @e[tag=bm1_3d_vars,limit=1] bm1_3d_add_liter matches 0.. run scoreboard players set @e[tag=bm1_3d_vars,limit=1] bm1_3d_add_liter 1
    execute as @a unless score @s bm1_3d_add_bar matches 0.. run scoreboard players set @s bm1_3d_add_bar 1

# multiplayer id
    execute as @e[tag=bm1_3d_vars,limit=1] unless score @s bm1_global_idmax matches 0.. run scoreboard players set @s bm1_global_idmax 1

# reset sel rotation
execute as @e[tag=bm1_3d_mtss] at @s run tp @s ~ ~ ~ 0 0