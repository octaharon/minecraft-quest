execute as @s if score @s bm1_3d_items_inv matches 1.. run scoreboard players set @s bm1_3d_items_tmp 1
scoreboard players set @s bm1_3d_items_inv 0
function buildmodeone:3d_editor/addons/invitems/clear

function buildmodeone:3d_editor/addons/chatgui/item
give @s minecraft:silverfish_spawn_egg{display: {Name:"{\"text\":\"Place Modelholder\",\"color\":\"aqua\",\"italic\":\"false\"}"},bm1_3d_itm:1,EntityTag: {NoGravity:1b,Silent:1b,Invulnerable:1b,CustomNameVisible:0b,PersistenceRequired:1b,NoAI:1b,CanPickUpLoot:0b,Tags: ["bm1_3d_mtp", "global.ignore"]}} 1
give @s minecraft:silverfish_spawn_egg{display: {Name:"{\"text\":\"Change Modelholder\",\"color\":\"aqua\",\"italic\":\"false\"}"},bm1_3d_itm:1,EntityTag: {NoGravity:1b,Silent:1b,Invulnerable:1b,CustomNameVisible:0b,PersistenceRequired:1b,NoAI:1b,CanPickUpLoot:0b,Tags: ["bm1_3d_mtc", "global.ignore"]}} 1

execute if score @s bm1_3d_tut_stat matches 1 run function buildmodeone:3d_editor/tutorial/items

execute as @s if score @s bm1_3d_items_tmp matches 1.. run scoreboard players set @s bm1_3d_items_inv 1
scoreboard players set @s bm1_3d_items_tmp 0