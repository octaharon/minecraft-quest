tag @s add bm1_3d_mpa
execute as @e[tag=bm1_3d_mtss] if score @s bm1_global_id = @a[tag=bm1_3d_mpa,limit=1,sort=nearest] bm1_global_id run tag @s add bm1_3d_mpa

execute as @e[tag=bm1_3d_mtss,tag=bm1_3d_mpa] at @s run tag @e[tag=bm1_3d_mth,tag=bm1_3d_5,distance=..0.5] remove bm1_3d_air
execute as @e[tag=bm1_3d_mtss,tag=bm1_3d_mpa] at @s run setblock ~ ~ ~ minecraft:air

tag @e[tag=bm1_3d_mpa] remove bm1_3d_mpa