# grid particles
execute as @e[tag=bm1_3d_mtss] at @s if entity @e[tag=bm1_3d_mth,tag=bm1_3d_5,distance=..0.5] run particle minecraft:dust 1 1 0 0.3 ~ ~0.02 ~-0.3 0 0 0.1 1 100

execute as @e[tag=bm1_3d_mtss] at @s if entity @e[tag=bm1_3d_mth,tag=bm1_3d_1,distance=..0.5] run particle minecraft:dust 0 0.4 1 0.5 ~-0.33 ~0.1 ~-0.33 0 0 0 0 1 normal
execute as @e[tag=bm1_3d_mtss] at @s if entity @e[tag=bm1_3d_mth,tag=bm1_3d_2,distance=..0.5] run particle minecraft:dust 0 0.4 1 0.5 ~ ~0.1 ~-0.33 0 0 0 0 1 normal
execute as @e[tag=bm1_3d_mtss] at @s if entity @e[tag=bm1_3d_mth,tag=bm1_3d_3,distance=..0.5] run particle minecraft:dust 0 0.4 1 0.5 ~0.33 ~0.1 ~-0.33 0 0 0 0 1 normal

execute as @e[tag=bm1_3d_mtss] at @s if entity @e[tag=bm1_3d_mth,tag=bm1_3d_4,distance=..0.5] run particle minecraft:dust 0 0.4 1 0.5 ~-0.33 ~0.1 ~ 0 0 0 0 1 normal
execute as @e[tag=bm1_3d_mtss] at @s if entity @e[tag=bm1_3d_mth,tag=bm1_3d_5,distance=..0.5] run particle minecraft:dust 0 0.4 1 0.5 ~ ~0.1 ~ 0 0 0 0 1 normal
execute as @e[tag=bm1_3d_mtss] at @s if entity @e[tag=bm1_3d_mth,tag=bm1_3d_6,distance=..0.5] run particle minecraft:dust 0 0.4 1 0.5 ~0.33 ~0.1 ~ 0 0 0 0 1 normal

execute as @e[tag=bm1_3d_mtss] at @s if entity @e[tag=bm1_3d_mth,tag=bm1_3d_7,distance=..0.5] run particle minecraft:dust 0 0.4 1 0.5 ~-0.33 ~0.1 ~0.33 0 0 0 0 1 normal
execute as @e[tag=bm1_3d_mtss] at @s if entity @e[tag=bm1_3d_mth,tag=bm1_3d_8,distance=..0.5] run particle minecraft:dust 0 0.4 1 0.5 ~ ~0.1 ~0.33 0 0 0 0 1 normal
execute as @e[tag=bm1_3d_mtss] at @s if entity @e[tag=bm1_3d_mth,tag=bm1_3d_9,distance=..0.5] run particle minecraft:dust 0 0.4 1 0.5 ~0.33 ~0.1 ~0.33 0 0 0 0 1 normal

# rotate particle
    execute as @e[tag=bm1_3d_mtss] at @s unless score @s bm1_3d_sel matches 1.. if entity @e[tag=bm1_3d_mth,tag=bm1_3d_5,distance=..0.5] run tp @s ~ ~ ~ ~10 ~
    execute as @e[tag=bm1_3d_mtss] at @s unless score @s bm1_3d_sel matches 1.. if entity @e[tag=bm1_3d_mth,tag=bm1_3d_5,distance=..0.5] run particle minecraft:end_rod ^0.7 ^0.15 ^ 0 0 0 0 1 normal 

    execute as @e[tag=bm1_3d_mtss] at @s if score @s bm1_3d_sel matches 2 if entity @e[tag=bm1_3d_mth,tag=bm1_3d_5,distance=..0.5] run function buildmodeone:3d_editor/multiplayer/particles/square_main

    #if entity @e[tag=bm1_3d_mtss,distance=..0.6] 
 
# selected particles
    execute as @e[tag=bm1_3d_mtps] at @s if entity @e[tag=bm1_3d_mth,tag=bm1_3d_5,distance=..0.5] run function buildmodeone:3d_editor/multiplayer/particles/select

# mark red if empty
    execute as @e[tag=bm1_3d_mth,tag=bm1_3d_5] at @s unless entity @e[tag=bm1_3d_mth,distance=..0.5,tag=bm1_3d_hasItem] unless entity @e[tag=bm1_3d_mtss,distance=..0.5] run particle minecraft:dust 1 0.0 0.3 1 ~ ~0.4 ~ 0.1 0.1 0.1 0 10
    scoreboard players enable @a bm1_3d_items_trg 

# kill base if no model
    execute as @e[tag=bm1_3d_mthb] at @s unless entity @e[tag=bm1_3d_mth,distance=..0.2,tag=bm1_3d_hasItem] run kill @s

# reselect
    execute as @e[tag=bm1_3d_mth,tag=bm1_3d_5] at @s if entity @e[tag=bm1_3d_mtss,distance=..0.5,sort=nearest] run tag @s add bm1_3d_mtsshere
    execute as @e[tag=bm1_3d_mth,tag=bm1_3d_5,tag=bm1_3d_mtsshere] at @s unless entity @e[tag=bm1_3d_mtss,distance=..0.5,sort=nearest] run tag @s remove bm1_3d_mtsshere

# misc
    function buildmodeone:3d_editor/main/blockremove
    function buildmodeone:3d_editor/rotation/rotate

# invitems check
    execute as @a if score @s bm1_3d_items_inv matches 1.. run function buildmodeone:3d_editor/addons/invitems/items

# give items
    execute as @a[scores={bm1_3d_items_trg=1..}] run function buildmodeone:3d_editor/main/items_trg

# candle particles
    execute as @e[tag=bm1_3d_mth,tag=candle] at @s run particle minecraft:dust 0.1 0.1 0.1 0.4 ~ ~0.6 ~ 0 0.1 0 0 1 normal

# reset item trigger
    execute as @a[scores={bm1_3d_items_trg=1..}] run scoreboard players set @s bm1_3d_items_trg 0

# chair effects
    effect give @e[tag=bm1_3d_esit] minecraft:invisibility 1000000 1 true
    effect give @e[tag=bm1_3d_esit] minecraft:instant_health 1000000 100 true

# item book use (chatgui)
    execute as @a[nbt={SelectedItem:{id:"minecraft:carrot_on_a_stick",tag: {CustomModelData: 1370001}}}] if score @s bm1_global_iu matches 1.. run function buildmodeone:3d_editor/tutorial/limit_features/switch_case
    execute as @a[scores={bm1_global_iu=1..}] run scoreboard players set @s bm1_global_iu 0
#

# multiplayer
    ## give ID
    execute as @a unless score @s bm1_global_id matches 0.. run function buildmodeone:3d_editor/multiplayer/id/give_id

    ## spawn eggs
    execute as @e[tag=bm1_3d_mtc] at @s run function buildmodeone:3d_editor/multiplayer/eggs/change
    execute as @e[tag=bm1_3d_mtp] at @s run function buildmodeone:3d_editor/multiplayer/eggs/spawn
#

# stopsound
    # stopsound @a[tag=bm1_3d_mpa] * minecraft:item.armor.equip_generic
#