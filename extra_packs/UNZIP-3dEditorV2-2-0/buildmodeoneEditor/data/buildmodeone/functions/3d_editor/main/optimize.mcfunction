# reset
execute as @e[tag=bm1_3d_vars,limit=1] run scoreboard players set @s bm1_3d_entityC 0
execute as @e[tag=bm1_3d_vars,limit=1] run scoreboard players set @s bm1_3d_optState 0

tag @s add bm1_3d_mpa
execute as @e[tag=bm1_3d_mtss] if score @s bm1_global_id = @a[tag=bm1_3d_mpa,limit=1,sort=nearest] bm1_global_id run tag @s add bm1_3d_mpa

execute as @e[tag=bm1_3d_mtss,tag=bm1_3d_mpa] at @s as @e[tag=bm1_3d_mth,tag=bm1_3d_5,sort=nearest,limit=1,distance=..0.5] run function buildmodeone:3d_editor/main/optimize/mtss
execute as @e[tag=bm1_3d_vars,limit=1] at @s run function buildmodeone:3d_editor/main/optimize/vars
execute as @e[tag=bm1_3d_mtss,tag=bm1_3d_mpa] at @s run function buildmodeone:3d_editor/main/optimize/mtss2

function buildmodeone:3d_editor/addons/litemode/reminder/schedule

tag @e[tag=bm1_3d_mpa] remove bm1_3d_mpa