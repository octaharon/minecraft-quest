# opt_states
## center
execute at @s if entity @s[tag=bm1_3d_hasItem] if entity @e[tag=bm1_3d_mth,distance=..0.5,tag=!bm1_3d_5,tag=!bm1_3d_hasItem,limit=8] run scoreboard players set @e[tag=bm1_3d_vars] bm1_3d_optState 1
## empty
execute at @s if entity @s[tag=!bm1_3d_hasItem] unless entity @e[tag=bm1_3d_mth,distance=..0.5,tag=!bm1_3d_5,tag=bm1_3d_hasItem,limit=8] run scoreboard players set @e[tag=bm1_3d_vars] bm1_3d_optState 2

## center + border
execute at @s if entity @s[tag=bm1_3d_hasItem] if entity @e[tag=bm1_3d_mth,distance=..0.5,tag=!bm1_3d_5,tag=bm1_3d_hasItem,limit=8] run scoreboard players set @e[tag=bm1_3d_vars] bm1_3d_optState 3

## border
execute at @s if entity @s[tag=!bm1_3d_hasItem] if entity @e[tag=bm1_3d_mth,distance=..0.5,tag=!bm1_3d_5,tag=bm1_3d_hasItem,limit=8] run scoreboard players set @e[tag=bm1_3d_vars] bm1_3d_optState 4

