# count

execute if score @s bm1_3d_optState matches 1 run scoreboard players set @s bm1_3d_entityC 8

execute if score @s bm1_3d_optState matches 2 run scoreboard players set @s bm1_3d_entityC 9

execute if score @s bm1_3d_optState matches 3 as @e[tag=bm1_3d_mtss] at @s as @e[tag=bm1_3d_mth,distance=..0.5,limit=7,tag=!bm1_3d_hasItem] run scoreboard players add @e[tag=bm1_3d_vars] bm1_3d_entityC 1

execute if score @s bm1_3d_optState matches 4 as @e[tag=bm1_3d_mtss] at @s as @e[tag=bm1_3d_mth,distance=..0.5,limit=7,tag=!bm1_3d_hasItem,tag=!bm1_3d_5] run scoreboard players add @e[tag=bm1_3d_vars] bm1_3d_entityC 1
