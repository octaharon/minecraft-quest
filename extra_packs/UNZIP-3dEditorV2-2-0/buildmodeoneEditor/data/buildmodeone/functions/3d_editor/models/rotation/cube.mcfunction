tag @s add bm1_3d_mpa
execute as @e[tag=bm1_3d_mtps] if score @s bm1_global_id = @a[tag=bm1_3d_mpa,limit=1,sort=nearest] bm1_global_id run tag @s add bm1_3d_mpa
function buildmodeone:3d_editor/rotation/clear
execute as @e[tag=bm1_3d_mtps,tag=bm1_3d_mpa] at @s unless entity @e[tag=bm1_3d_mthb,distance=..0.1] run summon armor_stand ~ ~ ~ {CustomNameVisible:0b,NoGravity:1b,Invulnerable:1b,Marker:1b,Invisible:1b,Tags: ["bm1_3d_mthb", "global.ignore", "bm1_3d_mpa"],ShowArms:1b,DisabledSlots:4144959,Small:1b}
function buildmodeone:3d_editor/models/rotation/base

execute as @e[tag=bm1_3d_mtps,tag=bm1_3d_mpa] at @s run tag @e[tag=bm1_3d_mth,distance=..0.1,type=armor_stand] add bm1_3d_hasItem
execute as @e[tag=bm1_3d_mtps,tag=bm1_3d_mpa] at @s run replaceitem entity @e[tag=bm1_3d_mthb,limit=1,distance=..0.1] armor.head minecraft:barrier{CustomModelData:1370035}

function buildmodeone:3d_editor/rotation/rs1
tag @e[tag=bm1_3d_mpa] remove bm1_3d_mpa