# header
    tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
    tellraw @s ["", {"text": "                                    Settings                              ", "color": "white"}]
    function buildmodeone:3d_editor/settings/version
    tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
    tellraw @s ["", {"text": ""}]
#
#Settings
    tellraw @s ["", {"text": ">> ", "color": "aqua"}, {"text": "InvItems Slots:", "color": "white", "bold": true}]
    tellraw @s ["", {"text": ""}]
    ## bl
        execute unless score @s bm1_3d_inv_slot matches 1.. run tellraw @s ["", {"text": "    "}, {"text": "█ █ █ ", "color": "green","clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/addons/invitems/customize/button/tl"}}, {"text": "█ █ █ ", "color": "gray"}, {"text": "█ █ █", "color": "green","clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/addons/invitems/customize/button/tr"}}, {"text": "    "}, {"text": "█ ", "color": "gold"}, {"text": "Selected InvItems Slots", "color": "gray"}]
    ## tl
        execute if score @s bm1_3d_inv_slot matches 1 run tellraw @s ["", {"text": "    "}, {"text": "█ █ █ ", "color": "gold","clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/addons/invitems/customize/button/tl"}}, {"text": "█ █ █ ", "color": "gray"}, {"text": "█ █ █", "color": "green","clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/addons/invitems/customize/button/tr"}}, {"text": "    "}, {"text": "█ ", "color": "gold"}, {"text": "Selected InvItems Slots", "color": "gray"}]
    ## tr
        execute if score @s bm1_3d_inv_slot matches 2 run tellraw @s ["", {"text": "    "}, {"text": "█ █ █ ", "color": "green","clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/addons/invitems/customize/button/tl"}}, {"text": "█ █ █ ", "color": "gray"}, {"text": "█ █ █", "color": "gold","clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/addons/invitems/customize/button/tr"}}, {"text": "    "}, {"text": "█ ", "color": "gold"}, {"text": "Selected InvItems Slots", "color": "gray"}]
    ## br
        execute if score @s bm1_3d_inv_slot matches 3 run tellraw @s ["", {"text": "    "}, {"text": "█ █ █ ", "color": "green","clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/addons/invitems/customize/button/tl"}}, {"text": "█ █ █ ", "color": "gray"}, {"text": "█ █ █", "color": "green","clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/addons/invitems/customize/button/tr"}}, {"text": "    "}, {"text": "█ ", "color": "gold"}, {"text": "Selected InvItems Slots", "color": "gray"}]

        tellraw @s ["", {"text": ""}]
        tellraw @s ["", {"text": "    "}, {"text": "█ █ █ █ █ █ █ █ █", "color": "gray"}, {"text": "    "}, {"text": "█ ", "color": "green"}, {"text": "Available Inventory Positions", "color": "gray"}]
        tellraw @s ["", {"text": ""}]

    ## bl
        execute unless score @s bm1_3d_inv_slot matches 1.. run tellraw @s ["", {"text": "    "}, {"text": "█ █ █ ", "color": "gold","clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/addons/invitems/customize/button/bl"}}, {"text": "█ █ █ ", "color": "gray"}, {"text": "█ █ █", "color": "green","clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/addons/invitems/customize/button/br"}}]
    ## tl
        execute if score @s bm1_3d_inv_slot matches 1 run tellraw @s ["", {"text": "    "}, {"text": "█ █ █ ", "color": "green","clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/addons/invitems/customize/button/bl"}}, {"text": "█ █ █ ", "color": "gray"}, {"text": "█ █ █", "color": "green","clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/addons/invitems/customize/button/br"}}]
    ## tr
        execute if score @s bm1_3d_inv_slot matches 2 run tellraw @s ["", {"text": "    "}, {"text": "█ █ █ ", "color": "green","clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/addons/invitems/customize/button/bl"}}, {"text": "█ █ █ ", "color": "gray"}, {"text": "█ █ █", "color": "green","clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/addons/invitems/customize/button/br"}}]
    ## br
        execute if score @s bm1_3d_inv_slot matches 3 run tellraw @s ["", {"text": "    "}, {"text": "█ █ █ ", "color": "green","clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/addons/invitems/customize/button/bl"}}, {"text": "█ █ █ ", "color": "gray"}, {"text": "█ █ █", "color": "gold","clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/addons/invitems/customize/button/br"}}]
    ##
#

tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": "  "}, {"text": "/!\\", "color": "dark_red", "bold": false, "underlined": true}, {"text": " Existing items in the selected slots will be overwritten! ", "color": "gray"}, {"text": "/!\\", "color": "dark_red", "bold": false, "underlined": true}]
tellraw @s ["", {"text": ""}]


# footer
    ## pages
        tellraw @s ["", {"text": "<<", "color": "gray", "bold": true,"clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/settings/switch/page2"}}, {"text": "                                 "}, {"text": "- ","color": "gray","bold": true,"clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/settings/switch/page1"}}, {"text": "- ","color": "gray","bold": true,"clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/settings/switch/page2"}}, {"text": "- ","color": "white","bold": true,"clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/settings/switch/selected"}}, {"text": "                                "}, {"text": ">>", "color": "dark_gray", "bold": true,"clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/settings/switch/selected"}}]

    ## design credit
        tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
        tellraw @s ["", {"text": "                        Design inspired by Moggla", "clickEvent": {"action": "open_url", "value": "https://www.planetminecraft.com/member/moggla/"}, "color": "gray"}]
        tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
#