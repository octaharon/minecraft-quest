# header
    tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
    tellraw @s ["", {"text": "                                    Settings                              ", "color": "white"}]
    function buildmodeone:3d_editor/settings/version
    tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
    tellraw @s ["", {"text": ""}]
#

# Extras
    ## Update
        tellraw @s ["", {"text": ">> ", "color": "gold", "clickEvent": {"action": "open_url", "value": "https://www.planetminecraft.com/data-pack/3d-model-editor/"}}, {"text": "Check for Updates ", "color": "gray", "clickEvent": {"action": "open_url", "value": "https://www.planetminecraft.com/data-pack/3d-model-editor/"}}]
    ## Start Tutorial
        tellraw @s ["", {"text": ">> ", "color": "gold", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/tutorial/start"}}, {"text": "Start Tutorial", "color": "gray", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/tutorial/start_reset"}}]
    ## 1.X to 2.0 Converter
        ### tellraw @s ["", {"text": ">> ", "color": "gold", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/addons/converter/run"}}, {"text": "Run 2.0 Converter", "color": "gray", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/addons/converter/msg"}}]
    ## 2.0 to 2.1 Converter
    tellraw @s ["", {"text": ">> ", "color": "gold", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/addons/converter_cgui/run"}}, {"text": "Run 2.1+ Converter", "color": "gray", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/addons/converter_cgui/msg"}}]
    ## transparency fix
    tellraw @s ["", {"text": ">> ", "color": "gold", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/converter/21to22/over_gui"}}, {"text": "Run Transparency Fixer", "color": "gray", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/converter/21to22/over_gui"}}]
    
#

tellraw @s ["", {"text": ""}]

# Settings
    ## Items in Inv
        execute if score @s bm1_3d_items_inv matches 1.. run tellraw @s ["", {"text": "[ ✔ ]", "color": "green", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/addons/invitems/disable"}}, {"text": " 3D-Items always in Inventory", "color": "gray"}]
        execute unless score @s bm1_3d_items_inv matches 1.. run tellraw @s ["", {"text": "[ ❌ ]", "color": "red", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/addons/invitems/enable"}}, {"text": " 3D-Items always in Inventory", "color": "gray"}]
    ## Barrier on Place
        execute if score @s bm1_3d_add_bar matches 1.. run tellraw @s ["", {"text": "[ ✔ ]", "color": "green", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/addons/barrier/disable"}}, {"text": " Barrier on Modelplace", "color": "gray"}]
        execute unless score @s bm1_3d_add_bar matches 1.. run tellraw @s ["", {"text": "[ ❌ ]", "color": "red", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/addons/barrier/enable"}}, {"text": " Barrier on Modelplace", "color": "gray"}]
    ## Chat-GUI (Standard)
        ### execute if score @e[tag=bm1_3d_vars,limit=1] bm1_3d_add_cgui matches 1.. run tellraw @s ["", {"text": "[ ✔ ]", "color": "green", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/addons/chatgui/disable"}}, {"text": " Chat-GUI Addon", "color": "gray"}]
        ### execute unless score @e[tag=bm1_3d_vars,limit=1] bm1_3d_add_cgui matches 1.. run tellraw @s ["", {"text": "[ ❌ ]", "color": "red", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/addons/chatgui/enable"}}, {"text": " Chat-GUI Addon", "color": "gray"}]
    ## LiteMode
        execute if score @e[tag=bm1_3d_vars,limit=1] bm1_3d_add_lite matches 1.. run tellraw @s ["", {"text": "[ ✔ ]", "color": "green", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/addons/litemode/disable"}}, {"text": " LiteMode Addon", "color": "gray"}]
        execute unless score @e[tag=bm1_3d_vars,limit=1] bm1_3d_add_lite matches 1.. run tellraw @s ["", {"text": "[ ❌ ]", "color": "red", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/addons/litemode/enable"}}, {"text": " LiteMode Addon", "color": "gray"}]
    ## LiteMode Reminder
        execute if score @e[tag=bm1_3d_vars,limit=1] bm1_3d_add_liter matches 1.. run tellraw @s ["", {"text": "[ ✔ ]", "color": "green", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/addons/litemode/reminder/disable"}}, {"text": " LiteMode Reminder", "color": "gray"}]
        execute unless score @e[tag=bm1_3d_vars,limit=1] bm1_3d_add_liter matches 1.. run tellraw @s ["", {"text": "[ ❌ ]", "color": "red", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/addons/litemode/reminder/enable"}}, {"text": " LiteMode Reminder", "color": "gray"}]
    ## Multiplayer Mode
        ### execute if score @e[tag=bm1_3d_vars,limit=1] bm1_3d_mp_mode matches 1.. run tellraw @s ["", {"text": "[ ✔ ]", "color": "green", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/addons/multiplayer/disable"}}, {"text": " Multiplayer Mode", "color": "gray"}]
        ### execute unless score @e[tag=bm1_3d_vars,limit=1] bm1_3d_mp_mode matches 1.. run tellraw @s ["", {"text": "[ ❌ ]", "color": "red", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/addons/multiplayer/enable"}}, {"text": " Multiplayer Mode", "color": "gray"}]
#

tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]

# footer
    ## pages
        tellraw @s ["", {"text": "<<", "color": "dark_gray", "bold": true,"clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/settings/switch/selected"}}, {"text": "                                 "}, {"text": "- ","color": "white","bold": true,"clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/settings/switch/selected"}}, {"text": "- ","color": "gray","bold": true,"clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/settings/switch/page2"}}, {"text": "- ","color": "gray","bold": true,"clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/settings/switch/page3"}}, {"text": "                                "}, {"text": ">>", "color": "gray", "bold": true,"clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/settings/switch/page2"}}]

    ## design credit
        tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
        tellraw @s ["", {"text": "                        Design inspired by Moggla", "clickEvent": {"action": "open_url", "value": "https://www.planetminecraft.com/member/moggla/"}, "color": "gray"}]
    tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
#