execute run playsound minecraft:ui.button.click master @s ~ ~ ~ .2 1 0.2

tag @s add bm1_3d_mpa
execute as @e[tag=bm1_3d_mtps] if score @s bm1_global_id = @a[tag=bm1_3d_mpa,limit=1,sort=nearest] bm1_global_id run tag @s add bm1_3d_mpa

# execute at @s if entity @s[tag=bm1_3d_1,tag=bm1_3d_mpa] run function buildmodeone:3d_editor/cgui/quickplace_error
# execute at @s if entity @s[tag=bm1_3d_2,tag=bm1_3d_mpa] run function buildmodeone:3d_editor/quickplace/place/2
# execute at @s if entity @s[tag=bm1_3d_2,tag=bm1_3d_mpa] run function buildmodeone:3d_editor/cgui/quickplace

execute as @e[tag=bm1_3d_mtps,tag=bm1_3d_mpa] at @s if entity @e[tag=bm1_3d_1,sort=nearest,distance=..0.1] as @a[tag=bm1_3d_mpa,sort=nearest,limit=1] run function buildmodeone:3d_editor/cgui/quickplace_error
execute as @e[tag=bm1_3d_mtps,tag=bm1_3d_mpa] at @s if entity @e[tag=bm1_3d_2,sort=nearest,distance=..0.1] as @a[tag=bm1_3d_mpa,sort=nearest,limit=1] run function buildmodeone:3d_editor/quickplace/place/2
execute as @e[tag=bm1_3d_mtps,tag=bm1_3d_mpa] at @s if entity @e[tag=bm1_3d_3,sort=nearest,distance=..0.1] as @a[tag=bm1_3d_mpa,sort=nearest,limit=1] run function buildmodeone:3d_editor/cgui/quickplace_error
execute as @e[tag=bm1_3d_mtps,tag=bm1_3d_mpa] at @s if entity @e[tag=bm1_3d_4,sort=nearest,distance=..0.1] as @a[tag=bm1_3d_mpa,sort=nearest,limit=1] run function buildmodeone:3d_editor/quickplace/place/4
execute as @e[tag=bm1_3d_mtps,tag=bm1_3d_mpa] at @s if entity @e[tag=bm1_3d_5,sort=nearest,distance=..0.1] as @a[tag=bm1_3d_mpa,sort=nearest,limit=1] run function buildmodeone:3d_editor/cgui/quickplace_error
execute as @e[tag=bm1_3d_mtps,tag=bm1_3d_mpa] at @s if entity @e[tag=bm1_3d_6,sort=nearest,distance=..0.1] as @a[tag=bm1_3d_mpa,sort=nearest,limit=1] run function buildmodeone:3d_editor/quickplace/place/6
execute as @e[tag=bm1_3d_mtps,tag=bm1_3d_mpa] at @s if entity @e[tag=bm1_3d_7,sort=nearest,distance=..0.1] as @a[tag=bm1_3d_mpa,sort=nearest,limit=1] run function buildmodeone:3d_editor/cgui/quickplace_error
execute as @e[tag=bm1_3d_mtps,tag=bm1_3d_mpa] at @s if entity @e[tag=bm1_3d_8,sort=nearest,distance=..0.1] as @a[tag=bm1_3d_mpa,sort=nearest,limit=1] run function buildmodeone:3d_editor/quickplace/place/8
execute as @e[tag=bm1_3d_mtps,tag=bm1_3d_mpa] at @s if entity @e[tag=bm1_3d_9,sort=nearest,distance=..0.1] as @a[tag=bm1_3d_mpa,sort=nearest,limit=1] run function buildmodeone:3d_editor/cgui/quickplace_error

tag @e[tag=bm1_3d_mpa] remove bm1_3d_mpa