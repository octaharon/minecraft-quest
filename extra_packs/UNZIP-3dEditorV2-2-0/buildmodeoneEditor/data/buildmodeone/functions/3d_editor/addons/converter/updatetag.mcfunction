# add new tags
tag @e[tag=mth,tag=5,tag=air] add bm1_3d_air
## mth -> bm1_3d_mth
execute as @e[tag=mth] run tag @s add bm1_3d_mth
## grid X -> bm1_3d_x
execute as @e[tag=bm1_3d_mth] run function buildmodeone:3d_editor/addons/converter/updatetag_1
## model base -> bm1_3d_hthb
execute as @e[tag=mthb] run tag @s add bm1_3d_mthb
execute as @e[tag=bm1_3d_mthb,tag=rs0] run tag @s add bm1_3d_rs0
execute as @e[tag=bm1_3d_mthb,tag=rs1] run tag @s add bm1_3d_rs1
execute as @e[tag=bm1_3d_mthb,tag=rs2] run tag @s add bm1_3d_rs2
execute as @e[tag=bm1_3d_mthb,tag=rs3] run tag @s add bm1_3d_rs3
## hasItem -> bm1_3d_hasItem
execute as @e[tag=hasItem,tag=bm1_3d_mth] run tag @s add bm1_3d_hasItem
## esita -> bm1_3d_esita
execute as @e[tag=esita] run tag @s add bm1_3d_esita
## esit -> bm1_3d_esit
execute as @e[tag=esit,type=horse] run tag @s add bm1_3d_esit
## team esitt -> bm1_3d_esitt
execute as @e[team=esitt,type=horse] run team join bm1_3d_esitt @s 

# rem old tags
## rem grid X
execute as @e[tag=mth,tag=bm1_3d_mth] run function buildmodeone:3d_editor/addons/converter/updatetag_2
## rem mht
execute as @e[tag=mth,tag=bm1_3d_mth] run tag @s remove mth
## rem mthb
execute as @e[tag=mthb,tag=bm1_3d_mthb] run tag @s remove mthb
## rem hasItem
execute as @e[tag=mth,tag=bm1_3d_mth] run tag @s remove hasItem
## rem esita
execute as @e[tag=esita,tag=bm1_3d_esita] run tag @s remove esita
## rem esit
execute as @e[tag=esit,tag=bm1_3d_esit] run tag @s remove esit
## rem air
execute as @e[tag=air,tag=bm1_3d_air] run tag @s remove air