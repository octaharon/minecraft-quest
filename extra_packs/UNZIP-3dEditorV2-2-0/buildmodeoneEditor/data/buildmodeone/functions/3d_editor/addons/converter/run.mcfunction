# add counter sb
scoreboard players set @e[tag=bm1_3d_vars] bm1_3d_add_cov 0
# count all entities
execute as @e[tag=mth] run scoreboard players add @e[tag=bm1_3d_vars] bm1_3d_add_cov 1
execute as @e[tag=mthb] run scoreboard players add @e[tag=bm1_3d_vars] bm1_3d_add_cov 1
execute as @e[tag=esita] run scoreboard players add @e[tag=bm1_3d_vars] bm1_3d_add_cov 1
execute as @e[tag=esit,type=horse] run scoreboard players add @e[tag=bm1_3d_vars] bm1_3d_add_cov 1
# update tags
function buildmodeone:3d_editor/addons/converter/updatetag

# update CustomModelData
execute as @e[tag=bm1_3d_mth] run function buildmodeone:3d_editor/addons/converter/updatecmd
## update model on base
execute as @e[tag=bm1_3d_mthb] if data entity @s ArmorItems[{id: "minecraft:carrot_on_a_stick", tag: {CustomModelData:32}}] run replaceitem entity @s armor.head minecraft:carrot_on_a_stick{CustomModelData:1370032}
execute as @e[tag=bm1_3d_mthb] if data entity @s ArmorItems[{id: "minecraft:carrot_on_a_stick", tag: {CustomModelData:35}}] run replaceitem entity @s armor.head minecraft:carrot_on_a_stick{CustomModelData:1370035}
# clean old versions
scoreboard objectives remove 3d-Items
scoreboard objectives remove entityCount
scoreboard objectives remove optimizeState
team remove esitt
kill @e[tag=mtps]
kill @e[tag=mtss]
kill @e[tag=3d_vars]
kill @e[tag=3d_opt]
# print count
