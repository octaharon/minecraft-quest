function buildmodeone:3d_editor/rotation/rotate
function buildmodeone:3d_editor/addons/litemode/spawn
function buildmodeone:3d_editor/addons/litemode/change
function buildmodeone:3d_editor/addons/litemode/blockremoveair

execute as @a if score @s bm1_3d_items_inv matches 1.. run function buildmodeone:3d_editor/addons/invitems/main 
execute as @a[scores={bm1_3d_items_trg=1..}] run function buildmodeone:3d_editor/main/items_trg

execute as @e[tag=bm1_3d_mth,tag=candle] at @s run particle minecraft:dust 0.1 0.1 0.1 0.4 ~ ~0.6 ~ 0 0.1 0 0 1 normal
effect give @e[tag=bm1_3d_esit] minecraft:invisibility 1000000 1 true
effect give @e[tag=bm1_3d_esit] minecraft:instant_health 1000000 100 true
execute as @a[scores={bm1_3d_items_trg=1..}] run scoreboard players set @s bm1_3d_items_trg 0
scoreboard players enable @a bm1_3d_items_trg

# item book use
execute as @a[nbt={SelectedItem:{id:"minecraft:carrot_on_a_stick",tag: {CustomModelData: 1370001}}}] if score @s bm1_global_iu matches 1.. run function buildmodeone:3d_editor/cgui/menu
execute as @a[scores={bm1_global_iu=1..}] run scoreboard players set @s bm1_global_iu 0

# multiplayer
execute as @a unless score @s bm1_global_id matches 0.. run function buildmodeone:3d_editor/multiplayer/id/give_id