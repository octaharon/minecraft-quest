function buildmodeone:3d_editor/addons/converter/run
# add counter sb
scoreboard players set @e[tag=bm1_3d_vars] bm1_3d_add_cov2 0
# count all entities
execute as @e[tag=bm1_3d_mth] unless entity @s[tag=bm1_3d_update] run scoreboard players add @e[tag=bm1_3d_vars] bm1_3d_add_cov2 1
execute as @e[tag=bm1_3d_mthb] unless entity @s[tag=bm1_3d_update] run scoreboard players add @e[tag=bm1_3d_vars] bm1_3d_add_cov2 1
execute as @e[tag=bm1_3d_esita] unless entity @s[tag=bm1_3d_update] run scoreboard players add @e[tag=bm1_3d_vars] bm1_3d_add_cov2 1
execute as @e[tag=bm1_3d_esit,type=horse] unless entity @s[tag=bm1_3d_update] run scoreboard players add @e[tag=bm1_3d_vars] bm1_3d_add_cov2 1
execute as @e[tag=bm1_3d_vars] run scoreboard players operation @s bm1_3d_add_cov2 += @s bm1_3d_add_cov
# update tags
function buildmodeone:3d_editor/addons/converter_cgui/updatetag

# update CustomModelData
execute as @e[tag=bm1_3d_mth] run function buildmodeone:3d_editor/addons/converter_cgui/updatecmd
## update model on base
execute as @e[tag=bm1_3d_mthb] if data entity @s ArmorItems[{id: "minecraft:carrot_on_a_stick", tag: {CustomModelData:1370032}}] run replaceitem entity @s armor.head minecraft:barrier{CustomModelData:1370032}
execute as @e[tag=bm1_3d_mthb] if data entity @s ArmorItems[{id: "minecraft:carrot_on_a_stick", tag: {CustomModelData:1370035}}] run replaceitem entity @s armor.head minecraft:barrier{CustomModelData:1370035}
