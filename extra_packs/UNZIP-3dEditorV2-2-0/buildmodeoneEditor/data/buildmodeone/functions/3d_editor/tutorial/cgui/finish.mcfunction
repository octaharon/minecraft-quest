execute as @s unless score @s bm1_3d_tut_prog matches 18.. run scoreboard players set @s bm1_3d_tut_prog 18
scoreboard players set @s bm1_3d_tut_stat 18

playsound minecraft:ui.button.click master @s ~ ~ ~ .2 1 0.2

tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]

tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": "                                   Tutorial", "color": "white"}]
function buildmodeone:3d_editor/settings/version
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": ">> ", "color": "aqua"}, {"text": "Click on ", "color": "gray"}, {"text": "Optimize", "color":"red"}, {"text": " to remove empty model positions.", "color": "gray"}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ">> ", "color": "aqua"}, {"text": "Finish:", "color": "white", "bold": true}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": "    "}, {"text": "- ", "color": "gray"},{"text": "Optimize", "color": "red", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/tutorial/cgui/finish_opt"}}]
tellraw @s ["", {"text": "    "}, {"text": "- ", "color": "gray"}, {"text": "Finish", "color": "dark_gray"}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
# tellraw @s ["", {"text": ">> Note: ", "color": "green"}, {"text": "Deleted ", "color": "gray", "bold":false}, {"score": {"name": "@e[tag=bm1_3d_vars,limit=1] ", "objective": "bm1_3d_entityC"}, "color": "red", "bold":false}, {"text": " entities!", "color": "gray", "bold":false}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": " "}, {"text": "Position", "color": "dark_gray", "bold": false}, {"text": " - ", "color": "dark_gray"}, {"text": "Models", "color": "dark_gray", "bold": false}, {"text": " - ", "color": "dark_gray"}, {"text": "Quickplace", "color": "dark_gray", "bold": false}, {"text": " - ", "color": "dark_gray"}, {"text": "Rotation", "color": "dark_gray", "bold": false}, {"text": " - ", "color": "dark_gray"}, {"text": "Blocktype", "color": "dark_gray", "bold": false}, {"text": " - ", "color": "dark_gray"}, {"text": "Finish", "color": "white", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/tutorial/cgui/finish"}}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": "<< Previous", "color": "white", "bold": true, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/tutorial/cgui/to_optimize"}}, {"text": "                                                    "}, {"text": "Next >>", "color": "white", "bold": true, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/tutorial/cgui/finish_opt"}}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]