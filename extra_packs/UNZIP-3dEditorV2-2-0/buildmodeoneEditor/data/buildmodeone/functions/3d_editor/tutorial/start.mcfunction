playsound minecraft:ui.button.click master @s ~ ~ ~ .2 1 0.2
execute as @s unless score @s bm1_3d_tut_prog matches 1.. run scoreboard players set @s bm1_3d_tut_prog 1
scoreboard players set @s bm1_3d_tut_stat 1
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]

tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": "                                   Tutorial", "color": "white"}]
function buildmodeone:3d_editor/settings/version
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ">> ", "color": "aqua"}, {"text": "Welcome to the Tutorial!", "color": "white", "bold": true}]
tellraw @s ["", {"text": ">> ", "color": "aqua"}, {"text": "At first, let's start with the items for this datapack!", "color": "gray"}]
tellraw @s ["", {"text": ">> ", "color": "aqua"}, {"text": "You can get the Items with the command:", "color": "gray"}]
tellraw @s ["", {"text": "    /trigger bm1_3d_items_trg", "color": "white", "clickEvent":{"action": "run_command", "value": "/trigger bm1_3d_items_trg"}}]
tellraw @s ["", {"text": ""}]

# Items in Inv
execute if score @s bm1_3d_items_inv matches 1.. run tellraw @s ["", {"text": ">> ", "color": "aqua"}, {"text": "Additional settings: ", "color": "gray"}, {"text": "[ ✔ ]", "color": "green", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/tutorial/invitems/disable"}}, {"text": " 3D-Items always in Inventory", "color": "gray"}]
execute unless score @s bm1_3d_items_inv matches 1.. run tellraw @s ["", {"text": ">> ", "color": "aqua"}, {"text": "Additional settings: ", "color": "gray"}, {"text": "[ ❌ ]", "color": "red", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/tutorial/invitems/enable"}}, {"text": " 3D-Items always in Inventory", "color": "gray"}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": "<< Previous", "color": "gold", "bold": true, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/tutorial/complete"}}, {"text": "                                                    "}, {"text": "Next >>", "color": "white", "bold": true, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/tutorial/items"}}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]