function buildmodeone:3d_editor/main/finish
execute as @s unless score @s bm1_3d_tut_prog matches 20.. run scoreboard players set @s bm1_3d_tut_prog 20
scoreboard players set @s bm1_3d_tut_stat 20

playsound minecraft:ui.button.click master @s ~ ~ ~ .2 1 0.2

tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]

tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": "                                   Tutorial", "color": "white"}]
function buildmodeone:3d_editor/settings/version
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": ">> ", "color": "green"}, {"text": "Note: ", "color": "green"}, {"text": "Deleted model positions can't be used again!", "color": "gray"}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ">> ", "color": "aqua"}, {"text": "Finish:", "color": "white", "bold": true}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": "    "}, {"text": "- ", "color": "gray"},{"text": "Optimize", "color": "dark_gray"}]
tellraw @s ["", {"text": "    "}, {"text": "- ", "color": "gray"}, {"text": "Finish", "color": "dark_gray"}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
# tellraw @s ["", {"text": ">> Note: ", "color": "green"}, {"text": "Deleted ", "color": "gray", "bold":false}, {"score": {"name": "@e[tag=bm1_3d_vars,limit=1] ", "objective": "bm1_3d_entityC"}, "color": "red", "bold":false}, {"text": " entities!", "color": "gray", "bold":false}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": " "}, {"text": "Position", "color": "dark_gray", "bold": false}, {"text": " - ", "color": "dark_gray"}, {"text": "Models", "color": "dark_gray", "bold": false}, {"text": " - ", "color": "dark_gray"}, {"text": "Quickplace", "color": "dark_gray", "bold": false}, {"text": " - ", "color": "dark_gray"}, {"text": "Rotation", "color": "dark_gray", "bold": false}, {"text": " - ", "color": "dark_gray"}, {"text": "Blocktype", "color": "dark_gray", "bold": false}, {"text": " - ", "color": "dark_gray"}, {"text": "Finish", "color": "white", "bold": false}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": "<< Previous", "color": "white", "bold": true, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/tutorial/cgui/finish_opt"}}, {"text": "                                                    "}, {"text": "Next >>", "color": "white", "bold": true, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/tutorial/change"}}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]