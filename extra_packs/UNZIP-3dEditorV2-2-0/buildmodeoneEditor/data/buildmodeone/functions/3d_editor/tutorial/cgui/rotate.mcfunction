playsound minecraft:ui.button.click master @s ~ ~ ~ .2 1 0.2
execute as @s unless score @s bm1_3d_tut_prog matches 14.. run scoreboard players set @s bm1_3d_tut_prog 14
scoreboard players set @s bm1_3d_tut_stat 14
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]

tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": "                                   Tutorial", "color": "white"}]
function buildmodeone:3d_editor/settings/version
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": ">> ", "color": "aqua"}, {"text": "Hover over the buttons, to choose a rotation and click it.", "color": "gray"}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ">> ", "color": "aqua"}, {"text": "Rotation:", "color": "white", "bold": true}]
tellraw @s ["", {"text": ""}]
# rotate
tellraw @s ["", {"text": "    "}, {"text": "[<<]", "color": "dark_aqua", "bold": true, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/rotation/base/n90"}, "hoverEvent":{"action": "show_text", "value": "-90°"}}, {"text": " [<]", "color": "gray","clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/rotation/base/n45"}, "hoverEvent":{"action": "show_text", "value": "-45°"}}, {"text": " [+] ", "color": "blue", "bold": true, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/rotation/base/flip"}, "hoverEvent":{"action": "show_text", "value": "180°"}}, {"text": "[>] ", "color": "gray", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/rotation/base/p45"}, "hoverEvent":{"action": "show_text", "value": "+45°"}}, {"text": "[>>]", "color": "dark_aqua", "bold": true, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/rotation/base/p90"}, "hoverEvent":{"action": "show_text", "value": "+90°"}}]
tellraw @s ["", {"text": "    "}, {"text": "[<<]", "color": "gold", "bold": true, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/rotation/base/n10"}, "hoverEvent":{"action": "show_text", "value": "-10°"}}, {"text": " [<]", "color": "gray","clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/rotation/base/n5"}, "hoverEvent":{"action": "show_text", "value": "-5°"}}, {"text": " [X] ", "color": "red", "bold": true, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/rotation/base/reset"}, "hoverEvent":{"action": "show_text", "value": "Reset"}}, {"text": "[>] ", "color": "gray", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/rotation/base/p5"}, "hoverEvent":{"action": "show_text", "value": "+5°"}}, {"text": "[>>]", "color": "gold", "bold": true, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/rotation/base/p10"}, "hoverEvent":{"action": "show_text", "value": "+10°"}}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ">> ", "color": "green"}, {"text": "Note: ", "color": "green"}, {"text": "Only the selected model will rotate!", "color": "gray"}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": " "}, {"text": "Position", "color": "dark_gray", "bold": false}, {"text": " - ", "color": "dark_gray"}, {"text": "Models", "color": "dark_gray", "bold": false}, {"text": " - ", "color": "dark_gray"}, {"text": "Quickplace", "color": "dark_gray", "bold": false}, {"text": " - ", "color": "dark_gray"}, {"text": "Rotation", "color": "white", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/tutorial/cgui/rotate"}}, {"text": " - ", "color": "dark_gray"}, {"text": "Blocktype", "color": "dark_gray", "bold": false}, {"text": " - ", "color": "dark_gray"}, {"text": "Finish", "color": "dark_gray", "bold": false}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": "<< Previous", "color": "white", "bold": true, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/tutorial/cgui/to_rotate"}}, {"text": "                                                    "}, {"text": "Next >>", "color": "white", "bold": true, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/tutorial/multiple"}}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
