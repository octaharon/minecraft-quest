execute as @s unless score @s bm1_3d_tut_prog matches 7.. run scoreboard players set @s bm1_3d_tut_prog 7
scoreboard players set @s bm1_3d_tut_stat 7

playsound minecraft:ui.button.click master @s ~ ~ ~ .2 1 0.2

tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]

tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": "                                   Tutorial", "color": "white"}]
function buildmodeone:3d_editor/settings/version
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": ">> ", "color": "aqua"}, {"text": "To orientate yourself, look at the direction of the ", "color": "gray"}, {"text": "particles", "color":"gold"}, {"text": ".", "color": "gray"}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ">> ", "color": "aqua"}, {"text": "Select position:", "color": "white", "bold": true}]
tellraw @s ["", {"text": ""}]
# grid
tellraw @s ["", {"text": "    "}, {"text": "█", "color": "blue","clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/tutorial/cgui/pos/pos1"}}, {"text": "  ", "color": "gray"}, {"text": "█", "color": "blue","clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/tutorial/cgui/pos/pos2"}}, {"text": "  ", "color": "gray"}, {"text": "█", "color": "blue","clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/tutorial/cgui/pos/pos3"}}]
tellraw @s ["", {"text": "    "}, {"text": "   ", "bold": false}, {"text": " ", "bold": true}, {"text": "█", "color": "gold"}, {"text": "   ", "bold": false}, {"text": " ", "bold": true}]
tellraw @s ["", {"text": "    "}, {"text": "█", "color": "blue","clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/tutorial/cgui/pos/pos4"}}, {"text": "  ", "color": "gray"}, {"text": "█", "color": "blue","clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/tutorial/cgui/pos/pos5"}}, {"text": "  ", "color": "gray"}, {"text": "█", "color": "blue","clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/tutorial/cgui/pos/pos6"}}]
tellraw @s ["", {"text": "    "}, {"text": "   ", "bold": false}, {"text": " ", "bold": true}, {"text": "  ", "bold": true}, {"text": "   ", "bold": false}, {"text": " ", "bold": true}]
tellraw @s ["", {"text": "    "}, {"text": "█", "color": "blue","clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/tutorial/cgui/pos/pos7"}}, {"text": "  ", "color": "gray"}, {"text": "█", "color": "blue","clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/tutorial/cgui/pos/pos8"}}, {"text": "  ", "color": "gray"}, {"text": "█", "color": "blue","clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/tutorial/cgui/pos/pos9"}}]
# /grid
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": " "}, {"text": "Position", "color": "white", "bold": false}, {"text": " - ", "color": "dark_gray"}, {"text": "Models", "color": "dark_gray", "bold": false}, {"text": " - ", "color": "dark_gray"}, {"text": "Quickplace", "color": "dark_gray", "bold": false}, {"text": " - ", "color": "dark_gray"}, {"text": "Rotation", "color": "dark_gray", "bold": false}, {"text": " - ", "color": "dark_gray"}, {"text": "Blocktype", "color": "dark_gray", "bold": false}, {"text": " - ", "color": "dark_gray"}, {"text": "Finish", "color": "dark_gray", "bold": false}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": "<< Previous", "color": "white", "bold": true, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/tutorial/cgui/selected"}}, {"text": "                                                    "}, {"text": "Next >>", "color": "white", "bold": true, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/tutorial/models"}}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]