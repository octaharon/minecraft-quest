execute as @s unless score @s bm1_3d_tut_prog matches 10.. run scoreboard players set @s bm1_3d_tut_prog 10
scoreboard players set @s bm1_3d_tut_stat 10

playsound minecraft:ui.button.click master @s ~ ~ ~ .2 1 0.2

tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]

tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": "                                   Tutorial", "color": "white"}]
function buildmodeone:3d_editor/settings/version
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": ">> ", "color": "aqua"}, {"text": "Click through the categories and select a ", "color": "gray"}, {"text": "model", "color":"dark_aqua"}, {"text": ".", "color": "gray"}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ">> ", "color": "aqua"}, {"text": "Model categories:", "color": "white", "bold": true}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": "    "}, {"text": "- ", "color": "gray"},{"text": "Tutorial Category", "color": "dark_aqua", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/tutorial/cgui/kitchen"}}]
tellraw @s ["", {"text": "    "}, {"text": "- ", "color": "gray"},{"text": "...", "color": "dark_gray"}]
tellraw @s ["", {"text": "    "}, {"text": "- ", "color": "gray"},{"text": "...", "color": "dark_gray"}]
tellraw @s ["", {"text": "    "}, {"text": "- ", "color": "gray"},{"text": "...", "color": "dark_gray"}]

tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": " "}, {"text": "Position", "color": "dark_gray", "bold": false}, {"text": " - ", "color": "dark_gray"}, {"text": "Models", "color": "white", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/tutorial/cgui/models"}}, {"text": " - ", "color": "dark_gray"}, {"text": "Quickplace", "color": "dark_gray", "bold": false}, {"text": " - ", "color": "dark_gray"}, {"text": "Rotation", "color": "dark_gray", "bold": false}, {"text": " - ", "color": "dark_gray"}, {"text": "Blocktype", "color": "dark_gray", "bold": false}, {"text": " - ", "color": "dark_gray"}, {"text": "Finish", "color": "dark_gray", "bold": false}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": "<< Previous", "color": "white", "bold": true, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/tutorial/cgui/to_models"}}, {"text": "                                                    "}, {"text": "Next >>", "color": "white", "bold": true, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/tutorial/cgui/kitchen"}}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]