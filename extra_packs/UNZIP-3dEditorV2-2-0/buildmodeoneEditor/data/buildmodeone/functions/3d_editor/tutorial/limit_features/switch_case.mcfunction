# normal
    execute unless score @s bm1_3d_tut_stat matches 1.. run function buildmodeone:3d_editor/cgui/menu

# in tutorial
    ## warning
        execute if score @s bm1_3d_tut_stat matches 1.. unless score @s bm1_3d_tut_stat matches 4..14 unless score @s bm1_3d_tut_stat matches 16 run function buildmodeone:3d_editor/tutorial/limit_features/titles
    ## reopen tutorial
        execute if score @s bm1_3d_tut_stat matches 1 run function buildmodeone:3d_editor/tutorial/start
        execute if score @s bm1_3d_tut_stat matches 2 run function buildmodeone:3d_editor/tutorial/items
        execute if score @s bm1_3d_tut_stat matches 3 run function buildmodeone:3d_editor/tutorial/grid
        execute if score @s bm1_3d_tut_stat matches 4 run function buildmodeone:3d_editor/tutorial/cgui/position
            execute if score @s bm1_3d_tut_stat matches 5 run function buildmodeone:3d_editor/tutorial/cgui/position
            execute if score @s bm1_3d_tut_stat matches 6 run function buildmodeone:3d_editor/tutorial/cgui/selected
            execute if score @s bm1_3d_tut_stat matches 7 run function buildmodeone:3d_editor/tutorial/cgui/orientate
        execute if score @s bm1_3d_tut_stat matches 8 run function buildmodeone:3d_editor/tutorial/cgui/to_models
        execute if score @s bm1_3d_tut_stat matches 9 run function buildmodeone:3d_editor/tutorial/cgui/to_models
        execute if score @s bm1_3d_tut_stat matches 10 run function buildmodeone:3d_editor/tutorial/cgui/models
        execute if score @s bm1_3d_tut_stat matches 11 run function buildmodeone:3d_editor/tutorial/cgui/kitchen
        execute if score @s bm1_3d_tut_stat matches 12 run function buildmodeone:3d_editor/tutorial/cgui/to_rotate
        execute if score @s bm1_3d_tut_stat matches 13 run function buildmodeone:3d_editor/tutorial/cgui/to_rotate
        execute if score @s bm1_3d_tut_stat matches 14 run function buildmodeone:3d_editor/tutorial/cgui/rotate
        execute if score @s bm1_3d_tut_stat matches 15 run function buildmodeone:3d_editor/tutorial/multiple
        execute if score @s bm1_3d_tut_stat matches 16 run function buildmodeone:3d_editor/tutorial/cgui/to_optimize
        execute if score @s bm1_3d_tut_stat matches 17 run function buildmodeone:3d_editor/tutorial/cgui/to_optimize
        execute if score @s bm1_3d_tut_stat matches 18 run function buildmodeone:3d_editor/tutorial/cgui/finish
        execute if score @s bm1_3d_tut_stat matches 19 run function buildmodeone:3d_editor/tutorial/cgui/finish_opt
        execute if score @s bm1_3d_tut_stat matches 20 run function buildmodeone:3d_editor/tutorial/cgui/finish_clicked
        execute if score @s bm1_3d_tut_stat matches 21 run function buildmodeone:3d_editor/tutorial/change
        execute if score @s bm1_3d_tut_stat matches 22 run function buildmodeone:3d_editor/tutorial/finish
    ##
#t