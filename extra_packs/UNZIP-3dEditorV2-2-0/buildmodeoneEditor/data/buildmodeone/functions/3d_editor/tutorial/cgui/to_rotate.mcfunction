execute as @s unless score @s bm1_3d_tut_prog matches 13.. run scoreboard players set @s bm1_3d_tut_prog 13
scoreboard players set @s bm1_3d_tut_stat 13

playsound minecraft:ui.button.click master @s ~ ~ ~ .2 1 0.2

tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]

tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": "                                   Tutorial", "color": "white"}]
function buildmodeone:3d_editor/settings/version
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": ">> ", "color": "aqua"}, {"text": "Click on ", "color": "gray"}, {"text": "Rotation", "color":"gold"}, {"text": ".", "color": "gray"}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ">> ", "color": "aqua"}, {"text": "Select position:", "color": "white", "bold": true}]
tellraw @s ["", {"text": ""}]
# grid
tellraw @s ["", {"text": "    "}, {"text": "█", "color": "dark_gray"}, {"text": "  ", "color": "gray"}, {"text": "█", "color": "dark_gray"}, {"text": "  ", "color": "gray"}, {"text": "█", "color": "dark_gray"}]
tellraw @s ["", {"text": "    "}, {"text": "   ", "bold": false}, {"text": " ", "bold": true}, {"text": "█", "color": "gray"}, {"text": "   ", "bold": false}, {"text": " ", "bold": true}]
tellraw @s ["", {"text": "    "}, {"text": "█", "color": "dark_gray"}, {"text": "  ", "color": "gray"}, {"text": "█", "color": "dark_gray"}, {"text": "  ", "color": "gray"}, {"text": "█", "color": "dark_gray"}]
tellraw @s ["", {"text": "    "}, {"text": "   ", "bold": false}, {"text": " ", "bold": true}, {"text": "  ", "bold": true}, {"text": "   ", "bold": false}, {"text": " ", "bold": true}]
tellraw @s ["", {"text": "    "}, {"text": "█", "color": "dark_gray"}, {"text": "  ", "color": "gray"}, {"text": "█", "color": "dark_gray"}, {"text": "  ", "color": "gray"}, {"text": "█", "color": "dark_gray"}]
# /grid
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": " "}, {"text": "Position", "color": "white", "bold": false}, {"text": " - ", "color": "dark_gray"}, {"text": "Models", "color": "dark_gray", "bold": false}, {"text": " - ", "color": "dark_gray"}, {"text": "Quickplace", "color": "dark_gray", "bold": false}, {"text": " - ", "color": "dark_gray"}, {"text": "Rotation", "color": "gold", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/tutorial/cgui/rotate"}}, {"text": " - ", "color": "dark_gray"}, {"text": "Blocktype", "color": "dark_gray", "bold": false}, {"text": " - ", "color": "dark_gray"}, {"text": "Finish", "color": "dark_gray", "bold": false}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": "<< Previous", "color": "white", "bold": true, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/tutorial/rotate"}}, {"text": "                                                    "}, {"text": "Next >>", "color": "white", "bold": true, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/tutorial/cgui/rotate"}}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]