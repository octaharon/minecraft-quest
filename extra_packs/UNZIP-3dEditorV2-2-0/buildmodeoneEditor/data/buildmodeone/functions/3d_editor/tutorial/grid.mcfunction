playsound minecraft:ui.button.click master @s ~ ~ ~ .2 1 0.2
execute as @s unless score @s bm1_3d_tut_prog matches 3.. run scoreboard players set @s bm1_3d_tut_prog 3
scoreboard players set @s bm1_3d_tut_stat 3
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]

tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": "                                   Tutorial", "color": "white"}]
function buildmodeone:3d_editor/settings/version
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ">> ", "color": "aqua"}, {"text": "How does the grid work?", "color": "white", "bold": true}]
tellraw @s ["", {"text": ""}]
# grid
tellraw @s ["", {"text": "    "}, {"text": "█", "color": "blue"}, {"text": "  ", "color": "gray"}, {"text": "█", "color": "blue"}, {"text": "  ", "color": "gray"}, {"text": "█", "color": "blue"}, {"text": "          "}, {"text": "█", "color": "blue"}, {"text": " available model positions", "color": "gray"}]
tellraw @s ["", {"text": "    "}, {"text": "   ", "bold": false}, {"text": " ", "bold": true}, {"text": "█", "color": "gold"}, {"text": "   ", "bold": false}, {"text": " ", "bold": true}, {"text": "          "}, {"text": "█", "color": "dark_green"}, {"text": " selected model positions", "color": "gray"}]
tellraw @s ["", {"text": "    "}, {"text": "█", "color": "blue"}, {"text": "  ", "color": "gray"}, {"text": "█", "color": "dark_green"}, {"text": "  ", "color": "gray"}, {"text": "█", "color": "blue"}, {"text": "          "}, {"text": "█", "color": "gold"}, {"text": " grid alignment (orientation GUI/block)", "color": "gray"}]
tellraw @s ["", {"text": "    "}, {"text": "   ", "bold": false}, {"text": " ", "bold": true}, {"text": "  ", "bold": true}, {"text": "   ", "bold": false}, {"text": " ", "bold": true}]
tellraw @s ["", {"text": "    "}, {"text": "█", "color": "blue"}, {"text": "  ", "color": "gray"}, {"text": "█", "color": "blue"}, {"text": "  ", "color": "gray"}, {"text": "█", "color": "blue"}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": "<< Previous", "color": "white", "bold": true, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/tutorial/items"}}, {"text": "                                                    "}, {"text": "Next >>", "color": "white", "bold": true, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/tutorial/book"}}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]