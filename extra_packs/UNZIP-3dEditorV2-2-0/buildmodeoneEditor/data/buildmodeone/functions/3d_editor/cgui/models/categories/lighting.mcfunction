playsound minecraft:ui.button.click master @s ~ ~ ~ .2 1 0.2

# header
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": "                                   Chat GUI", "color": "white"}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]

# body 
tellraw @s ["", {"text": ">> ", "color": "aqua"}, {"text": "Lighting:", "color": "white", "bold": true}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": "    "}, {"text": "- ", "color": "gray"},{"text": "Invisible Light", "color": "dark_aqua", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/models/lighting/inv"}}]
tellraw @s ["", {"text": "    "}, {"text": "- ", "color": "gray"},{"text": "Lightingbar:", "color": "white"}]
tellraw @s ["", {"text": "        "}, {"text": "- ", "color": "gray"},{"text": "Top", "color": "dark_aqua", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/models/lighting/bar1"}}]
tellraw @s ["", {"text": "        "}, {"text": "- ", "color": "gray"},{"text": "u. Trapdoor", "color": "dark_aqua", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/models/lighting/bar2"}}]
tellraw @s ["", {"text": "        "}, {"text": "- ", "color": "gray"},{"text": "Wall", "color": "dark_aqua", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/models/lighting/bar4"}}]
tellraw @s ["", {"text": "        "}, {"text": "- ", "color": "gray"},{"text": "Bottom", "color": "dark_aqua", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/models/lighting/bar3"}}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]

# pages
tellraw @s ["", {"text": "<<", "color": "dark_gray", "bold": true}, {"text": "                                 "}, {"text": "Menu", "color": "gray", "bold": true, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/models/menu"}}, {"text": "                                  "}, {"text": ">>", "color": "dark_gray", "bold": true}]

# footer
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": " "}, {"text": "Position", "color": "gray", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/position_click"}}, {"text": " - ", "color": "dark_gray"}, {"text": "Models", "color": "white", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/models"}}, {"text": " - ", "color": "dark_gray"}, {"text": "Quickplace", "color": "gray", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/quickplace"}}, {"text": " - ", "color": "dark_gray"}, {"text": "Rotation", "color": "gray", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/rotation"}}, {"text": " - ", "color": "dark_gray"}, {"text": "Blocktype", "color": "gray", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/blocktype"}}, {"text": " - ", "color": "dark_gray"}, {"text": "Finish", "color": "gray", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/finish"}}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]