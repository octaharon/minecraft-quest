playsound minecraft:ui.button.click master @s ~ ~ ~ .2 1 0.2

# header
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": "                                   Chat GUI", "color": "white"}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]

# body 
tellraw @s ["", {"text": ">> ", "color": "aqua"}, {"text": "Christmas:", "color": "white", "bold": true}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": "    "}, {"text": "- ", "color": "gray"},{"text": "Christmas Tree", "color": "dark_aqua", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/models/christmas/tree"}}]
tellraw @s ["", {"text": "    "}, {"text": "- ", "color": "gray"},{"text": "Presents:", "color": "white"}]
tellraw @s ["", {"text": "        "}, {"text": "- "},{"text":"█","color":"red","clickEvent":{"action":"run_command","value":"/function buildmodeone:3d_editor/models/christmas/red"}},{"text":"█","color":"dark_green","clickEvent":{"action":"run_command","value":"/function buildmodeone:3d_editor/models/christmas/green"}},{"text":"█","color":"gold","clickEvent":{"action":"run_command","value":"/function buildmodeone:3d_editor/models/christmas/yellow"}},{"text":"█","color":"dark_blue","clickEvent":{"action":"run_command","value":"/function buildmodeone:3d_editor/models/christmas/blue"}}]
tellraw @s ["", {"text": "    "}, {"text": "- ", "color": "gray"},{"text": "Ornaments:", "color": "white"}]
tellraw @s ["", {"text": "        "}, {"text": "- "},{"text":"█","color":"red","clickEvent":{"action":"run_command","value":"/function buildmodeone:3d_editor/models/christmas/o_red"}},{"text":"█","color":"dark_green","clickEvent":{"action":"run_command","value":"/function buildmodeone:3d_editor/models/christmas/o_green"}},{"text":"█","color":"gold","clickEvent":{"action":"run_command","value":"/function buildmodeone:3d_editor/models/christmas/o_yellow"}},{"text":"█","color":"dark_blue","clickEvent":{"action":"run_command","value":"/function buildmodeone:3d_editor/models/christmas/o_blue"}},{"text":"█","color":"gray","clickEvent":{"action":"run_command","value":"/function buildmodeone:3d_editor/models/christmas/o_lgray"},"hoverEvent":{"action":"show_text","value":["",{"text":"Light Gray"}]}},{"text":"█","color":"gray","clickEvent":{"action":"run_command","value":"/function buildmodeone:3d_editor/models/christmas/o_white"},"hoverEvent":{"action":"show_text","value":["",{"text":"White"}]}}]
tellraw @s ["", {"text": "    "}, {"text": "- ", "color": "gray"},{"text": "Sleigh", "color": "dark_aqua", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/models/christmas/sleigh"}}]
tellraw @s ["", {"text": "    "}, {"text": "- ", "color": "gray"},{"text": "Teddy Bear", "color": "dark_aqua", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/models/christmas/teddy"}}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]

# pages
tellraw @s ["", {"text": "<<", "color": "dark_gray", "bold": true}, {"text": "                                 "}, {"text": "Menu", "color": "gray", "bold": true, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/models/menu"}}, {"text": "                                  "}, {"text": ">>", "color": "dark_gray", "bold": true}]

# footer
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": " "}, {"text": "Position", "color": "gray", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/position_click"}}, {"text": " - ", "color": "dark_gray"}, {"text": "Models", "color": "white", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/models"}}, {"text": " - ", "color": "dark_gray"}, {"text": "Quickplace", "color": "gray", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/quickplace"}}, {"text": " - ", "color": "dark_gray"}, {"text": "Rotation", "color": "gray", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/rotation"}}, {"text": " - ", "color": "dark_gray"}, {"text": "Blocktype", "color": "gray", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/blocktype"}}, {"text": " - ", "color": "dark_gray"}, {"text": "Finish", "color": "gray", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/finish"}}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]