playsound minecraft:ui.button.click master @s ~ ~ ~ .2 1 0.2

# header
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": "                                   Chat GUI", "color": "white"}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]

# body 
tellraw @s ["", {"text": ">> ", "color": "aqua"}, {"text": "Garden:", "color": "white", "bold": true}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": "    "}, {"text": "- ", "color": "gray"},{"text": "Garden chair:", "color": "white"}]
tellraw @s ["", {"text": "        "}, {"text": "- ", "color": "gray"},{"text": "Birch", "color": "dark_aqua", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/models/garden/chairs/birch"}}]
tellraw @s ["", {"text": "        "}, {"text": "- ", "color": "gray"},{"text": "Oak", "color": "dark_aqua", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/models/garden/chairs/oak"}}]
tellraw @s ["", {"text": "        "}, {"text": "- ", "color": "gray"},{"text": "Spruce", "color": "dark_aqua", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/models/garden/chairs/spruce"}}]
tellraw @s ["", {"text": "        "}, {"text": "- ", "color": "gray"},{"text": "Dark Oak", "color": "dark_aqua", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/models/garden/chairs/dark_oak"}}]
tellraw @s ["", {"text": "        "}, {"text": "- ", "color": "gray"},{"text": "Jungle", "color": "dark_aqua", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/models/garden/chairs/jungle"}}]
tellraw @s ["", {"text": "        "}, {"text": "- ", "color": "gray"},{"text": "Acacia", "color": "dark_aqua", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/models/garden/chairs/acacia"}}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]

# pages
tellraw @s ["", {"text": "<<", "color": "dark_gray", "bold": true}, {"text": "                                 "}, {"text": "Menu", "color": "gray", "bold": true, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/models/menu"}}, {"text": "                                  "}, {"text": ">>", "color": "dark_gray", "bold": true}]

# footer
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": " "}, {"text": "Position", "color": "gray", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/position_click"}}, {"text": " - ", "color": "dark_gray"}, {"text": "Models", "color": "white", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/models"}}, {"text": " - ", "color": "dark_gray"}, {"text": "Quickplace", "color": "gray", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/quickplace"}}, {"text": " - ", "color": "dark_gray"}, {"text": "Rotation", "color": "gray", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/rotation"}}, {"text": " - ", "color": "dark_gray"}, {"text": "Blocktype", "color": "gray", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/blocktype"}}, {"text": " - ", "color": "dark_gray"}, {"text": "Finish", "color": "gray", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/finish"}}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]