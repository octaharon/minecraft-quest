playsound minecraft:ui.button.click master @s ~ ~ ~ .2 1 0.2

# header
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": "                                   Chat GUI", "color": "white"}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]

# body 
tellraw @s ["", {"text": ">> ", "color": "aqua"}, {"text": "Kitchen:", "color": "white", "bold": true}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": "    "}, {"text": "- ", "color": "gray"},{"text": "Plate", "color": "dark_aqua", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/models/kitchen/plate"}}]
tellraw @s ["", {"text": "    "}, {"text": "- ", "color": "gray"},{"text": "Fork", "color": "dark_aqua", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/models/kitchen/fork"}}]
tellraw @s ["", {"text": "    "}, {"text": "- ", "color": "gray"},{"text": "Knife", "color": "dark_aqua", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/models/kitchen/knife"}}]
tellraw @s ["", {"text": "    "}, {"text": "- ", "color": "gray"}, {"text":"Wine glass ","color":"dark_aqua","clickEvent":{"action":"run_command","value":"/function buildmodeone:3d_editor/models/kitchen/wineglass"}},{"text":"[Filled]","color":"gray","clickEvent":{"action":"run_command","value":"/function buildmodeone:3d_editor/models/kitchen/winefilled"}}]
tellraw @s ["", {"text": "    "}, {"text": "- ", "color": "gray"},{"text":"Water Glass","color":"dark_aqua","clickEvent":{"action":"run_command","value":"/function buildmodeone:3d_editor/models/kitchen/normalglass"}},{"text":"[Filled]","color":"gray","clickEvent":{"action":"run_command","value":"/function buildmodeone:3d_editor/models/kitchen/normalglassfilled"}}]
tellraw @s ["", {"text": "    "}, {"text": "- ", "color": "gray"},{"text":"Bottle ","color":"dark_aqua","clickEvent":{"action":"run_command","value":"/function buildmodeone:3d_editor/models/bottles/greenbottle"}},{"text":"█","color":"dark_green","clickEvent":{"action":"run_command","value":"/function buildmodeone:3d_editor/models/bottles/greenbottle"}},{"text":"█","color":"dark_red","clickEvent":{"action":"run_command","value":"/function buildmodeone:3d_editor/models/bottles/brownbottle"}},{"text":"█","color":"blue","clickEvent":{"action":"run_command","value":"/function buildmodeone:3d_editor/models/bottles/bluebottle"}},{"text":"█","color":"gray","clickEvent":{"action":"run_command","value":"/function buildmodeone:3d_editor/models/bottles/whitebottle"}}]
tellraw @s ["", {"text": "    "}, {"text": "- ", "color": "gray"},{"text": "Basket", "color": "dark_aqua", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/models/basket/empty"}}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]

# pages
tellraw @s ["", {"text": "<<", "color": "dark_gray", "bold": true}, {"text": "                                 "}, {"text": "Menu", "color": "gray", "bold": true, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/models/menu"}}, {"text": "                                  "}, {"text": ">>", "color": "dark_gray", "bold": true}]

# footer
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": " "}, {"text": "Position", "color": "gray", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/position_click"}}, {"text": " - ", "color": "dark_gray"}, {"text": "Models", "color": "white", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/models"}}, {"text": " - ", "color": "dark_gray"}, {"text": "Quickplace", "color": "gray", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/quickplace"}}, {"text": " - ", "color": "dark_gray"}, {"text": "Rotation", "color": "gray", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/rotation"}}, {"text": " - ", "color": "dark_gray"}, {"text": "Blocktype", "color": "gray", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/blocktype"}}, {"text": " - ", "color": "dark_gray"}, {"text": "Finish", "color": "gray", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/finish"}}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]