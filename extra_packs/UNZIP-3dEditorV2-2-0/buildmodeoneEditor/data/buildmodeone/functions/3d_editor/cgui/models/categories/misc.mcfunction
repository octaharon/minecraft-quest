playsound minecraft:ui.button.click master @s ~ ~ ~ .2 1 0.2

# header
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": "                                   Chat GUI", "color": "white"}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]

# body 
tellraw @s ["", {"text": ">> ", "color": "aqua"}, {"text": "Misc:", "color": "white", "bold": true}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": "    "}, {"text": "- ", "color": "gray"},{"text": "Paper", "color": "dark_aqua", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/models/misc/paper"}}]
tellraw @s ["", {"text": "    "}, {"text": "- ", "color": "gray"},{"text": "Plant ", "color": "dark_aqua", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/models/plants/flowerpotsmall"}},{"text":"[Large]","color":"gray","clickEvent":{"action":"run_command","value":"/function buildmodeone:3d_editor/models/plants/flowerpot"}}]
tellraw @s ["", {"text": "    "}, {"text": "- ", "color": "gray"},{"text": "Bonsai", "color": "dark_aqua", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/models/plants/plant1"}}]
tellraw @s ["", {"text": "    "}, {"text": "- ", "color": "gray"},{"text": "Candles:", "color": "white"}]
tellraw @s ["", {"text": "        "}, {"text": "- ", "color": "gray"},{"text":"[S]","color":"dark_aqua","clickEvent":{"action":"run_command","value":"/function buildmodeone:3d_editor/models/halloween/candlesmall"}},{"text":"[S] ","color":"gold","clickEvent":{"action":"run_command","value":"/function buildmodeone:3d_editor/models/halloween/candlelitsmall"}},{"text":"[M]","color":"dark_aqua","clickEvent":{"action":"run_command","value":"/function buildmodeone:3d_editor/models/halloween/candle"}},{"text":"[M] ","color":"gold","clickEvent":{"action":"run_command","value":"/function buildmodeone:3d_editor/models/halloween/candlelit"}},{"text":"[L]","color":"dark_aqua","clickEvent":{"action":"run_command","value":"/function buildmodeone:3d_editor/models/halloween/candlebig"}},{"text":"[L] ","color":"gold","clickEvent":{"action":"run_command","value":"/function buildmodeone:3d_editor/models/halloween/candlelitbig"}}]
tellraw @s ["", {"text": "    "}, {"text": "- ", "color": "gray"},{"text": "Trophy", "color": "dark_aqua", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/models/tutorial/trophy_normal"}}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]

# pages
tellraw @s ["", {"text": "<<", "color": "dark_gray", "bold": true}, {"text": "                                 "}, {"text": "Menu", "color": "gray", "bold": true, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/models/menu"}}, {"text": "                                  "}, {"text": ">>", "color": "dark_gray", "bold": true}]

# footer
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": " "}, {"text": "Position", "color": "gray", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/position_click"}}, {"text": " - ", "color": "dark_gray"}, {"text": "Models", "color": "white", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/models"}}, {"text": " - ", "color": "dark_gray"}, {"text": "Quickplace", "color": "gray", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/quickplace"}}, {"text": " - ", "color": "dark_gray"}, {"text": "Rotation", "color": "gray", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/rotation"}}, {"text": " - ", "color": "dark_gray"}, {"text": "Blocktype", "color": "gray", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/blocktype"}}, {"text": " - ", "color": "dark_gray"}, {"text": "Finish", "color": "gray", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/finish"}}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]