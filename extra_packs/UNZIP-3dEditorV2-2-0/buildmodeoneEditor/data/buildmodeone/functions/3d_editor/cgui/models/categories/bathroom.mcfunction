playsound minecraft:ui.button.click master @s ~ ~ ~ .2 1 0.2

# header
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": "                                   Chat GUI", "color": "white"}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]

# body 
tellraw @s ["", {"text": ">> ", "color": "aqua"}, {"text": "Bathroom:", "color": "white", "bold": true}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": "    "}, {"text": "- ", "color": "gray"},{"text": "Soap", "color": "dark_aqua", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/models/bathroom/soap"}}]
tellraw @s ["", {"text": "    "}, {"text": "- ", "color": "gray"},{"text": "Toilet ", "color": "dark_aqua", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/models/bathroom/toilet"}},{"text":"[Open] ","color":"gray","clickEvent":{"action":"run_command","value":"/function buildmodeone:3d_editor/models/bathroom/toilet_open"}}]
tellraw @s ["", {"text": "    "}, {"text": "- ", "color": "gray"},{"text": "Toilet Paper Holder", "color": "dark_aqua", "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/models/bathroom/toiletpaper"}}]
tellraw @s ["", {"text": "    "}, {"text": "- ", "color": "gray"}, {"text":"Sink ","color":"dark_aqua","clickEvent":{"action":"run_command","value":"/function buildmodeone:3d_editor/models/bathroom/sink"}},{"text":"[Filled] ","color":"gray","clickEvent":{"action":"run_command","value":"/function buildmodeone:3d_editor/models/bathroom/sink_filled"}},{"text":"[Running]","color":"gray","clickEvent":{"action":"run_command","value":"/function buildmodeone:3d_editor/models/bathroom/sink_running"}}]
tellraw @s ["", {"text": "    "}, {"text": "- ", "color": "gray"}, {"text":"Mirror ","color":"dark_aqua","clickEvent":{"action":"run_command","value":"/function buildmodeone:3d_editor/models/bathroom/mirror"}},{"text":"[1] ","color":"gray","clickEvent":{"action":"run_command","value":"/function buildmodeone:3d_editor/models/bathroom/mirror"}},{"text":"[2]","color":"gray","clickEvent":{"action":"run_command","value":"/function buildmodeone:3d_editor/models/bathroom/mirror_lamp"}}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]
tellraw @s ["", {"text": ""}]

# pages
tellraw @s ["", {"text": "<<", "color": "dark_gray", "bold": true}, {"text": "                                 "}, {"text": "Menu", "color": "gray", "bold": true, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/models/menu"}}, {"text": "                                  "}, {"text": ">>", "color": "dark_gray", "bold": true}]

# footer
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]
tellraw @s ["", {"text": " "}, {"text": "Position", "color": "gray", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/position_click"}}, {"text": " - ", "color": "dark_gray"}, {"text": "Models", "color": "white", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/models"}}, {"text": " - ", "color": "dark_gray"}, {"text": "Quickplace", "color": "gray", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/quickplace"}}, {"text": " - ", "color": "dark_gray"}, {"text": "Rotation", "color": "gray", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/rotation"}}, {"text": " - ", "color": "dark_gray"}, {"text": "Blocktype", "color": "gray", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/blocktype"}}, {"text": " - ", "color": "dark_gray"}, {"text": "Finish", "color": "gray", "bold": false, "clickEvent": {"action": "run_command", "value": "/function buildmodeone:3d_editor/cgui/finish"}}]
tellraw @s ["", {"text": "\u00A7m                                                                                ", "color": "dark_gray"}]