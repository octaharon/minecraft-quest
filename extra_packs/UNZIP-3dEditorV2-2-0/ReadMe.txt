  ____    _____      ______       _   _   _                      _____            _                                    _         __  ___        ___         ___   __  
 |___ \  |  __ \    |  ____|     | | (_) | |                    |  __ \          | |                                  | |       / / |__ \      |__ \       / _ \  \ \ 
   __) | | |  | |   | |__      __| |  _  | |_    ___    _ __    | |  | |   __ _  | |_    __ _   _ __     __ _    ___  | | __   | |     ) |        ) |     | | | |  | |
  |__ <  | |  | |   |  __|    / _` | | | | __|  / _ \  | '__|   | |  | |  / _` | | __|  / _` | | '_ \   / _` |  / __| | |/ /   | |    / /        / /      | | | |  | |
  ___) | | |__| |   | |____  | (_| | | | | |_  | (_) | | |      | |__| | | (_| | | |_  | (_| | | |_) | | (_| | | (__  |   <    | |   / /_   _   / /_   _  | |_| |  | |
 |____/  |_____/    |______|  \__,_| |_|  \__|  \___/  |_|      |_____/   \__,_|  \__|  \__,_| | .__/   \__,_|  \___| |_|\_\   | |  |____| (_) |____| (_)  \___/   | |
                                                                                               | |                              \_\                               /_/ 
                                                                                               |_|                                                                    
                                                                 
 ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______
/_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____/

																	𝗠𝗜𝗡𝗘𝗖𝗥𝗔𝗙𝗧 𝗩𝗘𝗥𝗦𝗜𝗢𝗡: 1.14.x - 1.15.x
 ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ 
/_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____/



𝗛𝗢𝗪 𝗧𝗢 𝗜𝗡𝗦𝗧𝗔𝗟𝗟:

	𝗗𝗔𝗧𝗔𝗣𝗔𝗖𝗞:(required)
	- Install Optifine (https://optifine.net/downloads)
	- Launch Minecraft. At the main menu, choose Singleplayer.
	- Select the world you want to install this data pack, then click on Edit button at the bottom of the screen. Choose Open World Folder. A window will pop out — open datapacks folder
	- Copy the folder "buildmodeoneEditor" from the downloaded file, and paste it into the datapack folder.
	- Back to the game, press Save to complete.

	𝗥𝗘𝗦𝗢𝗨𝗥𝗖𝗘𝗣𝗔𝗖𝗞: (required)
	- Launch Minecraft. Go to the main menu and click on options.
	- Go to Resource Pack and click on "Open Resource Pack Folder" - A window will pop out
	- Copy the folder "3D-Editor Resourcepack" from the downloaded file and paste it into the resource pack folder
	- Back to the game, enable the Resource Pack and enjoy!
	
	
	
	
	





	
 ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ ______ 
/_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____//_____/
	
	
	
𝗨𝗣𝗗𝗔𝗧𝗘 𝗟𝗢𝗚:

	𝗠𝗨𝗟𝗧𝗜𝗣𝗟𝗔𝗬𝗘𝗥 𝗨𝗣𝗗𝗔𝗧𝗘 (𝟮.𝟮.𝟬):
	- added Multiplayer compatibility
	- added new models:
		- garden chairs
		- sink
		- mirror
		- toilet
		- rotating x-Wing
	- CGUI revision
	- New standard GUI: ChatGUI
	- improved performance by reducing entity count
	- new Quickplace methods
	- more Rotation Speed options
	- improved change modelholder function
	- add Clear button to CGUI
	- customize select particles (shape and color)
	- extended Tutorial
	- added Lore to Items
	- FIXES:
		- added empty basket to CGUI
		- fixed Quickplace

	𝗖𝗵𝗮𝘁-𝗚𝗨𝗜 (𝟮.𝟭.𝟬):
	-completed Chat-GUI
	-added menu item for Chat-GUI
	-new model format for better compatibility between BuildmodeOne datapacks
	-bugfix: missing optimize count in model book

	𝗔𝗗𝗗𝗢𝗡𝗦 𝗨𝗣𝗗𝗔𝗧𝗘 (𝟮.𝟬.𝟮):
	-fixed chair functions
	-improved chair functions

	𝗔𝗗𝗗𝗢𝗡𝗦 𝗨𝗣𝗗𝗔𝗧𝗘 (𝟮.𝟬.𝟭):
	-fixed 2.x converter
	-improved performance 
	-better compatibility with other data packs 

	𝗔𝗗𝗗𝗢𝗡𝗦 𝗨𝗗𝗗𝗔𝗧𝗘 (𝟮.𝟬):
	-complete structure revision
	-> better compatibility with other data packs
	-added settings menu
	-added LiteMode Addon (saves performance)
	-added ChatGUI (WIP)
	-added 2.x Converter
	-added Auto-Converter
	-added Tutorial
	-changed update and reload messages
	-added exact values on hover over rotation buttons
	-added rubiks cube
	-added "barrier on place" option
	-added "3D-Items always in Inv" option
	-restructured model book
	-fixed chairs
	-more bug fixes...

	𝗢𝗩𝗘𝗥𝗛𝗔𝗨𝗟 𝗨𝗣𝗗𝗔𝗧𝗘(𝟭.𝟳.𝟮):
	-fixed setup function

	𝗢𝗩𝗘𝗥𝗛𝗔𝗨𝗟 𝗨𝗣𝗗𝗔𝗧𝗘(𝟭.𝟳.𝟭):
	-complete structure revision
	-reworked bottle model
	-added new bottle variant
	-fixed bonsai model pot
	-changed bonsai texture
	-completed carpet section in block type menu
	-added hover texts to carpet section in block type menu
	-improved optimize function
	-fixed 3D Editor book structure
	-fixed quickplace function
	-added version button in Editor book
	
	𝗢𝗩𝗘𝗥𝗛𝗔𝗨𝗟 𝗨𝗣𝗗𝗔𝗧𝗘(𝟭.𝟳):
	-added soap dispenser
	-added toilet paper holder
	-added new plant
	-retextured flower
	-reworked bottle model
	-added new bottle variant
	-added acacia and jungle variants for paintings
	-added update info
	-fixed version function
	-fixed unistall function
	-fixed office chair horse bug
	-fixed invisible light bug
	-removed secret model

	𝗖𝗛𝗥𝗜𝗦𝗧𝗠𝗔𝗦 𝗨𝗣𝗗𝗔𝗧𝗘(𝟭.𝟲):
	-"Sync w/ Player": Syncs the rotation of selected model with player rotation
	-rearrangement of model list
	-added working office chair
	-new lighting section
	-added invisible light source
	-added lighting bar with 4 variations
	-added Mini Christmas Tree
	-added 4 different presents
	-added 6 different ornaments
	-added sleigh
	-added teddy bear
	-visual rework of Editor Book
	-bug fixes

	𝗦𝗡𝗔𝗣𝗦𝗛𝗢𝗧 𝘀𝟰𝟱𝟭𝟵:
	-Added Desk Lamp
	-Armorstands are 'markes' now!
	-Bug fixes

	𝗛𝗔𝗟𝗟𝗢𝗪𝗘𝗘𝗡 𝗨𝗣𝗗𝗔𝗧𝗘(𝟭.𝟰):
	-integrated Quickplace into Model editor Book
	-added every slab into block types
	-added X-Wing from Star Wars
	-integrated basket model into Book
	-added 6 different candle models
	-added Version command (/function misc:version)
	-added "Suggest Model" Feature

	𝗦𝗡𝗔𝗣𝗦𝗛𝗢𝗧 𝘀𝟰𝟭𝟭𝟵:
	-added quick placement for dishes (/function quickplace:dishes)
	-changed blocktype change behaviour
	-added slabs as blocktype (/function blocktypes:slabs/oak)
	-added rotating and non-rotating companion cube from Portal
	-improved "change modelholder" function
	-added empty basket model (/function modelitems:basket/empty)
	-minor bug fixes

	𝗣𝗔𝗜𝗡𝗧𝗜𝗡𝗚𝗦 𝗨𝗣𝗗𝗔𝗧𝗘(𝗩𝟭.𝟮):
	-added every kind of carpet to the block type list
	-added quick access to different model pages
	-added new painting model with 16 variants
	-added Infinity Gauntlet with rotation base
	-added rotation speed option
	-fixed some bugs
