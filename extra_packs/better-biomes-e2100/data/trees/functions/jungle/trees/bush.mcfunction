setblock ~ ~ ~ jungle_wood
fill ~1 ~ ~2 ~-1 ~ ~-2 oak_leaves[persistent=true] keep
fill ~2 ~ ~1 ~-2 ~ ~-1 oak_leaves[persistent=true] keep
fill ~1 ~1 ~1 ~-1 ~1 ~-1 oak_leaves[persistent=true] keep
fill ~1 ~ ~2 ~-1 ~ ~-2 jungle_leaves[persistent=true] replace grass
fill ~2 ~ ~1 ~-2 ~ ~-1 jungle_leaves[persistent=true] replace grass
fill ~1 ~1 ~1 ~-1 ~1 ~-1 jungle_leaves[persistent=true] replace grass