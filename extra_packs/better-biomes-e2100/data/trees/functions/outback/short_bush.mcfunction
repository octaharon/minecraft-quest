fill ~-3 ~ ~-3 ~3 ~15 ~3 air replace minecraft:oak_leaves[persistent=false]
fill ~ ~ ~ ~ ~15 ~ air replace oak_log
setblock ~ ~ ~ acacia_log
setblock ~ ~1 ~ minecraft:acacia_leaves[persistent=true]
setblock ~1 ~ ~ minecraft:acacia_leaves[persistent=true]
setblock ~-1 ~ ~ minecraft:acacia_leaves[persistent=true]
setblock ~ ~ ~1 minecraft:acacia_leaves[persistent=true]
setblock ~ ~ ~-1 minecraft:acacia_leaves[persistent=true]