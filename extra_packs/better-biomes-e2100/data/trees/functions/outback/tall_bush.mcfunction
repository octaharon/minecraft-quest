fill ~-7 ~4 ~-7 ~7 ~10 ~7 air replace minecraft:acacia_leaves[persistent=false]
fill ~ ~ ~ ~ ~5 ~ air replace acacia_log
fill ~-3 ~5 ~-3 ~3 ~12 ~3 air replace acacia_log

fill ~ ~ ~ ~ ~1 ~ acacia_log
setblock ~ ~2 ~ minecraft:acacia_leaves[persistent=true]
setblock ~1 ~1 ~ minecraft:acacia_leaves[persistent=true]
setblock ~-1 ~1 ~ minecraft:acacia_leaves[persistent=true]
setblock ~ ~1 ~1 minecraft:acacia_leaves[persistent=true]
setblock ~ ~1 ~-1 minecraft:acacia_leaves[persistent=true]