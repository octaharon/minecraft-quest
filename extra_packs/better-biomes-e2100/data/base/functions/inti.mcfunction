function base:info
scoreboard objectives add p dummy
scoreboard objectives add number dummy
scoreboard players set 100 number 100
scoreboard players set 99 number 99
scoreboard players set 97 number 97
scoreboard players set 96 number 96
scoreboard players set 95 number 95
scoreboard players set 90 number 90
scoreboard players set 80 number 80
scoreboard players set 70 number 70
scoreboard players set 60 number 60
scoreboard players set 50 number 50
scoreboard players set 30 number 30
scoreboard players set 20 number 20
scoreboard players set 10 number 10
scoreboard players set 5 number 5
scoreboard players set 3 number 3
gamerule maxCommandChainLength 3000000
function base:player
