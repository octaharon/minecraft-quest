// Usage: node legacyTextToJson.js <text>
const string=process.argv[2];
const result=[''];
const delimiter=string.indexOf('§')>-1?'§':'&';
const pieces=string.split(delimiter).filter(Boolean);
let buffer={
    italic:false
};

const mapper={
    '0':{"color":"black"},
    '1':{"color":"dark_blue"},
    '2':{"color":"dark_green"},
    '3':{"color":"dark_aqua"},
    '4':{"color":"dark_red"},
    '5':{"color":"dark_purple"},
    '6':{"color":"gold"},
    '7':{"color":"gray"},
    '8':{"color":"dark_gray"},
    '9':{"color":"blue"},
    'a':{"color":"green"},
    'b':{"color":"aqua"},
    'c':{"color":"red"},
    'd':{"color":"light_purple"},
    'e':{"color":"yellow"},
    'f':{"color":"white"},
    'k':{"obfuscated":true},
    'l':{"bold":true},
    'm':{"strikethrough":true},
    'n':{"underline":true},
    'o':{"italic":true},
    'r':{"color":"reset"},
};

pieces.forEach(v=>{
    switch (true){
        case v.length===1:
            if (mapper[v]){
                Object.assign(buffer,mapper[v]);
            }
        break;
        default:
            if (Object.keys(buffer).includes("text"))
            {
                result.push(buffer);
                buffer={italic:false};
            }
            const sym=v.substr(0,1);
            if (mapper[sym]){
                Object.assign(buffer,mapper[sym]);
            }
            else
            buffer.text=(buffer.text || "") + "&"+sym;
            buffer.text=(buffer.text || "")+v.substr(1);
    }
});
if (Object.keys(buffer).includes("text"))
            result.push(buffer);
console.log(JSON.stringify(result));