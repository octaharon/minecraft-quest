/**
 *  Usage: node generateChestSpawn.js <source.csv> <target.mcfunction> <data_prefix> <tag_prefix>
 * 
 *   
 *  CSV Format:
 *  x,y,z,id,func,type
 *  
 *  
 */
let source = String(process.argv[2]).trim();
const target = String(process.argv[3]).trim();
const dataPrefix = String(process.argv[4]).trim();
let prefix = String(process.argv[5]).trim();
if (prefix) prefix += '_';

const fs = require('fs');
const path = require('path');
const csv = require('fast-csv')
const dirname = path.resolve(path.dirname(target)).split(path.sep).join('/');
source = path.resolve(source);

console.log({source,target,dataPrefix,prefix,dirname});

if (!fs.lstatSync(dirname).isDirectory()) throw new Error("target_dir is not a valid dirname");
if (!fs.lstatSync(source).isFile()) throw new Error("source.csv is not a valid file");
if (!dirname.includes(dataPrefix.split(':')[0]) || !dirname.includes(dataPrefix.split(':')[1])) throw new Error("Datapack prefix doesn't match the file scheme");
const tps = 20;
const cmd = [];
let ix = 0;
fs.createReadStream(source)
    .pipe(csv.parse({
        headers: true
    }))
    .on('error', error => {
        throw new Error("CSV read error: " + error);
    })
    .on('data', (row) => {
        ix++;
        const [x, y, z, id, func, type] = Object.values(row);
        const tag = `${prefix}${id}`;
        const base = `execute positioned ${x} ${y} ${z} as @p[distance=..5,tag=!${tag}] run`;
        const commands = [];
        commands.push(`#define tag ${tag}`);
        commands.push(`function utils:wipe_chest`)
        commands.push(`function ${func}/${type}`);
        commands.push(`tag @s add ${tag}`);
        fs.writeFileSync(
            path.join(dirname, `${tag}.mcfunction`),
            commands
            .map(t => t.replace(/[\u0080-\uFFFF]/g, (m) => "\\u" + ("0000" + m.charCodeAt(0).toString(16)).slice(-4)))
            .join("\n"));
        const _cmd = `${base} function ${dataPrefix}/${tag}`;
        console.log(_cmd);
        cmd.push(_cmd);
    })
    .on('end', (rowCount) => {
        console.log(`Parsed ${rowCount} rows`);
        fs.writeFileSync(
            target,
            cmd
            .map(t => t.replace(/[\u0080-\uFFFF]/g, (m) => "\\u" + ("0000" + m.charCodeAt(0).toString(16)).slice(-4)))
            .join("\n")
        );

    });