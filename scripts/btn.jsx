const TYPES={Planets='Planets',Characters='Characters'};

const Types=[{
    id:TYPE.Planets,
    caption:"Planets"
},
{
    id:TYPE.Characters,
    caption:"Characters"
}]

const handler=(type)=>{
    switch(type)
    {
        case TYPES.Characters:
            return ()=>({});
        case TYPES.Planets:
            return ()=>({});
    }
}

const Button=(props)=>{
    return <button onClick={handler(props.type)}>
        {(Types.find((v)=>v.type===props.type)||{}).caption}
    </button>
}

const Menu=()=>{
    return Types.map(type=>
        <li>
            <Button {...type}/>
        </li>
        );
}

const App=()=>{
    ReactDOM.render(<Menu/>,document.getElementById('root'));
}
