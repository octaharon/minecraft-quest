/**
 *  Usage: node generateFreeItems.js <source.csv> <target_dir> <datapack_prefix>
 *  
 *  CSV Format:
 *  min_reputation=0;count=1;item tag;run command;base_cooldown;group;caption;tooltip(item/JSON text)
 *  
 *  cooldown is calculated as (Base Cooldown - Reputation / 2 - <Hero Of The Village Level> *2) Seconds, to a minimum of 5 seconds
 */
let file = String(process.argv[2]).trim();
const dir = String(process.argv[3]).trim();
const prefix = String(process.argv[4]).trim();

String.prototype.shuffle = function () {
    var a = this.split(""),
        n = a.length;

    for (var i = n - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
    }
    return a.join("");
}

if (!(!!file && !!dir && prefix.includes(':'))) {
    console.log("\n\nUsage:\nnode generateFreeItems.js <source.csv> <target_dir> <datapack_prefix>\n\n");
    process.exit(-1);
}

const giveTemplate = ({
    id,
    predicate,
    item,
    count,
    func,
    cd,
    caption,
    tooltip,
    peekScore = 'freeItemPeek',
    cooldown = 'freeItemCooldown',
    seconds = 'freeItemSecs',
    trigger
}) => {
    const cmd = [`execute if score @s trgFreeItem matches ${id} unless score @s reputation matches ${predicate}.. run function ${prefix}/deny`];
    const cmdBase = `execute as @s[scores={trgFreeItem=${id},reputation=${predicate}..}] at @s run `;
    if (func)
        cmd.push(cmdBase + func);
    if (item && count > 0)
        cmd.push(cmdBase + `give @s ${item} ${count}`);
    cmd.push(`scoreboard players set @s[scores={trgFreeItem=${id},reputation=${predicate}..}] ${cooldown} ${cd * tps}`);
    return cmd;
};

const giveHeader = [
    `execute if score @s freeItemTicks matches 1.. run function ${prefix}/deny`
];

const giveFooter = [
    "playsound entity.player.levelup player @s ~ ~ ~ 0.9 0.6",
    "scoreboard players set @s trgFreeItem 0",
    `function ${prefix}/calc_cooldown`,
    `function ${prefix}/set_cooldown`,
    `function ${prefix}/start_timer`
];

const delimiter = {
    "text": "\u2588\u2588\u2588\u2588\u2588\u2588\u2593\u2593\u2593\u2593\u2592\u2592\u2592\u2592\u2591\u2591\u2591\u2591\u2592\u2592\u2592\u2592\u2593\u2593\u2593\u2593\u2588\u2588\u2588\u2588\u2588\u2588",
    "color": "dark_aqua"
};

const menuHeader = [{
    "text": "\n\n\n\n\n\n"
}, {
    "text": "\u2581\u2582\u2583\u2584\u2585\u2586\u2587\u2588\u2593\u2592\u2591",
    "color": "dark_aqua"
}, {
    "text": "Make a choice",
    "bold": true,
    "color": "white"
}, {
    "text": "\u2591\u2592\u2593\u2588\u2587\u2586\u2585\u2584\u2583\u2582\u2581",
    "color": "dark_aqua"
}];

const menuFooter = [delimiter];

const menuTemplate = [
    [
        `function ${prefix}/reset`,
        `function ${prefix}/calc_cooldown`,
        `function ${prefix}/click`,
        `tellraw @s ${JSON.stringify(menuHeader)}`,
    ],
    [
        `tellraw @s ${JSON.stringify(menuFooter)}`
    ]
];

const triggerTemplate = ({
    groupDatapath,
    timer = 'freeItemTicks'
}) => [
    `execute if score @s ${timer} matches 1.. at @s run function ${prefix}/deny`,
    `execute unless score @s ${timer} matches 1.. at @s run function ${groupDatapath}/menu`,
]

const jsonTextToString = (componentArray) => {
    if (!componentArray.forEach)
        return `'${JSON.stringify(componentArray)}'`;
    return `'[${componentArray.map(JSON.stringify).join(',')}]'`
}
const applyTooltip = ({
    tooltip = '',
    desaturate = false,
    element,
    display,
    prependLines = []
}) => {

    if (desaturate) {
        tooltip = tooltip
            .replace(/color":"[^"]+"/gim, 'color":"gray"')
            .replace(/color\\":\\"[^\\"]+\\"/gim, 'color\":\"gray\"');
    }
    const hoverEvent = {
        value: tooltip
    };
    if (/^\{.*id:.*\}$/i.test(tooltip)) // an item tooltip
    {
        hoverEvent.action = "show_item";
        try {
            if (!tooltip.includes('tag:{')) // no item tag
            {
                
                hoverEvent.value = tooltip.replace('}',
                    `,tag:{${display}}}`);
                    //console.log("===\n",tooltip,"\n\n",display,"\n\n",hoverEvent.value);
            } else // item has tag
            {
                const itemTag = tooltip.match(/(tag:(\{.*\}).+)/);
                let displayTag = itemTag[2].match(/(display:(\{.*\})[,}])/);
                if (!displayTag) // no display tag
                {
                    hoverEvent.value = tooltip.replace(itemTag[2], itemTag[2].replace('{', `{${display},`))
                    //console.log(itemTag,displayTag,value);
                } else // display tag present
                {
                    let newDisplayTag = displayTag[2];
                    /*if (!newDisplayTag.includes('Name')) //no Name in display (weird)
                        newDisplayTag = newDisplayTag.replace('{', `{Name:'${name}',`);*/
                    if (!newDisplayTag.includes('Lore')) //no Lore in display
                    {
                        newDisplayTag = newDisplayTag.replace('{', `{Lore:[${
                        prependLines.map(jsonTextToString).join(',')
                        }],`);
                    } else { //prepend to existing Lore and add an empty string
                        newDisplayTag = newDisplayTag.replace('Lore:[', `Lore:[${
                        [...prependLines,{"text":"---","color":"dark_gray"}].map(jsonTextToString).join(',')
                        },`)
                    }
                    newDisplayTag = newDisplayTag.replace(',]', ']').replace(',}', '}');
                    hoverEvent.value = tooltip.replace(itemTag[2], itemTag[2].replace(displayTag[2], newDisplayTag))
                }
            }
        } catch (e) {
            console.trace(e);
            console.error(tooltip, "\n");
        }
    } else // a json text tooltip
    {
        prependLines = prependLines.map(line => {
            if (!line.forEach) line = [line];
            return [...line, {
                "text": "\n"
            }]
        }).flat();
        hoverEvent.action = "show_text";
        let newTooltip=tooltip;
        if (tooltip) {
            try{
            tooltip = tooltip.trim();
            if (tooltip[0] !== '[')
                tooltip = '[' + tooltip + ']';
            newTooltip = JSON.parse(tooltip);
            }
            catch(e){
                console.error(e,"\n\n",tooltip);
            }
        } else {
            prependLines.pop();
            newTooltip = [];
        }

        hoverEvent.value = [...prependLines, ...newTooltip];
    }
    Object.assign(element, {
        hoverEvent
    });
    return element;
}

const offerTemplate = ({
    id,
    caption,
    cd,
    tooltip,
    predicate,
    trigger,
    item,
    func,
    seconds = 'freeItemSecs',
    cooldown = 'freeItemTicks'
}) => {
    const button = [{
        "text": "",
        "clickEvent": {
            "action": "run_command",
            "value": trigger
        },
        "extra": [{
                "text": "\u1455\u1368\u25ba ",
                "bold": true,
                "color": "gold"
            },
            {
                "text": caption,
                "underlined": true,
                "color": "yellow"
            },
            {
                "text": " \u27f2 ",
                "bold": true,
                "color": "gold"
            },
            {
                "score": {
                    "name": "@s",
                    "objective": seconds
                },
                "color": "aqua",
                "bold": false
            },
            {
                "text": "s",
                "bold": true,
                "color": "dark_aqua"
            },
            {
                "text": " \u25c4\u1368\u1450",
                "bold": true,
                "color": "gold"
            }
        ]
    }];
    const lore = [{
            "text": "Click to get this bonus",
            "color": "white",
            "italic": true
        },
        [{
            "text": "Base cooldown: ",
            "italic": false,
            "color": "gray"
        }, {
            "text": `${cd}`,
            "color": "aqua",
            "italic": false
        }, {
            "text": " seconds",
            "color": "dark_aqua",
            "italic": false
        }]
    ];
    const name = `{"text":"${caption}","color":"yellow","italic":"false","bold":false}`;
    const display = `display:{Lore:[${
            lore.map(jsonTextToString).join(',')
            }]}`
    button[0] = applyTooltip({
        element: button[0],
        tooltip,
        display,
        prependLines: lore
    });
    return button;
};

const blockedTemplate = ({
    id,
    caption,
    tooltip = '',
    predicate,
    obfuscated = false
}) => {
    const button = [{
        "color": "gray",
        "text": "",
        "extra": [{
                "text": "\u1455\u1368\u25ba ",
                "bold": true
            },
            {
                "text": `${obfuscated?caption.shuffle().trim():caption} `,
                "bold": false,
                "obfuscated": !!obfuscated
            },
            {
                "text": " \u26a0 ",
                "color": "dark_red",
                bold: false
            },
            {
                "text": String(predicate),
                "color": "green"
            }, {
                "text": "\u2698 ",
                "bold": true,
                "color": "dark_green"
            },
            {
                "text": "\u25c4\u1368\u1450",
                "bold": true,
                "color": "gray"
            }
        ]
    }];
    if (obfuscated)
        tooltip = `{id:enchanted_book,Count:1b,tag:{display:{Name:'{"text":"Unknown","color":"gray","bold":"false","italic":false}'}}}`;
    const lore = [
        [{
            "text": "\u26a0 ",
            "color": "dark_red",
            "italic": false
        }, {
            "text": "Requires ",
            "color": "white",
            "italic": false
        }, {
            "text": `${predicate} `,
            "color": "green",
            "italic": false
        }, {
            "italic": false,
            "text": "⚘",
            "color": "dark_green",
            "bold": true
        }, {
            "italic": false,
            "text": "Reputation",
            "color": "dark_green"
        }],
    ];
    const display = `display:{Name:'{"text":${caption},"color":"gray"}',Lore:[${
            lore.map(jsonTextToString).join(',')
            }]}`
    button[0] = applyTooltip({
        element: button[0],
        tooltip,
        desaturate: !obfuscated,
        display,
        prependLines: lore
    });
    return button;
};

const itemMenuLine = ({
    id,
    predicate,
    item,
    func,
    cd,
    caption,
    tooltip,
    peekScore = 'freeItemPeek',
    cooldown = 'freeItemCooldown',
    seconds = 'freeItemSecs',
    trigger
}) => [
    `scoreboard players set @s ${cooldown} ${cd * tps}`,
    `function ${prefix}/set_cooldown`,
    `tellraw @s[scores={reputation=${predicate}..}] ${JSON.stringify(offerTemplate({
            id,
            caption,
            tooltip,
            item,
            func,
            predicate,
            trigger,
            cd,
            seconds
        }))}`,
    `execute unless score @s reputation matches ${predicate}.. unless score @s ${peekScore} matches 1.. run tellraw @s ${JSON.stringify(blockedTemplate({
            id,
            caption,
            tooltip,
            predicate,
            trigger,
            cd,
            seconds,
            obfuscated: false
        }))}`,
    `execute unless score @s reputation matches ${predicate}.. if score @s ${peekScore} matches 1.. run tellraw @s ${JSON.stringify(blockedTemplate({
            id,
            caption,
            tooltip,
            predicate,
            trigger,
            cd,
            seconds,
            obfuscated: true
        }))}`,
    `execute unless score @s reputation matches ${predicate}.. unless score @s ${peekScore} matches 1.. run scoreboard players set @s ${peekScore} 1`
]

const groupPrefix = 'group_';

const fs = require('fs');
const path = require('path');
const csv = require('fast-csv')
const dirname = path.resolve(dir);
file = path.resolve(file)
if (!fs.lstatSync(dirname).isDirectory()) throw new Error("target_dir is not a valid dirname");
if (!fs.lstatSync(file).isFile()) throw new Error("source.csv is not a valid file");
if (!dirname.includes(prefix.split(':')[0]) || !dirname.includes(prefix.split(':')[1])) throw new Error("Datapack prefix doesn't match the file scheme");
const tps = 20;
const itemGroups = {};
let giveCommands = giveHeader.slice(0);
let ix = 0;
fs.createReadStream(file)
    .pipe(csv.parse({
        headers: true
    }))
    .on('error', error => {
        throw new Error("CSV read error: " + error);
    })
    .on('data', (row) => {
        ix++;
        const group = (row.group || '').trim();
        const itemIndex = Math.round(ix * 10000 + Math.random() * 9999);
        if (!group) {
            console.log(`empty group at row ${ix}, skipping...`);
            return;
        }
        if (!itemGroups[group])
            itemGroups[group] = {};
        const item = (Object.values(row)[2] || '').trim();
        const func = (Object.values(row)[3] || '').trim();
        const count = parseInt(Object.values(row)[1]) || 1;
        if (!item && !func) {
            console.log(`empty item/function at row ${ix}, skipping...`);
            return;
        }
        let tooltip=(Object.values(row)[7] || '').trim();
        if (item && count > 0 && !tooltip) {
            const itemData = item.split('{');
            if (itemData[1])
                tooltip = `{Count:${count}b,id:${itemData[0]},tag:{${itemData[1].replace('}', '')}}}`;
            else
                tooltip = `{Count:${count}b,id:${itemData[0]}}`;
        }
        const entry = {
            id: itemIndex,
            predicate: parseInt(Object.values(row)[0]) || 0,
            count,
            item,
            func,
            cd: parseFloat((Object.values(row)[4] || '').trim()) || 5,
            caption: (Object.values(row)[6] || '').trim(),
            tooltip
        };
        itemGroups[group][itemIndex] = entry;
        giveCommands = giveCommands.concat(giveTemplate(entry));
    })
    .on('end', (rowCount) => {
        console.log(`Parsed ${rowCount} rows`);
        giveCommands = giveCommands.concat(giveFooter);
        fs.writeFileSync(path.join(dirname, "give.mcfunction"), giveCommands.join("\n"));
        Object.keys(itemGroups).forEach(itemGroup => {
            itemGroups[itemGroup] = Object.values(itemGroups[itemGroup]);
            itemGroups[itemGroup].sort((a, b) => a.rep - b.rep);
            const groupPath = `${groupPrefix}${itemGroup}`;
            const groupDir = path.join(dirname, groupPath);
            const groupDatapath = `${prefix}/${groupPath}`;
            if (fs.existsSync(groupDir))
                fs.rmdirSync(groupDir, {
                    recursive: true
                });
            fs.mkdirSync(groupDir);
            let menu = menuTemplate[0].slice();
            itemGroups[itemGroup].forEach(item => {
                const triggerFilename = `trigger_${item.id}`;
                menu = menu.concat(itemMenuLine({
                    ...item,
                    trigger: `/trigger trgFreeItem set ${item.id}`
                }));
            });
            const menuCode = menu.concat(menuTemplate[1])
                .map(t => t.replace(/[\u0080-\uFFFF]/g, (m) => "\\u" + ("0000" + m.charCodeAt(0).toString(16)).slice(-4)))
                .join("\n");
            fs.writeFileSync(
                path.join(groupDir, 'menu.mcfunction'),
                menuCode
            );
            fs.writeFileSync(
                path.join(groupDir, 'trigger.mcfunction'),
                triggerTemplate({
                    groupDatapath
                })
                .map(t => t.replace(/[\u0080-\uFFFF]/g, (m) => "\\u" + ("0000" + m.charCodeAt(0).toString(16)).slice(-4)))
                .join("\n")
            );

        })
    });