- [Synopsis](#synopsis)
- [Global Quest](#global-quest)
  - [`Plot A`: Locking down the Mothership](#plot-a-locking-down-the-mothership)
    - [Quest start](#quest-start)
    - [Breach Codes](#breach-codes)
    - [Codes Location](#codes-location)
    - [Mothership Portal Key](#mothership-portal-key)
  - [`Plot B`:  Gaining Reputation](#plot-b-gaining-reputation)
    - [Quest start](#quest-start-1)
    - [Gaining Reputation](#gaining-reputation)
  - [`Plot C`: Activating Space Beacon](#plot-c-activating-space-beacon)
    - [Quest start](#quest-start-2)
    - [Source Items](#source-items)
- [Monsters](#monsters)
  - [Champion Mobs](#champion-mobs)
  - [Boss Mobs](#boss-mobs)
- [Locations](#locations)
  - [Location types](#location-types)
  - [Villages](#villages)
  - [Quest Locations](#quest-locations)
  - [Bonus Locations](#bonus-locations)
- [Items](#items)
  - [Currency](#currency)
  - [Expendable Items](#expendable-items)
  - [Unique Loot](#unique-loot)
  - [Quest Items](#quest-items)
- [Hermit's House](#hermits-house)
  - [Unlockable Recipes](#unlockable-recipes)
- [Mazecapade](#mazecapade)
  - [Unlockable Recipes](#unlockable-recipes-1)
  - [Related Quests](#related-quests)
- [Emporium](#emporium)
  - [Emporium Special quest](#emporium-special-quest)
  - [Offering Vendor](#offering-vendor)
  - [Quartermaster](#quartermaster)
  - [Oracle](#oracle)
- [University](#university)
  - [Unlockable recipes](#unlockable-recipes-2)
  - [Unique boss: `Krolly`, an angry rabbit](#unique-boss-krolly-an-angry-rabbit)
  - [Main Quest Start (Plots A and C)](#main-quest-start-plots-a-and-c)
  - [Thunder Shrine](#thunder-shrine)
    - [Offering](#offering)
- [Exampleton](#exampleton)
  - [Exampleton Reserved quest](#exampleton-reserved-quest)
  - [Unlockable recipes](#unlockable-recipes-3)
- [Glory Hall](#glory-hall)
  - [Objectives](#objectives)
  - [Unlockable recipes](#unlockable-recipes-4)
  - [The Impetuous Quest](#the-impetuous-quest)
    - [Objective](#objective)
    - [Quest Items](#quest-items-1)
- [Hanging Hills](#hanging-hills)
  - [Quests](#quests)
  - [Bricks from the End Quest](#bricks-from-the-end-quest)
  - [Gateway Blueprint Quest](#gateway-blueprint-quest)
    - [Quest Items](#quest-items-2)
    - [Returning Blueprint](#returning-blueprint)
- [The Outpost](#the-outpost)
- [Sprucewood Creek](#sprucewood-creek)
  - [Objectives](#objectives-1)
  - [Wolves control](#wolves-control)
    - [Make wolves aggresive](#make-wolves-aggresive)
    - [Reward](#reward)
  - [Reputation Check](#reputation-check)
    - [Reward](#reward-1)
- [Sandy Haven](#sandy-haven)
  - [Objectives](#objectives-2)
  - [Radioactive Dust Quest](#radioactive-dust-quest)
    - [Quest Item](#quest-item)
    - [Radiation Zone](#radiation-zone)
    - [Dust turn-in](#dust-turn-in)
  - [Deciphering quest](#deciphering-quest)
    - [Items](#items-1)
    - [Locking the chest](#locking-the-chest)
    - [Codeblocks (in order)](#codeblocks-in-order)
    - [Returning the artifact](#returning-the-artifact)
- [Snake Shrine](#snake-shrine)
  - [Objectives](#objectives-3)
  - [Snake Offering Quest](#snake-offering-quest)
    - [Quest Items](#quest-items-3)
    - [Making an offering](#making-an-offering)
  - [Need More Cats Quest](#need-more-cats-quest)
    - [Finishing the quest](#finishing-the-quest)
- [Gleamsville](#gleamsville)
  - [Objectives](#objectives-4)
  - [Gleamsville Treasures](#gleamsville-treasures)
  - [Items turn-in and reward](#items-turn-in-and-reward)
- [Trade](#trade)
- [Misc](#misc)


# Synopsis

>Work in progress

This is a design document for a minecraft (1.14) survival mode quest. The world copy is not included in the repository at the moment.


# Global Quest
```
scoreboard objectives add reputation dummy {"text":"Reputation","color":"green"}
scoreboard objectives add secretsFound dummy "Secrets Found"
```

Player is required to defend the slowly ongoing Alien Invasion
* `Plot A`: Gain Access to **Alien Mothership**, find 5 `Breach Codes` to lock it down.
* `Plot B`: Gain 30 Reputation to gain access to the **High Court** and rally the folk against Aliens
* `Plot C`: Craft a `Transmission Modem` and use **Space Legion Beacon** to send a signal

## `Plot A`: Locking down the Mothership
```scoreboard objectives add main_quest_a dummy "Plot Quest A"```

### Quest start
```function quests:plot_a/init```
> Quest is started by reading the book (`crafting:books/university/alien_parlance`) at the University. Player is let know that `Breach Codes`, `Communication Protocol` and `Transmission Modem` do exist and proposed to seek for them at certain locations.

### Breach Codes
> Breach Codes are specific objects which are guaranteed to appear in quest locations for each player with quest. Having at least 5 of them while accessing the Mothership Computer locks it down

* Collect 5 `Breach Codes`
* Find *Mothership Portal Key*
* Enter Codes into the Mothership Navigation Computer

### Codes Location
* Military Base
* Deep Lab
* Alien Transport
* Investigation Bureau
* Destroyer
* Dropping Point

### Mothership Portal Key
Manufactured  at **the Emporium** from `Broken Portal Key`, which is dropped by certain mobs at **Dropping Point** and **Alien Transport** if player has the quest, and `Communication Protocol`, which is obtained during `Plot C`

## `Plot B`:  Gaining Reputation
```scoreboard objectives add main_quest_b dummy "Plot Quest B"```

### Quest start
> Quest is started by talking to an NPC at **The Emporium** (TODO)

### Gaining Reputation
>Reputation stacks and can't be diminished. It's obtained from various sources:

* Completing quests in Villages
* Getting *Reputation Tokens* from rare mobs and bonus chests at most locations, which can be turned in for *Reputation* at **The Emporium**
* One-time boost of 5 Reputation is provided for completing `Plot C`

## `Plot C`: Activating Space Beacon 
```scoreboard objectives add main_quest_c dummy "Plot Quest C"```

### Quest start
```function quests:plot_a/init```

> Quest is started by reading the book (`crafting:books/university/alien_parlance`) at the University. Player is let know that `Breach Codes`, `Communication Protocol` and `Transmission Modem` do exist and proposed to seek for them at certain locations.

> Player activates **The Beacon** by approaching it's center having a *Transmission Modem* item in hand. The item is built from 4 different items at **Exampleton**

### Source Items

* `Communication Protocol` - is comprised of two parts, which are located at **Research Center** and **Investigation Bureau**
* `Decoherence Inhibitor` - found at **Ascendancy Hall**
* `Field Capacitor` - found at the **The Order**
* `Ionic Solvent` - found at **The Refinery**


# Monsters

## Champion Mobs
Name | Spawn Script | Drops | Locations
--- | ---- | --- | ---
Radiance | `mobs:summon/radiance` | TNT, Diamond | **Devastation Zone**, <br/>**Beacon**,<br/>**Dropping Point**
Wizard | `mobs:summon/wizard` | `mobs:basic_drop` loot table | **University**, <br/>**Glory Hall**,<br/>**Research Center**, <br/> **The Mothership**, <br/>**Fort Brexos**
Necromancer | `mobs:summon/necromancer` | `mobs:rare_drop` loot table | **Graveyard**, <br/>**University**, <br/>**Glory Hall**, <br/> **Dropping Point**
Undead Warrior | `mobs:summon/undead_warrior` | `mobs:basic_drop` loot table | **Graveyard**, <br/>**Zombie Village**, <br/>**Jagdhof**,<br/>**Fort Brexos**
Undead Archer | `mobs:summon/undead_archer` | `mobs:basic_drop` loot table | **Graveyard**, <br/>**Jagdhof**,<br/>**Fort Brexos**, <br/>**Ziggurat**
Alchemist | `mobs:summon/alchemist` | `mobs:basic_drop` loot table | **Glory Hall**, <br/>**Jagdhof**,<br/>**Ziggurat**, <br/>**University**
Alien Raider | `mobs:summon/alien_raider` | `mobs:basic_drop` loot table | **Mazecapade**, <br/>**The Refinery**,<br/>**Dropping Point**, <br/>**The Mothership**,<br/>**The Beacon**,</br>**Military Base**

## Boss Mobs
Name | Spawn Script | Drops | Locations
--- | ---- | --- | ---
Zombie Terror | `mobs:summon/boss/zombie_terror` | Loot Table: `chests:q_reward`  | **Zombie Village**
Chief Lorenzo | `mobs:summon/boss/chef_lorenzo` | Loot Table: `mobs:rare_drop` | **Research Center**
Xerus | Phase 1: `mobs:summon/boss/xerus_deceiver`<br/>Phase 2: `mobs:summon/boss/xerus_undying` | Loot Table: `chests:xerus`  | **Graveyard**

# Locations

## Location types

| Location type  | Sound                  | Color  | Function                            |
| -------------- | ---------------------- | ------ | ----------------------------------- |
| [Village](#villages)        | block.end_portal.spawn | green  | `locations:discover/village/<name>` |
| [Quest (Hard)](#quest-locations)   | entity.wither.spawn    | red    | `locations:discover/quest/<name>`   |
| [Quest (Normal)](#quest-locations) | block.end_portal.spawn | yellow | `locations:discover/quest/<name>`   |
| Bonus          | ui.toast.challenge_complete  | gold   | `locations:discover/bonus/<name>`   |

## Villages

> Villages provide side quests with a set reward and a **Reputation** bonus most of times. Every village has a quest wich provides a *Portal Key* to that village. The list is provided in the order at which they are discovered by the player if he follows the Story Quest.

Name | Location (X)| Location (Z) | Quests
 ------------ | --- | --- | --- | -----
[Snake Shrine](#snake-shrine) | 1060 | 9820 | [Snake Offering](#snake-offering-quest)<br/>[Need More Cats](#need-more-cats-quest)
[Sprucewood Creek](#sprucewood-creek) | 1120 | 10290 | [Wolf Control](#wolves-control)<br/>[Reputation Check: 5](#reputation-check)
[Hanging Hills](#hanging-hills) | 240 | 10050 | [Gateway Blueprint](#gateway-blueprint-quest)<br/>[Bricks from the End](#bricks-from-the-end-quest)
Woodstock | 2050 | 10350 
[Sandy Haven](#sandy-haven) | -840 | 8300 | [Secrets of the Ziggurat](#deciphering-quest)<br/>[Radioactive Dust](#radioactive-dust-quest) 
Crossing Sands | -310 | 9070 |
[Gleamsville](#gleamsville) | -1020 | 9520 | [Gleamsville Treasures](#gleamsville-treasures) 
Edgewater | 550 | 10050 |
Shadesbury | 730 | 9520 |
Sweet Meadows | 150 | 10500 |
The Outpost | 740 | 10720 |

## Quest Locations

Name | Location (X)| Location (Z) | Quests Related | Discovery check | Difficulty
 --- | --- | --- | --- | --- | ---
[Devastation Zone](#radiation-zone) | -160 | 8300 | [Radioactive Dust](#radioactive-dust-quest) | `discovery:quest/devastation_zone` | Hard
[Glory Hall](#glory-hall) | -110 | 9200 | [The Impetuous](#the-impetuous-quest) | `discovery:quest/glory_hall` | Medium
[The University](#university) | 910 | 10280 | [`Plot A`](#plot-a-locking-down-the-mothership)<br/>[`Plot C`](#plot-c-activating-space-beacon)<br/>[Bricks from the End](#bricks-from-the-end-quest)<br/>[Need More Cats](#need-more-cats-quest) | `discovery:quest/university` | Medium + 
[Mazecapade](#mazecapade) | 1720 | 9900 | [Gleamsville Treasures](#gleamsville-treasures) | `discovery:mazecapade` | Easy

## Bonus Locations
Name | Location (X)| Location (Z) | Quests Related | Discovery check | Difficulty | Notes
-----| --- | --- | --- | --- | --- | --- 
[The Emporium](#emporium) | 1430 | 10170 | [`Plot B`](#plot-b-gaining-reputation)<br/>[Emporium Special](#emporium-special-quest) | `discovery:bonus/emporium` | Peaceful | An important NPC Hub with vendors and lore
The Farm | 1620 | 9730 | | `discovery:bonus/farm` | Easy | Guaranteed to have a `chests:alchemy` container
Keeper's Lodge | 855 | 10075 | [Emporium Special](#emporium-special-quest) | `discovery:bonus/keepers_lodge` | Medium | `chests:profession` chest; `chests:quest` chest; book that hints to <span style="color:orange">**Repentance**</span> location
[Hermit's House](#hermits-house) | 1781 | 10529 | | `discovery:hermits_house` | Peaceful | Contains a `chests:quest` chest and a `crafting:diamond_block_quartz` recipe

# Items

## Currency

```
scoreboard objectives add trade_diamonds dummy "Diamonds in possession"
scoreboard objectives add emp_coins dummy "Emporium Coins in possession"
scoreboard objectives add emp_bonds dummy "Emporium Bonds in possession"
```

Name | Give Script | Description | Obtained from
--- | --- | --- | ---
**Reputation Token** | `quests:items/currency/reputation_token` | Can be exchanged for *1 Reputation* at **The Emporium** | `chests:quest` chests,<br/>Boss Drops
**Emporium Gift** | `quests:items/currency/emporium_gift` | Exchanged for unique items at **The Emporium** | Sold at **The Emporium** for **1 Reputation Token** + **1 Emporium Bond**
**Emporium Bond** | `quests:items/currency/emporium_bond` | Exchanged for rare items at **The Emporium** | Sold at **The Emporium** for **3 Emporium Coins** + **1 Nether Quartz**
**Emporium Coin** | `quests:items/currency/emporium_coin` | Exchanged for common items at **The Emporium** | Sold at **The Emporium**

## Expendable Items

Name | Give Script | Description | Obtained from
--- | --- | --- | ---
**Satchel Charge** | `crafting:consumables/satchel_charge` | AoE grenade that levitates things up | Quest Chests<br/> Mob Drops
**Aquamen Solution** | `crafting:consumables/aquamen_solution` | A consumable potion that provides all sorts of underwater bonuses | `chests:quest` chests,<br/>Rare Mob Drops,</br>Vendors at **The Emporium**
**Toxic Grenade** | `crafting:consumables/toxic_grenade` | AoE grenade that applies various debuffs and poison | `chests:quest` chests,<br/> Mob Drops,</br>Vendors at **The Emporium**
**Heroic Brew** | `crafting:consumables/heroic_brew` | A long lasting potion that makes you a Hero of The Village and provides Luck | Quest Chests,<br>`chests:quest` chests,<br/> Rare Drops
**Divine Infusion** | `crafting:consumables/divine_infusion` | A medium lasting potion that grants outstanding combat bonuses | Vendor at **The Emporium**

## Unique Loot
Name | Give Script | Description | Obtained from
--- | --- | --- | ---
<span style="color:orange">**Xerus Demise**</span> | `uniques:xerus_demise` | A Legendary Tier Bow| **Graveyard**,<br/> `Xerus` bossfight (Ziggurat Quest)
<span style="color:orange">**Brickwall**</span> | `uniques:brickwall` | A Legendary Tier Shield | **Sprucewood Creek** (Wolves quest)
<span style="color:orange">**Hilltops**</span> | `uniques:hilltops` | A Legendary Tier Boots | **Hanging Hills** (End Stone quest)
<span style="color:orange">**Starlight**</span> | `uniques:starlight` | A Legendary Tier Helmet | **Sandy Haven** (Radioactive Dust quest)
<span style="color:orange">**Repentance**</span> | `uniques:repentance` | A Legendary Tier Sword | **Casa Moreno** (Repentance quest) (*TODO*)
<span style="color:orange">**Axe Of Unbinding**</span> | `uniques:axe_of_unbinding` | A Legendary Tier Axe | Vendor at **The Emporium**
<span style="color:orange">**Wings Of Liberty**</span> | `uniques:wings_of_liberty` | A Legendary Tier Elytra | Vendor at **The Emporium**
<span style="color:orange">**The Impaler**</span> | `uniques:impaler` | A Legendary Tier Trident | Vendor at **The Emporium**
<span style="color:orange">**The Tempest**</span> | `uniques:tempest` | A Legendary Tier Crossbow | **The Refinery** (*TODO*)
<span style="color:orange">**Viper Tooth**</span> | `uniques:viper_tooth` | A Legendary Tier Sword | **Snake Shrine** (Offering Quest)

## Quest Items
Name | Give Script | Description | Obtained from
--- | --- | --- | ---
`Decryptor` | `quests:items/decryptor` | A tool do decipher Code Blocks | **Exampleton**
`Heart of the Ziggurat` | `quests:items/heart_of_the_ziggurat` | A quest item for **Riddle of the Ziggurat** | **Graveyard**
`Radioactive Dust` | `quests:items/radioactive_dust` | A quest item for **Radioactive Dust** | **Devastation Zone**
`Gleamsville Family Amulet` | `quests:items/gv_amulet` | A quest item for **Gleamsville Treasures** | **Mazecapade**
`Gleamsville Family Diamond` | `quests:items/gv_diamond` | A quest item for **Gleamsville Treasures** | Wandering Trader
`Gleamsville Family Clock` | `quests:items/gv_clock` | A quest item for **Gleamsville Treasures** | **Resort** (*TODO*)
`Snake Offering` | `quests:items/offerings/snake` | A quest item for [**Snake Offering**](#snake-offering-quest) | Vendor at **The Emporium**
`Wind Offering` | `quests:items/offerings/wind` | Applied at **Wind Shrine** (*TODO*) | Vendor at **The Emporium**
`Ocean Offering` | `quests:items/offerings/ocean` | Applied at **Ocean Shrine** (*TODO*) | Vendor at **The Emporium**
`Thunder Offering` | `quests:items/offerings/lightning` |  Applied at [**Thunder Shrine**](#thunder-shrine) | **Woodstock** Black Market Vendor (*TODO*)
`Blood Offering` | `quests:items/offerings/blood` | Applied at **Blood Shrine** (*TODO*) | **Woodstock** Black Market Vendor (*TODO*)

# Hermit's House

> A tiny house with a `chests:quest` chest and a recipe

## Unlockable Recipes
* `crafting:diamond_block_quartz`

# Mazecapade

> A small estate inside a grass maze with lit pathways.Scripts prevent the player from entering the maze in any way but the one supposed gateway. There are 5 `chests:quest` and 1 `chests:epic` chests inside the maze, as well as one `chests:quest` chest in the building guaranteed to contain 1 *Reputation Token*.

## Unlockable Recipes
* `crafting:ender_smelt`

## Related Quests

> One of barrels in the building contains [Gleamsville Family Amulet](#gleamsville-treasures)

# Emporium

> A friendly location with several available interactions. Supposed to be used a "base of operations" in the area, despite it doesn't offer it's own *Portal Key*, but has beds to set a respawn point.

## Emporium Special quest

> The Player is required to establish a supply chain with any manufacture to brew and deliver *Emporium Special* beverage. The quest is made with [Brewery plugin](https://github.com/DieReicheErethons/Brewery)

```
scoreboard objectives add q_emp_special dummy "Emporium Special progress"
```

Quest Stage | Location | Score Value
--- | --- | ---
Find an *Emporium Special* recipe | **Keepers Lodge** | 0 -> 1
Craft and try at least 4-start *Emporium Special* | Recipe: `crafting:books/emporium_special` | 1 -> 2
Set up a supply chain with any brewery | Brewery of **Exampleton** | 2 -> 3
Deliver a *Shipment Contract* to **The Emporium** | **The Emporium** | 3 -> 4; +1 *Reputation*

## Offering Vendor
```
function quests:emporium/vendors/offering
```
> Sells `Wind Offering`, `Snake Offering` and `Ocean Offering` 

## Quartermaster
```
function quests:emporium/vendors/offering
```

> Sells local [currency](#currency): `Emporium Coins` and `Emporium Bonds`, as well as some other items for Iron and Gold Nuggets.


## Oracle
```
scoreboard objectives add randomizer dummy "random value"
scoreboard objectives add oracle_advice dummy "Oracle advice id"
```
> Provides advices on currently open quests. Has 5 different instruction sets which are picked randomly every time. 1 piece of advice costs 15 xp. If no advice is found, provides minor buffs, loot drops, and can also unlock a couple of recipes.

> Starts Plot B for the player when used for the first time

> Used to turn in selected Plot B Quests 

Option | Quests | Function | Guaranteed one-time drops
--- | --- | --- | ---
#1  | [Bricks From The End](#bricks-from-the-end-quest)<br/>[Gleamsville Amulet](#gleamsville-treasures) | `quests:emporium/oracle/advices/piece1` | `crafting:recipes/horse_egg`
#2  | [Need More Cats](#need-more-cats-quest)<br/>[Secrets Of The Ziggurat](#deciphering-quest) | `quests:emporium/oracle/advices/piece2` | `crafting:recipes/horse_leather_armor`


# University

## Unlockable recipes
* `crafting:cat_egg`
* `crafting:diamond_block_gold`
* `crafting:dolphin_egg`
* `crafting:emerald_from_ice`
* `crafting:end_stone_bricks`
* `crafting:end_rod`
* `crafting:gold_block`
* `crafting:horse_armor_leather`
* `crafting:horse_egg`
* `crafting:iron_from_stones`
* `crafting:ocelot_egg`
* `crafting:quartz_from_stone`
* `crafting:rabbit_egg`
* `crafting:turtle_egg`

## Unique boss: `Krolly`, an angry rabbit
`function mobs:summon/boss/krolly`

## Main Quest Start (Plots A and C)
> Auditorium E12 (Linguistics)
    
```function quests:main/plot_a/init_condition```

## Thunder Shrine
> Second floor, West Wing
> 
### Offering 
> Grants 2 Reputation when done for the first time. Grants Health Boost, Haste and Jump Boost for 5 mins

```
execute as @p[distance=..6,nbt={SelectedItem:{id:"minecraft:blaze_rod",tag:{q_offering:"thunder"}}}] run function quests:university/thunder_shrine
```

# Exampleton

## Exampleton Reserved quest
> The player is required to deliver a bottle of 5-Star *Exampleton Reserved* to **Woodstock**, **Gleamsville** and **The Outpost** each

## Unlockable recipes
* 


```
scoreboard objectives add q_ex_reserved dummy "Exampleton Reserved progression"
```

# Glory Hall

## Objectives
Title | Scoreboard/Tag | Description | Reward
-- | -- | -- | --
<nobr>The Impetuos</nobr> | q_impetuos | ```["",{"text":"Investigate the wreckage of "},{"text":"The Impetuos","color":"yellow"}]``` | 2 Reputation, <br/>Bonus Chest,<br/><nobr>**Deep Lab** Portal Key</nobr>

## Unlockable recipes
* `crafting:obsidian_from_ice`
* `crafting:panda_egg`
* `crafting:prismarine_from_ice`

## The Impetuous Quest

> The Player is told a story about a freight vessel named **The Impetuos** which was trafficing some military cargo and had sunk to the south of Gleamsville for some reason. The player is required to investigate the incident site. When he does so he learns that the cargo is gone, apparently, but was supposed to be delivered to **Research Center**, and another unsuccessful attempt to deliver the same thing took time a while ago, leading to loss of another ship. To find the whereabouts of that ship, Player proceeeds to **Investigation Bureau** where he gets another shipwreck map leading to the **Sunken Cargo**, where he finds the *Journal*, letting him know both ships were not actually transporting, but testing some weapons, and both times tests failed. *The Journal* leads back to **Research Center**, where it unlocks a special room with a quest reward

### Objective
```
scoreboard objectives add q_impetuos dummy ["",{"text":"Investigate the wreckage of "},{"text":"The Impetuos","color":"yellow"}]
```

### Quest Items


# Hanging Hills
```
scoreboard objectives add q_hh_end_stone minecraft.crafted:minecraft.end_stone_bricks "Craft 30 End Stone Bricks"
scoreboard objectives add q_ss_blueprint dummy "Fetch Gateway Blueprint from Exampleton"
```

## Quests

Title | Scoreboard | Description | Reward
-- | -- | -- | --
Gateway Blueprint | **q_ss_blueprint** | ```["",{"text":"Fetch "},{"text":"Gateway Blueprint","color":"aqua"},{"text":" from "},{"text":"Exampleton","color":"dark_green"}]``` | *+2 Reputation*, <br/>Portal Key
Bricks from the End  | **q_hh_end_stone** | ```["",{"text":"Craft "},{"text":"30","color":"green"},{"text":" End Stone Bricks","color":"dark_aqua"}]``` | *+2 Reputation*, <br/><span style="color:orange">**Hilltops**</span>

## Bricks from the End Quest
> The Player is required to prove his skills by producing 30 *End Stone Bricks* by crafting. Aside from most obvious option (go to The End and to mine some End Stone) the player is told another crafting recipe is obtained from **The University**

## Gateway Blueprint Quest

> Player learns that the village of **Exampleton** has successfully completed a project to build automatic gateways around the place. Player is required to travel to **Exampleton** and fetch the *Blueprint*. When arriving there the Player is required to earn some local trust by exploring and investigating the **Zombie Village** to the north. There it gets down to killing 15 zombies and a Zombie Terror. Returning to **Exampleton** with this quest unlocks a special vendor at the Engineering Department, which allows for purchase of various **Blueprints**, including this one, and **Decoder**

### Quest Items
* `quests:items/gateway_blueprint` 
 
### Returning Blueprint
```
execute as @p[distance=..10,scores={q_ss_blueprint=0..1},nbt={Inventory:[{id:"minecraft:map",tag:{q_ss_blueprint:"1"}}]}] run function quests:hanging_hills/return_blueprint
```

# The Outpost
```
minecraft:give @p iron_ingot{q_op_golem:"pack",display:{Name:"[{\"text\":\"Iron Golem DIY Pack\",\"italic\":false,\"color\":\"dark_aqua\"}]",Lore:["[{\"text\":\"Probably this can somehow be used to create an Iron Golem\",\"italic\":false,\"color\":\"white\"}]"]},HideFlags:60} 1
```


# Sprucewood Creek

## Objectives
* `quests:sprucewood_creek/init`
  
Title | Scoreboard/Tag | Description | Reward
-- | -- | -- | --
Wolves Control | **q_sc_wolves**,<br/> *scWolvesKilled* | ```["",{"text":"Kill "},{"text":"15 Wolves","color":"red"}]``` | <nobr>*+3 Reputation*</nobr>, <br/><Span style="color:orange">**Brickwall**</span>
Reputation Check : 5 | *scRepCheck* | ```["",{"text":"Gain "},{"text":"5 Reputation","color":"green"}]``` | Portal Key,<br/> Bonus Chest


## Wolves control

> Player is required to kill 15 wolves in the nearest forest. A special trigger make all wolves in area hostile towards player while he has the quest

```
scoreboard objectives add q_sc_wolves minecraft.killed:minecraft.wolf ["",{"text":"Kill "},{"text":"15 Wolves","color":"red"}]
```

### Make wolves aggresive

```
execute as @a[distance=..50,scores={q_sc_wolves=..15},tag=!wolfAggro] run quests:sprucewood_creek/wolf_aggro
```

### Reward
```
execute as @p[distance=..10,tag=!scWolvesKilled,scores={q_sc_wolves=15..}] run function quests:sprucewood_creek/wolf_reward
```

## Reputation Check
> Player is told that the village welcomes respected members of local community as their citizens, requiring him to earn *5 Reputation*. Quest turns in automatically.

### Reward
```
execute as @p[distance=..10,tag=!scRepCheck,scores={reputation=5..}] run function quests:sprucewood_creek/rep_check
```

# Sandy Haven
## Objectives
* `quests:sandy_haven/init`
  
Title | Scoreboard | Description | Reward
-- | -- | -- | --
Radioactive Dust | **q_sh_dust** | ```["",{"text":"Obtain ","color":"white"},{"text":"Radioactive Dust","color":"green"},{"text":" from ","color":"white"},{"text":"Devastation Zone","color":"red"}]``` | *+3 Reputation*, <br/><Span style="color:orange">**Starlight**</span>
Riddle of the Ziggurat | **q_sh_cipher** | ```["",{"text":"Solve the riddle of ","color":"white"},{"text":"the Ziggurat","color":"red"}]``` | *+3 Reputation*, <br/>Portal Key
## Radioactive Dust Quest 
> Player is told of a disasterous event which happened somewhere to the east of the place, said to be an Alien attack on another village, leading to its destruction. Player is required to bring a proof of the accident. On travelling east player finds a vast area of mangled terrain, lava and fire, called **Devastation Zone**. At the bottom of it there's a spot which requires some parkour to get to. Also in its vicinity a radiation is applied to player, causing health loss and nausea. On getting to the spot player gets *Radioactive Dust* which he has to bring back to the village without dying.

### Quest Item
`quests:sandy_haven/items/radioactive_dust` : Give *Radioactive Dust* to command caller and set quest score to 1

### Radiation Zone
```
scoreboard objectives add radLevel dummy ["",{"text":"Radiation Level","color":"green"}]
```
* Apply radiation level and glow effect from `Radioactive Dust` and <span style="color:orange">Starlight</span>:<br/>
`quests:devastation_zone/radiation` (_tick_) 

* Spawn chest and give `Radioactive Dust` on approach<br/>
```execute as entity @p[distance=..5,tag=!chestDevZone] run function quests:devastation_zone/quest```
  

### Dust turn-in
```
execute as @p[distance=..12,scores={q_sh_dust=0..1},nbt={Inventory:[{tag:{q_sh_dust:"1"}}]}] run function quests:sandy_haven/return_dust
```


## Deciphering quest

>Player is required to "Solve the riddle of the Ziggurat". On arrival to **The Ziggurat** he discovers encrypted signs, which say a special item is required. That item is Decryptor, obtained from Engineering Department at Exampleton. Using a decryptor on 8 signs in a given order gives player a book, letting him know that the Artifact was stolen from the place and offering him to find the crypt of Xerus (located at the **Graveyard**) for further investigation. Access to the crypt requres the book as the key, and in the crypt a chest is found, containing the Artifact: *Heart of the Ziggurat*. Opening the chest triggers the boss fight with Xerus, who drops unique **Xerus Demise** bow. Returning the Artifact to Sandy Haven ends the quest.

```
scoreboard objectives add q_sh_cipher dummy 
```

### Items
* `quests:items/decryptor` a device to decipher the blocks in the Ziggurat, found at **Exampleton**
* `quests:items/secrets_of_the_ziggurat` a book that the Player obtains from deciphering all blocks at **The Ziggurat**
* `quests:items/heart_of_the_ziggurat` found at Xerus grave at **Graveyard**

### Locking the chest
data merge block ~ ~ ~1 {Lock:"Secrets of the Ziggurat"}

### Codeblocks (in order)
Code | Sign
--- | ---
`quests:ziggurat/codeblocks/block_1`|`quests:ziggurat/signs/sign_1`
`quests:ziggurat/codeblocks/block_2`|`quests:ziggurat/signs/sign_2`
`quests:ziggurat/codeblocks/block_3`|`quests:ziggurat/signs/sign_3`
`quests:ziggurat/codeblocks/block_4`|`quests:ziggurat/signs/sign_4`
`quests:ziggurat/codeblocks/block_5`|`quests:ziggurat/signs/sign_5`
`quests:ziggurat/codeblocks/block_6`|`quests:ziggurat/signs/sign_6`
`quests:ziggurat/codeblocks/block_7`|`quests:ziggurat/signs/sign_7`
`quests:ziggurat/codeblocks/block_8` => `quests:ziggurat/finish`|`quests:ziggurat/signs/sign_8`

### Returning the artifact
```
execute as @p[distance=..8,scores={q_sh_cipher=8},nbt={Inventory:[{id:"minecraft:firework_star",tag:{q_sh_cipher:"1"}}]}] run function quests:sandy_haven/return_artifact
```


# Snake Shrine

## Objectives

`quests:snake_shrine/init`

```
scoreboard objectives add q_ss_ocelot minecraft.used:minecraft.ocelot_spawn_egg "Spawn 5 Ocelots"
```

  
Title | Scoreboard | Description | Reward
-- | -- | -- | --
Snake Offering | **q_ss_offering** | ```["",{"text":"Make an offering at "},{"text":"Snake Shrine","color":"dark_green"}]``` | *+2 Reputation*, <br/><span style='color:orange'>**Viper Tooth**</span>
Need More Cats | **q_ss_ocelot** | ```["",{"text":"Spawn "},{"text":"5 Ocelots","color":"dark_aqua"}]``` | *+2 Reputation*, <br/>Portal Key

## Snake Offering Quest

> Being the first village the Player finds, it is built upon an ancient shrine and every visitor is required to make an offering. The whereabouts of the offering itself is not disclosed. When reading a starting quest book, the Player is told about **The Emporium**, which he eventually discovers. There the Player can buy the needed offerings among the others at *Offerings Vendor*. Approaching a Shrine with the Offering in hand ends the quest.

### Quest Items
* `quests:items/offerings/snake`

### Making an offering
```
execute as @p[distance=..5,scores={q_ss_offering=0..1},nbt={SelectedItem:{id:"minecraft:rabbit_foot",tag:{q_offering:"snake"}}}] run function quests:snake_shrine/make_offering
```


## Need More Cats Quest

> Player is told that local area is swarmed by snakes (obviously) which harms the ecosystem. To restore it the Player is requested to spawn 5 Ocelots (anywhere on map) and suggested that he looks at the University for spawn eggs or recipes. University indeed has all the recipes required and a bunch of ingredients, and some chests (`chests:alchemy` and `chests:q_reward` loot tables) have a chance to drop some eggs

### Finishing the quest
> Quests finishes automatically as the player approaches a book stand at Snake Shrine



# Gleamsville

## Objectives
> Player is told a story of a rich family which had founded the village long time ago. The family were told to own a lot of unique treasures, most of which was taken away on one unfortunate day, when a raid happened on the village. the Player is required to retrieve 3 particular pieces of that treasure from somewhere in the world: a *Clock*, an *Amulet* and a *Diamond*.
* `quests:gleamsville/init`
  
Scoreboard | Description | Reward
-- | -- | --  
**q_gv_clock** | ```["",{"text":"Gleamsville Family Clock ","color":"gold"},{"text":"found"}]``` | *+2 Reputation*
**q_gv_amulet** | ```["",{"text":"Gleamsville Family Amulet ","color":"gold"},{"text":"found"}]``` | *+2 Reputation*, <br/>Portal Key
**q_gv_diamond** | ```["",{"text":"Gleamsville Family Diamond ","color":"gold"},{"text":"found"}]``` | *+2 Reputation*
All three quests | `chests:q_reward_extra` | Bonus chest,<br/> including *+1 Reputation*


## Gleamsville Treasures
* `quests:items/gv_clock` - Clock
* `quests:items/gv_amulet` - Amulet
* `quests:items/gv_diamond` - Diamond

## Items turn-in and reward
* `quests:gleamsville/return_amulet`
* `quests:gleamsville/return_clock`
* `quests:gleamsville/return_diamond`
* `quests:gleamsville/complete_reward` - triggered when the last quest is completed

# Trade
```
experience add @p[distance=..5,level=5..] -5 levels
clone ~ ~ ~-2 ~ ~ ~-2 ~ ~ ~1 replace
tellraw @p ["",{"text":"Your service to","color":"green"},{"text":" The Ghost Town","color":"yellow"},{"text":" is much appreciated! You can now come back any time.","color":"green"}]
```

# Misc
`data modify block ~4 ~ ~3 SuccessCount set value 0`

`execute if entity @p[nbt={Inventory:[{id:"minecraft:emerald",Count:50b}]}] run clear @p emerald 50`