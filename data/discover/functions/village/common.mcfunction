title @s times 20 100 20
title @s subtitle {"text":"Village discovered","color":"white"}
title @s actionbar [{"text":"Quest Book","color":"gold","bold": true},{"text":" recorded","color":"white"}]
playsound ui.toast.challenge_complete player @s ~ ~ ~ .8 0.6
playsound block.end_portal.spawn player @s ~ ~ ~ 4 1.2
particle nautilus ~ ~2 ~ 0.25 0.5 0.25 2 250
effect give @s resistance 60 2 true