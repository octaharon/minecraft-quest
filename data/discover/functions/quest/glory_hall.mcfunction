title @p[distance=..64,tag=!visitGloryHall] subtitle {"text":"Location discovered","color":"white"}
title @p[distance=..64,tag=!visitGloryHall] title {"text":"Glory Hall","bold":true,"color":"yellow"}
playsound block.end_portal.spawn player @p[distance=..64,tag=!visitGloryHall] ~ ~ ~ 4
tellraw @p[distance=..64,tag=!visitGloryHall,tag=cameraman] {"text":"{...This place is so magnificient, now it's clear why many seek to see pictures of it...}","italic":true,"color":"yellow"}
tag @p[distance=..64,tag=!visitGloryHall] add visitGloryHall
function quests:glory_hall/spawn