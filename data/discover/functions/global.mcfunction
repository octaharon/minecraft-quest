execute as @a[x=1590,y=76,z=9704,dx=68,dy=80,dz=69,tag=!visitFarm] at @s run function discover:bonus/farm
execute as @a[x=1669,y=64,z=9843,dx=86,dy=80,dz=80,tag=!visitMazecapade] at @s run function discover:quest/mazecapade
execute as @a[x=1795,y=64,z=10804,dx=100,dy=80,dz=100,tag=!visitOrder] at @s run function discover:quest/order
execute as @a[x=-129,y=64,z=8573,dx=200,dy=80,dz=180,tag=!visitExampleton] at @s run function discover:village/exampleton
execute as @a[x=-954,y=68,z=9884,distance=..60,tag=!visitImpetuous] at @s run function discover:bonus/impetuos
execute as @a[x=1431,y=96,z=10177,distance=..64,tag=!visitEmporium] at @s run function discover:bonus/emporium
execute as @a[x=854,y=135,z=10077,distance=..45,tag=!visitKeepersLodge] at @s run function discover:bonus/keepers_lodge
execute as @a[x=-286,y=64,z=9700,dx=120,dy=40,dz=95,tag=!visitSaphireHotel] at @s run function discover:quest/sapphire
execute as @a[x=1781,y=75,z=10529,distance=..20,tag=!visitHermitsHouse] at @s run function discover:bonus/hermits_house
execute as @a[x=565,y=65,z=8372,distance=..50,tag=!visitWindShrine] at @s run function discover:bonus/wind_shrine
execute positioned 1055 71 9795 as @a[distance=..32,tag=!visitSnakeShrine] run function discover:village/snake_shrine
execute positioned 1113 71 10280 as @a[distance=..50,tag=!visitSprucewood] run function discover:village/sprucewood_creek
execute positioned -1029 68 9501 as @a[distance=..50,tag=!visitGleamsville] run function discover:village/gleamsville
execute positioned -839 71 8302 as @a[distance=..50,tag=!visitSandyHaven] run function discover:village/sandy_haven
execute positioned 2054 63 10382 as @a[x=1987,y=60,z=10297,dx=50,dy=50,dz=120,tag=!visitWoodstock] run function discover:village/woodstock
execute positioned 740 63 10760 as @a[distance=..64,tag=!visitOutpost] run function discover:village/outpost
execute positioned 261 65 10036 as @a[distance=..64,tag=!visitHangingHills] run function discover:village/hanging_hills
execute positioned 531 64 10107 as @a[x=495,y=60,z=10010,dy=100,dx=135,dz=140,tag=!visitEdgewater] run function discover:village/edgewater


## World Secrets

execute positioned 2319 78 10070 as @a[tag=!w_secret_1,distance=..8] at @s run function discover:secrets/w_secret_1
execute positioned 1490 66 9441 as @a[tag=!w_secret_3,distance=..4] at @s run function discover:secrets/w_secret_3
execute as @a[tag=!w_secret_2,y=72,x=-86,z=8575,dy=3,dx=4,dz=4] at @s run function discover:secrets/w_secret_2
execute positioned -226 73 9736 as @a[y=72,x=-222,z=9738,dy=10,dx=-7,dz=-2,tag=!sap_secret] run function discover:secrets/secret_sapphire