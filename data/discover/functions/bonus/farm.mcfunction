#define tag visitFarm
function discover:bonus/common
title @s title {"text":"The Farm","bold":true,"color":"gold"}
tag @s add visitFarm
execute positioned 1625 78 9742 run function chests:quest/west
execute positioned 1621 82 9739 run function chests:any/common/p50/east
execute positioned 1621 82 9738 run function chests:any/common/p50/east
execute positioned 1621 82 9741 run function chests:basic
execute positioned 1621 82 9742 run function chests:any/common/p50/east
execute positioned 1626 82 9738 run function chests:any/common/p50/west
execute positioned 1626 82 9739 run function chests:any/common/p50/west
execute positioned 1626 82 9741 run function chests:any/common/p50/west
execute positioned 1626 82 9742 run function chests:alchemy/west
execute positioned 1619 78 9736 run function mobs:summon/undead_archer
execute positioned 1627 78 9736 run function mobs:summon/undead_warrior
execute positioned 1627 78 9746 run function mobs:summon/undead_archer
execute positioned 1617 78 9745 run function mobs:summon/undead_warrior