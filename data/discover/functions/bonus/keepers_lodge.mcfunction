#define tag visitKeepersLodge
function discover:bonus/common
title @s title {"text":"Keeper's Lodge","bold":true,"color":"gold"}
execute positioned 858 123 10066 run function chests:basic/barrel
execute positioned 853 129 10077 run function chests:quest/north
execute positioned 855 129 10084 run function chests:profession/north
execute positioned 851 129 10081 run function mobs:summon/alien_raider
execute positioned 852 130 10070 run function mobs:summon/alien_raider
execute positioned 855 123 10075 run function mobs:summon/alien_raider
tag @s add visitKeepersLodge