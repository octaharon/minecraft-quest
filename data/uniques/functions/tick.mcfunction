## Test for glowstone dust or Starlight
scoreboard players set @a randomizer 0
execute as @a if data entity @s Inventory[{Slot:103b}].tag.glow run scoreboard players set @s randomizer 1
execute as @a if entity @s[nbt={Inventory:[{tag:{q_sh_dust:"1"}}]}] run scoreboard players set @s randomizer 1
### Remove blindness when has helmet on or Dust and give glow
effect give @a[scores={randomizer=1}] glowing 1 1
effect clear @a[scores={randomizer=1}] blindness

#alias entity foes_around @e[predicate=utils:foe,distance=..15,predicate=!utils:tamed]
## Tempest Slowing Aura (Range=15)
execute as @a[nbt={SelectedItem:{id:"minecraft:crossbow",tag:{q_tempest:"1"}}}] at @s run effect give @e[predicate=utils:foe,distance=..15,predicate=!utils:tamed] slowness 3 1

## Nemesis breaking stealth
execute as @a[nbt={SelectedItem:{id:"minecraft:bow",tag:{Nemesis:"1"}}}] at @s run effect clear @e[predicate=utils:foe,distance=..15,predicate=!utils:tamed] invisibility