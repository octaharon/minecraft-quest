#define objective avengerSetItems
scoreboard players set @a avengerSetItems 0
execute as @a[nbt={Inventory:[{Slot:100b,tag:{ItemSet:"avenger"}}]}] run scoreboard players add @s avengerSetItems 1
execute as @a[nbt={Inventory:[{Slot:101b,tag:{ItemSet:"avenger"}}]}] run scoreboard players add @s avengerSetItems 1
execute as @a[nbt={Inventory:[{Slot:102b,tag:{ItemSet:"avenger"}}]}] run scoreboard players add @s avengerSetItems 1
execute as @a[nbt={Inventory:[{Slot:103b,tag:{ItemSet:"avenger"}}]}] run scoreboard players add @s avengerSetItems 1
execute as @a[nbt={Inventory:[{Slot:-106b,id:"minecraft:trident",tag:{ItemSet:"avenger"}}]}] run scoreboard players add @s avengerSetItems 1
execute as @a[nbt={Inventory:[{Slot:-106b,id:"minecraft:iron_sword",tag:{ItemSet:"avenger"}}]}] run scoreboard players add @s avengerSetItems 1
execute as @a[nbt={SelectedItem:{id:"minecraft:trident",tag:{ItemSet:"avenger"}}}] run scoreboard players add @s avengerSetItems 1
execute as @a[nbt={SelectedItem:{id:"minecraft:iron_sword",tag:{ItemSet:"avenger"}}}] run scoreboard players add @s avengerSetItems 1
execute as @a[scores={avengerSetItems=2..}] run effect give @s dolphins_grace 5 0
execute as @a[scores={avengerSetItems=3..,poisonImmunity=..20}] run scoreboard players add @s poisonImmunity 20
execute as @a[scores={avengerSetItems=4..}] run effect give @s invisibility 5 0
execute as @a[scores={avengerSetItems=5..}] run effect give @s resistance 5 1
execute as @a[scores={avengerSetItems=3..,debuffImmunity=..20}] run scoreboard players add @s debuffImmunity 20