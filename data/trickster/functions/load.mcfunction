#define objective trickster "Trickster related data"
scoreboard objectives add trickster dummy 
#define objective trickster_timer timer to change the Trickster direction
scoreboard objectives add trickster_timer dummy 
#define objective trickster_store "Trickster storage data"
scoreboard objectives add trickster_store dummy 
gamerule doTraderSpawning false
#define objective trickster_carrot store data for items->chest lock
scoreboard objectives add trickster_carrot minecraft.used:minecraft.carrot_on_a_stick
scoreboard players set @a trickster 0