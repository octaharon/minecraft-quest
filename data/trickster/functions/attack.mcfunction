effect give @s minecraft:slowness 1 10 true

scoreboard players add @p trickster 1

#spell
execute at @s run summon minecraft:evoker_fangs ~1.5 ~ ~-1.5
execute at @s run summon minecraft:evoker_fangs ~1.5 ~ ~
execute at @s run summon minecraft:evoker_fangs ~1.5 ~ ~1.5
execute at @s run summon minecraft:evoker_fangs ~ ~ ~-1.5
execute at @s run summon minecraft:evoker_fangs ~ ~ ~1.5
execute at @s run summon minecraft:evoker_fangs ~-1.5 ~ ~-1.5
execute at @s run summon minecraft:evoker_fangs ~-1.5 ~ ~
execute at @s run summon minecraft:evoker_fangs ~-1.5 ~ ~1.5

execute at @s run summon minecraft:evoker_fangs ~3 ~ ~-1
execute at @s run summon minecraft:evoker_fangs ~3 ~ ~
execute at @s run summon minecraft:evoker_fangs ~3 ~ ~1
execute at @s run summon minecraft:evoker_fangs ~-3 ~ ~-1
execute at @s run summon minecraft:evoker_fangs ~-3 ~ ~
execute at @s run summon minecraft:evoker_fangs ~-3 ~ ~1
execute at @s run summon minecraft:evoker_fangs ~-1 ~ ~3
execute at @s run summon minecraft:evoker_fangs ~ ~ ~3
execute at @s run summon minecraft:evoker_fangs ~1 ~ ~3
execute at @s run summon minecraft:evoker_fangs ~-1 ~ ~-3
execute at @s run summon minecraft:evoker_fangs ~ ~ ~-3
execute at @s run summon minecraft:evoker_fangs ~1 ~ ~-3

execute at @s run summon minecraft:evoker_fangs ~-2 ~ ~2
execute at @s run summon minecraft:evoker_fangs ~-2 ~ ~-2
execute at @s run summon minecraft:evoker_fangs ~2 ~ ~2
execute at @s run summon minecraft:evoker_fangs ~2 ~ ~-2
