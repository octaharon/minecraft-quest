
#give new trades
execute as @e[type=wandering_trader,tag=!trickster_trader,limit=1] at @s run function trickster:transform/new

#attack after hit
execute as @e[tag=trickster_trader,nbt={HurtTime:10s}] at @s run function trickster:attack

#spawn 
function trickster:spawn/test

#activate items
function trickster:items/tick

#UUID of Player
execute as @a[tag=!trickster_hasuuid] run function trickster:uuid

#Redirect the trader
execute if score #timer trickster_timer matches 1.. run scoreboard players remove #timer trickster_timer 1
execute if score #timer trickster_timer matches ..0 if entity @e[type=wandering_trader,tag=trickster_trader] run function trickster:spawn/wander

#despawn a trader after 2 Days
execute as @e[type=wandering_trader,tag=trickster_trader] run scoreboard players add @s trickster 1
execute as @e[type=wandering_trader,tag=trickster_trader,scores={trickster=48000..}] at @s run function trickster:spawn/despawn