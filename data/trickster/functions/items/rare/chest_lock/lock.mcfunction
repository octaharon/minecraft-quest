#summon storage entity
summon minecraft:armor_stand ~ ~ ~ {ArmorItems:[{},{},{id:"minecraft:stone",Count:1b,tag:{Items:[]}},{}],NoGravity:1b,Invulnerable:1b,Small:1b,Marker:1b,Invisible:1b,NoBasePlate:1b,PersistenceRequired:1b,Tags:["trickster_chest_lock","trickster_chest_lock_new"]}
#lock chest
data merge block ~ ~ ~ {Lock:"afkejpwnefwubvpwuinvgewrpgew3gv"}
#store data
data modify entity @e[tag=trickster_chest_lock,distance=..0.1,limit=1] ArmorItems[2].tag.Items set from block ~ ~ ~ Items
#clear chest
data merge block ~ ~ ~ {Items:[]}
#store uuid
scoreboard players operation @e[tag=trickster_chest_lock,distance=..0.1] trickster_store = @s trickster_store

#message
playsound minecraft:block.iron_door.close block @s ~ ~ ~ 10 0.5
execute unless score #message trickster matches 1 run title @s actionbar {"text":"Chest has been locked"}
execute if score #message trickster matches 1 run tellraw @s {"text":"Chest has been locked"}