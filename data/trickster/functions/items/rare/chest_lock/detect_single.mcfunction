#lock chest
execute unless block ~ ~ ~ chest{Lock:"afkejpwnefwubvpwuinvgewrpgew3gv"} run function trickster:items/rare/chest_lock/lock
#not your chest
execute if block ~ ~ ~ chest{Lock:"afkejpwnefwubvpwuinvgewrpgew3gv"} unless entity @e[tag=trickster_chest_lock_new,distance=..0.1] unless score @s trickster_store = @e[tag=trickster_chest_lock,distance=..0.1,limit=1] trickster_store run function trickster:items/rare/chest_lock/fail
#unlock chest
execute if block ~ ~ ~ chest{Lock:"afkejpwnefwubvpwuinvgewrpgew3gv"} unless entity @e[tag=trickster_chest_lock_new,distance=..0.1] if score @s trickster_store = @e[tag=trickster_chest_lock,distance=..0.1,limit=1] trickster_store run function trickster:items/rare/chest_lock/unlock

#reset
tag @e[tag=trickster_chest_lock_new] remove trickster_chest_lock_new
