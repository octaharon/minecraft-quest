#double chest
execute if entity @s[distance=..5.5] positioned ^ ^ ^0.1 if block ~ ~ ~ chest unless block ~ ~ ~ chest[type=single] align xyz positioned ~0.5 ~ ~0.5 run function trickster:items/rare/chest_lock/detect_double
#single chest
execute if entity @s[distance=..5.5] positioned ^ ^ ^0.1 if block ~ ~ ~ chest[type=single] align xyz positioned ~0.5 ~ ~0.5 run function trickster:items/rare/chest_lock/detect_single

#test further for chest
execute if entity @s[distance=..5.5] positioned ^ ^ ^0.1 if block ~ ~ ~ #trickster:non_solid run function trickster:items/rare/chest_lock/detect
