#lock chest
data merge block ~ ~ ~ {Lock:""}
#store data
data modify block ~ ~ ~ Items set from entity @e[tag=trickster_chest_lock,distance=..0.1,limit=1] ArmorItems[2].tag.Items
#kill storage
kill @e[tag=trickster_chest_lock,distance=..0.1]

#message
playsound minecraft:block.iron_door.open block @s ~ ~ ~ 10 0.5
execute unless score #message trickster matches 1 run title @s actionbar {"text":"Chest has been unlocked"}
execute if score #message trickster matches 1 run tellraw @s {"text":"Chest has been unlocked"}