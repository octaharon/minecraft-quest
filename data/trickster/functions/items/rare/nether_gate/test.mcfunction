execute if predicate trickster:overworld in minecraft:the_nether run function trickster:items/rare/nether_gate/nether
execute if predicate trickster:end in minecraft:the_nether run function trickster:items/rare/nether_gate/nether

execute if predicate trickster:nether in minecraft:overworld run function trickster:items/rare/nether_gate/overworld

clear @s carrot_on_a_stick{trickster_nether_gate:1b} 1
tag @s remove trickster_gate_tp

effect give @s minecraft:resistance 20 5
effect give @s minecraft:blindness 5
effect give @s minecraft:nausea 5

playsound minecraft:block.portal.travel master @s ~ ~ ~ 