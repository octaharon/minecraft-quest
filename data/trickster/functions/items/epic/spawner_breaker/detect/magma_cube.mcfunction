#test for magma_cube spawner
execute if entity @s[distance=..5.5] positioned ^ ^ ^0.1 if block ~ ~ ~ spawner align xyz positioned ~0.5 ~ ~0.5 run data merge block ~ ~ ~ {SpawnData:{id:"minecraft:magma_cube"},SpawnPotentials:[{Entity:{id:"minecraft:magma_cube"},Weight:1}]}

#test further for spawner
execute if entity @s[distance=..5.5] positioned ^ ^ ^0.1 if block ~ ~ ~ #trickster:non_solid run function trickster:items/epic/spawner_breaker/detect/magma_cube
