#get coords
execute store result score #death_coords_x trickster_store run data get entity @s Pos[0]
execute store result score #death_coords_y trickster_store run data get entity @s Pos[1]
execute store result score #death_coords_z trickster_store run data get entity @s Pos[2]
#tell
tellraw @s ["",{"text":"Death Coords: ","bold":true,"color":"blue"},{"score":{"name":"#death_coords_x","objective":"trickster_store"},"bold":true,"color":"dark_blue"},{"text":" ","bold":true,"color":"dark_blue"},{"score":{"name":"#death_coords_y","objective":"trickster_store"},"bold":true,"color":"dark_blue"},{"text":" ","bold":true,"color":"dark_blue"},{"score":{"name":"#death_coords_z","objective":"trickster_store"},"bold":true,"color":"dark_blue"}]
execute at @s run playsound minecraft:block.conduit.attack.target master @s ~ ~ ~ 10 2
#clear item
clear @s minecraft:compass{trickster_death_coords:1b}