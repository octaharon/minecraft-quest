execute positioned ~1 ~ ~ if block ~ ~ ~ air if block ~ ~-1 ~ grass_block if predicate trickster:chance_30 run clone ~-1 ~ ~ ~-1 ~ ~ ~ ~ ~
execute positioned ~-1 ~ ~ if block ~ ~ ~ air if block ~ ~-1 ~ grass_block if predicate trickster:chance_30 run clone ~1 ~ ~ ~1 ~ ~ ~ ~ ~
execute positioned ~ ~ ~1 if block ~ ~ ~ air if block ~ ~-1 ~ grass_block if predicate trickster:chance_30 run clone ~ ~ ~-1 ~ ~ ~-1 ~ ~ ~
execute positioned ~ ~ ~-1 if block ~ ~ ~ air if block ~ ~-1 ~ grass_block if predicate trickster:chance_30 run clone ~ ~ ~1 ~ ~ ~1 ~ ~ ~

kill @s