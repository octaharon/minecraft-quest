#get the health
execute store result score @s trickster_store run data get entity @s Attributes[0].Base
#add 2 healths
scoreboard players add @s trickster_store 2
#if the score isn't over 30 set new health
execute unless score @s trickster_store matches 30.. store result entity @s Attributes[0].Base double 1 run scoreboard players get @s trickster_store
#kill horse upgrade: health 
execute unless score @s trickster_store matches 30.. run kill @e[type=item,sort=nearest,limit=1,nbt={Item:{tag:{trickster_horse_health:1b}}}]
#sound
execute unless score @s trickster_store matches 30.. run playsound minecraft:block.anvil.use master @p ~ ~ ~ 10 1.3