###common###
#define tag trickster_bone_dust
#death coords
execute as @a[nbt={Health:0f,Inventory:[{tag:{trickster_death_coords:1b}}]},limit=1] run function trickster:items/common/death_coords

#toggle weather
execute as @e[tag=trickster_toggle_weather] at @s run function trickster:items/common/toggle_weather

#toggle day time
execute as @e[tag=trickster_toggle_day] at @s run function trickster:items/common/toggle_day

#horse upgrade health
execute at @e[type=item,nbt={Item:{Count:1b,tag:{trickster_horse_health:1b}}},limit=1] as @e[type=horse,distance=..1,sort=nearest,limit=1] run function trickster:items/common/horse_health

#horse upgrade speed
execute at @e[type=item,nbt={Item:{Count:1b,tag:{trickster_horse_speed:1b}}},limit=1] as @e[type=horse,distance=..1,sort=nearest,limit=1] run function trickster:items/common/horse_speed

#horse upgrade jump
execute at @e[type=item,nbt={Item:{Count:1b,tag:{trickster_horse_jump:1b}}},limit=1] as @e[type=horse,distance=..1,sort=nearest,limit=1] run function trickster:items/common/horse_jump

#bone dust
execute as @e[tag=trickster_bone_dust] at @s if block ~ ~ ~ #minecraft:flowers run function trickster:items/common/bone_dust


###rare###
#xp cost reducer
execute at @e[type=item,nbt={OnGround:1b,Item:{Count:1b,tag:{trickster_cost_reducer:1b}}},limit=1] if block ~ ~-1 ~ minecraft:smithing_table as @e[type=item,distance=..1,sort=nearest,limit=1,nbt=!{Item:{tag:{trickster_cost_reducer:1b}}}] run function trickster:items/rare/xp_cost

#item magnet
execute at @a[nbt={SelectedItem:{tag:{trickster_magnet:1b}}}] as @e[type=item,distance=..5,nbt={OnGround:1b}] run function trickster:items/rare/item_magnet
execute at @a[nbt={Inventory:[{Slot:-106b,tag:{trickster_magnet:1b}}]}] as @e[type=item,distance=..5,nbt={OnGround:1b}] run function trickster:items/rare/item_magnet

#key
execute as @a[scores={trickster_carrot=1..},nbt={SelectedItem:{tag:{trickster_chest_lock:1b}}}] at @s anchored eyes run function trickster:items/rare/chest_lock/detect
execute at @e[tag=trickster_chest_lock] unless block ~ ~ ~ chest run function trickster:items/rare/chest_lock/broken

#nether Gate
execute as @a[scores={trickster_carrot=1..},nbt={SelectedItem:{tag:{trickster_nether_gate:1b}}}] at @s run function trickster:items/rare/nether_gate/test

###epic###
#spawner breaker
execute as @e[type=item,nbt={Item:{Count:1b,tag:{trickster_spawner_breaker:1b}}}] at @s if block ~ ~-1 ~ spawner run function trickster:items/epic/spawner_breaker/break

#creeper disruptor
execute as @e[tag=trickster_creeper_disruptor_spawn] at @s align xyz positioned ~0.5 ~ ~0.5 run function trickster:items/epic/creeper_disruptor/spawn
execute at @e[tag=trickster_creeper_disruptor] as @e[type=creeper,distance=..30,tag=!trickster_creeper_mob] at @s run function trickster:items/epic/creeper_disruptor/disruption
execute as @e[tag=trickster_creeper_disruptor] at @s unless block ~ ~ ~ minecraft:andesite_wall run function trickster:items/epic/creeper_disruptor/kill

#end Gate
execute as @a[scores={trickster_carrot=1..},nbt={SelectedItem:{tag:{trickster_end_gate:1b}}}] at @s run function trickster:items/epic/end_gate/test

###reset###
execute as @a[scores={trickster_carrot=1..}] run scoreboard players reset @s trickster_carrot