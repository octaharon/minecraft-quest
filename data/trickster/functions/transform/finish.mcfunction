data modify entity @s Offers.Recipes set value []
execute as @e[type=armor_stand,tag=trickster_rng_basic,limit=1,sort=random] run function trickster:transform/add_trade
execute as @e[type=armor_stand,tag=trickster_rng_basic,limit=1,sort=random] run function trickster:transform/add_trade
execute as @e[type=armor_stand,tag=trickster_rng_basic,limit=1,sort=random] run function trickster:transform/add_trade
execute as @e[type=armor_stand,tag=trickster_rng_rare,limit=1,sort=random] run function trickster:transform/add_trade
execute as @e[type=armor_stand,tag=trickster_rng_rare,limit=1,sort=random] run function trickster:transform/add_trade
execute as @e[type=armor_stand,tag=trickster_rng_rare,limit=1,sort=random] run function trickster:transform/add_trade
execute as @e[type=armor_stand,tag=trickster_rng_unique,limit=1,sort=random] run function trickster:transform/add_trade

kill @e[type=armor_stand,tag=trickster_rng_decor]
data merge entity @s {Attributes:[{Name:generic.knockbackResistance,Base:1.0d}]}

tag @s add trickster_trader
tag @s remove tr_spawn
function trickster:spawn/wander

scoreboard players reset #trickster_trades trickster