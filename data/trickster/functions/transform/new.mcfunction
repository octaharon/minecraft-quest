execute unless entity @e[tag=tr_spawn] run function trickster:transform/trades_basic
execute store result score #randomtrade_basic trickster run execute if entity @e[tag=trickster_rng_basic]
execute unless score #randomtrade_basic trickster matches ..3 run kill @e[tag=trickster_rng_basic,limit=1,sort=random]
execute unless entity @e[tag=tr_spawn] run function trickster:transform/trades_rare
execute store result score #randomtrade_rare trickster run execute if entity @e[tag=trickster_rng_rare]
execute unless score #randomtrade_rare trickster matches ..3 run kill @e[tag=trickster_rng_rare,limit=1,sort=random]
execute unless entity @e[tag=tr_spawn] run function trickster:transform/trades_unique
execute store result score #randomtrade_unique trickster run execute if entity @e[tag=trickster_rng_unique]
execute unless score #randomtrade_unique trickster matches ..1 run kill @e[tag=trickster_rng_unique,limit=1,sort=random]

#Assign Decor to llamas ("Suboptimal" solution with less tags and any number of llamas < 16)
execute unless entity @e[tag=tr_spawn] run function trickster:transform/decor
execute if entity @e[tag=trickster_rng_decor] run data modify entity @e[type=trader_llama,tag=tr_spawn_llama,sort=random,limit=1] DecorItem set from entity @e[type=trader_llama,tag=trickster_rng_decor,sort=random,limit=1] ArmorItems[0].tag.DecorItem
execute if entity @e[tag=trickster_rng_decor] run kill @e[tag=trickster_rng_decor,sort=random,limit=1]

execute unless entity @e[tag=tr_spawn] run tag @s add tr_spawn
execute if score #randomtrade_basic trickster matches ..3 if score #randomtrade_rare trickster matches ..3 if score #randomtrade_unique trickster matches ..1 run function trickster:transform/finish