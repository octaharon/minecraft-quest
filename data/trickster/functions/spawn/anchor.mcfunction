# an anchor turns into The Trickster after a minute
execute at @s run summon minecraft:armor_stand ~ ~ ~ {PortalCooldown:1200,Invisible:1,Silent:1b,Invulnerable:1b,Tags:["trickster_anchor"]}
execute at @s run tellraw @a[distance=..32] [{"text": "You hear a distant llamas' bleating...","italic": true,"color": "gray"}]
scoreboard players reset #timer trickster