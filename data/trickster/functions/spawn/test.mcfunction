#define tag trickster_anchor
#define tag trickster_trader
#define tag trickster_spawn_player

#magic constant to check a day cycle
execute store result score #timertime trickster run time query daytime
execute if score #timertime trickster matches 1337 run function trickster:spawn/day_cycle

#test if there are players above the ground
execute as @a at @s store result score @s randomizer if blocks ~ ~2 ~ ~ 255 ~ ~ ~2 ~ masked

#when there's no trader or acnhor, the 3 day timer has passed and there are players eligible, not in water or lava, summon a new anchor
execute unless entity @e[type=wandering_trader] if score #timer trickster matches 3.. unless entity @e[type=armor_stand,tag=trickster_anchor] as @a[y=50,dy=1000,scores={randomizer=0,trickster=0},predicate=utils:is_overworld,nbt={OnGround:true},sort=random,limit=1] at @s unless block ~ ~-0.1 ~ minecraft:air unless block ~ ~ ~ minecraft:water unless block ~ ~ ~ minecraft:lava run function trickster:spawn/anchor

#summon wandering trader at Anchor after a cooldown
execute as @e[type=armor_stand,tag=trickster_anchor,nbt={PortalCooldown:0}] run function trickster:spawn/spawn
