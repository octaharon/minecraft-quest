#define tag tr_spawn
#define tag tr_spawn_llama
summon wandering_trader ~ ~ ~ {CustomNameVisible:true,CustomName:'{"text": "The Trickster","color":"aqua"}',PersistenceRequired:1b,Tags:["tr_spawn"]}
summon trader_llama ~1 ~ ~ {DecorItem:{Count:1b,id:"cyan_carpet"},Variant:3,DespawnDelay:48000,Tags:["tr_spawn_llama"],Tame:1,Leashed:true}
summon trader_llama ~-1 ~ ~ {DecorItem:{Count:1b,id:"orange_carpet"},Variant:2,DespawnDelay:48000,Tags:["tr_spawn_llama"],Tame:1,Leashed:true}
execute as @e[type=trader_llama,tag=tr_spawn_llama] at @s run data modify entity @s Leash.UUIDMost set from entity @e[type=wandering_trader,tag=tr_spawn,sort=nearest,limit=1] UUIDMost
execute as @e[type=trader_llama,tag=tr_spawn_llama] at @s run data modify entity @s Leash.UUIDLeast set from entity @e[type=wandering_trader,tag=tr_spawn,sort=nearest,limit=1] UUIDLeast
execute as @e[type=trader_llama,tag=tr_spawn_llama] at @s run data modify entity @s OwnerUUIDMost set from entity @e[type=wandering_trader,tag=tr_spawn,sort=nearest,limit=1] UUIDMost
execute as @e[type=trader_llama,tag=tr_spawn_llama] at @s run data modify entity @s OwnerUUIDLeast set from entity @e[type=wandering_trader,tag=tr_spawn,sort=nearest,limit=1] UUIDLeast
scoreboard players set @e[type=wandering_trader,tag=tr_spawn] trickster 0
playsound minecraft:entity.wandering_trader.reappeared master @s
kill @s


