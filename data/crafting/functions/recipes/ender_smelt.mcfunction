#define tag hasEnderSmeltRecipe
tellraw @p[distance=..5,tag=!hasEnderSmeltRecipe] ["",{"text":"You've learned how to smelt an ","color":"white"},{"text":"Ender Chest","underlined":true,"color":"gold"},{"text":" from ","color":"white"},{"text":"Bedrock","color":"white","underlined":true},{"text": " using a Blast Furnace","color":"white"}]
recipe give @p[distance=..5,tag=!hasEnderSmeltRecipe] crafting:ender_smelt
tag @p[distance=..5,tag=!hasEnderSmeltRecipe] add hasEnderSmeltRecipe