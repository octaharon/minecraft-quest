#define tag hasEndRodRecipe
tellraw @p[distance=..5,tag=!hasEndRodRecipe] ["",{"text":"You've learned how to smelt an ","color":"white"},{"text":"End Rod","underlined":true,"color":"gold"},{"text":" from ","color":"white"},{"text":"Redstone Torch","color":"white","underlined":true},{"text": " using a Campfire","color":"white"}]
recipe give @p[distance=..5,tag=!hasEndRodRecipe] crafting:end_rod
tag @p[distance=..5,tag=!hasEndRodRecipe] add hasEndRodRecipe