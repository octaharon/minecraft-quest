#define tag hasObsidianConcreteRecipe
tellraw @s ["",{"text":"\u26b6 New Recipe Unlocked","color":"dark_aqua"},{"text":"\n"},{"text":"3x","color":"aqua"},{"text":" "},{"text":"Black Concrete","underlined":true},{"text":"\n"},{"text":"3x","color":"aqua"},{"text":" "},{"text":"Iron Ingot","underlined":true},{"text":"\n"},{"text":"1x","color":"aqua"},{"text":" "},{"text":"Redstone","underlined":true},{"text":"\n"},{"text":"Result:","color":"aqua"},{"text":" Obsidian","color":"yellow"}]
recipe give @s crafting:obsidian_from_concrete
tag @s add hasObsidianConcreteRecipe