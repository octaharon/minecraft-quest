#define objective potionQuality "The quality of Brewery potion just consumed"
#define objective poisonImmunity "Poison immunity ticks left"
#define objective debuffImmunity "Debuff immunity ticks left"

## executed by Brewery plugin, the antidote quality is stored in `potionQuality` score

execute at @s if score @s potionQuality matches ..3 run particle damage_indicator ~ ~ ~ 1 1 1 0.4 60
execute at @s if score @s potionQuality matches 4.. run particle angry_villager ~ ~ ~ 1 1 1 0.4 10
execute at @s if score @s potionQuality matches 5.. run particle angry_villager ~ ~ ~ 1 1 1 0.4 10
execute at @s if score @s potionQuality matches 5.. run particle angry_villager ~ ~ ~ 1 1 1 0.4 10
execute at @s if score @s potionQuality matches 6.. run particle angry_villager ~ ~ ~ 1 1 1 0.4 10
execute at @s if score @s potionQuality matches 7.. run particle angry_villager ~ ~ ~ 1 1 1 0.4 10
execute at @s if score @s potionQuality matches 8.. run particle angry_villager ~ ~ ~ 1 1 1 0.4 10
execute at @s if score @s potionQuality matches 9.. run particle angry_villager ~ ~ ~ 1 1 1 0.4 10
execute at @s if score @s potionQuality matches 10.. run particle angry_villager ~ ~ ~ 1 1 1 0.4 10
execute if score @s potionQuality matches ..3 run tellraw @s ["",{"text": "You quickly realize you shouldn't have done that","italic": true,"color":"gray"}]

# 18 seconds of poison immunity plus 2 seconds per quality over 4, up to 30
execute if score @s potionQuality matches 4.. run scoreboard players add @s poisonImmunity 360
execute if score @s potionQuality matches 5.. run scoreboard players add @s poisonImmunity 40
execute if score @s potionQuality matches 6.. run scoreboard players add @s poisonImmunity 40
execute if score @s potionQuality matches 7.. run scoreboard players add @s poisonImmunity 40
execute if score @s potionQuality matches 8.. run scoreboard players add @s poisonImmunity 40
execute if score @s potionQuality matches 9.. run scoreboard players add @s poisonImmunity 40
execute if score @s potionQuality matches 10.. run scoreboard players add @s poisonImmunity 40

# 20 seconds of debuff immunity plus 5 seconds per quality over 8, up to 30
execute if score @s potionQuality matches 8.. run scoreboard players add @s debuffImmunity 400
execute if score @s potionQuality matches 9.. run scoreboard players add @s debuffImmunity 100
execute if score @s potionQuality matches 10.. run scoreboard players add @s debuffImmunity 100

execute if score @s potionQuality matches ..3 run effect give @s slowness 30 5
execute if score @s potionQuality matches ..3 run effect give @s instant_damage 1 4
execute if score @s potionQuality matches ..3 run effect give @s weakness 30 5
execute if score @s potionQuality matches ..3 run effect give @s nausea 30 1
execute if score @s potionQuality matches 4.. run effect clear @s nausea
execute if score @s potionQuality matches 4.. run effect clear @s poison
execute if score @s potionQuality matches 5.. run effect clear @s weakness
execute if score @s potionQuality matches 6.. run effect clear @s slowness
execute if score @s potionQuality matches 7.. run effect clear @s unluck
execute if score @s potionQuality matches 8.. run effect clear @s blindness
execute if score @s potionQuality matches 9.. run effect clear @s mining_fatigue
execute if score @s potionQuality matches 10.. run effect clear @s bad_omen
execute if score @s potionQuality matches 10.. run effect clear @s wither

