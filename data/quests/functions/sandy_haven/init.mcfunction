#define objective q_sh_dust "Radioactive Dust quest progression"
#define objective q_sh_cipher "Secrets of the Ziggurat quest progression"
#define tag q_sh_book
execute as @s[tag=!q_sh_book] at @s run function utils:quest_new
tag @s[tag=!q_sh_book] add q_sh_book
execute as @a[distance=..8] unless score @s q_sh_dust matches 1.. run scoreboard players set @p q_sh_dust 0
execute as @a[distance=..8] unless score @s q_sh_cipher matches 1.. run scoreboard players set @p q_sh_cipher 0
execute as @a[distance=..8] run function quests:notify/q_sh_dust
execute as @a[distance=..8] run function quests:notify/q_sh_cipher