scoreboard players set @s q_sh_cipher 9
clear @s minecraft:firework_star{q_sh_cipher:"1"} 1
function utils:clone_quest_chest
title @s subtitle ["",{"text":"Solve the riddle of ","color":"white"},{"text":"the Ziggurat","color":"red"}]
function utils:quest_complete
scoreboard players add @s reputation 3
scoreboard players set @s q_sh_cipher 10