#Chests
fill -263 69 9738 -206 95 9757 nether_quartz_ore replace chest[facing=west]
fill -263 69 9738 -206 95 9757 gold_ore replace chest[facing=south]
fill -263 69 9738 -206 95 9757 bedrock replace chest[facing=east]
fill -263 69 9738 -206 95 9757 diamond_ore replace chest[facing=north]
fill -263 69 9738 -206 95 9757 chest[facing=west]{LootTable:"chests:common/random_33"} replace nether_quartz_ore
fill -263 69 9738 -206 95 9757 chest[facing=south]{LootTable:"chests:common/random_33"} replace gold_ore
fill -263 69 9738 -206 95 9757 chest[facing=east]{LootTable:"chests:common/random_33"} replace bedrock
fill -263 69 9738 -206 95 9757 chest[facing=north]{LootTable:"chests:common/random_33"} replace diamond_ore
execute positioned -252 94 9741 run function chests:basic/east
execute positioned -252 94 9741 run function quests:outpost/fishery/spice_chest
execute positioned -206 69 9752 run function chests:quest/east
execute positioned -221 69 9737 run function chests:basic/barrel
execute positioned -228 69 9736 run function chests:carpentry/barrel
#Barrels
fill -263 69 9738 -206 95 9757 nether_quartz_ore replace barrel[facing=west]
fill -263 69 9738 -206 95 9757 gold_ore replace barrel[facing=south]
fill -263 69 9738 -206 95 9757 bedrock replace barrel[facing=east]
fill -263 69 9738 -206 95 9757 diamond_ore replace barrel[facing=north]
fill -263 69 9738 -206 95 9757 lapis_ore replace barrel[facing=up]
fill -263 69 9738 -206 95 9757 barrel[facing=west]{LootTable:"chests:common/sapphire"} replace nether_quartz_ore
fill -263 69 9738 -206 95 9757 barrel[facing=south]{LootTable:"chests:common/sapphire"} replace gold_ore
fill -263 69 9738 -206 95 9757 barrel[facing=east]{LootTable:"chests:common/sapphire"} replace bedrock
fill -263 69 9738 -206 95 9757 barrel[facing=north]{LootTable:"chests:common/sapphire"} replace diamond_ore
fill -263 69 9738 -206 95 9757 barrel[facing=north]{LootTable:"chests:common/sapphire"} replace lapis_ore
kill @e[type=item,x=-263,y=68,z=9735,dx=60,dy=28,dz=25]