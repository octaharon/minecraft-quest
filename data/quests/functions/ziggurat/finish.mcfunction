clear @s compass{decryptor:"1"} 1
execute as @s run function quests:items/secrets_of_the_ziggurat
scoreboard players set @s q_sh_cipher 8
tellraw @s ["",{"text":"You've written down your discoveries to a book named","italic":true,"color":"white"},{"text":" Secrets of the Ziggurat ","color":"gold"},{"text":"book","italic":true,"color":"white"}]
function utils:quest_step