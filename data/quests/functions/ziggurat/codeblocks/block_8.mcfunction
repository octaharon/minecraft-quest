execute if score @p[distance=..3,nbt={SelectedItem:{id:"minecraft:compass",tag:{decryptor:"1"}}}] q_sh_cipher matches 7..7 run function quests:ziggurat/finish
execute if score @p[distance=..3] q_sh_cipher matches 8.. run function quests:ziggurat/codeblocks/common
execute unless score @p[distance=..3,nbt={SelectedItem:{id:"minecraft:compass",tag:{decryptor:"1"}}}] q_sh_cipher matches 7.. run tellraw @p[distance=..3] {"text":"You should probably try decoding other Code Blocks first...","italic":true,"color":"aqua"}
execute unless score @p[distance=..3,nbt=!{SelectedItem:{id:"minecraft:compass",tag:{decryptor:"1"}}}] q_sh_cipher matches 7.. run tellraw @p[distance=..3] {"text":"You need a special item to read this...","italic":true,"color":"aqua"}

