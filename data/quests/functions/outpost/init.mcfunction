#define objective q_op_bones "Bad Bones quest progression"
#define objective q_op_fishery "Fishery quest progression"
#define tag q_op_book
execute as @s[tag=!q_op_book] at @s run function utils:quest_new
tag @s[tag=!q_op_book] add q_op_book
execute unless score @s q_op_bones matches 0.. run scoreboard players set @s q_op_bones 0
execute unless score @s q_op_fishery matches 0.. run scoreboard players set @s q_op_fishery 0
function quests:notify/q_op_bones
function quests:notify/q_op_fishery