function utils:timer/reset
execute at @s run function utils:quests_fail
tag @s remove q_op_fishery_carrier
kill @e[type=item,nbt={Item:{id:"minecraft:salmon_bucket",tag:{q_op_fishery:"1"}}}]
tellraw @s [{"text":"\u26a0 ","color":"dark_red"},{"text":"You've lost the Mackerel! Return to the ","color":"red"},{"text":"The Outpost","color":"dark_green","bold": true},{"text": " to start over","color":"red"}]