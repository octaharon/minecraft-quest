#define tag q_op_fishery_carrier
#define tag q_op_fishery_delivery
#define tag q_op_fishery_spice
#define tag q_op_fishery_cook
#define tag q_op_fishery_complete
function utils:timer/reset
tag @s remove q_op_fishery_delivery
tag @s add q_op_fishery_carrier
#resummon the trader to the dialogue closes
execute at @e[type=villager,tag=op_angler] run function quests:outpost/master_angler
tellraw @s [{"text":"Master Angler","color":"dark_green","bold": true},{"text": ": ","color":"gray","bold": false},{"text":" Oh, here you are, at last! Grab that quickly, the ","color":"gray","italic": true,"bold": false},{"text": "Quartermaster","color":"red","bold": true},{"text": " is awaiting the delivery, and Mackerel doesn't survive too long unless frozen, so you should hurry! You have approximately three minutes, starting NOW!","color":"gray","italic": true,"bold": false}]
scoreboard players set @s timerTicks 4000
function quests:outpost/fishery/bucket
function quests:notify/q_op_fishery