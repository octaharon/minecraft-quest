## give player a bucket when he talks to angler
execute as @a[tag=!q_op_fishery_carrier,tag=talkAngler,nbt=!{Inventory:[{id:"minecraft:salmon_bucket",tag:{q_op_fishery:"1"}}]}] if score @s q_op_fishery matches 0 at @s run function quests:outpost/fishery/start_delivery

## show timer to a player holding the bucket
execute as @a[tag=q_op_fishery_carrier,scores={timerTicks=1..},nbt={SelectedItem:{id:"minecraft:salmon_bucket",tag:{q_op_fishery:"1"}}}] at @s run function utils:timer/notify


## drop delivery if timer expires
execute as @a[tag=q_op_fishery_carrier,scores={timerTicks=..0}] at @s run function quests:outpost/fishery/fail_delivery
## drop delivery if player uses teleportation
execute as @a[tag=tpPlayer,nbt={Inventory:[{id:"minecraft:salmon_bucket",tag:{q_op_fishery:"1"}}]}] at @s run function quests:outpost/fishery/fail_delivery
## drop delivery if player losts the bucket
execute as @a[tag=q_op_fishery_carrier,nbt=!{Inventory:[{id:"minecraft:salmon_bucket",tag:{q_op_fishery:"1"}}]}] run function quests:outpost/fishery/bucket_lost

## End delivery, Step 2
execute at @e[type=villager,tag=emp_quartermaster] as @a[tag=q_op_fishery_carrier,distance=..4,scores={q_op_fishery=0},nbt={Inventory:[{id:"minecraft:salmon_bucket",tag:{q_op_fishery:"1"}}]}] at @s run function quests:outpost/fishery/end_delivery

## Found spice, Step 3
execute as @a[scores={q_op_fishery=1},nbt={Inventory:[{id:"minecraft:yellow_dye",tag:{HideFlags:7,Enchantments:[{id:"minecraft:fire_aspect"}]}}]}] at @s run function quests:outpost/fishery/spice_found

## Give In Cake, Step 4, Recurring
execute as @a[tag=talkAngler,nbt={SelectedItem:{id:"minecraft:pumpkin_pie",tag:{q_op_fishery:"1"}}}] at @s run function quests:outpost/fishery/turn_in_cake

## Quest completion
execute positioned 739 61 10760 as @a[distance=..15,scores={q_op_fishery=3}] run function quests:outpost/fishery/finish