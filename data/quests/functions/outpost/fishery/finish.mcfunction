scoreboard players set @s q_op_fishery 4
function utils:clone_quest_chest
title @s subtitle ["",{"text":"Deliver the gifts between","color":"white","bold":false},{"text":"The Outpost","color": "dark_green","bold": true},{"text":" and ","color": "white"},{"text": "The Emporium","color": "gold","bold": true}]
function utils:quest_complete
scoreboard players add @s reputation 2