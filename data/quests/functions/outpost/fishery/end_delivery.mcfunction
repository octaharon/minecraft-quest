tag @s remove q_op_fishery_carrier
scoreboard players set @s q_op_fishery 1
clear @s salmon_bucket{q_op_fishery:"1"}
execute at @e[type=villager,tag=emp_quartermaster] run function quests:emporium/vendors/quartermaster
function quests:outpost/fishery/recipe
function utils:quest_step
tellraw @s ["",{"text":"Quartermaster: ","color":"red","bold": true},{"color":"gray","italic": true,"text": "What? Mackerel? Is it from my old friend "},{"text":"Master Angler","color":"dark_green","bold": true},{"text":"? Really nice of him! Would you please help me doing him a return favor? I used to cook a really good ","color":"gray","italic": true},{"text":"Salmon Quiche","color":"blue"},{"text": " that he admires, but it requires a ","color":"gray","italic": true},{"text":"Secret Spice Mix","color":"yellow","italic": true},{"text":", which I ran out of. Chances are though it's available at some fashionable kitchens, like those at ","color":"gray","italic": true},{"text":"Sapphire Hotel","color":"gold"},{"text":" or ","color":"gray","italic": true},{"text":"Ocean View Resort","color":"gold"},{"text":". If you find any, we will arrange something","color":"gray","italic": true}]
function quests:notify/q_op_fishery