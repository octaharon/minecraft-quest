function utils:timer/reset
tag @s remove q_op_fishery_carrier
execute at @s run function utils:quests_fail
clear @s salmon_bucket{q_op_fishery:"1"}
tellraw @s [{"text":"\u26a0 ","color":"dark_red"},{"text":"The fish has gone bad, return to the ","color":"red"},{"text":"The Outpost","color":"dark_green","bold": true},{"text": " to start over","color":"red"}]