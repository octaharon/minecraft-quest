scoreboard players set @s q_op_fishery 3
tellraw @s ["",{"text":"Master Angler: ","bold":true,"color":"dark_green"},{"text":"It cannot be! Did he give you the recipe? Fantastic, thank you! ","italic":true,"color":"gray"},{"text":"Salmon Quiche","color":"blue"},{"text":" is my favorite, so if you manage to cook any more, don't hesitate to bring them to me. I'm your best wholesale buyer, mate! For now, accept this as my personal gratitude","italic":true,"color":"gray"}]
replaceitem entity @s weapon.mainhand minecraft:gold_ingot{display:{Lore:['{"italic":false,"color":"gold","text":"Can be exchanged for exclusive items at The Emporium"}'],Name:'{"bold":true,"italic":false,"color":"red","text":"Emporium Gift"}'},Enchantments:[{id:"minecraft:vanishing_curse",lvl:1}],"emporium":"1"}
function utils:quest_step
function quests:notify/q_op_fishery
