#define tag q_op_bones_collected
#define tag q_op_bones_melted
#define tag q_op_bones_elerium
#define tag q_op_bones_armor
#define tag q_op_bones_complete
#define objective q_op_bone_blocks
execute if score @s q_op_bones matches 0.. store result score @s q_op_bones run clear @s minecraft:bone{q_op_bones:"1"} 0
execute if score @s q_op_bones matches 0.. store result score @s q_op_bone_blocks run clear @s minecraft:iron_ingot{q_op_bones:"1"} 0
execute as @s[type=player,tag=!q_op_bones_collected,scores={q_op_bones=50..}] run function quests:outpost/bad_bones/step_1
execute as @s[type=player,tag=q_op_bones_collected,tag=talkBlacksmith,tag=!q_op_bones_melted] unless score @s q_op_bone_blocks matches 0.. run function quests:outpost/bad_bones/step_2
execute as @s[type=player,tag=!q_op_bones_melted,tag=!q_op_bones_elerium,scores={q_op_bone_blocks=7..}] run function quests:outpost/bad_bones/step_3
execute as @s[type=player,tag=q_op_bones_melted,tag=!q_op_bones_elerium,tag=!q_op_bones_armor,nbt={Inventory:[{id:"minecraft:quartz",tag:{elerium:"1"}}]}] run function quests:outpost/bad_bones/step_4
execute as @s[type=player,tag=q_op_bones_elerium,tag=!q_op_bones_armor,nbt={Inventory:[{id:"minecraft:iron_chestplate",tag:{q_op_bones:"1"}}]}] run function quests:outpost/bad_bones/step_5
