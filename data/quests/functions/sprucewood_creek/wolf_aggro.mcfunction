#define tag wolfAggro "Players has been marked as a wolf target"
#define tag playerAggro "wolfs that are aggroed at player"
execute as @e[type=wolf,distance=..100] run tag @s add playerAggro
tellraw @s {"text":"You hear a malevolent howling in the forest...","italic":true,"color":"gray"}
execute at @e[type=wolf,tag=playerAggro] run summon snowball ~ ~2 ~
execute at @e[type=wolf,tag=playerAggro] run data modify entity @e[type=snowball,sort=nearest,limit=1] owner.L set from entity @s UUIDLeast
execute at @e[type=wolf,tag=playerAggro] run data modify entity @e[type=snowball,sort=nearest,limit=1] owner.M set from entity @s UUIDMost
tag @e[tag=playerAggro] remove playerAggro
tag @s add wolfAggro