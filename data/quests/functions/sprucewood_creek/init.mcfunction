#define tag q_sc_book
execute as @s[tag=!q_sc_book] at @s run function utils:quest_new
tag @s[tag=!q_sc_book] add q_sc_book
#define objective q_sc_wolves "Wolves killed at Sprucewood Creek"
execute unless score @s q_sc_wolves matches 1.. run scoreboard players set @s q_sc_wolves 0
execute unless score @s reputation matches -100.. run scoreboard players set @s reputation 0
function quests:notify/q_sc_wolves
function quests:notify/q_sc_rep