function quests:emporium/vendors/pre_trade
execute as @s[tag=!inventoryFull,nbt=!{Inventory:[{id:"minecraft:book",tag:{"emporium":"1"}},{id:"minecraft:heart_of_the_sea",tag:{reputation:"1"}}]}] run tellraw @s ["",{"text":"\u26a0","color":"red"},{"text":" You need a "},{"text":"Reputation Token","color":"dark_green"},{"text":" and an "},{"text":"Emporium Bond","color":"blue"}]
execute as @s[tag=!inventoryFull,nbt={Inventory:[{id:"minecraft:book",tag:{"emporium":"1"}},{id:"minecraft:heart_of_the_sea",tag:{reputation:"1"}}]}] run function quests:emporium/vendors/gift/exchange_rep_token
