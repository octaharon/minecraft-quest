clear @s minecraft:heart_of_the_sea{reputation:"1"} 1
clear @s minecraft:book{emporium:"1"} 1
playsound minecraft:block.conduit.activate player @s ~ ~ ~ 1
playsound minecraft:entity.player.levelup player @s ~ ~ ~ 1
execute at @s run particle ambient_entity_effect ~ ~1 ~ 0.25 0.5 0.25 0.2 100
function quests:items/currency/emporium_gift
tellraw @s ["",{"text":"Reputation Token","color":"dark_green"},{"text":" and an "},{"text":"Emporium Bond","color":"blue","bold": true},{"text":" lost, gained "},{"text":"1 Emporium Gift","color":"red"}]