clear @s minecraft:heart_of_the_sea{reputation:"1"} 1
playsound minecraft:block.conduit.activate player @s ~ ~ ~ 1
playsound minecraft:entity.player.levelup player @s ~ ~ ~ 1
execute at @s run particle happy_villager ~ ~1 ~ 0.25 0.5 0.25 0.2 100
scoreboard players add @s reputation 1
tellraw @s ["",{"text":"Reputation Token","color":"dark_green"},{"text":" lost, gained "},{"text":"1 Reputation","color":"green"}]