tellraw @s ["",{"text":"You've lost ","italic":true,"color":"gray"},{"text":"1 Emporium Gift","bold":true,"color":"red"}]
clear @s minecraft:gold_ingot{"emporium":"1"} 1
playsound minecraft:block.chest.locked player @s ~ ~ ~ 3 0.75
loot replace block 1394 120 10178 container.0 loot chests:epic
execute positioned 1394 121 10178 run function utils:celebrate
tellraw @s ["",{"text":"You hear a rumbling roar from above:","italic":true,"color":"gray"},{"text":" Spoils of war are bestowed upon you...","color":"light_purple"}]
playsound minecraft:entity.ender_dragon.ambient player @s ~ ~ ~ 4 0.5
playsound minecraft:entity.wither.spawn player @s ~ ~ ~ 2 2