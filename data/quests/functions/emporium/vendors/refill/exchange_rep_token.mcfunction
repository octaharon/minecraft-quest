clear @s minecraft:heart_of_the_sea{reputation:"1"} 1
clear @s minecraft:book{emporium:"1"} 1
playsound minecraft:block.conduit.activate player @s ~ ~ ~ 1
playsound minecraft:entity.player.levelup player @s ~ ~ ~ 1
execute at @s run particle ambient_entity_effect ~ ~1 ~ 0.25 0.5 0.25 0.2 100
tag @s remove emp_chest_se
tag @s remove emp_chest_nw
tag @s remove emp_chest_brewery
tag @s remove emp_chest_ne
tellraw @s ["",{"text":"Reputation Token","color":"dark_green"},{"text":" lost, all common chests and barrels at  "},{"text":"The Emporium","color":"green"},{"text":" have been refilled"}]