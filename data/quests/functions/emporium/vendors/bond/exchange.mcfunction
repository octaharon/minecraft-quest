clear @s minecraft:clay_ball{emporium:"1"} 3
clear @s minecraft:quartz 1
playsound minecraft:block.conduit.activate player @s ~ ~ ~ 1
playsound minecraft:entity.player.levelup player @s ~ ~ ~ 1
execute at @s run particle ambient_entity_effect ~ ~1 ~ 0.25 0.5 0.25 0.2 100
function quests:items/currency/emporium_bond
tellraw @s ["",{"text":"3 Emporium Coins","color":"aqua","italic": true},{"text":" and Nether Quartz lost, gained "},{"text":"1 Emporium Bond","color":"blue"}]