#define tag emp_special_unlocked
playsound minecraft:block.chest.locked player @s[tag=!emp_special_unlocked] ~ ~ ~ 4 1
tellraw @s[tag=!emp_special_unlocked] ["",{"text":"\u26a0","bold":true,"color":"red"},{"text":" Out of stock!","italic": true}]
execute as @s[tag=emp_special_unlocked] run function quests:emporium/vendors/pre_trade
tellraw @s[tag=!inventoryFull,scores={emp_coins=0},tag=emp_special_unlocked] ["",{"text":"\u26a0","color":"red"},{"text":" You need an"},{"text":" Emporium Coin","color":"aqua","italic": true}]
execute as @s[tag=!inventoryFull,scores={emp_coins=1..},tag=emp_special_unlocked] run function quests:emporium/vendors/brewing/special/exchange