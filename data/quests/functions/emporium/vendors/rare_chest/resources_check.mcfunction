function quests:emporium/vendors/pre_trade
execute as @s[tag=!inventoryFull] unless entity @s[scores={emp_bonds=3..}] run tellraw @s ["",{"text":"\u26a0","color":"red"},{"text":" You need "},{"text":"3 Emporium Bonds","color":"blue","bold": true}]
execute as @s[tag=!inventoryFull,scores={emp_bonds=3..}] run function quests:emporium/vendors/rare_chest/exchange