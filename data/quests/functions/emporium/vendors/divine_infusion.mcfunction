tellraw @s ["",{"text":"You hear a rumbling roar from above:","italic":true,"color":"gray"},{"text":" Strike unexpected. Crush everyone. See your foes crumble before you...", "color":"light_purple"}]
clear @s minecraft:gold_ingot{"emporium":"1"} 1
effect give @s absorption 600 10
execute at @s run function utils:celebrate
playsound minecraft:entity.ender_dragon.ambient player @s ~ ~ ~ 4 0.5
playsound minecraft:entity.wither.spawn player @s ~ ~ ~ 2 2
function crafting:consumables/divine_infusion