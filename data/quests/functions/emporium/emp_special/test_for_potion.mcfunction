# Potion quality at stored at `potionQuality`
#define tag craftedEmpSpecial
tag @s[tag=!craftedEmpSpecial,scores={potionQuality=8..}] add craftedEmpSpecial
tellraw @s[tag=!craftedEmpSpecial,scores={q_emp_special=0..1}] ["",{"text":"\u26a0 ","color":"red"},{"text":"That's not good enough to be accepted for trade","color":"gray","italic": true}]
execute as @s[tag=craftedEmpSpecial,scores={q_emp_special=0..1}] at @s run function utils:quest_step
scoreboard players set @s[tag=craftedEmpSpecial,scores={q_emp_special=0..1}] q_emp_special 2
execute if score @s q_emp_special matches 0.. run function quests:notify/q_emp_special