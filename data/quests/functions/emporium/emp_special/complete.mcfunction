scoreboard players set @s q_emp_special 4
scoreboard players add @s reputation 1
tag @s add emp_special_unlocked
clear @s written_book{shipment:"exampleton"} 1
function quests:items/currency/emporium_gift
experience add @s 50 points
tellraw @s ["",{"text":"You've gained ","color":"white","italic":false},{"text": "1 Reputation","color":"green"},{"text":" and ","color":"white"},{"text":"1 Emporium Gift","color":"red","bold": true}]
title @s subtitle ["",{"text":"Establish a supply of "},{"text":"Emporium Special","color":"gold","bold": true}]
function utils:quest_complete