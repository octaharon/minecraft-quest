# A player without a Leather Horse Armor recipes gets one
execute if entity @s[nbt=!{recipeBook:{recipes:["crafting:horse_iron_armor"]}}] run scoreboard players set @s oracle_advice 10

# A player has Ocelot quest, has the recipe and has spawned some
execute if entity @s[scores={q_ss_ocelot=1..4},tag=q_ss_ocelot_started,tag=!q_ss_ocelot_complete,nbt={recipeBook:{recipes:["crafting:ocelot_egg"]}}] run scoreboard players set @s oracle_advice 1
# A player has Ocelot quest, has the recipe but has not spawned any
execute if entity @s[scores={q_ss_ocelot=0},tag=q_ss_ocelot_started,tag=!q_ss_ocelot_complete,nbt={recipeBook:{recipes:["crafting:ocelot_egg"]}}] run scoreboard players set @s oracle_advice 2
# A player has Ocelot quest, but doesn't have a recipe
execute if entity @s[tag=q_ss_ocelot_started,tag=!q_ss_ocelot_complete,nbt=!{recipeBook:{recipes:["crafting:ocelot_egg"]}}] run scoreboard players set @s oracle_advice 3

# A player has Decipherer but doesn't have a Ziggurat quest
execute if entity @s[nbt={Inventory:[{id:"minecraft:compass",tag:{decryptor:"1"}}]}] unless score @s q_sh_cipher matches 0.. run scoreboard players set @s oracle_advice 4
# A player has Decipherer, has Ziggurat quest but hasn't deciphered any blocks
execute if entity @s[nbt={Inventory:[{id:"minecraft:compass",tag:{decryptor:"1"}}]}] if score @s q_sh_cipher matches 0..0 run scoreboard players set @s oracle_advice 5
# A player has started Ziggurat quest but has no Decipherer
execute if entity @s[nbt=!{Inventory:[{id:"minecraft:compass",tag:{decryptor:"1"}}]}] if score @s q_sh_cipher matches 0..0 run scoreboard players set @s oracle_advice 6
# A player has started Ziggurat and deciphered all blocks, but hasn't found Xerus
execute if score @s[tag=!spawnedXerus] q_sh_cipher matches 8..8 run scoreboard players set @s oracle_advice 7


# Messages

execute if score @s oracle_advice matches 10..10 run function quests:emporium/oracle/advices/iron_horse_armor

tellraw @s[scores={oracle_advice=1}] ["",{"text":"The wind whispers:","italic":true,"color":"gray"},{"text":" If you're in need for more alchemic supplies for your environmental efforts, check out "},{"text":"The Farm","color":"yellow"},{"text":" to the north of here (1625, 9750)"}]

tellraw @s[scores={oracle_advice=2}] ["",{"text":"The wind whispers:","italic":true,"color":"gray"},{"text":" yes, "},{"text":"Ocelots","color":"dark_aqua"},{"text":" are expensive to spawn, but you can setup a sustainable production chain if you also breed "},{"text":"Rabbits","color":"dark_aqua"},{"text":" and "},{"text":"Cats","color":"dark_aqua"},{"text":". "},{"text":"The University","color":"dark_green"},{"text":" has recipes for all of them."}]

tellraw @s[scores={oracle_advice=3}] ["",{"text":"The wind whispers:","italic":true,"color":"gray"},{"text":" so, "},{"text":"Snake Shrine","color":"dark_green"},{"text":" folks sent you on a quest to find "},{"text":"Ocelots","color":"dark_aqua"},{"text":"? If any of them cared to enter "},{"text":"The University","color":"dark_green"},{"text":", they could have learned to reproduce them on their own..."}]

tellraw @s[scores={oracle_advice=4}] ["",{"text":"The wind whispers:","italic":true,"color":"gray"},{"text":" oh, that item you have... There's an ancient place in the far west where you can probably find a worthy riddle for it"}]

tellraw @s[scores={oracle_advice=5}] ["",{"text":"The wind whispers:","italic":true,"color":"gray"},{"text":" So, you are equipped... After all those years, the "},{"text":"Mystery of The Ziggurat","color":"gold"},{"text":" is now destined to be unraveled."},{"text":" ","italic":true}]

tellraw @s[scores={oracle_advice=6}] ["",{"text":"The wind whispers:","italic":true,"color":"gray"},{"text":" So, "},{"text":"The Ziggurat","color":"red"},{"text":" has called you... Drop by at "},{"text":"Exampleton","color":"dark_green"},{"text":", they have what you need."}]

tellraw @s[scores={oracle_advice=7}] ["",{"text":"The wind whispers:","italic":true,"color":"gray"},{"text":" Well, now you know the history of "},{"text":"Xerus The Deceiver","color":"gold"},{"text":", right? It's only a matter of time until you find his grave. You'd start looking to the south-east of "},{"text":"Exampleton","color":"dark_green"},{"text":"."}]