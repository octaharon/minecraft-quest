# A player without a Horse Egg recipes gets one
execute if entity @s[nbt=!{recipeBook:{recipes:["crafting:horse_egg"]}}] run scoreboard players set @s oracle_advice 10

# A player is on "Bricks from the End" quest and has university recipe and started crafting
execute if entity @s[scores={q_hh_end_stone=1..29},tag=q_hh_end_stone_started,tag=!q_hh_end_stone_complete,nbt={recipeBook:{recipes:["crafting:end_stone_bricks"]}}] run scoreboard players set @s oracle_advice 1
# A player is on "Bricks from the End" quest and has not the university recipe and started crafting
execute if entity @s[scores={q_hh_end_stone=1..29},tag=q_hh_end_stone_started,tag=!q_hh_end_stone_complete,nbt=!{recipeBook:{recipes:["crafting:end_stone_bricks"]}}] run scoreboard players set @s oracle_advice 2
# A player is on "Bricks from the End" quest and has university recipe and hadn't started crafting
execute if entity @s[scores={q_hh_end_stone=0},tag=q_hh_end_stone_started,tag=!q_hh_end_stone_complete,nbt={recipeBook:{recipes:["crafting:end_stone_bricks"]}}] run scoreboard players set @s oracle_advice 3
# A player is on "Bricks from the End" quest and has not the university recipe and hadn't started crafting
execute if entity @s[scores={q_hh_end_stone=0},tag=q_hh_end_stone_started,tag=!q_hh_end_stone_complete,nbt=!{recipeBook:{recipes:["crafting:end_stone_bricks"]}}] run scoreboard players set @s oracle_advice 4
# A player has Gleamsville Amulet but doesn't have the quest
execute if entity @s[nbt={Inventory:[{id:"minecraft:ghast_tear",tag:{q_gleamsville:"amulet"}}]}] unless score @s q_q_gv_amulet matches 0.. run scoreboard players set @s oracle_advice 5
# A player has Gleamsville Amulet quest but doesn't have an item yet
execute if entity @s[nbt=!{Inventory:[{id:"minecraft:ghast_tear",tag:{q_gleamsville:"amulet"}}]}] if score @s q_q_gv_amulet matches 0..0 run scoreboard players set @s oracle_advice 6

# Messages

tellraw @s[scores={oracle_advice=3}] ["",{"text":"The wind whispers: ","italic":true,"color":"gray"},{"text":"Hanging Hills","color":"dark_green"},{"text":" is expecting for you to craft more "},{"text":"End Stone Bricks","color":"dark_aqua"},{"text":", and you possess the best recipe available, so what are you waiting for? If you're short on supplies, try some vendors around here"}]

tellraw @s[scores={oracle_advice=2}] ["",{"text":"The wind whispers: ","italic":true,"color":"gray"},{"text":"It seems you had some success crafting "},{"text":"End Stone Bricks","color":"dark_aqua"},{"text":", but it would be much easier if you had a simpler recipe from "},{"text":"the University","color":"dark_green"}]

tellraw @s[scores={oracle_advice=1}] ["",{"text":"The wind whispers: ","italic":true,"color":"gray"},{"text":"Struggling to craft more "},{"text":"End Stone Bricks","color":"dark_aqua"},{"text":"? Check out local vendors."}]

tellraw @s[scores={oracle_advice=4}] ["",{"text":"The wind whispers: ","italic":true,"color":"gray"},{"text":"The Univerisity","color":"dark_green"},{"text":" conducts studies on masonry, construction and alchemy, among others. They for sure have some recipes you might put to use."}]

tellraw @s[scores={oracle_advice=5}] ["",{"text":"The wind whispers: ","italic":true,"color":"gray"},{"text":"A precious item you possess, it seems. There's a village far in the west which would love to have it back."}]

tellraw @s[scores={oracle_advice=6}] ["",{"text":"The wind whispers:","italic":true,"color":"gray"},{"text":" Rich people love building fancy stuff, like mazes. On a coincidence, there's one to the north-east from here, which might contain what you're looking for..."}]

execute if score @s oracle_advice matches 10..10 run function quests:emporium/oracle/advices/horse_egg