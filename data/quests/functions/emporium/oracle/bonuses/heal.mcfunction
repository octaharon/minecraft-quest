effect clear @s wither
effect clear @s poison
effect clear @s blindness
effect clear @s weakness
effect give @s regeneration 1 0
effect give @s saturation 1 0
execute at @s run particle minecraft:heart ~ ~ ~ 0.1 0.4 0.1 0.01 2
particle minecraft:composter ~ ~.5 ~ 0.2 0.5 0.2 0.05 2