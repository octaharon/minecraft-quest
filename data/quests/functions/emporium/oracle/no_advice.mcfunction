function quests:emporium/oracle/set_random
execute if score @s randomizer matches ..1 run function quests:emporium/oracle/bonuses/bonus1
execute if score @s randomizer matches 2..2 run function quests:emporium/oracle/bonuses/bonus2
execute if score @s randomizer matches 3..3 run function quests:emporium/oracle/bonuses/bonus3
execute if score @s randomizer matches 4..4 run function quests:emporium/oracle/bonuses/bonus4
execute if score @s randomizer matches 5.. run function quests:emporium/oracle/bonuses/bonus5