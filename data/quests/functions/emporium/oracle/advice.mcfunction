#define objective oracle_advice Oracle random value (Emporium)
scoreboard players set @s oracle_advice 0
function quests:emporium/oracle/set_random
execute if score @s randomizer matches ..1 run function quests:emporium/oracle/advices/piece1
execute if score @s randomizer matches 2..2 run function quests:emporium/oracle/advices/piece2
execute if score @s randomizer matches 3..3 run function quests:emporium/oracle/advices/piece3
execute if score @s randomizer matches 4..4 run function quests:emporium/oracle/advices/piece4
execute if score @s randomizer matches 5.. run function quests:emporium/oracle/advices/piece5
execute if score @s oracle_advice matches 0..0 run function quests:emporium/oracle/no_advice
experience add @s -15 points
playsound minecraft:block.note_block.bell player @s ~ ~ ~ 4
playsound minecraft:ambient.underwater.exit player @s ~ ~ ~ 4
tellraw @s ["",{"text": "You've paid ","italic": true},{"text": "15 XP","color":"green","italic": true},{"text": " to the Oracle\n","italic": true}]
