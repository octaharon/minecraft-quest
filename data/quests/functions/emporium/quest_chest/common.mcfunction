#define tag usedQuestChest
playsound minecraft:block.note_block.chime ambient @s ~ ~ ~ 1 0.75
playsound quests:ui.bell neutral @s ~ ~ ~ 0.8 1
particle minecraft:firework ~ ~0.5 ~ 0.15 0.55 0.15 0.08 250
particle minecraft:effect ~ ~ ~ 0.15 0.5 0.15 0.01 100
tag @s[tag=!usedQuestChest] add usedQuestChest