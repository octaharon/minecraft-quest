#define objective q_ss_blueprint "Gateway Blueprint progression"
#define objective q_hh_end_stone "End Stones quest progression"
#define tag q_hh_end_stone_complete
#define tag q_hh_end_stone_started
#define tag q_hh_book
execute as @s[tag=!q_hh_book] at @s run function utils:quest_new
tag @s[tag=!q_hh_book] add q_hh_book
execute unless score @s q_ss_blueprint matches 1.. run scoreboard players set @s q_ss_blueprint 0
function quests:notify/q_ss_blueprint
scoreboard players set @s[tag=!q_hh_end_stone_started,tag=!q_hh_end_stone_complete] q_hh_end_stone 0
function quests:notify/q_hh_end_stone
tag @s[tag=!q_hh_end_stone_started,tag=!q_hh_end_stone_complete] add q_hh_end_stone_started