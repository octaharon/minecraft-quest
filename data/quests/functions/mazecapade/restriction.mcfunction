#define tag enteredMazecapade
tellraw @s [{"text": "An unknown force catches you and drags to the entrance of the maze","italic": true,"color":"aqua"}]
effect give @s instant_damage 1 0
tp @s 1706 68 9924 -180 0
execute at @s run playsound block.end_portal_frame.fill player @s ~ ~ ~ 4
execute at @s run playsound item.firecharge.use player @s ~ ~ ~ 4
execute as @a[tag=enteredMazecapade] unless entity @s[x=1692,y=68,z=9858,dx=75,dy=60,dz=62] run tag @s remove enteredMazecapade