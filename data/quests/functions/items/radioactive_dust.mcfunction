give @s glowstone_dust{q_sh_dust:"1",display:{Name:'[{"text":"Radioactive Dust","italic":false,"color":"green"}]', Lore:['[{"text":"There is no power on Earth which could have created this substance. It hurts even to look at it.","italic":false,"color":"dark_aqua"}]']},HideFlags:60,AttributeModifiers:[{AttributeName:generic.maxHealth,Amount:-4,Operation:0,UUIDLeast:125641700,UUIDMost:99,Slot:mainhand,Name:generic.maxHealth},{AttributeName:generic.maxHealth,Amount:-4,Operation:0,UUIDLeast:125641800,UUIDMost:99,Slot:offhand,Name:generic.maxHealth}]} 1
tellraw @s {"text":"You lean down and scratch up a handful of glowing substance. You don't know what it is, but this is probably the source of that grievous emanation around","italic":true,"color":"aqua"}
title @s actionbar ["",{"text":"Radioactive Dust ","color":"green"},{"text":"found!"}]
playsound minecraft:block.glass.break player @s ~ ~ ~ 1
scoreboard players set @s q_sh_dust 1

