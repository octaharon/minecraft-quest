#define objective emp_coins "Emporium Coins owned"
#define objective emp_bonds "Emporium Bonds owned"
#define objective trade_diamonds "Diamonds owned"
execute store result score @s emp_coins run clear @s minecraft:clay_ball{"emporium":"1"} 0
execute store result score @s emp_bonds run clear @s minecraft:book{"emporium":"1"} 0
execute store result score @s trade_diamonds run clear @s minecraft:diamond 0