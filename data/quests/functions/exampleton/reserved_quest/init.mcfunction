#define objective q_ex_reserved "Quest progression on Exampleton Reserved quest"
#define tag q_ex_reserved_woodstock
#define tag q_ex_reserved_gleamsville
#define tag q_ex_reserved_outpost
execute unless score @s q_ex_reserved matches 1.. run scoreboard players set @s q_ex_reserved 0
function quests:notify/q_ex_reserved