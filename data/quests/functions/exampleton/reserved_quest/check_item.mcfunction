# Test for Exampleton Reserved, assuming the quest completion check is done before
function quests:exampleton/reserved_quest/test_for_quality

## Potion quantity doesn't match
execute if entity @s[nbt={SelectedItem:{id:"minecraft:potion",tag:{CustomPotionColor:16777200}}}] unless entity @s[nbt={SelectedItem:{Count:1b}}] run tellraw @s [{"text":"\u26a0 ","color":"red"},{"text": "You should be holding a single bottle!","color":"white"}]
## Potion quality doesn't match
execute if entity @s[nbt={SelectedItem:{Count:1b,id:"minecraft:potion",tag:{CustomPotionColor:16777200}}}] unless score @s potionQuality matches 10.. run tellraw @s [{"text":"\u26a0 ","color":"red"},{"text": "You should be offering a 5-star bottle as a sample!","color":"white"}]
## 1 5-Star potion is chosen
execute if entity @s[nbt={SelectedItem:{Count:1b, id:"minecraft:potion",tag:{CustomPotionColor:16777200}}}] if score @s potionQuality matches 10.. run function quests:exampleton/reserved_quest/give_in
execute if score @s q_ex_reserved matches 3 run function quests:notify/q_ex_reserved