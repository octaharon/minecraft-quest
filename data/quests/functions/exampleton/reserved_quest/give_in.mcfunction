scoreboard players add @s q_ex_reserved 1
title @s actionbar [{"text":"You've delivered a bottle of ","color":"white"},{"text":"Exampleton Reserved","color":"light_purple"}]
tellraw @s [{"text":"\u26a0 ","color":"gold"},{"text": "Bottles delivered: ","color":"gold"},{"score": {"name": "@s","objective": "q_ex_reserved"},"color":"dark_purple"},{"text":"/","color":"white"},{"text": "3","color":"light_purple"}]
execute at @s run playsound entity.player.levelup player @s ~ ~ ~ 1 1
#replaceitem entity @s weapon.mainhand minecraft:air
clear @s minecraft:potion{PublicBukkitValues:{},CustomPotionColor:16777200} 1
execute at @s run particle entity_effect ~ ~1 ~ 0.25 0.5 0.25 0.05 125
experience add @s 20 points
tag @s[tag=talkGleamsville] add q_ex_reserved_gleamsville
tag @s[tag=talkOutpost] add q_ex_reserved_outpost
tag @s[tag=talkWoodstock] add q_ex_reserved_woodstock