#define tag ex_chest_treasury
execute positioned -11 74 8694 run function chests:quest/west
execute positioned -11 74 8691 run function chests:epic/west
execute positioned -11 74 8689 run function chests:random/rare/p100/west
execute positioned -16 74 8695 run function chests:basic/north
execute positioned -14 74 8692 run function chests:basic/east
execute positioned -14 74 8695 run function chests:quest/north
execute positioned -16 74 8695 run function chests:random/common/p100/north
execute positioned -18 74 8695 run function chests:smithing/east
execute positioned -14 74 8689 run function chests:extra/south
execute positioned -16 74 8689 run function chests:quest/south
execute positioned -18 74 8689 run function chests:basic/barrel
execute positioned -18 74 8692 run function chests:alchemy/barrel
execute positioned -18 75 8690 run function chests:epic/barrel
clone -18 72 8694 -18 72 8694 -18 75 8693 replace
tag @s add ex_chest_treasury