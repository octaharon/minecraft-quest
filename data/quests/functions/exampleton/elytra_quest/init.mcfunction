#define tag hasElytraRecipe
#define objective q_ex_elytra
#define tag q_ex_elytra_crafted
#define tag q_ex_elytra_finished
execute unless score @s q_ex_elytra matches 0.. run function utils:quest_new
execute as @s[tag=!hasElytraRecipe] run function crafting:recipes/elytra
execute unless score @s q_ex_elytra matches 0.. run scoreboard players set @s q_ex_elytra 0
function quests:notify/q_ex_elytra