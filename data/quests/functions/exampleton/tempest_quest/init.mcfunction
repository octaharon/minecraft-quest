#define objective q_ex_tempest Tempest quest progression
#define tag q_ex_tempest_crafted
execute unless score @s q_ex_tempest matches 0.. run function utils:quest_new
execute unless score @s q_ex_tempest matches 0.. run scoreboard players set @s q_ex_tempest 0
function quests:notify/q_ex_tempest