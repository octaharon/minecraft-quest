#define tag thunderOffering
clear @s blaze_rod{q_offering:"thunder"} 1
tellraw @s ["",{"text":"A thunder strikes, and you hear a deafening roar in your head:"},{"text":" Your offering has been accepted...","italic":true,"color":"gold"}]
weather rain
playsound item.trident.thunder player @s ~ ~ ~ 4 0.8
playsound entity.lightning_bolt.thunder player @s ~ ~ ~ 0.8 1.2
playsound entity.lightning_bolt.thunder player @s ~ ~ ~ 2 0.5
playsound entity.lightning_bolt.impact player @s ~ ~ ~ 4 0.5
effect give @s haste 300 0
effect give @s health_boost 300 3
effect give @s jump_boost 300 1
playsound ui.toast.challenge_complete player @s[tag=!thunderOffering] ~ ~ ~ 4
title @s[tag=!thunderOffering] actionbar ["",{"text":"Gained","color":"white"},{"text":" 2 Reputation","color":"green"}]
scoreboard players add @s[tag=!thunderOffering] reputation 2
tag @s[tag=!thunderOffering] add thunderOffering
