fill 963 98 10269 1048 103 10327 nether_quartz_ore replace barrel[facing=up]
fill 963 98 10269 1048 103 10327 iron_ore replace barrel[facing=west]
fill 963 98 10269 1048 103 10327 gold_ore replace barrel[facing=south]
fill 963 98 10269 1048 103 10327 coal_ore replace barrel[facing=east]
fill 963 98 10269 1048 103 10327 diamond_ore replace barrel[facing=north]
kill @e[type=item,x=963,y=98,z=10269,dx=85,dy=5,dz=58]
fill 963 98 10269 1048 103 10327 barrel[facing=up]{LootTable:"quests:barrel_uni_spellpass"} replace diamond_ore
fill 963 98 10269 1048 103 10327 barrel[facing=west]{LootTable:"quests:barrel_uni_spellpass"} replace coal_ore
fill 963 98 10269 1048 103 10327 barrel[facing=south]{LootTable:"quests:barrel_uni_spellpass"} replace nether_quartz_ore
fill 963 98 10269 1048 103 10327 barrel[facing=east]{LootTable:"quests:barrel_uni_spellpass"} replace gold_ore
fill 963 98 10269 1048 103 10327 barrel[facing=north]{LootTable:"quests:barrel_uni_spellpass"} replace iron_ore
execute positioned 1049 98 10327 run function chests:quest/west
execute positioned 1036 98 10298 run function mobs:summon/necromancer
tag @s add uni_chest_east_attic