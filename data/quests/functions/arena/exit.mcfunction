kill @e[type=item,x=-538,y=77,z=-931,dx=18,dy=10,dz=30]
kill @e[tag=monster,x=-538,y=77,z=-931,dx=16,dy=10,dz=30]
kill @e[type=vex,x=-538,y=77,z=-931,dx=16,dy=10,dz=30]
kill @e[type=#minecraft:raiders,x=-538,y=77,z=-931,dx=16,dy=10,dz=30]
kill @e[type=#minecraft:skeletons,x=-538,y=77,z=-931,dx=16,dy=10,dz=30]
playsound minecraft:ambient.underwater.enter player @s ~ ~ ~ 0.5 2
playsound minecraft:ambient.underwater.enter player @s ~ ~ ~ 1 0.8
tp @a[distance=..3] -518 79 -914 0 0
execute at @s run particle portal ~ ~ ~ 0.2 0.5 0.2 0.01 1000
tellraw @s ["",{"text":"\u2691","color":"yellow","bold": true},{"text": " You've left a combat simulation","italic": true,"color":"dark_aqua"}]