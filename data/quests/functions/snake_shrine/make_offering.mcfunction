scoreboard players set @s q_ss_offering 2
clear @s rabbit_foot{q_offering:"snake"} 1
tellraw @s ["",{"text":"A hissing voice is whispering inside your head:"},{"text":" Your offering has been accepted...","italic":true,"color":"gold"}]
execute positioned 1055 69 9796 run function utils:clone_quest_chest
title @p[scores={q_ss_offering=2},distance=..8] subtitle ["",{"text":"Make an offering at "},{"text":"Snake Shrine","color":"dark_green"}]
function utils:quest_complete
scoreboard players add @s reputation 2
scoreboard players set @s q_ss_offering 3