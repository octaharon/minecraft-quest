#define tag q_ss_ocelot_complete
execute positioned 1055 69 9794 run function utils:clone_quest_chest
title @s subtitle ["",{"text":"Spawn "},{"text":"5 Ocelots","color":"dark_aqua"}]
function utils:quest_complete
scoreboard players add @s reputation 2
tag @s add q_ss_ocelot_complete
