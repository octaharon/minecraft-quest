#define objective q_ss_offering "Snake Shrine Offering quest progression"
#define objective q_ss_ocelot "Need more cats quest progression"
#define tag q_ss_ocelot_started
#define tag q_ss_book
execute as @s[tag=!q_ss_book] at @s run function utils:quest_new
tag @s[tag=!q_ss_book] add q_ss_book
execute unless score @s q_ss_offering matches 1.. run scoreboard players set @s q_ss_offering 0
function quests:notify/q_ss_offering
scoreboard players set @s[tag=!q_ss_ocelot_started,tag=!q_ss_ocelot_complete] q_ss_ocelot 0
function quests:notify/q_ss_ocelot
tag @s[tag=!q_ss_ocelot_started,tag=!q_ss_ocelot_complete] add q_ss_ocelot_started