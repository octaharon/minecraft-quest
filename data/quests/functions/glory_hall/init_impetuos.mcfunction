execute unless score @s q_impetuous matches 1.. run scoreboard players set @s q_impetuous 0
tellraw @s[scores={q_impetuous=0}] ["",{"text":"\u26a0 ","bold":true,"color":"gold"},{"text":"Investigate "},{"text":"The Impetuous","color":"yellow"},{"text":" wreckage"}]
tellraw @s[scores={q_impetuous=1}] ["",{"text":"\u26a0 ","bold":true,"color":"gold"},{"text":"Find another shipwreck"}]
tellraw @s[scores={q_impetuous=2}] ["",{"text":"\u26a0 ","bold":true,"color":"gold"},{"text":"Locate the "},{"text":"Research Center","color":"red"}]
tellraw @s[scores={q_impetuous=3}] ["",{"text":"\u26a0 ","bold":true,"color":"gold"},{"text":"Collect any traces of "},{"text":"Weapon Test Plans","color":"light_purple","italic": false}]
tellraw @s[scores={q_impetuous=4..}] ["",{"text":"You've successfully unraveled the "},{"text":"Weapon Test Plans","color":"light_purple","italic": false}]