#define tag windOffering
clear @s totem_of_undying{q_offering:"wind"} 1
tellraw @s ["",{"text":"A gust of wind deafens you abrubtly, and in amidst the silence you hear a resonating voice :"},{"text":" Your offering has been accepted...","italic":true,"color":"gold"}]
weather clear
playsound item.trident.thunder player @s ~ ~ ~ 4 0.8
playsound entity.wither.shoot player @s ~ ~ ~ 0.7 0.5
playsound entity.wither.shoot player @s ~ ~ ~ 0.8 1.2
playsound block.beacon.ambient player @s ~ ~ ~ 4 2
effect give @s haste 300 0
effect give @s health_boost 300 3
effect give @s strength 300 3
playsound ui.toast.challenge_complete player @s[tag=!windOffering] ~ ~ ~ 4
title @s[tag=!windOffering] actionbar ["",{"text":"Gained","color":"white"},{"text":" 2 Reputation","color":"green"}]
scoreboard players add @s[tag=!windOffering] reputation 2
tag @s[tag=!windOffering] add windOffering
