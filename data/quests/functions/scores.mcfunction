#define objective main_quest_a "Plot A progression"
#define objective main_quest_c "Plot C progression"
#define objective main_quest_b "Plot B progression"
#define objective reputation "Reputation"
scoreboard objectives add reputation dummy [{"text":"\u2698","bold":true,"color":"dark_green"},{"text":" Reputation ","color":"green"},{"text":"\u2698","bold":true,"color":"dark_green"}]
scoreboard objectives add main_quest_a dummy {"text":"Story Plot A progress","color": "dark_aqua"}
scoreboard objectives add main_quest_b dummy {"text":"Story Plot B progress","color": "dark_aqua"}
scoreboard objectives add main_quest_c dummy {"text":"Story Plot C progress","color": "dark_aqua"}