#define tag chestGleamsville
tag @s add chestGleamsville
effect give @s luck 60 1
execute at @s run title @p actionbar [{"text":"\u2691","color":"gold","bold": true},{"text": "You've earned a ","color":"gold"},{"text":" Bonus Chest "}]
execute positioned ~ ~2 ~ run function chests:extra/east
execute positioned ~ ~2 ~ run function utils:celebrate_extra
