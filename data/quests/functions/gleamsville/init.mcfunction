#define tag q_gv_book
execute as @s[tag=!q_gv_book] at @s run function utils:quest_new
tag @s[tag=!q_gv_book] add q_gv_book
execute as @s unless score @s q_gv_clock matches 1.. run scoreboard players set @s q_gv_clock 0
execute as @s unless score @s q_gv_diamond matches 1.. run scoreboard players set @s q_gv_diamond 0
execute as @s unless score @s q_gv_amulet matches 1.. run scoreboard players set @s q_gv_amulet 0
execute as @s run function quests:notify/q_gv_all