#define objective hasUniBook Player has had University Spellpass
#define objective monsterLimit Monsters around
#define tag enteredMazecapade "A player has entered the Mazecapade through the entrance"


## Exampleton
### Exampleton Secrets
execute positioned -51 74 8682 as @p[distance=..3,tag=!ex_chest_library] run function quests:exampleton/secrets/library
execute positioned -42 72 8655 as @p[distance=..3,tag=!ex_chest_tavern] run function quests:exampleton/secrets/tavern
execute positioned 28 60 8683 as @p[distance=..2,tag=!ex_underground_passage] run function quests:exampleton/secrets/treasury_entrance
execute positioned 1 72 8620 as @p[distance=..1,tag=!ex_chest_portal] run function quests:exampleton/secrets/hidden_portal
execute positioned -31 89 8663 as @p[distance=..4,tag=!ex_chest_church] run function quests:exampleton/secrets/church
execute as @a[tag=!ex_chest_treasury,x=-18,y=64,z=8689,dx=10,dy=15,dz=10] run function quests:exampleton/secrets/treasury_chests
### Library barrels 
execute positioned -49 74 8678 as @p[distance=..4,tag=!ex_barrels_library] run function quests:exampleton/library
### Quest Chest removal at Guesthouse
execute positioned -64 81 8675 as @a[distance=4..50,tag=usedQuestChest] run function quests:emporium/quest_chest/clear
### Elytra Quest
execute as @a[tag=hasElytraRecipe,scores={q_ex_elytra=1..},tag=!q_ex_elytra_crafted] at @s run function quests:exampleton/elytra_quest/crafted
execute as @a[tag=q_ex_elytra_crafted,tag=!q_ex_elytra_finished,tag=talkBlacksmith] at @s run function quests:exampleton/elytra_quest/complete
### Exampleton Reserved Quest turn-in
execute positioned -47 78 8655 as @p[distance=..5,scores={q_ex_reserved=3},tag=q_ex_reserved_gleamsville,tag=q_ex_reserved_outpost,tag=q_ex_reserved_woodstock] run function quests:exampleton/reserved_quest/complete
### Remove gamekeeper when exiting the Guild
execute as @e[type=villager,tag=ex_gamekeeper] at @s unless entity @p[distance=..32] run function quests:exampleton/vendors/gamekeeper/remove
### Hidden Teleport
execute positioned 1 69 8616 run function quests:exampleton/portal

## Emporium
### Emp Special quest
execute as @a[scores={q_emp_special=2}] if data entity @s SelectedItem.tag.shipment at @s run function quests:emporium/emp_special/step_3
execute positioned 1435 111 10140 as @a[scores={q_emp_special=3},distance=..5,nbt={Inventory:[{id:"minecraft:written_book",tag:{shipment:"exampleton"}}]}] run function quests:emporium/emp_special/complete
### Heal at Hero's Rest
execute positioned 1391 113 10176 as @a[scores={health=..19},distance=..4] run function quests:emporium/oracle/bonuses/heal
### Reset quest chest at Emporium
execute positioned 1396 116 10178 unless block ~ ~ ~ air unless entity @p[tag=usedQuestChest,distance=..4] run function quests:emporium/quest_chest/clear
### Animations
execute positioned 1396 123 10178 as @e[type=armor_stand,distance=..5,limit=1,sort=nearest] run function quests:emporium/vendors/totem_of_war
execute positioned 1435 119 10146 run particle minecraft:campfire_cosy_smoke ~ ~0.5 ~ 0 1 0 0.07 0

#Nightmare protection
execute as @e[type=phantom,distance=..50,x=1433,y=120,z=10177] run effect give @s instant_health 1 1

## Mobs population control
execute as @a[distance=..256,x=1000,y=10280,z=64] at @s store result score @s monsterLimit run execute if entity @e[type=wolf,distance=..64]
execute at @a[scores={monsterLimit=50..}] run kill @e[type=wolf,distance=..64,sort=random,limit=1]
#execute as @a store result score @s monsterLimit run execute if entity @e[type=bat]
#execute as @a[scores={monsterLimit=150..}] run kill @e[type=bat,limit=1,sort=random]
execute as @a store result score @s monsterLimit run execute if entity @e[type=pillager]
execute as @a[scores={monsterLimit=45..}] run kill @e[type=pillager,limit=1,sort=random]
### Lag reduction for 1.14
#execute as @e[type=#minecraft:raiders] run data merge entity @s {Patrolling:0b}

## University
### Thunder Shrine access
execute if entity @e[type=minecraft:item_frame,x=792,y=91,z=10325,distance=..1,nbt={Item:{id:"minecraft:book",tag:{"unlocks":"university"}}}] if block 791 91 10323 air run setblock 791 91 10323 minecraft:oak_button[facing=west] replace
execute unless block 791 91 10323 air unless entity @e[type=minecraft:item_frame,x=792,y=91,z=10325,distance=..1,nbt={Item:{id:"minecraft:book",tag:{"unlocks":"university"}}}] run setblock 791 91 10323 air replace
### Portal Room access
execute if entity @e[type=minecraft:item_frame,x=806,y=84,z=10319,distance=..1,nbt={Item:{id:"minecraft:book",tag:{"unlocks":"university"}}}] if block 805 84 10317 air run setblock 805 84 10317 minecraft:lever[facing=west] replace
execute unless block 805 84 10317 air unless entity @e[type=minecraft:item_frame,x=806,y=84,z=10319,distance=..1,nbt={Item:{id:"minecraft:book",tag:{"unlocks":"university"}}}] run setblock 805 84 10317 air replace
### Board Office access
execute if entity @e[type=minecraft:item_frame,x=1034,y=91,z=10309,distance=..1,nbt={Item:{id:"minecraft:book",tag:{"unlocks":"university"}}}] if block 1034 91 10311 air run setblock 1034 91 10311 minecraft:lever[facing=east] replace
execute unless block 1034 91 10311 air unless entity @e[type=minecraft:item_frame,x=1034,y=91,z=10309,distance=..1,nbt={Item:{id:"minecraft:book",tag:{"unlocks":"university"}}}] run setblock 1034 91 10311 air replace
### Return the book to player if he moves away from the lock
execute as @e[x=790,y=83,z=10308,dx=250,dy=9,dz=20,type=minecraft:item_frame,nbt={Item:{id:"minecraft:book",tag:{"unlocks":"university"}}}] at @s if entity @p[scores={hasUniBook=1},nbt=!{Inventory:[{id:"minecraft:book",tag:{"unlocks":"university"}}]},distance=6..] run function quests:university/return_book_lock
### Effects at the Cantine
execute positioned 817 73 10309 run function quests:university/cantine
### Portals
function quests:university/portal
### Mark the owners of University Spellock
execute as @a[scores={hasUniBook=0},nbt={Inventory:[{id:"minecraft:book",tag:{"unlocks":"university"}}]}] run scoreboard players set @s hasUniBook 1

## Resort 
function quests:resort/tick

## Gleamsville
### Quests
execute positioned -1032 66 9501 as @p[distance=..10,scores={q_gv_diamond=0..1},nbt={Inventory:[{id:"minecraft:diamond",tag:{q_gleamsville:"diamond"}}]}] run function quests:gleamsville/return_diamond
execute positioned -1031 66 9501 as @p[distance=..8,scores={q_gv_amulet=0..1},nbt={Inventory:[{id:"minecraft:gold_nugget",tag:{q_gleamsville:"amulet"}}]}] run function quests:gleamsville/return_amulet
execute positioned -1028 66 9501 as @p[distance=..12,scores={q_gv_amulet=3,q_gv_clock=3,q_gv_diamond=3},tag=!chestGleamsville] run function quests:gleamsville/complete_reward
#### Portal Key
execute positioned -1030 66 9501 as @p[distance=..10,scores={q_gv_clock=0..1},nbt={Inventory:[{id:"minecraft:clock",tag:{q_gleamsville:"clock"}}]}] run function quests:gleamsville/return_clock

## Sandy Haven
### Quests
execute positioned -840 69 8302 as @p[distance=..12,scores={q_sh_dust=0..1},nbt={Inventory:[{tag:{q_sh_dust:"1"}}]}] run function quests:sandy_haven/return_dust
#### Portal Key
execute positioned -838 69 8302 as @p[distance=..12,scores={q_sh_cipher=8},nbt={Inventory:[{id:"minecraft:firework_star",tag:{q_sh_cipher:"1"}}]}] run function quests:sandy_haven/return_artifact

## Sprucewood
### Quests
#### Portal Key
execute positioned 1113 69 10281 as @p[distance=..10,tag=!scRepCheck,scores={reputation=5..}] run function quests:sprucewood_creek/rep_check
execute positioned 1113 69 10279 as @p[distance=..10,tag=!scWolvesKilled,scores={q_sc_wolves=15..}] run function quests:sprucewood_creek/wolf_reward
### Wolf 
execute as @p[x=1088,y=88,z=10275,distance=..32,scores={q_sc_wolves=0..14},tag=!wolfAggro] at @s run function quests:sprucewood_creek/wolf_aggro

## Hanging Hills
### Quests
execute positioned 261 63 10037 as @a[distance=..10,tag=!q_hh_end_stone_complete,scores={q_hh_end_stone=30..}] at @s if entity @s[tag=q_hh_end_stone_started] run function quests:hanging_hills/end_stone_complete
execute positioned 261 63 10035 as @a[distance=..10,scores={q_ss_blueprint=0..1},nbt={Inventory:[{id:"minecraft:book",tag:{q_ss_blueprint:"1"}}]}] at @s run function quests:hanging_hills/return_blueprint

## Outpost
### Bad Bones
execute as @a[tag=!q_op_bones_complete] at @s run function quests:outpost/bad_bones/set_score
execute positioned 741 61 10760 as @a[tag=q_op_bones_collected,tag=q_op_bones_melted,tag=q_op_bones_elerium,tag=!q_op_bones_complete,distance=..15,scores={hasBoneArmor=1..}] run function quests:outpost/bad_bones/complete
### Fishery
function quests:outpost/fishery/tick

## Snake Shrine 
### Quests
#### Portal Key
execute as @a[x=1055,y=69,z=9795,distance=..8,tag=!q_ss_ocelot_complete,scores={q_ss_ocelot=5..}] if entity @s[tag=q_ss_ocelot_started] run function quests:snake_shrine/ocelots_complete
execute as @p[x=1053,y=72,z=9795,distance=..5,scores={q_ss_offering=0..1},nbt={SelectedItem:{id:"minecraft:rabbit_foot",tag:{q_offering:"snake"}}}] run function quests:snake_shrine/make_offering

## Wind Shrine Offering
execute as @a[x=565,y=65,z=8372,distance=..5,nbt={SelectedItem:{id:"minecraft:totem_of_undying",tag:{q_offering:"wind"}}}] at @s run function quests:wind_shrine_offering

### Mazecapade entrance restriction
execute as @a[x=1693,y=65,z=9859,dx=60,dy=60,dz=60,tag=!enteredMazecapade,gamemode=!creative] run function quests:mazecapade/restriction
execute as @a[tag=enteredMazecapade] unless entity @s[x=1692,y=65,z=9858,dx=75,dy=15,dz=62] run tag @s remove enteredMazecapade

## Check talks, Exampleton Reserved Quest
execute as @a[tag=talkOutpost,scores={q_ex_reserved=0..2},tag=!q_ex_reserved_outpost] run function quests:exampleton/reserved_quest/check_item
execute as @a[tag=talkGleamsville,scores={q_ex_reserved=0..2},tag=!q_ex_reserved_gleamsville] run function quests:exampleton/reserved_quest/check_item
execute as @a[tag=talkWoodstock,scores={q_ex_reserved=0..2},tag=!q_ex_reserved_woodstock] run function quests:exampleton/reserved_quest/check_item

# Reset tick states
execute as @a run function utils:talk_detector/tick
execute as @a run function utils:tp_detect/tick