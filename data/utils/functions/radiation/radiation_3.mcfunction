tellraw @a[tag=!radWarning3,scores={radLevel=3}] {"text":"Extreme pain stings through all your existence. You'd rather get out of here, now","italic":true,"color":"dark_aqua"}
execute at @a[scores={radLevel=3..},nbt=!{ActiveEffects:[{Id:19b}]}] run summon minecraft:small_fireball ~ ~3 ~ {direction:[0.0,-10.0,0.0]}
effect give @a[scores={radLevel=3..},nbt=!{ActiveEffects:[{Id:9b}]}] nausea 15
effect give @a[scores={radLevel=3..},nbt=!{ActiveEffects:[{Id:20b}]}] wither 5 0
effect give @a[scores={radLevel=3..},nbt=!{ActiveEffects:[{Id:19b}]}] poison 5 1
tag @a[tag=!radWarning3,scores={radLevel=3}] add radWarning3
