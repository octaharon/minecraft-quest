tellraw @a[tag=!radWarning2,scores={radLevel=2}] {"text":"Something is very wrong here. You can barely see, but you can feel skin burning, your intestines twirling and your sanity fading away...","italic":true,"color":"dark_aqua"}
effect give @a[scores={radLevel=2..},nbt=!{ActiveEffects:[{Id:9b}]}] nausea 5
effect give @a[scores={radLevel=2..},nbt=!{ActiveEffects:[{Id:19b}]}] poison 5 1
tag @a[tag=!radWarning2,scores={radLevel=2}] add radWarning2
