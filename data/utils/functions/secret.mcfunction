title @s actionbar [{"text":"You've found a Secret!","color":"gold"}]
experience add @s 15 points
execute at @s run playsound entity.player.levelup neutral @s ~ ~ ~ 0.6
execute at @s run playsound quests:ui.gong player @s ~ ~ ~ 1
effect give @s luck 60 1
effect give @s instant_health 1 100 
scoreboard players add @s secretsFound 1