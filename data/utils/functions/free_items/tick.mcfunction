execute as @a[scores={freeItemTicks=1..}] run function utils:free_items/calc_seconds
execute as @a[scores={freeItemTicks=1}] run function utils:free_items/reset
scoreboard players remove @a[scores={freeItemTicks=2..}] freeItemTicks 1
execute as @a[scores={freeItemTicks=..0,trgFreeItem=1..}] at @s run function utils:free_items/give