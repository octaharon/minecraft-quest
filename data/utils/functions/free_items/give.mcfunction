execute if score @s freeItemTicks matches 1.. run function utils:free_items/deny
execute if score @s trgFreeItem matches 18681 unless score @s reputation matches 0.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=18681,reputation=0..}] at @s run give @s apple 1
scoreboard players set @s[scores={trgFreeItem=18681,reputation=0..}] freeItemCooldown 400
execute if score @s trgFreeItem matches 26175 unless score @s reputation matches 15.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=26175,reputation=15..}] at @s run give @s golden_apple 1
scoreboard players set @s[scores={trgFreeItem=26175,reputation=15..}] freeItemCooldown 800
execute if score @s trgFreeItem matches 33551 unless score @s reputation matches 25.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=33551,reputation=25..}] at @s run give @s enchanted_golden_apple 1
scoreboard players set @s[scores={trgFreeItem=33551,reputation=25..}] freeItemCooldown 1200
execute if score @s trgFreeItem matches 40832 unless score @s reputation matches 0.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=40832,reputation=0..}] at @s run give @s glass_bottle 1
scoreboard players set @s[scores={trgFreeItem=40832,reputation=0..}] freeItemCooldown 400
execute if score @s trgFreeItem matches 55162 unless score @s reputation matches 5.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=55162,reputation=5..}] at @s run give @s potion{Potion:"minecraft:water"} 1
scoreboard players set @s[scores={trgFreeItem=55162,reputation=5..}] freeItemCooldown 400
execute if score @s trgFreeItem matches 61723 unless score @s reputation matches 10.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=61723,reputation=10..}] at @s run give @s potion{Potion:"minecraft:regeneration"} 1
scoreboard players set @s[scores={trgFreeItem=61723,reputation=10..}] freeItemCooldown 500
execute if score @s trgFreeItem matches 73785 unless score @s reputation matches 15.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=73785,reputation=15..}] at @s run give @s potion{Potion:"minecraft:poison"} 1
scoreboard players set @s[scores={trgFreeItem=73785,reputation=15..}] freeItemCooldown 600
execute if score @s trgFreeItem matches 86729 unless score @s reputation matches 20.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=86729,reputation=20..}] at @s run give @s potion{Potion:"minecraft:healing"} 1
scoreboard players set @s[scores={trgFreeItem=86729,reputation=20..}] freeItemCooldown 700
execute if score @s trgFreeItem matches 96027 unless score @s reputation matches 25.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=96027,reputation=25..}] at @s run give @s potion{Potion:"minecraft:strong_healing"} 1
scoreboard players set @s[scores={trgFreeItem=96027,reputation=25..}] freeItemCooldown 800
execute if score @s trgFreeItem matches 106348 unless score @s reputation matches 30.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=106348,reputation=30..}] at @s run give @s splash_potion{Potion:"minecraft:strong_regeneration"} 1
scoreboard players set @s[scores={trgFreeItem=106348,reputation=30..}] freeItemCooldown 900
execute if score @s trgFreeItem matches 115188 unless score @s reputation matches 35.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=115188,reputation=35..}] at @s run give @s lingering_potion{Potion:"minecraft:strong_healing"} 1
scoreboard players set @s[scores={trgFreeItem=115188,reputation=35..}] freeItemCooldown 1000
execute if score @s trgFreeItem matches 121605 unless score @s reputation matches 0.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=121605,reputation=0..}] at @s run give @s cookie 1
scoreboard players set @s[scores={trgFreeItem=121605,reputation=0..}] freeItemCooldown 400
execute if score @s trgFreeItem matches 133215 unless score @s reputation matches 5.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=133215,reputation=5..}] at @s run give @s melon_slice 1
scoreboard players set @s[scores={trgFreeItem=133215,reputation=5..}] freeItemCooldown 500
execute if score @s trgFreeItem matches 142807 unless score @s reputation matches 10.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=142807,reputation=10..}] at @s run give @s cooked_chicken 1
scoreboard players set @s[scores={trgFreeItem=142807,reputation=10..}] freeItemCooldown 600
execute if score @s trgFreeItem matches 155186 unless score @s reputation matches 15.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=155186,reputation=15..}] at @s run give @s cooked_porkchop 1
scoreboard players set @s[scores={trgFreeItem=155186,reputation=15..}] freeItemCooldown 700
execute if score @s trgFreeItem matches 165896 unless score @s reputation matches 20.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=165896,reputation=20..}] at @s run give @s rabbit_stew 1
scoreboard players set @s[scores={trgFreeItem=165896,reputation=20..}] freeItemCooldown 800
execute if score @s trgFreeItem matches 170261 unless score @s reputation matches 25.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=170261,reputation=25..}] at @s run function crafting:consumables/nourishing_stew
scoreboard players set @s[scores={trgFreeItem=170261,reputation=25..}] freeItemCooldown 900
execute if score @s trgFreeItem matches 184031 unless score @s reputation matches 0.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=184031,reputation=0..}] at @s run give @s stick 1
scoreboard players set @s[scores={trgFreeItem=184031,reputation=0..}] freeItemCooldown 400
execute if score @s trgFreeItem matches 193384 unless score @s reputation matches 5.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=193384,reputation=5..}] at @s run give @s bowl 1
scoreboard players set @s[scores={trgFreeItem=193384,reputation=5..}] freeItemCooldown 500
execute if score @s trgFreeItem matches 202673 unless score @s reputation matches 10.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=202673,reputation=10..}] at @s run give @s charcoal 1
scoreboard players set @s[scores={trgFreeItem=202673,reputation=10..}] freeItemCooldown 600
execute if score @s trgFreeItem matches 214184 unless score @s reputation matches 15.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=214184,reputation=15..}] at @s run give @s dried_kelp_block 1
scoreboard players set @s[scores={trgFreeItem=214184,reputation=15..}] freeItemCooldown 800
execute if score @s trgFreeItem matches 223793 unless score @s reputation matches 20.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=223793,reputation=20..}] at @s run give @s lava_bucket 1
scoreboard players set @s[scores={trgFreeItem=223793,reputation=20..}] freeItemCooldown 1000
execute if score @s trgFreeItem matches 236433 unless score @s reputation matches 25.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=236433,reputation=25..}] at @s run give @s coal_block 1
scoreboard players set @s[scores={trgFreeItem=236433,reputation=25..}] freeItemCooldown 1200
execute if score @s trgFreeItem matches 247534 unless score @s reputation matches 0.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=247534,reputation=0..}] at @s run give @s birch_boat 1
scoreboard players set @s[scores={trgFreeItem=247534,reputation=0..}] freeItemCooldown 900
execute if score @s trgFreeItem matches 255567 unless score @s reputation matches 5.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=255567,reputation=5..}] at @s run give @s minecart 1
scoreboard players set @s[scores={trgFreeItem=255567,reputation=5..}] freeItemCooldown 1200
execute if score @s trgFreeItem matches 263904 unless score @s reputation matches 10.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=263904,reputation=10..}] at @s run effect give @s speed 300 0
scoreboard players set @s[scores={trgFreeItem=263904,reputation=10..}] freeItemCooldown 1500
execute if score @s trgFreeItem matches 275102 unless score @s reputation matches 15.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=275102,reputation=15..}] at @s run function mobs:eggs/drafter
scoreboard players set @s[scores={trgFreeItem=275102,reputation=15..}] freeItemCooldown 1800
execute if score @s trgFreeItem matches 281985 unless score @s reputation matches 20.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=281985,reputation=20..}] at @s run effect give @s dolphins_grace 300 0
scoreboard players set @s[scores={trgFreeItem=281985,reputation=20..}] freeItemCooldown 2100
execute if score @s trgFreeItem matches 294727 unless score @s reputation matches 25.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=294727,reputation=25..}] at @s run function mobs:eggs/emporium_stallion
scoreboard players set @s[scores={trgFreeItem=294727,reputation=25..}] freeItemCooldown 2400
execute if score @s trgFreeItem matches 301853 unless score @s reputation matches 30.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=301853,reputation=30..}] at @s run effect give @s speed 300 1
scoreboard players set @s[scores={trgFreeItem=301853,reputation=30..}] freeItemCooldown 3000
execute if score @s trgFreeItem matches 318788 unless score @s reputation matches 35.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=318788,reputation=35..}] at @s run function mobs:eggs/undead_horse
scoreboard players set @s[scores={trgFreeItem=318788,reputation=35..}] freeItemCooldown 3600
execute if score @s trgFreeItem matches 320724 unless score @s reputation matches 0.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=320724,reputation=0..}] at @s run give @s arrow 5
scoreboard players set @s[scores={trgFreeItem=320724,reputation=0..}] freeItemCooldown 600
execute if score @s trgFreeItem matches 338846 unless score @s reputation matches 6.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=338846,reputation=6..}] at @s run give @s tipped_arrow{Potion:"minecraft:long_slowness"} 3
scoreboard players set @s[scores={trgFreeItem=338846,reputation=6..}] freeItemCooldown 600
execute if score @s trgFreeItem matches 344642 unless score @s reputation matches 12.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=344642,reputation=12..}] at @s run give @s tipped_arrow{Potion:"minecraft:long_weakness"} 3
scoreboard players set @s[scores={trgFreeItem=344642,reputation=12..}] freeItemCooldown 600
execute if score @s trgFreeItem matches 357719 unless score @s reputation matches 18.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=357719,reputation=18..}] at @s run give @s spectral_arrow 3
scoreboard players set @s[scores={trgFreeItem=357719,reputation=18..}] freeItemCooldown 600
execute if score @s trgFreeItem matches 364757 unless score @s reputation matches 24.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=364757,reputation=24..}] at @s run give @s tipped_arrow{Potion:"minecraft:harming"} 3
scoreboard players set @s[scores={trgFreeItem=364757,reputation=24..}] freeItemCooldown 600
execute if score @s trgFreeItem matches 374662 unless score @s reputation matches 30.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=374662,reputation=30..}] at @s run function crafting:consumables/rockets/explosive_bolt
scoreboard players set @s[scores={trgFreeItem=374662,reputation=30..}] freeItemCooldown 600
execute if score @s trgFreeItem matches 380200 unless score @s reputation matches 0.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=380200,reputation=0..}] at @s run give @s orange_bed 1
scoreboard players set @s[scores={trgFreeItem=380200,reputation=0..}] freeItemCooldown 600
execute if score @s trgFreeItem matches 391589 unless score @s reputation matches 4.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=391589,reputation=4..}] at @s run give @s iron_hoe 1
scoreboard players set @s[scores={trgFreeItem=391589,reputation=4..}] freeItemCooldown 800
execute if score @s trgFreeItem matches 409105 unless score @s reputation matches 8.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=409105,reputation=8..}] at @s run give @s leather_chestplate 1
scoreboard players set @s[scores={trgFreeItem=409105,reputation=8..}] freeItemCooldown 1000
execute if score @s trgFreeItem matches 418552 unless score @s reputation matches 12.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=418552,reputation=12..}] at @s run give @s orange_shulker_box 1
scoreboard players set @s[scores={trgFreeItem=418552,reputation=12..}] freeItemCooldown 1200
execute if score @s trgFreeItem matches 424488 unless score @s reputation matches 16.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=424488,reputation=16..}] at @s run give @s iron_pickaxe 1
scoreboard players set @s[scores={trgFreeItem=424488,reputation=16..}] freeItemCooldown 1400
execute if score @s trgFreeItem matches 438519 unless score @s reputation matches 20.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=438519,reputation=20..}] at @s run give @s iron_sword 1
scoreboard players set @s[scores={trgFreeItem=438519,reputation=20..}] freeItemCooldown 1600
execute if score @s trgFreeItem matches 441641 unless score @s reputation matches 24.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=441641,reputation=24..}] at @s run give @s crossbow 1
scoreboard players set @s[scores={trgFreeItem=441641,reputation=24..}] freeItemCooldown 1800
execute if score @s trgFreeItem matches 454375 unless score @s reputation matches 28.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=454375,reputation=28..}] at @s run give @s turtle_helmet 1
scoreboard players set @s[scores={trgFreeItem=454375,reputation=28..}] freeItemCooldown 2000
execute if score @s trgFreeItem matches 461687 unless score @s reputation matches 32.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=461687,reputation=32..}] at @s run function uniques:nemesis
scoreboard players set @s[scores={trgFreeItem=461687,reputation=32..}] freeItemCooldown 2400
execute if score @s trgFreeItem matches 471548 unless score @s reputation matches 0.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=471548,reputation=0..}] at @s run function utils:tp/sprucewood
scoreboard players set @s[scores={trgFreeItem=471548,reputation=0..}] freeItemCooldown 200
execute if score @s trgFreeItem matches 482203 unless score @s reputation matches 5.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=482203,reputation=5..}] at @s run function utils:tp/snake_shrine
scoreboard players set @s[scores={trgFreeItem=482203,reputation=5..}] freeItemCooldown 200
execute if score @s trgFreeItem matches 496697 unless score @s reputation matches 10.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=496697,reputation=10..}] at @s run function utils:tp/crossing_sands
scoreboard players set @s[scores={trgFreeItem=496697,reputation=10..}] freeItemCooldown 200
execute if score @s trgFreeItem matches 507831 unless score @s reputation matches 12.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=507831,reputation=12..}] at @s run function utils:tp/exampleton
scoreboard players set @s[scores={trgFreeItem=507831,reputation=12..}] freeItemCooldown 200
execute if score @s trgFreeItem matches 518283 unless score @s reputation matches 14.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=518283,reputation=14..}] at @s run function utils:tp/zoo
scoreboard players set @s[scores={trgFreeItem=518283,reputation=14..}] freeItemCooldown 200
execute if score @s trgFreeItem matches 528278 unless score @s reputation matches 16.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=528278,reputation=16..}] at @s run function utils:tp/hanging_hills
scoreboard players set @s[scores={trgFreeItem=528278,reputation=16..}] freeItemCooldown 200
execute if score @s trgFreeItem matches 533671 unless score @s reputation matches 18.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=533671,reputation=18..}] at @s run function utils:tp/sandy_haven
scoreboard players set @s[scores={trgFreeItem=533671,reputation=18..}] freeItemCooldown 200
execute if score @s trgFreeItem matches 549978 unless score @s reputation matches 20.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=549978,reputation=20..}] at @s run function utils:tp/village
scoreboard players set @s[scores={trgFreeItem=549978,reputation=20..}] freeItemCooldown 200
execute if score @s trgFreeItem matches 558541 unless score @s reputation matches 22.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=558541,reputation=22..}] at @s run function utils:tp/outpost
scoreboard players set @s[scores={trgFreeItem=558541,reputation=22..}] freeItemCooldown 200
execute if score @s trgFreeItem matches 567728 unless score @s reputation matches 24.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=567728,reputation=24..}] at @s run function utils:tp/shadesbury
scoreboard players set @s[scores={trgFreeItem=567728,reputation=24..}] freeItemCooldown 200
execute if score @s trgFreeItem matches 579602 unless score @s reputation matches 26.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=579602,reputation=26..}] at @s run function utils:tp/gleamsville
scoreboard players set @s[scores={trgFreeItem=579602,reputation=26..}] freeItemCooldown 200
execute if score @s trgFreeItem matches 582160 unless score @s reputation matches 28.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=582160,reputation=28..}] at @s run function utils:tp/sweet_meadows
scoreboard players set @s[scores={trgFreeItem=582160,reputation=28..}] freeItemCooldown 200
execute if score @s trgFreeItem matches 594554 unless score @s reputation matches 30.. run function utils:free_items/deny
execute as @s[scores={trgFreeItem=594554,reputation=30..}] at @s run function utils:tp/high_court
scoreboard players set @s[scores={trgFreeItem=594554,reputation=30..}] freeItemCooldown 200
playsound entity.player.levelup player @s ~ ~ ~ 0.9 0.6
scoreboard players set @s trgFreeItem 0
function utils:free_items/calc_cooldown
function utils:free_items/set_cooldown
function utils:free_items/start_timer