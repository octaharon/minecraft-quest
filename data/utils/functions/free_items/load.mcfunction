#define score_holder #freeItemsTimer
scoreboard objectives add trgFreeItem trigger [{"text": "Free item selector (Emporium)","color": "yellow"}]
scoreboard objectives add freeItemTicks dummy {"text": "Cooldown on free items ticks","color": "white"}
scoreboard objectives add freeItemCooldown dummy {"text": "Cooldown calculation","color": "white"}
scoreboard objectives add freeItemCDR dummy {"text": "Player Cooldown Reduction","color": "white"}
scoreboard objectives add freeItemSecs dummy {"text": "Cooldown on free items seconds","color": "white"}
scoreboard objectives add freeItemPeek dummy {"text": "True if the player has peeked the next item in group","color": "white"}
scoreboard players set @a trgFreeItem 0
scoreboard players set @a freeItemPeek 0
scoreboard players set @a freeItemTicks 0
scoreboard players set @a freeItemCooldown 0
scoreboard players set @a freeItemSecs 0
scoreboard players set @a freeItemCDR -1
scoreboard players set #freeItemsTimer freeItemCDR 20