scoreboard players set #freeItemsTimer freeItemTicks 0
execute store result score #freeItemsTimer freeItemSecs run scoreboard players get @s reputation
# multiply reputation by 20 - means 1 sec per 1 reputation
scoreboard players operation #freeItemsTimer freeItemSecs *= #freeItemsTimer freeItemCDR
# reduce cooldown by reputation factor
scoreboard players operation #freeItemsTimer freeItemTicks += #freeItemsTimer freeItemSecs
# cooldown is reduced by hero of the village by 5 seconds / level
execute if data entity @s ActiveEffects[{Id:32b,Amplifier:0b}] run scoreboard players add #freeItemsTimer freeItemTicks 200
execute if data entity @s ActiveEffects[{Id:32b,Amplifier:1b}] run scoreboard players add #freeItemsTimer freeItemTicks 400
execute if data entity @s ActiveEffects[{Id:32b,Amplifier:2b}] run scoreboard players add #freeItemsTimer freeItemTicks 600
execute if data entity @s ActiveEffects[{Id:32b,Amplifier:3b}] run scoreboard players add #freeItemsTimer freeItemTicks 800
execute if data entity @s ActiveEffects[{Id:32b,Amplifier:4b}] run scoreboard players add #freeItemsTimer freeItemTicks 1000
# set player's CDR
execute store result score @s freeItemCDR run scoreboard players get #freeItemsTimer freeItemTicks