# current cooldown is stored in @s/freeItemCooldown in ticks
execute store result score #freeItemsTimer freeItemTicks run scoreboard players get @s freeItemCooldown
# apply CDR
scoreboard players operation #freeItemsTimer freeItemTicks -= @s freeItemCDR
# cap at 5 seconds
execute if score #freeItemsTimer freeItemTicks matches ..100 run scoreboard players set #freeItemsTimer freeItemTicks 100
# copy back to players score
execute store result score @s freeItemCooldown run scoreboard players get #freeItemsTimer freeItemTicks
execute store result score @s freeItemSecs run scoreboard players get @s freeItemCooldown
scoreboard players operation @s freeItemSecs /= #countdown timerSeconds