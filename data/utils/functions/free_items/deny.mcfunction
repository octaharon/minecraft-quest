playsound quests:ui.deny player @s ~ ~ ~ 0.5 1
playsound minecraft:block.conduit.deactivate ambient @s ~ ~ ~ 0.5 2
scoreboard players add @s freeItemSecs 1
title @s actionbar ["",{"text":"\u26a0 ","color":"red"},{"text":"Available in "},{"score":{"name":"@s","objective":"freeItemSecs"},"color":"red"},{"text":" seconds!"},{"text":" \u26a0","color":"red"}]
scoreboard players remove @s freeItemSecs 1