#define tag uniqueMount
#define tag markedMount
#alias entity existingUniqueMount @e[type=#utils:mount,tag=uniqueMount,tag=markedMount]
#alias entity newUniqueMount @e[type=#utils:mount,tag=uniqueMount,tag=!markedMount]
execute as @e[type=#utils:mount,tag=uniqueMount,tag=markedMount] at @s if entity @e[type=#utils:mount,tag=uniqueMount,tag=!markedMount] unless entity @p[distance=..64] run function utils:wipe_mob
execute as @e[type=#utils:mount,tag=uniqueMount,tag=!markedMount] unless entity @e[type=#utils:mount,tag=uniqueMount,tag=markedMount] at @s run tag @s add markedMount