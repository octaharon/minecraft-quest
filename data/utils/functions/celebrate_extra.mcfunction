execute at @a[distance=..20,tag=celebration] run particle minecraft:sneeze ~ ~1 ~ 0.25 0.25 0.25 0.05 150
playsound entity.player.levelup player @a[distance=..20,tag=celebration] ~ ~ ~ 4 0.75 1
particle minecraft:dragon_breath ~ ~ ~ 0.5 0.5 0.5 0.01 200
particle minecraft:nautilus ~ ~ ~ 0.5 0.5 0.5 .5 500
particle minecraft:portal ~ ~ ~ 0.25 0.25 0.25 .5 200
tag @a[distance=..20,tag=celebration] remove celebration