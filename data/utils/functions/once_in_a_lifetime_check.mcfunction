#define objective hadDied "Death count"
execute unless score @s hadDied matches 0.. run scoreboard players set @s hadDied 1
execute unless score @s hadDied matches 1.. run tellraw @s ["",{"text":"\u26a0 You can do it only once in a lifetime","color":"red"}]
execute unless score @s hadDied matches 1.. run playsound quests:quest.fail player @s ~ ~ ~ 1 0.8
execute unless score @s hadDied matches 1.. run playsound minecraft:block.conduit.deactivate neutral @s ~ ~ ~ 0.8 1

