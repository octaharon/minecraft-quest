#define tag tpItem
tag @s add tpItem
execute at @s run particle portal ~ ~ ~ 0.25 0.5 0.2 0.01 1000
execute at @s as @a[tag=!tpItem,distance=..24] run playsound quests:portal.departure ambient @s ~ ~ ~ 1 1 0.2
execute at @s as @a[tag=!tpItem,distance=..24] run playsound block.conduit.deactivate ambient @s ~ ~ ~ 0.6 1 0.1