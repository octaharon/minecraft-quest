scoreboard objectives add playerPosX dummy
scoreboard objectives add playerPosZ dummy
scoreboard objectives add playerVelX dummy
scoreboard objectives add playerVelZ dummy
#define tag tpPlayer "A player is being teleported"
#define objective playerPosX
#define objective playerPosZ
#define objective playerVelX
#define objective playerVelZ