playsound minecraft:block.portal.travel ambient @s ~ ~ ~ 0.5 0.7 
playsound quests:portal.arrival player @a[distance=..24] ~ ~ ~ 1 1 0.4
particle portal ~ ~ ~ 0.25 0.5 0.25 0.01 250 force
particle minecraft:ambient_entity_effect ~ ~ ~ 0.3 1 0.3 10 1000 force
effect give @s nausea 10 2
effect give @s weakness 10 1
function utils:timer/reset
tag @s remove tpItem