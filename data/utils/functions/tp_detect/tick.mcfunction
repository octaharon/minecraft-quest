execute as @s[tag=tpPlayer] at @s run function utils:tp_detect/arrival
tag @s remove tpPlayer
execute store result score @s playerVelX run data get entity @s Pos[0] 1.0
execute store result score @s playerVelZ run data get entity @s Pos[2] 1.0
scoreboard players operation @s playerVelX -= @s playerPosX
scoreboard players operation @s playerVelZ -= @s playerPosZ
function utils:tp_detect/set_pos
execute unless score @s playerVelX matches -18..18 run tag @s add tpPlayer
execute unless score @s playerVelZ matches -18..18 run tag @s add tpPlayer