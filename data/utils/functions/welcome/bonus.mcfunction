loot give @s loot minecraft:chests/spawn_bonus_chest
give @s minecart
give @s emerald 15
function crafting:consumables/heroic_brew
function quests:main/rule_book
title @s times 20 100 20
title @s title [{"text":"Welcome!","bold": true,"color":"gold"}]
playsound ui.toast.challenge_complete player @s ~ ~ ~ 4
playsound quests:start player @s ~ ~ ~ 4
tellraw @s ["",{"text":"Hello, stranger "},{"selector": "@s","bold": true,"color":"light_purple"},{"text":". Villagers of "},{"text":"The North","bold":true,"italic":true,"color":"white"},{"text":" welcome you! This world is a dangerous place, but you can make it better. Proceed to "},{"text":"The Village","color":"dark_green"},{"text":" (-500, -850) to learn how exactly, and take this small offer to help your survive on your way there."}]
scoreboard players set @s hasUniBook 0
tag @s add welcomeBonus