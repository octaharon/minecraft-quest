#alias entity tp_stand @e[type=armor_stand,tag=tpPlayer,distance=..2,limit=1]
tp @s ~ ~ ~ ~ ~
summon armor_stand ~ ~ ~ {Invisible:1b,Tags:["tpPlayer"]}
execute store result score @s playerPosX run data get entity @e[type=armor_stand,tag=tpPlayer,distance=..2,limit=1] Pos[0] 1.0
execute store result score @s playerPosZ run data get entity @e[type=armor_stand,tag=tpPlayer,distance=..2,limit=1] Pos[2] 1.0
kill @e[type=armor_stand,tag=tpPlayer,distance=..2,limit=1]
function utils:tp_detect/arrival
