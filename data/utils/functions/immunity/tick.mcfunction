execute as @a unless score @s poisonImmunity matches 0.. run scoreboard players set @s poisonImmunity 0
execute as @a unless score @s debuffImmunity matches 0.. run scoreboard players set @s debuffImmunity 0
execute as @a[scores={poisonImmunity=1..}] run function utils:immunity/poison
execute as @a[scores={debuffImmunity=1..}] run function utils:immunity/debuff