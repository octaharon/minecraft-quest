execute store result score @s timerMinutes run scoreboard players get @s timerTicks
execute store result score @s timerSeconds run scoreboard players get @s timerTicks
execute store result score @s timerBlink run scoreboard players get @s timerTicks
scoreboard players operation @s timerMinutes /= #countdown timerMinutes
scoreboard players operation @s timerSeconds %= #countdown timerMinutes
scoreboard players operation @s timerSeconds /= #countdown timerSeconds
scoreboard players operation @s timerBlink /= #countdown timerTicks
scoreboard players operation @s timerBlink %= #countdown timerBlink
