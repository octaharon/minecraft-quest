execute as @a[scores={timerTicks=1..}] run scoreboard players remove @s timerTicks 1
execute as @a[scores={timerTicks=1..}] run function utils:timer/calc
execute as @a[scores={timerTicks=0}] run function utils:timer/reset
execute as @a unless score @s timerTicks matches 0.. run scoreboard players set @s timerTicks 0
