#define objective timerTicks
#define objective timerSeconds
#define objective timerMinutes
#define objective timerBlink
#define score_holder #timer
scoreboard objectives add timerTicks dummy {"text": "Countdown timer ticks"}
scoreboard objectives add timerSeconds dummy {"text": "Countdown timer seconds"}
scoreboard objectives add timerMinutes dummy {"text": "Countdown timer minutes"}
scoreboard objectives add timerBlink dummy {"text": "Countdown timer Blink"}
