#define tag talkGleamsville "A player had talked to a villager in Gleamsville"
#define tag talkOutpost "A player had talked to a villager in Outpost"
#define tag talkWoodstock "A player had talked to a villager in Woodstock"
#define tag talkBlacksmith "A player had talked to Blacksmith at Exampleton"
#define tag talkQuartermaster "A player had talked to Quartermaster at Emporium"
#define tag talkAngler "A player had talked to Master Angler at The Outpost"
#define objective hadTalked
tag @s[tag=talkGleamsville] remove talkGleamsville
tag @s[tag=talkQuartermaster] remove talkQuartermaster
tag @s[tag=talkOutpost] remove talkOutpost
tag @s[tag=talkWoodstock] remove talkWoodstock
tag @s[tag=talkBlacksmith] remove talkBlacksmith
tag @s[tag=talkAngler] remove talkAngler
execute positioned -1029 68 9501 as @a[distance=..64,scores={hadTalked=1..}] run tag @s add talkGleamsville
execute positioned 2047 63 10352 as @a[distance=..64,scores={hadTalked=1..}] run tag @s add talkWoodstock
execute positioned 740 63 10760 as @a[distance=..64,scores={hadTalked=1..}] run tag @s add talkOutpost
execute at @e[type=villager,tag=ex_blacksmith] as @a[distance=..5,scores={hadTalked=1..}] run tag @s add talkBlacksmith
execute at @e[type=villager,tag=op_angler] as @a[distance=..5,scores={hadTalked=1..}] run tag @s add talkAngler
execute at @e[type=villager,tag=emp_quartermaster] as @a[distance=..5,scores={hadTalked=1..}] run tag @s add talkQuartermaster
scoreboard players set @s hadTalked 0 